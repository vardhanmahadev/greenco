-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 03, 2022 at 08:23 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `greenco`
--

-- --------------------------------------------------------

--
-- Table structure for table `assessor_verify_log`
--

CREATE TABLE `assessor_verify_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `assessor_id` int(11) DEFAULT NULL,
  `verified_status` tinyint(3) UNSIGNED DEFAULT NULL,
  `verified_remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verified_at` timestamp NULL DEFAULT NULL,
  `verified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assessor_verify_log`
--

INSERT INTO `assessor_verify_log` (`id`, `assessor_id`, `verified_status`, `verified_remark`, `verified_at`, `verified_by`, `created_at`, `updated_at`) VALUES
(1, 26, 2, 'remark', '2022-01-03 05:20:57', 1, '2022-01-03 05:20:57', '2022-01-03 05:20:57'),
(2, 25, 2, 'reamark reamark reamark reamark reamark reamark reamark reamark reamark reamark reamark reamark reamark reamark reamark reamark reamark reamark reamark reamark reamark reamark reamark reamark', '2022-01-03 06:25:14', 1, '2022-01-03 06:25:14', '2022-01-03 06:25:14'),
(3, 20, 2, NULL, '2022-01-03 07:10:34', 1, '2022-01-03 07:10:34', '2022-01-03 07:10:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assessor_verify_log`
--
ALTER TABLE `assessor_verify_log`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assessor_verify_log`
--
ALTER TABLE `assessor_verify_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
