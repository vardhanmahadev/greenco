-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 05, 2022 at 07:06 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `greenco`
--

-- --------------------------------------------------------

--
-- Table structure for table `consultants`
--

CREATE TABLE `consultants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT 0,
  `verification_status` int(11) NOT NULL DEFAULT 0,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verification_done_by` int(11) DEFAULT NULL,
  `verification_done_at` timestamp NULL DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_updated_by` int(11) DEFAULT NULL,
  `status_updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `consultants`
--

INSERT INTO `consultants` (`id`, `name`, `email`, `mobile`, `email_verified_at`, `status`, `verification_status`, `password`, `remember_token`, `verification_done_by`, `verification_done_at`, `remark`, `status_updated_by`, `status_updated_at`, `created_at`, `updated_at`) VALUES
(1, 'mahadev vardhan', 'vardhanm@mirakitech.com', '7036221419', NULL, 0, 2, '$2y$10$AmYNRKFg0S3qej6LasD/JuXpIn.zUZ5pXujbzfP1kE.G/7QUPr2Ly', NULL, 1, '2021-12-20 21:51:55', 'Rejected', NULL, NULL, NULL, '2021-12-21 09:51:55'),
(2, 'mahadev vardhan', 'vardhanm123@mirakitech.com', '7036221420', NULL, 0, 2, '$2y$10$G4uNxTqNrXub8tEn7IQ5HOa/vC7xhTVH3Z1TCX.BVXSNIbC9e7qvm', NULL, 1, '2021-12-20 21:51:11', 'Rejected', 1, '2021-12-21 05:09:33', '2021-12-20 09:55:28', '2021-12-21 09:51:11'),
(3, 'Nanda Gopal', 'basarlapudi@yopmail.com', '9799089809', NULL, 0, 1, '$2y$10$qIU2L1nWiBLoabIcVU45cehEcbRG/blwaoqtdw0dgEQyq1rmAdcae', NULL, 1, '2021-12-20 21:50:35', NULL, 1, '2021-12-21 05:09:37', '2021-12-20 12:30:07', '2021-12-21 09:50:35'),
(4, 'puri', 'puri@yopmail.com', '7867867867', NULL, 0, 1, '$2y$10$MOXV/IbkU.eBZNE4AZToU.PHMruPyMjviOBKNeiaW.goWicyOFgsG', NULL, 1, '2021-12-20 21:49:43', NULL, 1, '2021-12-20 00:38:58', '2021-12-20 12:35:35', '2021-12-21 09:49:43'),
(9, 'mahadev vardhan', 'vardhanmm@mirakitech.com', '7036221555', NULL, 0, 0, '$2y$10$1Ti4nEPuHbAxOaiVEZnyaubT912y2u9XMsqXGbm1m3H5NRDbeYFe6', NULL, 1, '2022-01-04 07:05:36', NULL, 1, '2022-01-04 07:05:36', '2022-01-04 07:05:36', '2022-01-04 07:05:36'),
(10, 'mahadev vardhan', 'vardhanma@mirakitech.com', '7036234234', NULL, 0, 1, '$2y$10$M/O3/FNzxsToIZ8/o3usIOGaxtC8qP12O82toP.ZDxg78WClWk2qW', NULL, 1, '2022-01-05 06:05:25', NULL, 1, '2022-01-05 06:05:25', '2022-01-05 06:05:25', '2022-01-05 06:05:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `consultants`
--
ALTER TABLE `consultants`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `consultants_email_unique` (`email`),
  ADD UNIQUE KEY `consultants_mobile_unique` (`mobile`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `consultants`
--
ALTER TABLE `consultants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
