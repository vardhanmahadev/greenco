-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 05, 2022 at 01:39 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `greenco`
--

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`, `country_id`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_by`, `deleted_at`) VALUES
(1, 'Andaman and Nicobar Islands', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Andhra Pradesh', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Arunachal Pradesh', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'Assam', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Bihar', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'Chandigarh', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'Chhattisgarh', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'Dadra and Nagar Haveli', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'Daman and Diu', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'Delhi', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'Goa', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'Gujarat', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'Haryana', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'Himachal Pradesh', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'Jammu and Kashmir', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'Jharkhand', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'Karnataka', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 'Kenmore', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 'Kerala', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 'Lakshadweep', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 'Madhya Pradesh', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 'Maharashtra', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 'Manipur', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 'Meghalaya', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'Mizoram', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 'Nagaland', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 'Narora', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 'Natwar', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 'Odisha', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 'Paschim Medinipur', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 'Pondicherry', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 'Punjab', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 'Rajasthan', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 'Sikkim', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(35, 'Tamil Nadu', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 'Telangana', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 'Tripura', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 'Uttar Pradesh', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 'Uttarakhand', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 'Vaishali', 101, NULL, NULL, NULL, NULL, NULL, NULL),
(41, 'West Bengal', 101, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4122;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
