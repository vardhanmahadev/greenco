-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 04, 2022 at 04:19 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `greenco`
--

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reg_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` bigint(20) UNSIGNED DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal_address_pincode` int(11) DEFAULT NULL,
  `billing_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address_pincode` int(11) DEFAULT NULL,
  `mst_industry_id` bigint(20) DEFAULT NULL,
  `mst_entity_id` bigint(20) DEFAULT NULL,
  `mst_sector_id` bigint(20) DEFAULT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `turnover` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `budget_allocation` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_employees` bigint(20) DEFAULT NULL,
  `contract_employees` bigint(20) DEFAULT NULL,
  `area` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `electrical_consumption` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thermal_consumption` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `water_consumption` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanno` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `panno` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gstno` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `proposal_document` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `po_document` bigint(20) DEFAULT NULL,
  `account_status` int(11) NOT NULL DEFAULT 0,
  `verified_status` int(11) NOT NULL DEFAULT 0,
  `verified_remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verified_by` bigint(20) DEFAULT NULL,
  `verified_at` timestamp NULL DEFAULT NULL,
  `status_updated_by` int(11) DEFAULT NULL,
  `status_updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `reg_id`, `name`, `email`, `password`, `mobile`, `location`, `state`, `postal_address`, `postal_address_pincode`, `billing_address`, `billing_address_pincode`, `mst_industry_id`, `mst_entity_id`, `mst_sector_id`, `product_name`, `turnover`, `budget_allocation`, `no_of_employees`, `contract_employees`, `area`, `electrical_consumption`, `thermal_consumption`, `water_consumption`, `tanno`, `panno`, `gstno`, `proposal_document`, `po_document`, `account_status`, `verified_status`, `verified_remark`, `verified_by`, `verified_at`, `status_updated_by`, `status_updated_at`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'C00001', 'miraki', 'vardhanm@mirakitech.com', '$2y$10$fYktf3COlV/.n0yKeSi91Ohyt3hZ8ZyvK.7Ri6/tSIqlt5nItxKKe', 7036221419, 'NORTH POLE', 'Chandigarh', '71 ST. NICHOLAS DRIVE\r\nabc', 71, '71 ST. NICHOLAS DRIVE\r\nabc', 71, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 1, '2022-01-04 14:03:34', NULL, NULL, 1, NULL, '2022-01-04 14:03:34', '2022-01-04 14:03:34', NULL),
(2, 'C00002', 'miraki', 'vardhanmsad@mirakitech.com', '$2y$10$/0w61dLCoxb8x5q5ns2BJubGCACeJ32fbO6R8nIFGzEk5pI36fnG6', 7036221443, 'NORTH POLE', 'Karnataka', '71 ST. NICHOLAS DRIVE\r\nabc', 71, '71 ST. NICHOLAS DRIVE\r\nabc', 71, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, 1, '2022-01-04 14:04:17', NULL, NULL, 1, NULL, '2022-01-04 14:04:17', '2022-01-04 14:04:47', NULL),
(3, 'C00003', 'das', 'vardhanmmmm@yopmail.com', '$2y$10$rQKQt8ZUehEI1fveLylKruZWvcmmBU7FJfnsZQs78rmATmy/vutqK', 8778784444, 'NORTH POLE', 'Telangana', '71 ST. NICHOLAS DRIVE\r\nabc', 71, '71 ST. NICHOLAS DRIVE\r\nabc', 71, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, 1, '2022-01-04 14:04:33', NULL, NULL, 1, NULL, '2022-01-04 14:04:33', '2022-01-04 14:06:32', NULL),
(4, 'C00004', 'asdf', 'asd@yopmail.com', '$2y$10$TA5zs/aSvI1nzSQAgulnIO0h7IR7ibuOeu4H5xkQ/JuRYWaPleNzG', 6776676666, 'Hyderabda', 'Telangana', '71 ST. NICHOLAS DRIVE\r\nabc', 71, '71 ST. NICHOLAS DRIVE\r\nabc', 71, 2, 3, 3, 'name', '1', NULL, 1, 1, '1', '1', '1', '1', '1', '1', '1', NULL, NULL, 1, 1, NULL, 1, '2022-01-04 14:23:43', NULL, NULL, 1, NULL, '2022-01-04 14:23:43', '2022-01-04 15:10:23', NULL),
(5, '', 'miraki', 'vardhanm1234567889@mirakitech.com', NULL, 7123456777, 'NORTH POLE', 'Telangana', '71 ST. NICHOLAS DRIVE\r\nabc', 71, '71 ST. NICHOLAS DRIVE\r\nabc', 71, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 1, '2022-01-04 14:24:06', NULL, NULL, 1, NULL, '2022-01-04 14:24:06', '2022-01-04 14:34:30', NULL),
(6, 'C00005', 'miraki', 'vardhanm1234567890@mirakitech.com', '$2y$10$FaPqHxGRSgiip.N97ie/8ORed6zJLgrMhfgEKzJKGYREVnqQInSgG', 7123456789, 'NORTH POLE', 'Telangana', '71 ST. NICHOLAS DRIVE\r\nabc', 71, '71 ST. NICHOLAS DRIVE\r\nabc', 71, 1, 2, 3, 'name', '1', NULL, 1, 1, '1', '1', '1', '1', '1', '1', '1', NULL, NULL, 1, 1, NULL, 1, '2022-01-04 15:12:59', NULL, NULL, 1, NULL, '2022-01-04 15:12:59', '2022-01-04 15:13:27', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
