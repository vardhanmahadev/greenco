@extends(ASSESSOR_THEME_NAME.'.layouts.app')

@section('content')
<div class="app-content content content-ps-stl pt-4">
    <div class="">
        <div class="content-overlay"></div>
        <!-- <div class="header-navbar-shadow"></div> -->
        <div class="content-wrapper">
            <div class="content-body">
                <ul class="nav nav-tabs tabs-pages tab-leftside" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link tab-links mini-border" href="{{route('assessor.profile')}}" aria-selected="true">
                            <i data-feather="user"></i>Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link tab-links mini-border" href="{{route('assessor.changePassword')}}" aria-controls="changepasswordIcon" aria-selected="false">
                            <i data-feather="lock"></i>Change Password</a>
                    </li>
                </ul>

                 <section class=" p-relative">
                    <div class="col-12">
                        <section id="advanced-search-datatable ">
                            <form action="{{route('assessor.profileupdate')}}" onSubmit="return false" class="profileUpdate" id="profileUpdate" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="card ">
                                    
                                        <div class="col-lg-12 col-xl-12 col-md-12 col-12 " >
                                           
                                               
                                        </div>
                                      
                                        <input type='hidden' name="assessorid" id="assessorid" value="{{$profile->id}}">
                                        <input type='hidden' name="profile_updated_status" id="profile_updated_status" value="{{$profile->profile_updated}}">
                                        <input type='hidden' name="verification_status" id="verification_status" value="{{$profile->verification_status}}">
                                                    
                                        <div class="row">
                                        <div class="col-lg-12 col-xl-12 col-md-12 col-12">
                                            <div class="d-flex v-row title-strip">
                                                <div class="col-6 pl-0">Non Disclosure Aggrements List</div>
                                            </div>
                                            
                                            <div class="card-body ">
                                                <div class="row ">
                                                    <div class="col-lg-12 form-group">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-4">
                                                                        <div class="form-group font-small-4"> Non-Disclosure Agreement <br> (Year: 2021-2022)</label>
                                                                        </div>
                                                                    </div>
                                                                          
                                                                    <div class="col-lg-7 col-md-8">
                                                                        <div class="v-row uploadedfile border-bb mb-1">
                                                                            <div class="col-lg-8 pl-0 word-wrap-all">
                                                                                <label class="mr-1 ">
                                                                                    <p class="mb-0">Uploaded File</p>
                                                                                    <span>{{basename($profile->ndc_form)}}</span>
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-lg-4 text-right pr-0">
                                                                                <a href="{{url($profile->ndc_form)}}" target="_blank" class="btn btn-outline-secondary waves-effect" tooltip='View' data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                    <a href="{{url($profile->ndc_form)}}" download class="btn btn-outline-secondary waves-effect ml-1" data-toggle="tooltip" title data-original-title="Download"><i data-feather="download"></i></a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                   
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                        
                                                    </div>

                                                    <div class="col-lg-12 form-group">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-4">
                                                                        <div class="form-group font-small-4"> Non-Disclosure Agreement </label>
                                                                        </div>
                                                                    </div>  
                                                                    <div class="col-lg-7 col-md-8">
                                                                        <div class="custom-file">
                                                                            <input type="file" class="custom-file-input"  accept="application/pdf,image/jpg,image/jpeg,image/png" id="ndc_form" name="ndc_form" >
                                                                            <label class="custom-file-label" for="ndc_form">Choose file</label>
                                                                            <span id="ndc_form-error-add" class="error"></span>
                                                                        </div>
                                                                    </div>    
                                                                </div>  
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="row">
                                                                    <div class="col-lg-4 col-md-4">
                                                                        <a href="{{url('app-assets/forms/Confidentiality Agreement.doc')}}" class="btn btn-outline-secondary  waves-effect"><i data-feather="download"></i> Download Sample File</a>
                                                                    </div>
                                                                    <div class="col-lg-4 col-md-4">
                                                                        <select name="year" id="year" class="form-control">
                                                                            <option value="">Select Year</option>
                                                                            <option value="2021">2021</option>
                                                                            <option value="2022">2022</option>
                                                                            <option value="2023">2023</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
   
                                                    <div class="col-lg-12 mt-2 m0-auto text-center">
                                                        @if( $profile->profile_updated == 0)
                                                                <input type='hidden' name="finalsubmitval" id="finalsubmitval" value='false' >
                                                                <button type="button" class="btn btn-primary mr-1 waves-effect waves-float waves-light page_save_btn" id="page_save" name="page_save">Save</button>
                                                           
                                                        @elseif($profile->profile_updated == 1)
                                                            <input type='hidden' name="finalsubmitval" id="finalsubmitval" value='true' >   
                                                            <button type="button" class="btn btn-primary mr-1 waves-effect waves-float waves-light page_submit_btn" id="page_submit" name="page_submit"> Save</button>
                                                        @endif

                                                            <button type="button" class="btn btn-outline-secondary mr-1 waves-effect waves-float waves-light cancelpage_btn" id="cancelpage" name="cancelpage">Cancel</button>

                                                    </div>
                                                   
                                                    
                                                </div>
                                            </div>

                                        </div>
                                        </div>

                                        
                                    </div>
                                </div>
                            </form>
                        </section>
                    </div>
                </section>
            <!-- </div> -->
                <!-- Modern Horizontal Wizard -->
            </div>
        </div>
    </div>
</div>
@endsection

@push('PAGE_SCRIPTS')
<script type="text/javascript">
    $("#cancelpage").click(function(){
        location.reload();
        $('html, body').scrollTop(0);
    });

 
 // multiselect specific packages
 $('.industry-category').select2({
     placeholder: "Select Industry Category",
     allowClear: true
    });



var Profile = function () {
    return { //main function to initiate the module
        init: function () {
            jQuery.validator.addMethod("mobileNo", function(value, element) {
               return this.optional(element) || /^[6-9][0-9]{9}$/.test(value);
            }, "Mobile Number is invalid"); 

            jQuery.validator.addMethod("noSpace", function(value, element) { 
              return value == '' || value.trim().length != 0;  
            }, "This field is invalid");

            jQuery.validator.addMethod("doubleSpace", function(value, element) {
               return this.optional(element) || /^\S((?!.*  ).*\S)?$/.test(value);
            }, "This field is invalid");  

            jQuery.validator.addMethod("lettersonly", function(value, element) 
            {
                return this.optional(element) || /^[A-Z," "]+$/i.test(value);
            }, "Letters and spaces only please");

            jQuery.validator.addMethod("alphanumeric", function(value, element) 
            {
                return this.optional(element) || /^[A-Z0-9]+$/i.test(value);
            }, "Letters and spaces only please");
            


            $('.select2').on('change', function(e) {
                $('.select2').removeClass('error');
                $('#ind_category-error-add').css('display','none');
                if ($('.select2').val() == '') {
                    if($('#finalsubmitval').val() =='true')
                    {
                        $('.select2').addClass('error');
                        $('#ind_category-error-add').css('display','block');
                    }
                    // Stop submiting
                    e.preventDefault();
                    return false;
                }
            });

       


            $('#profileUpdate').validate({
                ignore: [],
                rules: {
                    'name': {
                        noSpace: true,
                        doubleSpace:true,
                        maxlength: 50,
                        minlength:3
                    },
                    'email': {
                        email: true,
                    },
                    'ind_category': {
                            required: true,
                            },

                    'state': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                    },
                    'mobile':{
                        noSpace: true,
                        digits:true,
                        minlength:10,
                        mobileNo: true
                    },
                    'alt_mobile':{
                       
                        noSpace: true,
                        digits:true,
                        minlength:10,
                        mobileNo: true
                    },

                    'address_1': {
                        // required:true,
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        doubleSpace:true,
                        noSpace: true,
                        minlength:3
                    },

                    'address_2': {
                        doubleSpace:true,
                        noSpace: true,
                        minlength:3
                    },

                    'city': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        doubleSpace:true,
                        noSpace: true,
                        minlength:3,
                        lettersonly:true,
                    },

                    'pincode': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        
                        digits:true,
                        minlength:6
                    },

                    'pancardno': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        minlength:10,
                        maxlength:10,
                        alphanumeric:true,
                    },

                    'gstnumber': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        minlength:15,
                        maxlength:15,
                        alphanumeric:true,
                    },
                    
                    'em_contact_name': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        doubleSpace:true,
                        noSpace: true,
                        maxlength: 50,
                        minlength:3,
                        lettersonly:true,
                    },
                    'em_contact_no': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        noSpace: true,
                        digits:true,
                        minlength:10,
                        mobileNo: true
                    },
                    // 'em_contact_address': {
                    'em_address_1':{
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        doubleSpace:true,
                        noSpace: true,
                        minlength:3
                    },

                    'em_address_2': {
                        doubleSpace:true,
                        noSpace: true,
                        minlength:3
                    },

                    'em_city': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        doubleSpace:true,
                        noSpace: true,
                        minlength:3,
                        lettersonly:true,
                    },

                    'em_pincode': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        
                        digits:true,
                        minlength:6
                    },

                    'em_state': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                    },

                    'bank_name': {
                        // required: '#adding:checked',
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        doubleSpace:true,
                        noSpace: true,
                        maxlength: 50,
                        minlength:3,
                        lettersonly:true,
                    },
                    'branch_name': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        doubleSpace:true,
                        noSpace: true,
                        maxlength: 50,
                        minlength:3,
                        lettersonly:true,

                    },
                    'ifsc_code': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        doubleSpace:true,
                        noSpace: true,
                        maxlength: 50,
                        minlength:3,
                        alphanumeric:true,
                    },
                    'account_number': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        doubleSpace:true,
                        noSpace: true,
                        maxlength: 50,
                        minlength:10,
                        alphanumeric:true,
                    },

                    'biodata': {
                        required: function(){
                            return ($("#biodatadoccheck").val() == "2" && $("#finalsubmitval").val() == "true");
                        },
                        extension : "png|jpeg|jpg|pdf",
                        accept:"application/pdf,image/jpg,image/jpeg,image/png"
                        
                    },
                    
                    'cancelled_cheque': {
                        required: function(){
                            return ($("#chequedoccheck").val() == "2" && $("#finalsubmitval").val() == "true");
                        },
                        extension : "png|jpeg|jpg|pdf",
                        accept:"application/pdf,image/jpg,image/jpeg,image/png"
                        
                    },
                    'gst_doc': {
                        required: function(){
                            return ($("#gstdoccheck").val() == "2" && $("#finalsubmitval").val() == "true");
                        },
                        extension : "png|jpeg|jpg|pdf",
                        accept:"application/pdf,image/jpg,image/jpeg,image/png"
                        
                    },
                    'vendorstamp_doc': {
                        required: function(){
                            return ($("#vrfoccheck").val() == "2" && $("#finalsubmitval").val() == "true");
                        },
                        extension : "png|jpeg|jpg|pdf",
                        accept:"application/pdf,image/jpg,image/jpeg,image/png"
                        
                    },
                    'ndc_form':{
                        required: function(){
                            return ($("#ndcdoccheck").val() == "2" && $("#finalsubmitval").val() == "true");
                        },
                        extension : "png|jpeg|jpg|pdf",
                        accept:"application/pdf,image/jpg,image/jpeg,image/png"
                        
                    },
                    'health_doc':{
                        required: function(){
                            return ($("#healthdoccheck").val() == "2" && $("#finalsubmitval").val() == "true");
                        },
                        extension : "png|jpeg|jpg|pdf",
                        accept:"application/pdf,image/jpg,image/jpeg,image/png"
                        
                    },
                    'pancard_doc':{
                        required: function(){
                            return ($("#pandoccheck").val() == "2" && $("#finalsubmitval").val() == "true");
                        },
                        extension : "png|jpeg|jpg|pdf",
                        accept:"application/pdf,image/jpg,image/jpeg,image/png"
                        
                    }
                },
                messages: {
                    name: {
                        required: "The Name field is required",
                        noSpace: "Please Enter a Valid Name",
                        doubleSpace: "Please Enter a Valid Name"
                        
                    },
                    email: {
                        required: "The Email Address field is required",
                        email : "Please Enter a Valid Email Address"
                    },
                    ind_category:{
                        required: "The Category field is required",
                    },
                    state:{
                        required: "The State field is required",
                    },
                    mobile: {
                        required: "The Mobile Number field is required",
                    }, 

                    alt_mobile: {  
                        mobileNo: "Please Enter a Valid Alternate Mobile Number"
                    },

                    address_1: {
                        required: "The Address Line 1 field is required",
                        noSpace: "Please Enter a Valid Address Line 1",
                        doubleSpace: "Please Enter a Valid Address Line 1"
                    },

                    address_2: {
                        noSpace: "Please Enter a Valid Address Line 2",
                        doubleSpace: "Please Enter a Valid Address Line 2"
                    },

                    city: {
                        required: "The City field is required",
                        noSpace: "Please Enter a Valid City",
                        doubleSpace: "Please Enter a Valid City",
                        lettersonly: "Please Enter a Valid City"
                    },

                    pincode: {
                        required: "The Pincode field is required",
                    }, 

                    pancardno:{
                        required: "The Pancard Number field is required",
                        alphanumeric: "Please Enter a Valid Pancard Number"
                    },
                    gstnumber:{
                        required: "The GST Number field is required",
                        alphanumeric: "Please Enter a Valid GST Number"
                    },


                    em_contact_name: {
                        required: "The Contact Name field is required",
                        noSpace: "Please Enter a Valid Contact Name",
                        doubleSpace: "Please Enter a Valid Contact Name",
                        lettersonly: "Please Enter a Valid Contact Name"
                    },
                    em_contact_no: {
                        required: "The Mobile Number field is required",
                        mobileNo: "Please Enter a Valid Mobile Number"
                    },
                    em_address_1: {
                        required: "The  Address Line 1 field is required",
                        noSpace: "Please Enter a Valid Address Line 1",
                        doubleSpace: "Please Enter a Valid Address Line 1"
                    },

                    em_address_2: {
                        noSpace: "Please Enter a Valid Address Line 2",
                        doubleSpace: "Please Enter a Valid Address Line 2"
                    },

                    em_city: {
                        required: "The City field is required",
                        noSpace: "Please Enter a Valid City",
                        doubleSpace: "Please Enter a Valid City",
                        lettersonly: "Please Enter a Valid City"
                    },

                    em_pincode: {
                        required: "The Pincode field is required",
                    }, 

                    em_state: {
                        required: "The State field is required",
                    }, 

                    bank_name: {
                        required: "The Bank Name field is required",
                        noSpace: "Please Enter a Valid Bank Name",
                        doubleSpace: "Please Enter a Valid Bank Name",
                        lettersonly: "Please Enter a Valid Bank Name"
                    },
                    branch_name: {
                        required: "The Branch Name field is required",
                        noSpace: "Please Enter a Valid Branch Name",
                        doubleSpace: "Please Enter a Valid Branch Name",
                        lettersonly: "Please Enter a Valid Branch Name"
                    },
                    ifsc_code: {
                        required: "The IFSC Code  field is required",
                        noSpace: "Please Enter a Valid IFSC Code",
                        doubleSpace: "Please Enter a Valid IFSC Code",
                        alphanumeric: "Please Enter a Valid IFSC Code"
                    },
                    account_number: {
                        required: "The Account Number field is required",
                        noSpace: "Please Enter a Valid Account Number",
                        doubleSpace: "Please Enter a Valid Account Number",
                        alphanumeric: "Please Enter a Valid Account Number"
                    },
                    biodata: {
                        required: "The Bio Data is required",
                        extension: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only",
                        accept: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only"
                        },  
                    cancelled_cheque: {
                        required: "The Cancelled Cheque is required",
                        extension: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only",
                        accept: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only"
                        },  
                    gst_doc: {
                        required: "The GST Declaration is required",
                        extension: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only",
                        accept: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only"
                        },  
                    vendorstamp_doc: {
                        required: "The Vendor Registration Form is required",
                        extension: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only",
                        accept: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only"
                        },  
                    ndc_form: {
                        required: "The Non-Disclosure Aggrement is required",
                        extension: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only",
                        accept: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only"
                        },  
                    health_doc: {
                        required: "The Health Declaration is required",
                        extension: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only",
                        accept: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only"
                        },  
                    pancard_doc: {
                        required: "The PAN Card is required",
                        extension: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only",
                        accept: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only"
                        },  
                },
                focusInvalid: false,
                invalidHandler: function() {
                    $('html, body').scrollTop(0);
                    $(this).find(":input.error:first").focus();
                }
                
            });
            
            // save button code

            $("#page_save").click(function(){ 
                $('#finalsubmitval').val('false');
                $("#ind_category").removeAttr("required");
                if ($('#profileUpdate').valid() == true) 
                {
                    $("#page_save").prop('disabled',true);
                    $("#page_submit").prop('disabled',true);
                    $("#cancelpage").prop('disabled',true);
                    
                    $("#page_save").html("Loading....");
                    var form_data = new FormData(document.getElementById("profileUpdate"));
                    
                    $.ajax({
                        type: "POST",
                        url: $('form#profileUpdate').attr('action'),
                        data: form_data,
                        processData: false,
                        contentType: false,
                        success: function(response) {
                            
                            if(response.status == 'errors') {
                                $("#page_save").prop('disabled',false);
                                $("#page_submit").prop('disabled',false);
                                $("#page_save").html("Save");
                                $("#cancelpage").prop('disabled',false);
                                $.each(response.errors, function (key, error) {
                                    $('form#profileUpdate').find("#" + key + "-icon-error").removeClass('borderRightError').addClass('borderRightError');
                                    $('form#profileUpdate').find("#" + key).removeClass('error').addClass('error');
                                    $('form#profileUpdate').find("#" + key + "-error-add").css('display','block');
                                    $('form#profileUpdate').find("#" + key + "-error-add").text(error[0]);
                                });
                                
                            }
                            if(response.status == 'success') {
                                    setTimeout(function(){
                                        location.reload();
                                        $('html, body').scrollTop(0);
                                    }, 3000);
                                    JsUtility.showToastr('success', 'Profile', response.message);
                                }
                            

                            if(response.status == 'error') {
                                $("#page_save").prop('disabled',false);
                                $("#page_submit").prop('disabled',false);
                                $("#page_save").html("Save");
                                $("#cancelpage").prop('disabled',false);
                                JsUtility.showToastr('error', 'Profile', response.message);
                            }
                            if(response.status =='errorlogout')
                            {
                                setTimeout(function(){
                                    window.location.href = "{{route('assessor.login')}}";
                                    }, 3000);
                                JsUtility.showToastr('error', 'Assessor', response.message);
                            }
                        },
                        error: function(response) {
                            $("#page_save").prop('disabled',false);
                            $("#page_submit").prop('disabled',false);
                            $("#page_save").html("Save");
                            $("#cancelpage").prop('disabled',false);
                            JsUtility.showToastr('error', 'Profile', response.message);
                        }
                    });   
                }
                
               
            });

            // final submit code
          
            $("#page_submit").click(function(){ 
                $('#finalsubmitval').val('true');
                $("#ind_category").attr("required");
                if ($('.select2').val() == '') {
                    if($('#finalsubmitval').val() =='true' && $('#profile_updated_status').val() !='1')
                    {
                        $('.select2').addClass('error');
                        $('#ind_category-error-add').css('display','block');
                        $('html, body').scrollTop(0);
                        $(this).find(":input.error:first").focus();
                        e.preventDefault();
                        return false;
                    }
                }
                    // Stop submiting
                   
                if ($('#profileUpdate').valid() == true) 
                {
                    if($('#profile_updated_status').val() =='1')
                    {
                        var text = "Are you sure, you want to Save the Data ?";
                        var buttontext = "Save";
                    } 
                    else
                    {
                        var text = "Are you sure, Once Submitted Data Cannot be reverted again ?" ;
                        var buttontext = "Submit";
                    }
                        
                    Swal.fire({
                            title: text,
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonText: buttontext,
                            showClass: {
                                popup: 'animate__animated animate__fadeIn'
                            },
                            customClass: {
                                confirmButton: 'btn btn-primary',
                                cancelButton: 'btn btn-outline-danger ml-1'
                            },
                            buttonsStyling: false
                            }).then(function(result) {
                            if (result.value) {
                                var form_data = new FormData(document.getElementById("profileUpdate"));
                                $("#page_save").attr('disabled',true);
                                $("#page_submit").attr('disabled',true);
                                $(".back").css('pointer','none');
                                $("#page_submit").html("Loading....");
                                $("#cancelpage").prop('disabled',true);

                                $.ajax({
                                    type: "POST",
                                    url: "{{ route('assessor.submitprofile') }}",
                                    data: form_data,
                                    processData: false,
                                    contentType: false,
                                    success: function(response) {
                                        if(response.status == 'errors') {
                                            $.each(response.errors, function (key, error) {
                                                $("#page_save").attr('disabled',false);
                                                $("#page_submit").attr('disabled',false);
                                                $("#page_submit").html("Final Submit");
                                                $("#cancelpage").prop('disabled',false);
                                                $('form#profileUpdate').find("#" + key + "-icon-error").removeClass('borderRightError').addClass('borderRightError');
                                                $('form#profileUpdate').find("#" + key).removeClass('error').addClass('error');
                                                $('form#profileUpdate').find("#" + key + "-error-add").css('display','block');
                                                $('form#profileUpdate').find("#" + key + "-error-add").text(error[0]);
                                                $('html, body').scrollTop(0);
                                                $(this).find(":input.error:first").focus();
                                            });
                                        }

                                        if(response.status == 'success') {
                                        
                                            JsUtility.showToastr('success', 'Assessor', response.message);
                                            setTimeout(function(){
                                                    location.reload();
                                                    $('html, body').scrollTop(0);
                                                }, 3000);
                                            
                                            
                                        }

                                        if(response.status == 'error') {
                                            $("#page_save").attr('disabled',false);
                                            $("#page_submit").attr('disabled',false);
                                            $("#page_submit").html("Final Submit");
                                            $("#cancelpage").prop('disabled',false);
                                            JsUtility.showToastr('error', 'Assessor', response.message);
                                        }
                                        if(response.status =='errorlogout')
                                        {
                                            setTimeout(function(){
                                                window.location.href = "{{route('assessor.login')}}";
                                                }, 3000);
                                            JsUtility.showToastr('error', 'Assessor', response.message);
                                        }
                                    },
                                    error: function() {
                                        $("#page_save").attr('disabled',false);
                                        $("#page_submit").attr('disabled',false);
                                        $("#page_submit").html("Final Submit");
                                        $("#cancelpage").prop('disabled',false);
                                        JsUtility.showToastr('error', 'Assessor', response.message);
                                    }
                                }); 
                            }
                        });
                }
                

            });

           
        }
    }
}();

jQuery(document).ready(function() {
    Profile.init();
    
    if($('#profile_updated_status').val() =='1')
    {
        $('#finalsubmitval').val('true');
        $("#bankdetail *").prop('disabled',true);
        $("#basicdetail *").prop('disabled',true);
        $("#bankdetail :input").addClass('remarks_data');
        $("#basicdetail :input").addClass('remarks_data');
        $(".select2-selection--multiple").addClass('samplestyle');
        $('.select2-selection--multiple').addClass('removedropdownstyle');


    }

    $('input[type=text],select').blur(function(){
        $("#profileUpdate").validate().element(this);
    });

    $('input[type=text]').on('keyup',function(event){
        event.currentTarget.value = this.value.toUpperCase();
        $("#profileUpdate").validate().element(this);
    });

    $('input,select').on('keyup',function(event){
        $("#profileUpdate").validate().element(this);
    });


});

</script>
@endpush
