@extends(ASSESSOR_THEME_NAME.'.layouts.app')

@section('content')
<div class="app-content content content-ps-stl pt-4">
    <div class="">
        <div class="content-overlay"></div>
        <!-- <div class="header-navbar-shadow"></div> -->
        <div class="content-wrapper">
            <div class="content-body">
                <ul class="nav nav-tabs tabs-pages tab-leftside" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link tab-links active mini-border" href="{{route('assessor.profile')}}" aria-selected="true">
                            <i data-feather="user"></i>Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link tab-links mini-border" href="{{route('assessor.changePassword')}}" aria-controls="changepasswordIcon" aria-selected="false">
                            <i data-feather="lock"></i>Change Password</a>
                    </li>
                </ul>
                 <section class=" p-relative">
                    <div class="col-12">
                        <section id="advanced-search-datatable ">
                            <form action="{{route('assessor.profileupdate')}}" onSubmit="return false" class="profileUpdate" id="profileUpdate" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="card ">
                                    
                                        <div class="col-lg-12 col-xl-12 col-md-12 col-12 " >
                                            <div class="d-flex v-row title-strip">
                                                <div class="col-6 pl-0">Basic Details</div>
                                            </div>
                                            <div class="col-lg-12 mt-1 pl-1">
                                            <ul class="info-form">
                                                <li>
                                                <span>Note:</span> The Applicant has to fill Basic Details along with biodata to be Assessor.  
                                            </li>
                                            </ul>
                                        </div>
                                                
                                                <input type='hidden' name="assessorid" id="assessorid" value="{{$profile->id}}">
                                                <input type='hidden' name="profile_updated_status" id="profile_updated_status" value="{{$profile->profile_updated}}">
                                                
                                                <input type='hidden' name="verification_status" id="verification_status" value="{{$profile->verification_status}}">
                                                <div class="card-body " id='basicdetail'>
                                                    <div class="row ">
                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Name <span class="text-danger">*</span> </label>
                                                            </div>
                                                            <input class="form-control border-right-line remarks_data" disabled='true' autocomplete="off" type="text" onkeypress="return /[a-zA-Z\s]/i.test(event.key)" maxlength="50" name="name" id="name" placeholder="Name*" value="{{old('name', $profile->name)}}"/>
                                                            <span id="name-error-add" class="error"></span>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Email Address <span class="text-danger">*</span> </label>
                                                            </div>
                                                             <input  class="form-control border-right-line  remarks_data" disabled='true' id="email" type="email" name="email" placeholder="Email Address*"  value="{{old('email', $profile->email)}}" autocomplete="false"  />
                                                             <span id="email-error-add" class="error"></span>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Mobile Number <span class="text-danger">*</span> </label>
                                                            </div>
                                                            <input type="text" onkeypress="return /[0-9]/i.test(event.key)" class="form-control border-right-line remarks_data" disabled='true' maxlength="10" oninput="this.value=this.value.slice(0,this.maxLength)" id="mobile" name="mobile" placeholder="Mobile Number*"  value="{{old('mobile', $profile->mobile)}}"/>
                                                            <span id="mobile-error-add" class="error"></span>
                                                        </div>   
                                                        
                                                        <div class="col-lg-4 col-md-4 selectcheck-div">
                                                            <div class="form-label-group form-group select-checkvarlist ">
                                                                <label  class="ind-category-label font-small-4">Industry Category <span class="text-danger">*</span></label>
                                                                <div class="select-tag ">
                                                                    <select class="select2 form-control form-control-md"  multiple data-placeholder="Select Industry Category" name="ind_category[]" id="ind_category">
                                                                        
                                                                    <!-- <option disabled>Select Industry Category</option> -->
                                                                    @php $cdata = json_decode($profile->category,TRUE); @endphp
                                                                    @foreach($categories as $category)
                                                                        @if($cdata)
                                                                            @if(in_array($category->id,$cdata))
                                                                            <option value="{{$category->id}}" selected>{{$category->name}}</option> 
                                                                            @else
                                                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                                                            @endif
                                                                        @else
                                                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                                                        @endif
                                                                    @endforeach
                                                                    </select>
                                                                </div>
                                                                <span class="error" id="ind_category-error-add" style="display:none;">The Industry Category field is required</span>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Alternate Mobile Number </label>
                                                            </div>
                                                            <input type="text" maxlength="10" oninput="this.value=this.value.slice(0,this.maxLength)" onkeypress="return /[0-9]/i.test(event.key)"  class="form-control dt-input bg-white" placeholder="Alternate Mobile Number" name="alt_mobile" id="alt_mobile" value="{{old('alt_mobile', isset($profile->alt_mobile)?$profile->alt_mobile:'')}}">
                                                            <span class="error" id="alt_mobile-error-add" ></span>
                                                        </div>

                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Address Line 1 <span class="text-danger">*</span></label>
                                                            </div>
                                                            <input type="text"  maxlength="100" class="form-control dt-input bg-white"  placeholder="Address Line 1*" name="address_1" id="address_1" value="{{old('address_1', isset($profile->address_1)?$profile->address_1:'')}}">
                                                            <span class="error" id="address_1-error-add" ></span>
                                                        </div>

                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Address Line 2 </label>
                                                            </div>
                                                            <input type="text"  maxlength="100" class="form-control dt-input bg-white"  placeholder="Address Line 2" name="address_2" id="address_2" value="{{old('address_2', isset($profile->address_2)?$profile->address_2:'')}}">
                                                            <span class="error" id="address_2-error-add" ></span>
                                                        </div>

                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">City <span class="text-danger">*</span></label>
                                                            </div>
                                                            <input type="text"  maxlength="100" class="form-control dt-input bg-white"  onkeypress="return /[a-zA-Z\s]/i.test(event.key)" placeholder="City*" name="city" id="city" value="{{old('city', isset($profile->city)?$profile->city:'')}}">
                                                            <span class="error" id="city-error-add" ></span>
                                                        </div>

                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">State <span class="text-danger">*</span></label>
                                                            </div>
                                                            <select class="form-control" name="state" id="state">
                                                                <option value="">Select State</option>
                                                                @foreach($states as $state)
                                                                    <option value="{{$state->id}}" @if(isset($profile))@if($profile->state == $state->id) selected @endif @endif>{{$state->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="error" id="state-error-add" ></span>
                                                        </div>

                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Pincode <span class="text-danger">*</span></label>
                                                            </div>
                                                            <input type="text"  maxlength="6" class="form-control dt-input bg-white" onkeypress="return /[0-9]/i.test(event.key)"  placeholder="Pincode*" name="pincode" id="pincode" value="{{old('pincode', isset($profile->pincode)?$profile->pincode:'')}}">
                                                            <span class="error" id="pincode-error-add" ></span>
                                                        </div>

                                                        

                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Pancard Number <span class="text-danger">*</span></label>
                                                            </div>
                                                            <input type="text"  maxlength="10" class="form-control dt-input bg-white" onkeypress="return /[a-zA-Z0-9]/i.test(event.key)"  placeholder="Pancard Number*" name="pancardno" id="pancardno" value="{{old('pancardno', isset($profile->pan_number)?$profile->pan_number:'')}}">
                                                            <span class="error" id="pancard-error-add" ></span>
                                                        </div>

                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">GST Number <span class="text-danger">*</span></label>
                                                            </div>
                                                            <input type="text"  maxlength="15" class="form-control dt-input bg-white" onkeypress="return /[a-zA-Z0-9]/i.test(event.key)"  placeholder="GST Number*" name="gstnumber" id="gstnumber" value="{{old('gstnumber', isset($profile->gst_number)?$profile->gst_number:'')}}">
                                                            <span class="error" id="gstnumber-error-add" ></span>
                                                        </div>
                                                    </div>
                                                    <div class="row ">
                                                        <div class="col-lg-4 col-lg-4">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-md-6 mb-75">
                                                                <label class="mb-0 font-small-4 accountdata_info">Account Status </label>
                                                                </div>
                                                                <div class="col-lg-6 col-md-6">
                                                                <p class="card-text font-small-4 mt-0 verified-text badge badge-light-{{$profile->status =='1' ? 'success' : 'danger' }} badge-pill ">{{$profile->status =='1' ? "Active" : "In-Active"}}</p>
                                                                </div>
                                                                <div class="col-lg-6 col-md-6 ">
                                                                <label class="mb-0 font-small-4 accountdata_info">Account Activation Date </label>
                                                                </div>
                                                                <div class="col-lg-6 col-md-6">
                                                                    <p class="card-text font-small-4 mt-0  p-a0 verified-text ">{{ $profile->status_updated_at != NULL ? date('j F, Y',strtotime($profile->status_updated_at)) : " "}}</p>   
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @if($profile->profile_updated ==1)
                                                        <div class="col-lg-4 col-lg-4">
                                                            <div class="row">
                                                                <div class="col-lg-6 col-md-6 mb-75">
                                                                    <label class="mb-0 font-small-4 accountdata_info">Approval Status </label>
                                                                </div>
                                                                <div class="col-lg-6 col-md-6">
                                                                    <p class="card-text font-small-4 mt-0 verified-text badge badge-light-{{$profile->verification_status =='1' ? 'success' : 'warning' }} badge-pill ">{{$profile->verification_status =='1' ? "Approved" : "Pending"}}</p>
                                                                </div>
                                                                <div class="col-lg-6 col-md-6 ">
                                                                    <label class="mb-0 font-small-4 accountdata_info">Approval Date </label>
                                                                </div>
                                                                <div class="col-lg-6 col-md-6">
                                                                    @if($profile->verification_done_at != NULL)
                                                                    <p class="card-text font-small-4 mt-0 p-a0 verified-text ">{{  date('j F, Y',strtotime($profile->verification_done_at)) }} </p>
                                                                    @else
                                                                    <p class="card-text mt-0 p-a0 verified-text " style="font-size:10px">━━━━━━━</p>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif
                                                    </div>
                                                    
                                        </div>
                                        <div class="row">
                                        <div class="col-lg-12 col-xl-12 col-md-12 col-12">
                                            <div class="d-flex v-row title-strip">
                                                <div class="col-6 pl-0">Emergency Contact Details</div>
                                            </div>
                                            <div class="card-body ">
                                            <div class="row ">
                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Contact Name <span class="text-danger">*</span></label>
                                                            </div>
                                                            <input type="text" class="form-control dt-input bg-white" onkeypress="return /[a-zA-Z\s]/i.test(event.key)" placeholder="Contact Name*" name="em_contact_name" id="em_contact_name" value="{{old('em_contact_name', isset($profile->em_contact_name)?$profile->em_contact_name:'')}}">
                                                            <span class="error" id="em_contact_name-error-add" ></span>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Mobile Number <span class="text-danger">*</span></label>
                                                            </div>
                                                            <input type="text" maxlength="10" oninput="this.value=this.value.slice(0,this.maxLength)" onkeypress="return /[0-9]/i.test(event.key)"  class="form-control dt-input bg-white" placeholder="Mobile Number*" name="em_contact_no" id="em_contact_no" value="{{old('em_contact_no', isset($profile->em_contact_no)?$profile->em_contact_no:'')}}">
                                                            <span class="error" id="em_contact_no-error-add" ></span>
                                                        </div>
                                                        
                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Address Line 1 <span class="text-danger">*</span></label>
                                                            </div>
                                                            <input type="text"  maxlength="100" class="form-control dt-input bg-white"  placeholder="Address Line 1*" name="em_address_1" id="em_address_1" value="{{old('em_address_1', isset($profile->em_address_1)?$profile->em_address_1:'')}}">
                                                            <span class="error" id="em_address_1-error-add" ></span>
                                                        </div>

                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Address Line 2 </label>
                                                            </div>
                                                            <input type="text"  maxlength="100" class="form-control dt-input bg-white"  placeholder="Address Line 2" name="em_address_2" id="em_address_2" value="{{old('em_address_2', isset($profile->em_address_2)?$profile->em_address_2:'')}}">
                                                            <span class="error" id="em_address_2-error-add" ></span>
                                                        </div>

                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">City <span class="text-danger">*</span></label>
                                                            </div>
                                                            <input type="text"  maxlength="100" class="form-control dt-input bg-white" onkeypress="return /[a-zA-Z\s]/i.test(event.key)" placeholder="City*" name="em_city" id="em_city" value="{{old('em_city', isset($profile->em_city)?$profile->em_city:'')}}">
                                                            <span class="error" id="em_city-error-add" ></span>
                                                        </div>

                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">State <span class="text-danger">*</span></label>
                                                            </div>
                                                            <select class="form-control" name="em_state" id="em_state">
                                                                <option value="">Select State</option>
                                                                @foreach($states as $state)
                                                                    <option value="{{$state->id}}" @if(isset($profile))@if($profile->em_state == $state->id) selected @endif @endif>{{$state->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="error" id="em_state-error-add" ></span>
                                                            
                                                        </div>

                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Pincode <span class="text-danger">*</span></label>
                                                            </div>
                                                            <input type="text"  maxlength="6" class="form-control dt-input bg-white" onkeypress="return /[0-9]/i.test(event.key)" placeholder="Pincode*" name="em_pincode" id="em_pincode" value="{{old('em_pincode', isset($profile->em_pincode)?$profile->em_pincode:'')}}">
                                                            <span class="error" id="em_pincode-error-add" ></span>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                        </div>

                                        <div class="row">
                                        <div class="col-lg-12 col-xl-12 col-md-12 col-12 " >
                                            <div class="d-flex v-row title-strip">
                                                <div class="col-6 pl-0">Bank Details</div>
                                            </div>
                                            <div class="card-body " id='bankdetail'>
                                                <div class="row ">
                                                    <div class="col-lg-4 col-md-4 form-group">
                                                        <div class=""><label class="font-small-4">Bank Name <span class="text-danger">*</span></label>
                                                        </div>
                                                        <input class="form-control border-right-line" autocomplete="off" type="text" onkeypress="return /[a-zA-Z\s]/i.test(event.key)" maxlength="50" name="bank_name" id="bank_name" placeholder="Bank Name*" value="{{old('bank_name', $profile->bank_name)}}"/>
                                                        <span id="bank_name-error-add" class="error"></span>

                                                    </div>
                                                    <div class="col-lg-4 col-md-4 form-group">
                                                        <div class=""><label class="font-small-4">Account Number <span class="text-danger">*</span></label>
                                                        </div>
                                                        <input class="form-control border-right-line" autocomplete="off" type="text" onkeypress="return /[A-Z0-9]/i.test(event.key)" maxlength="20" name="account_number" id="account_number" placeholder="Account Number*" value="{{old('account_number', $profile->acc_no)}}"/>
                                                        <span id="account_number-error-add" class="error"></span>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 form-group">
                                                        <div class=""><label class="font-small-4">Branch Name <span class="text-danger">*</span></label>
                                                        </div>
                                                        <input class="form-control border-right-line" autocomplete="off" type="text" onkeypress="return /[A-Za-z\s]/i.test(event.key)" maxlength="50" name="branch_name" id="branch_name" placeholder="Branch Name*" value="{{old('branch_name', $profile->branch_name)}}"/>
                                                        <span id="branch_name-error-add" class="error"></span>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 form-group">
                                                        <div class=""><label class="font-small-4">IFSC Code <span class="text-danger">*</span></label>
                                                        </div>
                                                        <input class="form-control border-right-line" autocomplete="off" type="text" onkeypress="return /[A-Z0-9\s]/i.test(event.key)" maxlength="20" name="ifsc_code" id="ifsc_code" placeholder="IFSC Code*" value="{{old('ifsc_code', $profile->ifsc_code)}}"/>
                                                        <span id="ifsc_code-error-add" class="error"></span>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>

                                        <div class="row">
                                        <div class="col-lg-12 col-xl-12 col-md-12 col-12">
                                            <div class="d-flex v-row title-strip">
                                                <div class="col-6 pl-0">Upload Documents(*pdf, *jpg, *png, *jpeg)</div>
                                            </div>
                                            
                                            <div class="card-body ">
                                                <div class="row ">
                                                     <div class="col-lg-12 mb-2 pl-1">
                                                        <ul class="info-form">
                                                            <li>
                                                                <span>Note:</span> Please download the below provided sample. Fill in your details and upload the same file.
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div class="col-lg-12 form-group">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-4">
                                                                        <div class="form-group font-small-4">1. Bio Data <span class="text-danger">*</span></label>
                                                                        </div>
                                                                    </div>
                                                                         @if(  $profile->biodata_status === NULL || $profile->biodata_status == 2)
                                                                        <div class="col-lg-7 col-md-8">
                                                                            <div class="custom-file">
                                                                                <input type="file" class="custom-file-input" accept="application/pdf,image/jpg,image/jpeg,image/png"  id="biodata" name="biodata" >
                                                                                <label class="custom-file-label" for="biodata">Choose file</label>
                                                                                <span id="biodata-error-add" class="error"></span>
                                                                            </div>
                                                                        </div>
                                                                        @endif
                                                                        <input type='hidden' name='biodatadoccheck' id='biodatadoccheck' value="{{(isset($profile->biodata) && $profile->biodata_status != 2) ? '1': '2'}}">

                                                                         @if(isset($profile->biodata))
                                                                            @if(($profile->biodata_status === NULL ||  $profile->biodata_status == 2))
                                                                                <div class="col-lg-5 col-md-4"></div>
                                                                            @endif
                                                                            <div class="col-lg-7 col-md-8">
                                                                                <div class="v-row uploadedfile border-bb mb-1">
                                                                                    <div class="col-lg-8 pl-0 word-wrap-all">
                                                                                        <label class="mr-1 ">
                                                                                            <p class="mb-0">Uploaded File</p>
                                                                                            <span>{{basename($profile->biodata)}}</span>
                                                                                        </label>
                                                                                    </div>
                                                                                    
                                                                                    @if($profile->biodata_status == 1 )
                                                                                    <div class="col-lg-4 text-right pr-0">
                                                                                        <a href="{{url($profile->biodata)}}" target="_blank" class="btn btn-outline-secondary waves-effect" tooltip='View' data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                        <a href="{{url($profile->biodata)}}" download class="btn btn-outline-secondary waves-effect ml-1" data-toggle="tooltip" title data-original-title="Download"><i data-feather="download"></i></a>
                                                                                    </div>
                                                                                    @else   
                                                                                    <div class="col-lg-4 text-right pr-0"> 
                                                                                    <a href="{{url($profile->biodata)}}" target="_blank" class="btn btn-outline-secondary waves-effect"data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a></div>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                       
                                                                        @if(isset($profile->biodata_status))
                                                                            <div class="col-lg-5 col-md-4">
                                                                                <div class="form-group font-small-4">Approval Status :</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-7 col-md-8">
                                                                                <div class="form-group">
                                                                                        <p class="font-small-4 badge badge-light-{{APPROVAL_STATUS_COLORS[$profile->biodata_status]}} badge-pill">{{APPROVAL_STATUS[$profile->biodata_status]}}</p>
                                                                                </div>
                                                                            </div>
                                                                        
                                                                        @endif
                                                                </div>
                                                                
                                                            </div>
                                                            
                                                             @if(  $profile->biodata_status === NULL || $profile->biodata_status == 2)  
                                                                <div class="col-lg-6">
                                                                    <!-- <a href="{{url('app-assets/forms/GST_Declaration.docx')}}" class="btn btn-outline-secondary  waves-effect"><i data-feather="download"></i> Download Sample File</a> -->
                                                                    @if( $profile->biodata_status != 1 && $profile->biodata_remark != NULL) 
                                                                        <div class="mt--1 remark-title">
                                                                            <label>Remarks :</label>
                                                                            <textarea name="" class="form-control remarks_data" disabled='true' rows="3" >{{$profile->biodata_remark}}</textarea>
                                                                        </div>  
                                                                    @endif
                                                                </div>
                                                            @endif 
                                                            
                                                           
                                                            
                                                        </div>
                                                        
                                                    </div>
                                                    

                                                    <div class="col-lg-12 form-group">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-4">
                                                                        <div class="form-group font-small-4">2. Vendor Registration Form  <span class="text-danger">*</span></label>
                                                                        </div>
                                                                    </div>
                                                                         @if(  $profile->vendor_stamp_status === NULL || $profile->vendor_stamp_status == 2)
                                                                        <div class="col-lg-7 col-md-8">
                                                                            <div class="custom-file">
                                                                                <input type="file" class="custom-file-input"  accept="application/pdf,image/jpg,image/jpeg,image/png" id="vendorstamp_doc" name="vendorstamp_doc" >
                                                                                <label class="custom-file-label" for="vendorstamp_doc">Choose file</label>
                                                                                <span id="vendorstamp_doc-error-add" class="error"></span>
                                                                            </div>
                                                                        </div>
                                                                        @endif
                                                                        <input type='hidden' name='vrfoccheck' id='vrfoccheck' value="{{(isset($profile->vendor_stamp) &&  $profile->vendor_stamp_status != 2) ? '1': '2'}}">

                                                                        @if(isset($profile->vendor_stamp))
                                                                             @if(($profile->vendor_stamp_status === NULL ||  $profile->vendor_stamp_status == 2))
                                                                                <div class="col-lg-5 col-md-4"></div>
                                                                             @endif
                                                                            <div class="col-lg-7 col-md-8">
                                                                                <div class="v-row uploadedfile border-bb mb-1">
                                                                                    <div class="col-lg-8 pl-0 word-wrap-all">
                                                                                        <label class="mr-1 ">
                                                                                            <p class="mb-0">Uploaded File</p>
                                                                                            <span>{{basename($profile->vendor_stamp)}}</span>
                                                                                        </label>
                                                                                    </div>
                                                                                    
                                                                                    @if($profile->vendor_stamp_status == 1 )
                                                                                    <div class="col-lg-4 text-right pr-0">
                                                                                        <a href="{{url($profile->vendor_stamp)}}" target="_blank" class="btn btn-outline-secondary waves-effect" tooltip='View' data-toggle="tooltip" title="View" data-original-title="View"><i data-feather="eye"></i></a>
                                                                                         <a href="{{url($profile->vendor_stamp)}}" download class="btn btn-outline-secondary waves-effect ml-1" data-toggle="tooltip" title data-original-title="Download"><i data-feather="download"></i></a>
                                                                                    </div>
                                                                                    @else   
                                                                                    <div class="col-lg-4 text-right pr-0"> 
                                                                                        <a href="{{url($profile->vendor_stamp)}}" target="_blank" class="btn btn-outline-secondary waves-effect" data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                    </div>
                                                                                    @endif
                                                                                   
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                       
                                                                        @if(isset($profile->vendor_stamp_status))
                                                                            <div class="col-lg-5 col-md-4">
                                                                                <div class="form-group font-small-4">Approval Status :</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-7 col-md-8">
                                                                                <div class="form-group">
                                                                                        <p class="font-small-4 badge badge-light-{{APPROVAL_STATUS_COLORS[$profile->vendor_stamp_status]}} badge-pill">{{APPROVAL_STATUS[$profile->vendor_stamp_status]}}</p>
                                                                                </div>
                                                                            </div>
                                                                        
                                                                        @endif
                                                                </div>
                                                                
                                                            </div>

                                                             @if(  $profile->vendor_stamp_status === NULL || $profile->vendor_stamp_status == 2)  
                                                                <div class="col-lg-6">
                                                                @if(  $profile->vendor_stamp_status === NULL || $profile->vendor_stamp_status == 2)    
                                                                    <a href="{{url('app-assets/forms/Vendor_Application_Form.xls')}}" class="btn btn-outline-secondary  waves-effect"><i data-feather="download"></i> Download Sample File</a>
                                                                 @endif
                                                                    @if( $profile->vendor_stamp_status != 1 && $profile->vendor_stamp_remark != NULL)
                                                                        <div class="mt-1 remark-title">
                                                                            <label>Remarks :</label>
                                                                            <textarea name="" class="form-control remarks_data" disabled='true' rows="3" >{{$profile->vendor_stamp_remark}}</textarea>
                                                                        </div>  
                                                                    @endif
                                                                </div>
                                                            @endif 
                                                            
                                                           
                                                            
                                                        </div>
                                                        
                                                    </div>

                                                    <div class="col-lg-12 form-group">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-4">
                                                                        <div class="form-group font-small-4">3. Non-Disclosure Agreement <span class="text-danger">*</span></label>
                                                                        </div>
                                                                    </div>
                                                                         @if(  $profile->ndc_form_status === NULL || $profile->ndc_form_status == 2)
                                                                        <div class="col-lg-7 col-md-8">
                                                                            <div class="custom-file">
                                                                                <input type="file" class="custom-file-input"  accept="application/pdf,image/jpg,image/jpeg,image/png" id="ndc_form" name="ndc_form" >
                                                                                <label class="custom-file-label" for="ndc_form">Choose file</label>
                                                                                <span id="ndc_form-error-add" class="error"></span>
                                                                            </div>
                                                                        </div>
                                                                        @endif
                                                                        <input type='hidden' name='ndcdoccheck' id='ndcdoccheck' value="{{(isset($profile->ndc_form) && $profile->ndc_form_status != 2) ? '1': '2'}}">

                                                                        @if(isset($profile->ndc_form))
                                                                             @if(($profile->ndc_form_status === NULL ||  $profile->ndc_form_status == 2))
                                                                                <div class="col-lg-5 col-md-4"></div>
                                                                             @endif
                                                                            <div class="col-lg-7 col-md-8">
                                                                                <div class="v-row uploadedfile border-bb mb-1">
                                                                                    <div class="col-lg-8 pl-0 word-wrap-all">
                                                                                        <label class="mr-1 ">
                                                                                            <p class="mb-0">Uploaded File</p>
                                                                                            <span>{{basename($profile->ndc_form)}}</span>
                                                                                        </label>
                                                                                    </div>
                                                                                    
                                                                                    @if($profile->ndc_form_status == 1 )
                                                                                    <div class="col-lg-4 text-right pr-0">
                                                                                        <a href="{{url($profile->ndc_form)}}" target="_blank" class="btn btn-outline-secondary waves-effect" tooltip='View' data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                         <a href="{{url($profile->ndc_form)}}" download class="btn btn-outline-secondary waves-effect ml-1" data-toggle="tooltip" title data-original-title="Download"><i data-feather="download"></i></a>
                                                                                    </div>
                                                                                    @else   
                                                                                    <div class="col-lg-4 text-right pr-0"> 
                                                                                        <a href="{{url($profile->ndc_form)}}" target="_blank" class="btn btn-outline-secondary waves-effect" data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                    </div>
                                                                                    @endif
                                                                                   
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                       
                                                                        @if(isset($profile->ndc_form_status))
                                                                            <div class="col-lg-5 col-md-4">
                                                                                <div class="form-group font-small-4">Approval Status :</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-7 col-md-8">
                                                                                <div class="form-group">
                                                                                        <p class="font-small-4 badge badge-light-{{APPROVAL_STATUS_COLORS[$profile->ndc_form_status]}} badge-pill">{{APPROVAL_STATUS[$profile->ndc_form_status]}}</p>
                                                                                </div>
                                                                            </div>
                                                                        
                                                                        @endif
                                                                </div>
                                                                
                                                            </div>

                                                             @if(  $profile->ndc_form_status === NULL || $profile->ndc_form_status == 2)  
                                                                <div class="col-lg-6">
                                                                @if(  $profile->ndc_form_status === NULL || $profile->ndc_form_status == 2)
                                                                    <a href="{{url('app-assets/forms/Confidentiality Agreement.doc')}}" class="btn btn-outline-secondary  waves-effect"><i data-feather="download"></i> Download Sample File</a>
                                                                @endif
                                                                    @if( $profile->ndc_form_status != 1 && $profile->ndc_form_remark != NULL)
                                                                        <div class="mt-1 remark-title">
                                                                            <label>Remarks :</label>
                                                                            <textarea name="" class="form-control remarks_data" disabled='true' rows="3" >{{$profile->ndc_form_remark}}</textarea>
                                                                        </div>  
                                                                    @endif
                                                                </div>
                                                            @endif 
                                                            
                                                           
                                                            
                                                        </div>
                                                        
                                                    </div>

                                                    <div class="col-lg-12 form-group">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-4">
                                                                        <div class="form-group font-small-4">4. Health Declaration <span class="text-danger">*</span></label>
                                                                        </div>
                                                                    </div>
                                                                         @if(  $profile->health_doc_status === NULL || $profile->health_doc_status == 2)
                                                                        <div class="col-lg-7 col-md-8">
                                                                            <div class="custom-file">
                                                                                <input type="file" class="custom-file-input"  accept="application/pdf,image/jpg,image/jpeg,image/png" id="health_doc" name="health_doc" >
                                                                                <label class="custom-file-label" for="health_doc">Choose file</label>
                                                                                <span id="health_doc-error-add" class="error"></span>
                                                                            </div>
                                                                        </div>
                                                                        @endif
                                                                        <input type='hidden' name='healthdoccheck' id='healthdoccheck' value="{{(isset($profile->health_doc) && $profile->health_doc_status != 2) ? '1': '2'}}">

                                                                        @if(isset($profile->health_doc))
                                                                             @if(($profile->health_doc_status === NULL ||  $profile->health_doc_status == 2))
                                                                                <div class="col-lg-5 col-md-4"></div>
                                                                             @endif
                                                                            <div class="col-lg-7 col-md-8">
                                                                                <div class="v-row uploadedfile border-bb mb-1">
                                                                                    <div class="col-lg-8 pl-0 word-wrap-all">
                                                                                        <label class="mr-1 ">
                                                                                            <p class="mb-0">Uploaded File</p>
                                                                                            <span>{{basename($profile->health_doc)}}</span>
                                                                                        </label>
                                                                                    </div>
                                                                                    
                                                                                    @if($profile->health_doc_status == 1 )
                                                                                    <div class="col-lg-4 text-right pr-0">
                                                                                        <a href="{{url($profile->health_doc)}}" target="_blank" class="btn btn-outline-secondary waves-effect" tooltip='View' data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                         <a href="{{url($profile->health_doc)}}" download class="btn btn-outline-secondary waves-effect ml-1" data-toggle="tooltip" title data-original-title="Download"><i data-feather="download"></i></a>
                                                                                    </div>
                                                                                    @else   
                                                                                    <div class="col-lg-4 text-right pr-0"> 
                                                                                        <a href="{{url($profile->health_doc)}}" target="_blank" class="btn btn-outline-secondary waves-effect" data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                    </div>
                                                                                     @endif
                                                                                   
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                       
                                                                        @if(isset($profile->health_doc_status))
                                                                            <div class="col-lg-5 col-md-4">
                                                                                <div class="form-group font-small-4">Approval Status :</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-7 col-md-8">
                                                                                <div class="form-group">
                                                                                        <p class="font-small-4 badge badge-light-{{APPROVAL_STATUS_COLORS[$profile->health_doc_status]}} badge-pill">{{APPROVAL_STATUS[$profile->health_doc_status]}}</p>
                                                                                </div>
                                                                            </div>
                                                                        
                                                                        @endif
                                                                </div>
                                                                
                                                            </div>

                                                             @if(  $profile->health_doc_status === NULL || $profile->health_doc_status == 2)  
                                                                <div class="col-lg-6">
                                                                @if(  $profile->health_doc_status === NULL || $profile->health_doc_status == 2)
                                                                    <a href="{{url('app-assets/forms/GreenCo_Assessor_Health_Condition_Declaration_Format.doc')}}" class="btn btn-outline-secondary  waves-effect"><i data-feather="download"></i> Download Sample File</a>
                                                                @endif
                                                                    @if( $profile->health_doc_status != 1 && $profile->health_doc_remark != NULL)
                                                                        <div class="mt-1 remark-title">
                                                                            <label>Remarks :</label>
                                                                            <textarea name="" class="form-control remarks_data" disabled='true' rows="3" >{{$profile->health_doc_remark}}</textarea>
                                                                        </div>  
                                                                    @endif
                                                                </div>
                                                            @endif 
                                                            
                                                           
                                                            
                                                        </div>
                                                        
                                                    </div>

                                                    <div class="col-lg-12 form-group">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-4">
                                                                        <div class="form-group font-small-4">5. GST Declaration <span class="text-danger">*</span></label>
                                                                        </div>
                                                                    </div>
                                                                         @if(  $profile->gst_form_status === NULL || $profile->gst_form_status == 2)
                                                                        <div class="col-lg-7 col-md-8">
                                                                            <div class="custom-file">
                                                                                <input type="file" class="custom-file-input"  accept="application/pdf,image/jpg,image/jpeg,image/png" id="gst_doc" name="gst_doc" >
                                                                                <label class="custom-file-label" for="gst_doc">Choose file</label>
                                                                                <span id="gst_doc-error-add" class="error"></span>
                                                                            </div>
                                                                        </div>
                                                                        @endif
                                                                        <input type='hidden' name='gstdoccheck' id='gstdoccheck' value="{{(isset($profile->gst_form) && $profile->gst_form_status != 2) ? '1': '2'}}">
                                                                       
                                                                        @if(isset($profile->gst_form))
                                                                             @if(($profile->gst_form_status === NULL ||  $profile->gst_form_status == 2))
                                                                                <div class="col-lg-5 col-md-4"></div>
                                                                             @endif
                                                                            <div class="col-lg-7 col-md-8">
                                                                                <div class="v-row uploadedfile border-bb mb-1">
                                                                                    <div class="col-lg-8 pl-0 word-wrap-all">
                                                                                        <label class="mr-1 ">
                                                                                            <p class="mb-0">Uploaded File</p>
                                                                                            <span>{{basename($profile->gst_form)}}</span>
                                                                                        </label>
                                                                                    </div>
                                                                                    
                                                                                    @if($profile->gst_form_status == 1 )
                                                                                    <div class="col-lg-4 text-right pr-0">
                                                                                        <a href="{{url($profile->gst_form)}}" target="_blank" class="btn btn-outline-secondary waves-effect" tooltip='View' data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                         <a href="{{url($profile->gst_form)}}" download class="btn btn-outline-secondary waves-effect ml-1" data-toggle="tooltip" title data-original-title="Download"><i data-feather="download"></i></a>
                                                                                    </div>
                                                                                    @else   
                                                                                    <div class="col-lg-4 text-right pr-0"> 
                                                                                        <a href="{{url($profile->gst_form)}}" target="_blank" class="btn btn-outline-secondary waves-effect" data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                    </div>
                                                                                    @endif
                                                                                    <!-- @if(isset($profile->gst_form_status))
                                                                                    <div class="col-lg-10">
                                                                                        <div class="form-group mt-1">
                                                                                            <label> <b>Approval Status : {{APPROVAL_STATUS[$profile->gst_form_status]}}</b> </label>      
                                                                                        </div>
                                                                                    </div>
                                                                                    @endif -->
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                       
                                                                        @if(isset($profile->gst_form_status))
                                                                            <div class="col-lg-5 col-md-4">
                                                                                <div class="form-group font-small-4">Approval Status :</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-7 col-md-8">
                                                                                <div class="form-group">
                                                                                        <p class="font-small-4 badge badge-light-{{APPROVAL_STATUS_COLORS[$profile->gst_form_status]}} badge-pill">{{APPROVAL_STATUS[$profile->gst_form_status]}}</p>
                                                                                </div>
                                                                            </div>
                                                                        
                                                                        @endif
                                                                </div>
                                                                
                                                            </div>

                                                             @if(  $profile->gst_form_status === NULL || $profile->gst_form_status == 2)
                                                                <div class="col-lg-6">
                                                                    @if(  $profile->gst_form_status === NULL || $profile->gst_form_status == 2)
                                                                    <a href="{{url('app-assets/forms/GST_Declaration.docx')}}" class="btn btn-outline-secondary  waves-effect"><i data-feather="download"></i> Download Sample File</a>
                                                                    @endif
                                                                    @if( $profile->gst_form_status != 1 && $profile->gst_form_remark != NULL)
                                                                        <div class="mt-1 remark-title">
                                                                            <label>Remarks :</label>
                                                                            <textarea name="" class="form-control remarks_data" disabled='true' rows="3" >{{$profile->gst_form_remark}}</textarea>
                                                                        </div>  
                                                                    @endif
                                                                </div>
                                                            @endif 
                                                            
                                                           
                                                            
                                                        </div>
                                                        
                                                    </div>

                                                    <div class="col-lg-12 form-group">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-4">
                                                                        <div class="form-group font-small-4">6. PAN Card <span class="text-danger">*</span></label>
                                                                        </div>
                                                                    </div>
                                                                         @if(  $profile->pan_status === NULL || $profile->pan_status == 2)
                                                                        <div class="col-lg-7 col-md-8">
                                                                            <div class="custom-file">
                                                                                <input type="file" class="custom-file-input"   accept="application/pdf,image/jpg,image/jpeg,image/png" id="pancard_doc" name="pancard_doc" >
                                                                                <label class="custom-file-label" for="pancard_doc">Choose file</label>
                                                                                <span id="pancard_doc-error-add" class="error"></span>
                                                                            </div>
                                                                        </div>
                                                                        @endif
                                                                        <input type='hidden' name='pandoccheck' id='pandoccheck' value="{{(isset($profile->pan) && $profile->pan_status != 2) ? '1': '2'}}">

                                                                         @if(isset($profile->pan))
                                                                            @if(($profile->pan_status === NULL ||  $profile->pan_status == 2))
                                                                                <div class="col-lg-5 col-md-4"></div>
                                                                            @endif
                                                                            <div class="col-lg-7 col-md-8">
                                                                                <div class="v-row uploadedfile border-bb mb-1">
                                                                                    <div class="col-lg-8 pl-0 word-wrap-all">
                                                                                        <label class="mr-1 ">
                                                                                            <p class="mb-0">Uploaded File</p>
                                                                                            <span>{{basename($profile->pan)}}</span>
                                                                                        </label>
                                                                                    </div>
                                                                                    
                                                                                    @if($profile->pan_status == 1 )
                                                                                    <div class="col-lg-4 text-right pr-0">
                                                                                        <a href="{{url($profile->pan)}}" target="_blank" class="btn btn-outline-secondary waves-effect" tooltip='View' data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                        <a href="{{url($profile->pan)}}" download class="btn btn-outline-secondary waves-effect ml-1" data-toggle="tooltip" title data-original-title="Download"><i data-feather="download"></i></a>
                                                                                    </div>
                                                                                    @else   
                                                                                    <div class="col-lg-4 text-right pr-0"> 
                                                                                    <a href="{{url($profile->pan)}}" target="_blank" class="btn btn-outline-secondary waves-effect"data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a></div>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                       
                                                                        @if(isset($profile->pan_status))
                                                                            <div class="col-lg-5 col-md-4">
                                                                                <div class="form-group font-small-4">Approval Status :</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-7 col-md-8">
                                                                                <div class="form-group">
                                                                                        <p class="font-small-4 badge badge-light-{{APPROVAL_STATUS_COLORS[$profile->pan_status]}} badge-pill">{{APPROVAL_STATUS[$profile->pan_status]}}</p>
                                                                                </div>
                                                                            </div>
                                                                        
                                                                        @endif
                                                                </div>
                                                                
                                                            </div>

                                                             @if(  $profile->pan_status === NULL || $profile->pan_status == 2)  
                                                                <div class="col-lg-6">
                                                                    <!-- <a href="{{url('app-assets/forms/GST_Declaration.docx')}}" class="btn btn-outline-secondary  waves-effect"><i data-feather="download"></i> Download Sample File</a> -->
                                                                    @if( $profile->pan_status != 1 && $profile->pan_remark != NULL)
                                                                        <div class="mt--1 remark-title">
                                                                            <label>Remarks :</label>
                                                                            <textarea name="" class="form-control remarks_data" disabled='true' rows="3" >{{$profile->pan_remark}}</textarea>
                                                                        </div>  
                                                                    @endif
                                                                </div>
                                                            @endif 
                                                            
                                                           
                                                            
                                                        </div>
                                                        
                                                    </div>

                                                    <div class="col-lg-12 form-group">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-4">
                                                                        <div class="form-group font-small-4">7. Cancelled Cheque <span class="text-danger">*</span></label>
                                                                        </div>
                                                                    </div>
                                                                         @if(  $profile->cancelled_check_status === NULL || $profile->cancelled_check_status == 2)
                                                                        <div class="col-lg-7 col-md-8">
                                                                            <div class="custom-file">
                                                                                <input type="file" class="custom-file-input" accept="application/pdf,image/jpg,image/jpeg,image/png"  id="cancelled_cheque" name="cancelled_cheque" >
                                                                                <label class="custom-file-label" for="cancelled_cheque">Choose file</label>
                                                                                <span id="cancelled_cheque-error-add" class="error"></span>
                                                                            </div>
                                                                        </div>
                                                                        @endif
                                                                        <input type='hidden' name='chequedoccheck' id='chequedoccheck' value="{{(isset($profile->cancelled_check) && $profile->cancelled_check_status != 2) ? '1': '2'}}">

                                                                         @if(isset($profile->cancelled_check))
                                                                            @if(($profile->cancelled_check_status === NULL ||  $profile->cancelled_check_status == 2))
                                                                                <div class="col-lg-5 col-md-4"></div>
                                                                            @endif
                                                                            <div class="col-lg-7 col-md-8">
                                                                                <div class="v-row uploadedfile border-bb mb-1">
                                                                                    <div class="col-lg-8 pl-0 word-wrap-all">
                                                                                        <label class="mr-1 ">
                                                                                            <p class="mb-0">Uploaded File</p>
                                                                                            <span>{{basename($profile->cancelled_check)}}</span>
                                                                                        </label>
                                                                                    </div>
                                                                                    
                                                                                    @if($profile->cancelled_check_status == 1 )
                                                                                    <div class="col-lg-4 text-right pr-0">
                                                                                        <a href="{{url($profile->cancelled_check)}}" target="_blank" class="btn btn-outline-secondary waves-effect" tooltip='View' data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                        <a href="{{url($profile->cancelled_check)}}" download class="btn btn-outline-secondary waves-effect ml-1" data-toggle="tooltip" title data-original-title="Download"><i data-feather="download"></i></a>
                                                                                    </div>
                                                                                    @else   
                                                                                    <div class="col-lg-4 text-right pr-0"> 
                                                                                    <a href="{{url($profile->cancelled_check)}}" target="_blank" class="btn btn-outline-secondary waves-effect"data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a></div>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                       
                                                                        @if(isset($profile->cancelled_check_status))
                                                                            <div class="col-lg-5 col-md-4">
                                                                                <div class="form-group font-small-4">Approval Status :</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-7 col-md-8">
                                                                                <div class="form-group">
                                                                                        <p class="font-small-4 badge badge-light-{{APPROVAL_STATUS_COLORS[$profile->cancelled_check_status]}} badge-pill">{{APPROVAL_STATUS[$profile->cancelled_check_status]}}</p>
                                                                                </div>
                                                                            </div>
                                                                        
                                                                        @endif
                                                                </div>
                                                                
                                                            </div>

                                                             @if(  $profile->cancelled_check_status === NULL || $profile->cancelled_check_status == 2)  
                                                                <div class="col-lg-6">
                                                                    <!-- <a href="{{url('app-assets/forms/GST_Declaration.docx')}}" class="btn btn-outline-secondary  waves-effect"><i data-feather="download"></i> Download Sample File</a> -->
                                                                    @if( $profile->cancelled_check_status != 1 && $profile->cancelled_check_remark != NULL)
                                                                        <div class="mt--1 remark-title">
                                                                            <label>Remarks :</label>
                                                                            <textarea name="" class="form-control remarks_data" disabled='true' rows="3" >{{$profile->cancelled_check_remark}}</textarea>
                                                                        </div>  
                                                                    @endif
                                                                </div>
                                                            @endif 
                                                            
                                                           
                                                            
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                    
                                                    
                                                    
                                                    <div class="col-lg-12 mt-2 m0-auto text-center">
                                                        @if( $profile->profile_updated == 0)
                                                            <!-- <button type="button" class="btn btn-primary mr-1 waves-effect waves-float waves-light page_save_btn" id="page_save" name="page_save">Save</button> -->
                                                            <!-- <button type="button" class="btn btn-primary mr-1 waves-effect waves-float waves-light page_submit_btn" id="page_submit" name="page_submit">Final Submit</button> -->
                                                            @if( isset($profile->health_doc) && isset($profile->biodata)  && isset($profile->gst_form) && isset($profile->vendor_stamp) && isset($profile->ndc_form) && 
                                                            isset($profile->cancelled_check)  && isset($profile->bank_name) && isset($profile->acc_no) && isset($profile->ifsc_code) && isset($profile->branch_name) 
                                                            && isset($profile->category) && isset($profile->state) && isset($profile->city) && isset($profile->pincode) && isset($profile->address_1)  
                                                              && isset($profile->em_contact_name) && isset($profile->em_contact_no) && isset($profile->pan_number) && isset($profile->gst_number)  && isset($profile->em_state) && isset($profile->em_city) && isset($profile->em_pincode)
                                                               && isset($profile->em_address_1)   && isset($profile->pan) && $profile->profile_updated==0)  
                                                               <input type='hidden' name="finalsubmitval" id="finalsubmitval" value='true' >
                                                                <button type="button" class="btn btn-primary mr-1 waves-effect waves-float waves-light page_submit_btn" id="page_submit" name="page_submit">Final Submit</button>
                                                        
                                                            @else
                                                                <input type='hidden' name="finalsubmitval" id="finalsubmitval" value='false' >
                                                                <button type="button" class="btn btn-primary mr-1 waves-effect waves-float waves-light page_save_btn" id="page_save" name="page_save">Save</button>
                                                            @endif
                                                        @elseif($profile->profile_updated == 1)
                                                            <input type='hidden' name="finalsubmitval" id="finalsubmitval" value='true' >   
                                                            <button type="button" class="btn btn-primary mr-1 waves-effect waves-float waves-light page_submit_btn" id="page_submit" name="page_submit"> Save</button>
                                                        @endif

                                                            <button type="button" class="btn btn-outline-secondary mr-1 waves-effect waves-float waves-light cancelpage_btn" id="cancelpage" name="cancelpage">Cancel</button>

                                                    </div>
                                                   
                                                    
                                                </div>
                                            </div>

                                        </div>
                                        </div>

                                        
                                    </div>
                                </div>
                            </form>
                        </section>
                    </div>
                </section>
            <!-- </div> -->
                <!-- Modern Horizontal Wizard -->
            </div>
        </div>
    </div>
</div>
@endsection

@push('PAGE_SCRIPTS')
<script type="text/javascript">
    $("#cancelpage").click(function(){
        location.reload();
        $('html, body').scrollTop(0);
    });

 
 // multiselect specific packages
 $('.industry-category').select2({
     placeholder: "Select Industry Category",
     allowClear: true
    });



var Profile = function () {
    return { //main function to initiate the module
        init: function () {
            jQuery.validator.addMethod("mobileNo", function(value, element) {
               return this.optional(element) || /^[6-9][0-9]{9}$/.test(value);
            }, "Mobile Number is invalid"); 

            jQuery.validator.addMethod("noSpace", function(value, element) { 
              return value == '' || value.trim().length != 0;  
            }, "This field is invalid");

            jQuery.validator.addMethod("doubleSpace", function(value, element) {
               return this.optional(element) || /^\S((?!.*  ).*\S)?$/.test(value);
            }, "This field is invalid");  

            jQuery.validator.addMethod("lettersonly", function(value, element) 
            {
                return this.optional(element) || /^[A-Z," "]+$/i.test(value);
            }, "Letters and spaces only please");

            jQuery.validator.addMethod("alphanumeric", function(value, element) 
            {
                return this.optional(element) || /^[A-Z0-9]+$/i.test(value);
            }, "Letters and spaces only please");
            


            $('.select2').on('change', function(e) {
                $('.select2').removeClass('error');
                $('#ind_category-error-add').css('display','none');
                if ($('.select2').val() == '') {
                    if($('#finalsubmitval').val() =='true')
                    {
                        $('.select2').addClass('error');
                        $('#ind_category-error-add').css('display','block');
                    }
                    // Stop submiting
                    e.preventDefault();
                    return false;
                }
            });

       


            $('#profileUpdate').validate({
                ignore: [],
                rules: {
                    'name': {
                        noSpace: true,
                        doubleSpace:true,
                        maxlength: 50,
                        minlength:3
                    },
                    'email': {
                        email: true,
                    },
                    'ind_category': {
                            required: true,
                            },

                    'state': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                    },
                    'mobile':{
                        noSpace: true,
                        digits:true,
                        minlength:10,
                        mobileNo: true
                    },
                    'alt_mobile':{
                       
                        noSpace: true,
                        digits:true,
                        minlength:10,
                        mobileNo: true
                    },

                    'address_1': {
                        // required:true,
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        doubleSpace:true,
                        noSpace: true,
                        minlength:3
                    },

                    'address_2': {
                        doubleSpace:true,
                        noSpace: true,
                        minlength:3
                    },

                    'city': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        doubleSpace:true,
                        noSpace: true,
                        minlength:3,
                        lettersonly:true,
                    },

                    'pincode': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        
                        digits:true,
                        minlength:6
                    },

                    'pancardno': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        minlength:10,
                        maxlength:10,
                        alphanumeric:true,
                    },

                    'gstnumber': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        minlength:15,
                        maxlength:15,
                        alphanumeric:true,
                    },
                    
                    'em_contact_name': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        doubleSpace:true,
                        noSpace: true,
                        maxlength: 50,
                        minlength:3,
                        lettersonly:true,
                    },
                    'em_contact_no': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        noSpace: true,
                        digits:true,
                        minlength:10,
                        mobileNo: true
                    },
                    // 'em_contact_address': {
                    'em_address_1':{
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        doubleSpace:true,
                        noSpace: true,
                        minlength:3
                    },

                    'em_address_2': {
                        doubleSpace:true,
                        noSpace: true,
                        minlength:3
                    },

                    'em_city': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        doubleSpace:true,
                        noSpace: true,
                        minlength:3,
                        lettersonly:true,
                    },

                    'em_pincode': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        
                        digits:true,
                        minlength:6
                    },

                    'em_state': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                    },

                    'bank_name': {
                        // required: '#adding:checked',
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        doubleSpace:true,
                        noSpace: true,
                        maxlength: 50,
                        minlength:3,
                        lettersonly:true,
                    },
                    'branch_name': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        doubleSpace:true,
                        noSpace: true,
                        maxlength: 50,
                        minlength:3,
                        lettersonly:true,

                    },
                    'ifsc_code': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        doubleSpace:true,
                        noSpace: true,
                        maxlength: 50,
                        minlength:3,
                        alphanumeric:true,
                    },
                    'account_number': {
                        required: function(){
                            return $("#finalsubmitval").val() == "true";
                        },
                        doubleSpace:true,
                        noSpace: true,
                        maxlength: 50,
                        minlength:10,
                        alphanumeric:true,
                    },

                    'biodata': {
                        required: function(){
                            return ($("#biodatadoccheck").val() == "2" && $("#finalsubmitval").val() == "true");
                        },
                        extension : "png|jpeg|jpg|pdf",
                        accept:"application/pdf,image/jpg,image/jpeg,image/png"
                        
                    },
                    
                    'cancelled_cheque': {
                        required: function(){
                            return ($("#chequedoccheck").val() == "2" && $("#finalsubmitval").val() == "true");
                        },
                        extension : "png|jpeg|jpg|pdf",
                        accept:"application/pdf,image/jpg,image/jpeg,image/png"
                        
                    },
                    'gst_doc': {
                        required: function(){
                            return ($("#gstdoccheck").val() == "2" && $("#finalsubmitval").val() == "true");
                        },
                        extension : "png|jpeg|jpg|pdf",
                        accept:"application/pdf,image/jpg,image/jpeg,image/png"
                        
                    },
                    'vendorstamp_doc': {
                        required: function(){
                            return ($("#vrfoccheck").val() == "2" && $("#finalsubmitval").val() == "true");
                        },
                        extension : "png|jpeg|jpg|pdf",
                        accept:"application/pdf,image/jpg,image/jpeg,image/png"
                        
                    },
                    'ndc_form':{
                        required: function(){
                            return ($("#ndcdoccheck").val() == "2" && $("#finalsubmitval").val() == "true");
                        },
                        extension : "png|jpeg|jpg|pdf",
                        accept:"application/pdf,image/jpg,image/jpeg,image/png"
                        
                    },
                    'health_doc':{
                        required: function(){
                            return ($("#healthdoccheck").val() == "2" && $("#finalsubmitval").val() == "true");
                        },
                        extension : "png|jpeg|jpg|pdf",
                        accept:"application/pdf,image/jpg,image/jpeg,image/png"
                        
                    },
                    'pancard_doc':{
                        required: function(){
                            return ($("#pandoccheck").val() == "2" && $("#finalsubmitval").val() == "true");
                        },
                        extension : "png|jpeg|jpg|pdf",
                        accept:"application/pdf,image/jpg,image/jpeg,image/png"
                        
                    }
                },
                messages: {
                    name: {
                        required: "The Name field is required",
                        noSpace: "Please Enter a Valid Name",
                        doubleSpace: "Please Enter a Valid Name"
                        
                    },
                    email: {
                        required: "The Email Address field is required",
                        email : "Please Enter a Valid Email Address"
                    },
                    ind_category:{
                        required: "The Category field is required",
                    },
                    state:{
                        required: "The State field is required",
                    },
                    mobile: {
                        required: "The Mobile Number field is required",
                    }, 

                    alt_mobile: {  
                        mobileNo: "Please Enter a Valid Alternate Mobile Number"
                    },

                    address_1: {
                        required: "The Address Line 1 field is required",
                        noSpace: "Please Enter a Valid Address Line 1",
                        doubleSpace: "Please Enter a Valid Address Line 1"
                    },

                    address_2: {
                        noSpace: "Please Enter a Valid Address Line 2",
                        doubleSpace: "Please Enter a Valid Address Line 2"
                    },

                    city: {
                        required: "The City field is required",
                        noSpace: "Please Enter a Valid City",
                        doubleSpace: "Please Enter a Valid City",
                        lettersonly: "Please Enter a Valid City"
                    },

                    pincode: {
                        required: "The Pincode field is required",
                    }, 

                    pancardno:{
                        required: "The Pancard Number field is required",
                        alphanumeric: "Please Enter a Valid Pancard Number"
                    },
                    gstnumber:{
                        required: "The GST Number field is required",
                        alphanumeric: "Please Enter a Valid GST Number"
                    },


                    em_contact_name: {
                        required: "The Contact Name field is required",
                        noSpace: "Please Enter a Valid Contact Name",
                        doubleSpace: "Please Enter a Valid Contact Name",
                        lettersonly: "Please Enter a Valid Contact Name"
                    },
                    em_contact_no: {
                        required: "The Mobile Number field is required",
                        mobileNo: "Please Enter a Valid Mobile Number"
                    },
                    em_address_1: {
                        required: "The  Address Line 1 field is required",
                        noSpace: "Please Enter a Valid Address Line 1",
                        doubleSpace: "Please Enter a Valid Address Line 1"
                    },

                    em_address_2: {
                        noSpace: "Please Enter a Valid Address Line 2",
                        doubleSpace: "Please Enter a Valid Address Line 2"
                    },

                    em_city: {
                        required: "The City field is required",
                        noSpace: "Please Enter a Valid City",
                        doubleSpace: "Please Enter a Valid City",
                        lettersonly: "Please Enter a Valid City"
                    },

                    em_pincode: {
                        required: "The Pincode field is required",
                    }, 

                    em_state: {
                        required: "The State field is required",
                    }, 

                    bank_name: {
                        required: "The Bank Name field is required",
                        noSpace: "Please Enter a Valid Bank Name",
                        doubleSpace: "Please Enter a Valid Bank Name",
                        lettersonly: "Please Enter a Valid Bank Name"
                    },
                    branch_name: {
                        required: "The Branch Name field is required",
                        noSpace: "Please Enter a Valid Branch Name",
                        doubleSpace: "Please Enter a Valid Branch Name",
                        lettersonly: "Please Enter a Valid Branch Name"
                    },
                    ifsc_code: {
                        required: "The IFSC Code  field is required",
                        noSpace: "Please Enter a Valid IFSC Code",
                        doubleSpace: "Please Enter a Valid IFSC Code",
                        alphanumeric: "Please Enter a Valid IFSC Code"
                    },
                    account_number: {
                        required: "The Account Number field is required",
                        noSpace: "Please Enter a Valid Account Number",
                        doubleSpace: "Please Enter a Valid Account Number",
                        alphanumeric: "Please Enter a Valid Account Number"
                    },
                    biodata: {
                        required: "The Bio Data is required",
                        extension: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only",
                        accept: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only"
                        },  
                    cancelled_cheque: {
                        required: "The Cancelled Cheque is required",
                        extension: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only",
                        accept: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only"
                        },  
                    gst_doc: {
                        required: "The GST Declaration is required",
                        extension: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only",
                        accept: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only"
                        },  
                    vendorstamp_doc: {
                        required: "The Vendor Registration Form is required",
                        extension: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only",
                        accept: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only"
                        },  
                    ndc_form: {
                        required: "The Non-Disclosure Aggrement is required",
                        extension: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only",
                        accept: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only"
                        },  
                    health_doc: {
                        required: "The Health Declaration is required",
                        extension: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only",
                        accept: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only"
                        },  
                    pancard_doc: {
                        required: "The PAN Card is required",
                        extension: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only",
                        accept: "Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only"
                        },  
                },
                focusInvalid: false,
                invalidHandler: function() {
                    $('html, body').scrollTop(0);
                    $(this).find(":input.error:first").focus();
                }
                
            });
            
            // save button code

            $("#page_save").click(function(){ 
                $('#finalsubmitval').val('false');
                $("#ind_category").removeAttr("required");
                if ($('#profileUpdate').valid() == true) 
                {
                    $("#page_save").prop('disabled',true);
                    $("#page_submit").prop('disabled',true);
                    $("#cancelpage").prop('disabled',true);
                    
                    $("#page_save").html("Loading....");
                    var form_data = new FormData(document.getElementById("profileUpdate"));
                    
                    $.ajax({
                        type: "POST",
                        url: $('form#profileUpdate').attr('action'),
                        data: form_data,
                        processData: false,
                        contentType: false,
                        success: function(response) {
                            
                            if(response.status == 'errors') {
                                $("#page_save").prop('disabled',false);
                                $("#page_submit").prop('disabled',false);
                                $("#page_save").html("Save");
                                $("#cancelpage").prop('disabled',false);
                                $.each(response.errors, function (key, error) {
                                    $('form#profileUpdate').find("#" + key + "-icon-error").removeClass('borderRightError').addClass('borderRightError');
                                    $('form#profileUpdate').find("#" + key).removeClass('error').addClass('error');
                                    $('form#profileUpdate').find("#" + key + "-error-add").css('display','block');
                                    $('form#profileUpdate').find("#" + key + "-error-add").text(error[0]);
                                });
                                
                            }
                            if(response.status == 'success') {
                                    setTimeout(function(){
                                        location.reload();
                                        $('html, body').scrollTop(0);
                                    }, 3000);
                                    JsUtility.showToastr('success', 'Profile', response.message);
                                }
                            

                            if(response.status == 'error') {
                                $("#page_save").prop('disabled',false);
                                $("#page_submit").prop('disabled',false);
                                $("#page_save").html("Save");
                                $("#cancelpage").prop('disabled',false);
                                JsUtility.showToastr('error', 'Profile', response.message);
                            }
                            if(response.status =='errorlogout')
                            {
                                setTimeout(function(){
                                    window.location.href = "{{route('assessor.login')}}";
                                    }, 3000);
                                JsUtility.showToastr('error', 'Assessor', response.message);
                            }
                        },
                        error: function(response) {
                            $("#page_save").prop('disabled',false);
                            $("#page_submit").prop('disabled',false);
                            $("#page_save").html("Save");
                            $("#cancelpage").prop('disabled',false);
                            JsUtility.showToastr('error', 'Profile', response.message);
                        }
                    });   
                }
                
               
            });

            // final submit code
          
            $("#page_submit").click(function(){ 
                $('#finalsubmitval').val('true');
                $("#ind_category").attr("required");
                if ($('.select2').val() == '') {
                    if($('#finalsubmitval').val() =='true' && $('#profile_updated_status').val() !='1')
                    {
                        $('.select2').addClass('error');
                        $('#ind_category-error-add').css('display','block');
                        $('html, body').scrollTop(0);
                        $(this).find(":input.error:first").focus();
                        e.preventDefault();
                        return false;
                    }
                }
                    // Stop submiting
                   
                if ($('#profileUpdate').valid() == true) 
                {
                    if($('#profile_updated_status').val() =='1')
                    {
                        var text = "Are you sure, you want to Save the Data ?";
                        var buttontext = "Save";
                    } 
                    else
                    {
                        var text = "Are you sure, Once Submitted Data Cannot be reverted again ?" ;
                        var buttontext = "Submit";
                    }
                        
                    Swal.fire({
                            title: text,
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonText: buttontext,
                            showClass: {
                                popup: 'animate__animated animate__fadeIn'
                            },
                            customClass: {
                                confirmButton: 'btn btn-primary',
                                cancelButton: 'btn btn-outline-danger ml-1'
                            },
                            buttonsStyling: false
                            }).then(function(result) {
                            if (result.value) {
                                var form_data = new FormData(document.getElementById("profileUpdate"));
                                $("#page_save").attr('disabled',true);
                                $("#page_submit").attr('disabled',true);
                                $(".back").css('pointer','none');
                                $("#page_submit").html("Loading....");
                                $("#cancelpage").prop('disabled',true);

                                $.ajax({
                                    type: "POST",
                                    url: "{{ route('assessor.submitprofile') }}",
                                    data: form_data,
                                    processData: false,
                                    contentType: false,
                                    success: function(response) {
                                        if(response.status == 'errors') {
                                            $.each(response.errors, function (key, error) {
                                                $("#page_save").attr('disabled',false);
                                                $("#page_submit").attr('disabled',false);
                                                $("#page_submit").html("Final Submit");
                                                $("#cancelpage").prop('disabled',false);
                                                $('form#profileUpdate').find("#" + key + "-icon-error").removeClass('borderRightError').addClass('borderRightError');
                                                $('form#profileUpdate').find("#" + key).removeClass('error').addClass('error');
                                                $('form#profileUpdate').find("#" + key + "-error-add").css('display','block');
                                                $('form#profileUpdate').find("#" + key + "-error-add").text(error[0]);
                                                $('html, body').scrollTop(0);
                                                $(this).find(":input.error:first").focus();
                                            });
                                        }

                                        if(response.status == 'success') {
                                        
                                            JsUtility.showToastr('success', 'Assessor', response.message);
                                            setTimeout(function(){
                                                    location.reload();
                                                    $('html, body').scrollTop(0);
                                                }, 3000);
                                            
                                            
                                        }

                                        if(response.status == 'error') {
                                            $("#page_save").attr('disabled',false);
                                            $("#page_submit").attr('disabled',false);
                                            $("#page_submit").html("Final Submit");
                                            $("#cancelpage").prop('disabled',false);
                                            JsUtility.showToastr('error', 'Assessor', response.message);
                                        }
                                        if(response.status =='errorlogout')
                                        {
                                            setTimeout(function(){
                                                window.location.href = "{{route('assessor.login')}}";
                                                }, 3000);
                                            JsUtility.showToastr('error', 'Assessor', response.message);
                                        }
                                    },
                                    error: function() {
                                        $("#page_save").attr('disabled',false);
                                        $("#page_submit").attr('disabled',false);
                                        $("#page_submit").html("Final Submit");
                                        $("#cancelpage").prop('disabled',false);
                                        JsUtility.showToastr('error', 'Assessor', response.message);
                                    }
                                }); 
                            }
                        });
                }
                

            });

           
        }
    }
}();

jQuery(document).ready(function() {
    Profile.init();
    
    if($('#profile_updated_status').val() =='1')
    {
        $('#finalsubmitval').val('true');
        $("#bankdetail *").prop('disabled',true);
        $("#basicdetail *").prop('disabled',true);
        $("#bankdetail :input").addClass('remarks_data');
        $("#basicdetail :input").addClass('remarks_data');
        $(".select2-selection--multiple").addClass('samplestyle');
        $('.select2-selection--multiple').addClass('removedropdownstyle');


    }

    $('input[type=text],select').blur(function(){
        $("#profileUpdate").validate().element(this);
    });

    $('input[type=text]').on('keyup',function(event){
        event.currentTarget.value = this.value.toUpperCase();
        $("#profileUpdate").validate().element(this);
    });

    $('input,select').on('keyup',function(event){
        $("#profileUpdate").validate().element(this);
    });


});

</script>
@endpush
