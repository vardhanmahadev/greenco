@extends(ASSESSOR_THEME_NAME.'.layouts.app')

@section('content')
    <div class="app-content content content-ps-stl">
        <div class="">
            <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper">
                <div class="content-body">
                    <section class=" p-relative mt--1">
                        @include('assessor.components.application_nave')
                        <div class="col-12">
                            <section id="advanced-search-datatable" class="assessment_section_div">
                                <div class="row">
                                    <div class="col-12">
                                        <form method="POST" action="{{ $route }}" class="form-validate">
                                            @csrf
                                            <div class="card">
                                                <div class=" sub-tabpages">
                                                    <div class="form-div">
                                                        <div class="card-body">
                                                            @isset($master_inputs)
                                                                @foreach ($master_inputs as $input)
                                                                    <div @if ($input->visible == 0) hidden @endif
                                                                        class="row  border-bb  pb-1 pt-1">
                                                                        <div class="col-lg-10">
                                                                            <div class="row ">
                                                                                <div class="col-lg-6 col-md-8 tab-assets-list">
                                                                                    <div class="">
                                                                                        <label> {{ $input->param_name }} :
                                                                                        </label>
                                                                                    </div>
                                                                                    <p class="p-0">{{ $input->description }}</p>
                                                                                </div>
                                                                                <div class="col-lg-6 col-md-4">
                                                                                    <div class="v-row uploadedfile">
                                                                                        <div class="col-lg-10 pl-0">
                                                                                            <label>value</label>
                                                                                            @if ($input->field_type=="i")
                                                                                                    @if ($input->param_field_name == 'T_5')
                                                                                                    <div class="input-group" >
                                                                                                        <input type="number" max="2" name="score[{{ $input->param_field_name }}][value]"
                                                                                                            maxlength="1" max="6"
                                                                                                            autocomplete="off"
                                                                                                            value="{{ old('score['. $input->param_field_name .'][value]') }}"
                                                                                                            class="form-control">
                                                                                                            @switch($application->category_id)
                                                                                                            @case(1)
                                                                                                                @if ($input->small_note!=null)
                                                                                                                    <span class="input-group-text" id="basic-addon2">{{$input->small_note}}</span>
                                                                                                                @endif
                                                                                                            @break
                                                                                                            @case(2)
                                                                                                                @if ($input->medium_note!=null)
                                                                                                                    <span class="input-group-text" id="basic-addon2">{{$input->medium_note}}</span>
                                                                                                                @endif
                                                                                                            @break
                                                                                                            @case(3)
                                                                                                                @if ($input->large_note!=null)
                                                                                                                    <span class="input-group-text" id="basic-addon2">{{$input->large_note}}</span>
                                                                                                                @endif
                                                                                                            @break
                                                                                                        @endswitch
                                                                                                    </div>
                                                                                                    @else
                                                                                                    <div class="input-group">
                                                                                                        <input type="number"
                                                                                                            name="score[{{ $input->param_field_name }}][value]"
                                                                                                            autocomplete="off"
                                                                                                            step='0.01'
                                                                                                            max="100000"
                                                                                                            value="{{ old($input->param_field_name) }}"
                                                                                                            class="form-control">
                                                                                                            @switch($application->category_id)
                                                                                                                @case(1)
                                                                                                                    @if ($input->small_note!=null)
                                                                                                                        <span class="input-group-text" id="basic-addon2">{{$input->small_note}}</span>
                                                                                                                    @endif
                                                                                                                @break
                                                                                                                @case(2)
                                                                                                                    @if ($input->medium_note!=null)
                                                                                                                        <span class="input-group-text" id="basic-addon2">{{$input->medium_note}}</span>
                                                                                                                    @endif
                                                                                                                @break
                                                                                                                @case(3)
                                                                                                                    @if ($input->large_note!=null)
                                                                                                                        <span class="input-group-text" id="basic-addon2">{{$input->large_note}}</span>
                                                                                                                    @endif
                                                                                                                @break
                                                                                                            @endswitch

                                                                                                    </div>
                                                                                                    @endif
                                                                                            @elseif($input->field_type=="r")
                                                                                                @switch($application->category_id)
                                                                                                    @case(1)
                                                                                                    @foreach ($input->small_config as $sl=> $check)
                                                                                                        <div class="form-check">
                                                                                                            <input class="form-check-input" type="radio"  @if (old($input->param_field_name,$input->assessor_input) == "$sl") checked @endif  value="{{$sl}}" name="score[{{$input->param_field_name}}][value]" id="checkBox{{$input->param_field_name,}}-{{$sl}}">
                                                                                                            <label class="form-check-label" for="checkBox{{$input->param_field_name,}}-{{$sl}}">
                                                                                                                {{$check->label}}
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    @endforeach
                                                                                                        @break
                                                                                                    @case(2)
                                                                                                    @foreach ($input->medium_config as $sl=> $check)
                                                                                                        <div class="form-check">
                                                                                                            <input class="form-check-input" type="radio" max="100000"  @if (old($input->param_field_name,$input->assessor_input) == "$sl") checked @endif  value="{{$sl}}" name="score[{{$input->param_field_name}}][value]" id="checkBox{{$input->param_field_name,}}-{{$sl}}">
                                                                                                            <label class="form-check-label" for="checkBox{{$input->param_field_name,}}-{{$sl}}">
                                                                                                                {{$check->label}}
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    @endforeach
                                                                                                        @break
                                                                                                    @case(3)
                                                                                                    @foreach ($input->small_config as $sl=> $check)
                                                                                                        <div class="form-check">
                                                                                                            <input class="form-check-input" type="radio"  @if (old($input->param_field_name,$input->assessor_input) == "$sl") checked @endif  value="{{$sl}}" name="score[{{$input->param_field_name}}][value]" id="checkBox{{$input->param_field_name,}}-{{$sl}}">
                                                                                                            <label class="form-check-label" for="checkBox{{$input->param_field_name,}}-{{$sl}}">
                                                                                                                {{$check->label}}
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    @endforeach
                                                                                                    @break
                                                                                                @endswitch
                                                                                                @elseif($input->field_type=="c")
                                                                                                @php
                                                                                                    $prevues_data=array();
                                                                                                    if (old($input->param_field_name)) {
                                                                                                        $prevues_data=old($input->param_field_name);
                                                                                                    }
                                                                                                @endphp
                                                                                                @switch($application->category_id)
                                                                                                    @case(1)
                                                                                                    @foreach ($input->small_config as $sl=> $check)
                                                                                                    <div class="form-check">
                                                                                                        <input class="form-check-input" type="checkbox"  @if (in_array($sl,$prevues_data)) checked @endif  value="{{$sl}}" name="score[{{$input->param_field_name}}][value][]" id="checkBox{{$input->param_field_name,}}-{{$sl}}">
                                                                                                        <label class="form-check-label" for="checkBox{{$input->param_field_name,}}-{{$sl}}">
                                                                                                            {{$check->label}}
                                                                                                        </label>
                                                                                                    </div>
                                                                                                    @endforeach
                                                                                                        @break
                                                                                                    @case(2)
                                                                                                    @foreach ($input->medium_config as $sl=> $check)
                                                                                                        <div class="form-check">
                                                                                                            <input class="form-check-input" type="checkbox"  @if (in_array($sl,old($input->param_field_name))) checked @endif  value="{{$sl}}" name="score[{{$input->param_field_name}}][value][]" id="checkBox{{$input->param_field_name,}}-{{$sl}}">
                                                                                                            <label class="form-check-label" for="checkBox{{$input->param_field_name,}}-{{$sl}}">
                                                                                                                {{$check->label}}
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    @endforeach
                                                                                                        @break
                                                                                                    @case(3)
                                                                                                    @foreach ($input->small_config as $sl=> $check)
                                                                                                        <div class="form-check">
                                                                                                            <input class="form-check-input" type="checkbox"  @if (old($input->param_field_name)) checked @endif  value="{{$sl}}" name="score[{{$input->param_field_name}}][value][]" id="checkBox{{$input->param_field_name,}}-{{$sl}}">
                                                                                                            <label class="form-check-label" for="checkBox{{$input->param_field_name,}}-{{$sl}}">
                                                                                                                {{$check->label}}
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    @endforeach
                                                                                                    @break
                                                                                                @endswitch
                                                                                            @endif

                                                                                        </div>


                                                                                       {{--  <input type="hidden"
                                                                                            name="score[{{ $input->param_field_name }}][id]"
                                                                                            value="{{ $input->id }}">

                                                                                        <input type="hidden"
                                                                                            name="score[{{ $input->param_field_name }}][param_name]"
                                                                                            value="{{ $input->param_name }}">
                                                                                        <input type="hidden"
                                                                                            name="score[{{ $input->param_field_name }}][description]"
                                                                                            value="{{ $input->description }}"> --}}
                                                                                        <input type="hidden"
                                                                                            name="score[{{ $input->param_field_name }}][visible]"
                                                                                            value="{{ $input->visible }}">
                                                                                    </div>
                                                                                </div>
                                                                                @error('score[' . $input->param_field_name .
                                                                                    '][value]')
                                                                                    <div class="col-md-12">
                                                                                        <span class="text-danger">
                                                                                            {{ $message }} </span>
                                                                                    </div>
                                                                                @enderror

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            @endisset

                                                            @isset($exist_inputs)
                                                            @if(@$remark!=null)
                                                                <div class="row mt-2" >
                                                                    <div class="col " >
                                                                        <div>
                                                                            <label>Remarks :</label>
                                                                        </div>
                                                                        <div class="v-row justify-content-between mb-50">

                                                                            <textarea name="remark" disabled class="form-control" value=""
                                                                                rows="1">{{ @$remark }}</textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                                @foreach ($exist_inputs as $input)
                                                                    <div @if ($input->parameter_visible == 0) hidden @endif
                                                                        class="row  border-bb  pb-1 pt-1">
                                                                        <div class="col-lg-10">
                                                                            <div class="row ">
                                                                                <div class="col-lg-4 col-md-8 tab-assets-list">
                                                                                    <div class="">
                                                                                        <label> {{ $input->parameter_title }} :
                                                                                        </label>
                                                                                    </div>
                                                                                    <p class="p-a0">
                                                                                        {{ $input->parameter_description }}</p>
                                                                                </div>
                                                                                <div class="col-lg-8 col-md-4">
                                                                                    <div class="row" >
                                                                                        <div class="col-md-8" >
                                                                                            <div class="v-row uploadedfile">
                                                                                                <div class="col-lg-10 pl-0">
                                                                                                    <label>Value</label>
                                                                                                      @if ($input->field_type=="i")
                                                                                                        @if ($input->parameter_field_name == 'T_5')
                                                                                                                <input type="number"
                                                                                                                maxlength="1" max="6"
                                                                                                                name="score[{{ $input->parameter_field_name }}][value]" disabled
                                                                                                                value="{{ old('score[' . $input->parameter_field_name . '][value]', $input->score_inpute) }}"
                                                                                                                class="form-control">
                                                                                                        @else
                                                                                                        <div class="input-group" >
                                                                                                            <input type="text"
                                                                                                                name="score[{{ $input->parameter_field_name }}][value]" disabled
                                                                                                                value="{{ old('score[' . $input->parameter_field_name . '][value]', $input->score_inpute) }}"
                                                                                                                class="form-control">
                                                                                                                @if ($input->unit!=null)
                                                                                                                    <span class="input-group-text" id="basic-addon2">{{$input->unit}}</span>
                                                                                                                @endif
                                                                                                        </div>
                                                                                                        @endif
                                                                                                        @elseif($input->field_type=="r")
                                                                                                        @foreach ($input->score_config as $sl=> $check)
                                                                                                            <div class="form-check">
                                                                                                                <input class="form-check-input" disabled  type="radio"  @if (old($input->parameter_field_name,$input->score_inpute) == "$sl") checked @endif  value="{{$sl}}" name="score[{{$input->parameter_field_name}}][value]" id="radioBox{{$input->parameter_field_name}}-{{$sl}}">
                                                                                                                <label class="form-check-label" for="radioBox{{$input->parameter_field_name,}}-{{$sl}}">
                                                                                                                    {{$check->label}}
                                                                                                                </label>
                                                                                                            </div>
                                                                                                        @endforeach

                                                                                                        @elseif($input->field_type=="c")
                                                                                                        @php
                                                                                                            $previous_data=json_decode($input->score_inpute);
                                                                                                        @endphp

                                                                                                        @foreach ($input->score_config as $sl=> $check)
                                                                                                            <div class="form-check">
                                                                                                                <input class="form-check-input" disabled  type="checkbox"  @if (in_array($sl,old($input->parameter_field_name,$previous_data))) checked @endif  value="{{$sl}}" name="score[{{$input->parameter_field_name}}][value][]" id="checkBox{{$input->parameter_field_name}}-{{$sl}}">
                                                                                                                <label class="form-check-label" for="checkBox{{$input->parameter_field_name,}}-{{$sl}}">
                                                                                                                    {{$check->label}}
                                                                                                                </label>
                                                                                                            </div>
                                                                                                            @endforeach
                                                                                                        @endif
                                                                                                    </div>

                                                                                             </div>
                                                                                        </div>
                                                                                        <div class="col-md-4" >
                                                                                            <label>Score</label>
                                                                                            <div class="v-row uploadedfile">
                                                                                            <input type="text" value="{{$input->assessor_score}}" disabled class="form-control" >
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>


                                                        </div>
                                                        @error('score[' . $input->param_field_name . '][value]')
                                                            <div class="col-md-12">
                                                                <span class="text-danger"> {{ $message }} </span>
                                                            </div>
                                                        @enderror

                                                    </div>
                                                </div>
                                                <div class="col-md-2 remark-title">

                                                </div>
                                        </div>
                                        @endforeach
                                    @endisset


                                        @if (!isset($exist_inputs))
                                            <div class="m0-auto text-center">
                                                <button class="btn btn-primary mt-1 text-center">Submit</button>
                                            </div>
                                        @else
                                            <div class="row mt-1 justify-content-end" >
                                                <div class="col-md-4" >
                                                    <h5>Total score : <span>{{$exist_inputs->sum('assessor_score')}}</span></h5>
                                                </div>
                                            </div>
                                        @endif
                                </div>
                        </div>

                </div>
            </div>
            </form>
        </div>
    </div>
    </section>
    </div>
    </section>
    </div>
    </div>
    </div>
    </div>
@endsection
