@extends(ASSESSOR_THEME_NAME.'.layouts.app')

@section('content')
<div class="app-content content content-ps-stl">
    <div class="">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <section class="">

                    <div class="col-12">
                        <section id="advanced-search-datatable ">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card table-filters-design">
                                        <div class="d-flex v-row title-strip">
                                            <div class="col-6 pl-0">Task List</div>
                                            <div class="col-6 pr-0">
                                                <div class="filter-tab-btn card-option ">
                                                    <span class="minimize-card btn btn-primary "><i data-feather="filter"></i> Filters</span>
                                                    <span style="display:none"><i data-feather="filter"></i> Filters</span></li>
                                                </div>
                                            </div>
                                        </div>
                                        <!--Search Form -->
                                        <div class="card-body " style="display:none;">
                                            <div method="POST">
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <label>Application Date</label>
                                                        <div class="form-group mb-0">
                                                            <input type="text" id="filterDisplayDate"
                                                                class="form-control dt-date flatpickr-range dt-input"
                                                                data-column="5" placeholder="StartDate to EndDate"
                                                                data-column-index="4" name="dt_date"/>
                                                            <input type="hidden" id="filterStartDate"
                                                                class="form-control dt-date start_date dt-input"
                                                                data-column="5" data-column-index="4"
                                                                name="value_from_start_date" />
                                                            <input type="hidden" id="filterEndDate"
                                                                class="form-control dt-date end_date dt-input"
                                                                name="value_from_end_date" data-column="5"
                                                                data-column-index="4" />
                                                        </div>
                                                    </div>

                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label>Application Number</label>
                                                            <input type="text" id="filterApplicationNumber"
                                                                class="form-control dt-input dt-full-name"
                                                                data-column="2" placeholder="Application Number " />
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label>CII Reference</label>
                                                            <input type="text" id="filterCiiRefNumber"
                                                                class="form-control dt-input dt-full-name"
                                                                data-column="2" placeholder="Application Number " />
                                                        </div>
                                                    </div>

                                                    <div class="col-3 float-right">
                                                        <div class="form-group">
                                                            <button type="submit" id="filterButton"
                                                                class="btn btn-primary m0-auto  ">Submit</button>
                                                            <button type="submit" id="filterResetButton"
                                                                class="btn btn-danger m0-auto  ">Reset</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                        <div class="basic-datatable datatable-list">
                                            <table id="applicationTable" class="dt-vendorlist table table-bordered table-responsive">
                                                <thead>
                                                    <th>S.No</th>
                                                    <th>Application No </th>
                                                    <th>CII Reference Number </th>
                                                    <th>Date</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

<div class="modal float-labeldispaly fade" id="view-vendor" tabindex="-1" role="dialog" aria-labelledby="editWarehouseTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <form class="add-new-user modal-content pt-0">
            <div class="modal-header mb-1">
                <h5 class="modal-title">Assign to  Assessor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
            </div>
            <div class="modal-body flex-grow-1">
                <div class="form-group">
                    <label>Select Assessor</label>
                    <select class="form-control select2">
                        <option>Select</option>
                        <option>Assessor 1 </option>
                        <option>Assessor 2</option>
                        <option>Assessor 3 </option>
                        <option>Assessor 4</option>
                    </select>
                </div>
                <div class="col-12 text-center">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary m0-auto  ">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal float-labeldispaly fade" id="Approve-modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <form class="add-new-user modal-content pt-0">
            <div class="modal-header mb-1">
                <h5 class="modal-title">Approve/ Dis-Approve</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
            </div>
            <div class="modal-body flex-grow-1">
                <div class="form-group">
                    <label>Select Region</label>
                    <select class="form-control select2">
                    <option>Select Region</option>
                        <option>National</option>
                        <option>Regional </option>
                        <option>State</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Category</label>
                    <select class="form-control select2">
                    <option>Select Category</option>
                        <option>Small</option>
                        <option>Medium </option>
                        <option>Large</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Remarks</label>
                   <textarea class="form-control"></textarea>
                </div>
                <div class="col-12 text-center">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary m0-auto  ">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection


@push('PAGE_SCRIPTS')

<script>

    var Application = function() {
        return {
            init: function() {
                let dt_ajax_table = $('#applicationTable');
                let dataTable = null;
                let rows_selected=[];

                if (dt_ajax_table.length) {

                    dataTable = dt_ajax_table.DataTable({
                        serverSide: true,
                            processing: true,
            ajax: {
                    url: "{{ route('assessor.application.data') }}",
                    data: function (d) {
                        d.startDate = $('#filterStartDate').val();
                        d.endDate = $('#filterEndDate').val();
                        d.applicationNumber = $('#filterApplicationNumber').val();
                        d.cii_ref_no = $('#filterCiiRefNumber').val();
                        d.vendorName = $('#filterVanderName').val();
                    }
                },
            columns: [
                {
                    data: 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'application_no',
                    name: 'application_no'
                },
                {
                    data: 'cii_ref_no',
                    name: 'cii_ref_no'
                },
                {
                    data: 'application_date',
                    name: 'application_date'
                },
                {
                    data: 'status',
                    name:'status'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                }
        ],
        displayLength: 10,
            scrollY: "350px",
            scrollX: true,
            scrollCollapse: true,
            fixedColumns: {
                leftColumns: 0,
                rightColumns: 0
            },
        columnDefs: [
                    {
                                    // Label
                    targets: 4,
                    render: function(data, type, full, meta) {
                        var $status_number = full['status'];
                        var $status = {
                            0: {
                                                title: 'Application under Review',
                                                class: 'badge-light-success'
                                            },
                                            1: {
                                                title: 'Pre-Registered',
                                                class: 'badge-light-success'
                                            },
                                            2: {
                                                title: 'Disapproved',
                                                class: 'badge-light-danger'
                                            },
                                            3: {
                                                title: 'Sites Selected',
                                                class: 'badge-light-success'
                                            },
                                            4: {
                                                title: 'Registered',
                                                class: 'badge-light-success'
                                            },
                                            5: {
                                                title: 'Assessment in progress',
                                                class: 'badge-light-success'
                                            },
                                            6: {
                                                title: 'Reports received',
                                                class: 'badge-light-success'
                                            },
                                            7: {
                                                title: 'Jury review',
                                                class: 'badge-light-success'
                                            },
                                            8: {
                                                title: 'Certificate issued',
                                                class: 'badge-light-success'
                                            },
                        };
                        if (typeof $status[$status_number] === 'undefined') {
                            return data;
                        }
                        return ('<span class="badge badge-pill ' + $status[
                                $status_number].class +
                            '" >' +
                            $status[$status_number].title + '</span>');
                    }
                }
                ],
                dom: '<"row d-flex justify-content-between align-items-center m-1"' + '<"col-lg-1 d-flex align-items-center"l>' + '<"col-lg-5 "f>' + '<"col-lg-5 d-flex align-items-center justify-content-lg-end flex-wrap  p-0"<"dt-action-buttons text-right"B>>' + '>t' + '<"d-flex justify-content-between  mx-2"' + '<"col-sm-12 col-md-6"i>' + '<"col-sm-12 col-md-6"p>' + '>',
            // orderCellsTop: !0,
            select: {
                style: "multi",
                selector: "td:first-child",
                items: "row"
            },
            language: {
                sLengthMenu: 'Show _MENU_',
                search: '',
                searchPlaceholder: ' What are you Looking For',
                paginate: {
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            },

            buttons: [{
                extend: 'collection',
                className: 'btn btn-outline-secondary dropdown-toggle ml-1',
                text: '<i class="fas fa-tasks "></i>' + 'Bulk Actions',
                buttons: [
                {
                    text: feather.icons['file-text'].toSvg({
                        class: 'font-small-4 mr-50'
                    }) + 'Export',
                    className: 'dropdown-item',
                    attr: {
                        id: 'exportExceleData'
                    },
                }],
                init: function(api, node, config) {
                    $(node).removeClass('btn-secondary');
                    $(node).parent().removeClass('btn-group');
                    setTimeout(function() {
                        $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                    }, 50);
                }
            }],
            drawCallback: function() {
                $(document).find('[data-toggle="tooltip"]').tooltip();
            }

        })
    }
                $("#filterButton").click(function (e) {
                    e.preventDefault();
                    dataTable.draw();
                });


                $("#filterResetButton").click(function (e) {
                    e.preventDefault();
                    $("#filterApplicationNumber").val('')
                    $("#filterCiiRefNumber").val('')
                    $("#filterStartDate").val('')
                    $("#filterEndDate").val('')
                    $("#filterDisplayDate").val('')
                    dataTable.draw();
                });


                    $(document).on('click', '#exportExceleData', function(e) {
                        e.preventDefault();

                        var url = "{{ route('assessor.application.bulk_export') }}?export=1";

                        if ($('body').find('div.dataTables_filter input[type="search"]').val() != '') url +=
                            "&fsearch=" + encodeURIComponent($('#fsearch').val());

                        window.location.href = url;
                    });



            }

        }

    }();

    jQuery(document).ready(function() {
        Application.init();

    });
</script>
@endpush
