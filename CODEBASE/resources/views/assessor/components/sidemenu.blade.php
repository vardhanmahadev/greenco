<div class="deznav">
    <div class="deznav-scroll">
        <ul class="metismenu" id="menu">
            <li class="{{($pageTitle=='Dashboard')?'active':''}}"><a class="ai-icon"  href="{{route('assessor.dashboard')}}" aria-expanded="false">
                <i data-feather="airplay"></i><span class="menu-title " data-i18n="Dashboards"> Dashboard </span></a>
            </li>
            <li class="{{($pageTitle=='Profile')?'active':''}}"><a class="ai-icon" href="{{route('assessor.profile')}}" aria-expanded="false">
                <i class="fal fa-id-card pr-0"></i><span class="menu-title " data-i18n="Invoice">  Profile </span></a>
            </li>
            <li class="{{($pageTitle=='NDA Documents List')?'active':''}}"><a class="ai-icon" href="{{route('assessor.ndadocumentslist')}}" aria-expanded="false">
                <i class="fal fa-file-upload pr-0"></i><span class="menu-title " data-i18n="Invoice"> NDA Documents List</span></a>
            </li>
        </ul>
    </div>
</div>

