@extends(ASSESSOR_THEME_NAME.'.layouts.auth')

@section('content')
<div class="auth-wrapper auth-v2">
    <div class="auth-inner row m-0">
        <div class="col-lg-8 width-img pl-0 pr-0">
            <div class="item">
                <div class="slider-block">
                <img class="img-fluid authPage_banner_image" src="{{ url('app-assets/images/assessor_login_banner.png') }}" alt="Login V2">
                <!-- <div class="carousel-caption"> </div> -->
            </div>
        </div>
    </div> 
       
        <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-2">
            <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                <a class="brand-logo" href="javascript:void(0);" style="cursor:default!important;">
                    <h2 class="brand-text text-primary ">
                        <img class="img-fluid" src="{{ url('/app-assets/images/logo/greenco.png') }}" width="150" alt="{{APP_NAME}}">
                    </h2>
                </a>
                <h2 class="card-title font-weight-bold mb-0 text-center">Welcome to Assessor Portal</h2>
                <p class="card-text mb-3">&nbsp;</p>
                <form class="auth-login-form auth-form-div" id="auth-login-form" action="{{route('assessor.login')}}" method="POST">
                    @csrf
                    <div class="form-label-group">
                        <div class="input-group">
                            <div class="input-group-append">
                                <span class="input-group-text border-left-line cursor-pointer"><i data-feather="user"></i></span>
                            </div>
                            <input class="form-control border-right-line border-left-none form-caontrol-merge" id="email" type="text" name="email" placeholder="Email Address*" aria-describedby="email" value="{{old('email')}}" autocomplete="false" tabindex="1" />
                            <label for="email" class="float-label">Email Address <span class="text-danger">*</span></label>
                           
                        </div>
                        <span class="form-text text-danger">{{$errors->first('email')}}</span>
                    </div>
                    <div class="form-label-group mb-h5">
                        <div class="d-flex justify-content-end label-link position-absolute-right">
                            <a href="{{route('assessor.forgotPasswordPage')}}" class="id-anchor font-small-4">Forgot Password?</a>
                        </div>
                        <div class="input-group input-group-merge form-password-toggle">
                            <div class="input-group-append">
                                <span class="input-group-text  border-left-line cursor-pointer"><i data-feather="lock"></i></span>
                            </div>
                            <input type="password" class="form-control form-control-merge" id="password" name="password" tabindex="2" placeholder="Password*" onkeypress="  return /[a-zA-Z0-9@]/i.test(event.key);" aria-describedby="password" value="{{old('password')}}"/>
                            <label for="password" class="float-label">Password <span class="text-danger">*</span></label>
                            <span class="form-text text-danger">{{$errors->first('password')}}</span>
                            <div class="input-group-append">
                                <span class="input-group-text border-right-line  cursor-pointer"><i data-feather="eye"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="mt-2">
                        <button class="btn btn-primary btn-block mr-1 waves-effect waves-float waves-light" tabindex="4" type="submit" id="login" name="login" value="login">Sign in</button>
                    </div>
                   
                </form>
                
                <p class="text-center mt-2 font-small-4">
					<span>Don't have an Account? </span>
					<a href="{{route('assessor.register')}}">
						<span>Register</span>
					</a>
				</p>
            </div>
        </div>
        <!-- /Login-->
    </div>
</div>
@endsection

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Login = function () {
    return { //main function to initiate the module
        init: function () {
            
            jQuery.validator.addMethod("emailAddress", function(value, element) {
               return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
            }, "Please Enter a Valid Email Address");  

            jQuery.validator.addMethod("noSpace", function(value, element) { 
              return value == '' || value.trim().length != 0;  
            }, "This field is required");

            jQuery.validator.addMethod("doubleSpace", function(value, element) {
               return this.optional(element) || /^\S((?!.*  ).*\S)?$/.test(value);
            }, "Mobile Number is invalid");  


            $('.auth-login-form').validate({
                rules: {
                    'email': {
                        required: true,
                        emailAddress: true
                    },
                    'password': {
                        required: true,
                        noSpace:true,
                        doubleSpace:true,
                        minlength:6,
                    }
                },
                messages: {
                    email: {
                        required: "The email address field is required",
                        emailAddress : "Please enter a valid email address"
                    },  
                    password :{
                        required: "The password field is required",
                        noSpace:"Please enter a valid password",
                        doubleSpace:"Please enter a valid password",
                    }
                }
            });

 
        }
    }
}();

jQuery(document).ready(function() {
    Login.init();
    $('input').blur(function(){
        $(".auth-login-form").validate().element(this);
    });

    $('input').on('keyup',function(event){
        $(".auth-login-form").validate().element(this);
    });
});
</script>
@endpush
