@extends(ASSESSOR_THEME_NAME.'.layouts.auth')

@section('content')
<div class="auth-wrapper auth-v2">
    <div class="auth-inner row m-0">
        <div class="col-lg-8 width-img pl-0 pr-0">
            <div class="item">
                <div class="slider-block">
                    <img class="img-fluid authPage_banner_image" src="{{ url('app-assets/images/assessor_login_banner.png') }}" alt="Login V2">
                    <!-- <div class="carousel-caption"> </div> -->
                </div>
            </div>
        </div>
        <div class="d-flex col-lg-4 align-items-center auth-bg px-2 reg-successtext">
            <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
            <span class="success-tick">✔</span>
                <h3 class="card-title font-weight-bold text-center mb-0">Account Activated!</h3>
                <p class="text-center mt-1 font-small-4">Your account has been activated. Please login to the portal using the credentials sent to your registered email address and update your profile.</p>
                <div class="form-group d-flex justify-content-center mb-0">
                        <a class="btn btn-primary mr-1 waves-effect waves-float waves-light" href="{{ route('assessor.login') }}" tabindex="5">Click here to Login</a>
                </div>
            </div>
        </div>
        <!-- /Login-->
    </div>
</div>
@endsection
