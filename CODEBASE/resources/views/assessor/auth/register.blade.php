@extends(COMPANY_THEME_NAME.'.layouts.auth')

@section('content')
<div class="auth-wrapper auth-v2">
    <div class="auth-inner row m-0">
        <div class="col-lg-8  width-img pl-0 pr-0">
            <div class="item">
                <div class="slider-block">
                <img class="img-fluid authPage_banner_image" src="{{ url('app-assets/images/assessor_login_banner.png') }}" alt="Login V2">
                <!-- <div class="carousel-caption"> </div> -->
            </div>
        </div>
     </div>
        <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-2">
            <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                <a class="brand-logo" href="javascript:void(0);" style="cursor:default!important;">
                    <h2 class="brand-text text-primary ">
                        <img class="img-fluid" src="{{ url('/app-assets/images/logo/greenco.png') }}" width="150" alt="{{APP_NAME}}">
                    </h2>
                </a>
                <h2 class="card-title font-weight-bold mb-0 text-center">Assessor Registration</h2>
                <p class="card-text mb-3">&nbsp;</p>
                <form class="auth-register-form auth-form-div" onSubmit="return false" id="auth-register-form" action="{{route('assessor.register')}}" method="POST">
                    @csrf
                    <div class="form-label-group">
                        <div class="input-group" id="name-icon-error">
                            <div class="input-group-append">
                                <span class="input-group-text border-left-line cursor-pointer"  ><i data-feather="user"></i></span>
                            </div>
                                              
                            <input class="form-control form-control-merge border-right-line username-class" id="name"  onkeypress="$('#name-error-add').text(''); return /[a-zA-Z\s]/i.test(event.key);"  type="text" name="name" placeholder="Name*" aria-describedby="name" autocomplete="off" tabindex="1" value="{{old('name')}}" />
                            <label for="name" class="float-label">Name <span class="text-danger">*</span></label>
                        </div>
                        <span class="error" id="name-error-add"></span>
                    </div>
                    <!-- $('#email-error-add').css('display','block'); -->
                    <div class="form-label-group">
                        <div class="input-group" id="email-icon-error">
                            <div class="input-group-append">
                                <span class="input-group-text border-left-line cursor-pointer"  ><i data-feather="at-sign"></i></span>
                            </div>
                            <input class="form-control form-control-merge border-right-line" onkeypress=" $('#email-error-add').text('');" id="email" type="text"  name="email" placeholder="Email Address*" aria-describedby="email" autocomplete="false" tabindex="1" value="{{old('email')}}" />
                            <label for="email" class="float-label">Email Address <span class="text-danger">*</span></label>
                        </div>
                        <span class="error" id="email-error-add"></span>
                    </div>
                    <div class="form-label-group">
                        <div class="input-group" id="mobileno-icon-error">
                            <div class="input-group-append">
                                <span class="input-group-text border-left-line cursor-pointer"  ><i data-feather="phone"></i></span>
                            </div>
                            <input class="form-control form-control-merge border-right-line" maxlength="10"   onkeypress="$('#mobileno-error-add').text(''); return /[0-9]/i.test(event.key)" oninput="this.value=this.value.slice(0,this.maxLength)" id="mobileno" type="text" name="mobileno" value="{{old('mobileno')}}" placeholder="Mobile Number*" aria-describedby="mobileno" autocomplete="false" tabindex="1" />
                            <label for="mobileno" class="float-label">Mobile Number <span class="text-danger">*</span></label>
                        </div>
                        <span class="error" id="mobileno-error-add"> </span>                     
                    </div>
                    
    
                    <div class="mt-2">
                        <button class="btn btn-primary btn-block mr-1 waves-effect waves-float waves-light" tabindex="4" type="submit" id="register" name="register" value="register">Register</button>
                    </div>
                </form>
                
                <p class="text-center mt-2 font-small-4">
					<span>Do you have an Account?</span>
					<a href="{{ route('assessor.login') }}">
						<span>Login</span>
					</a>
				</p>

            </div>
        </div>
        <!-- /Login-->
    </div>
</div>
@endsection

@push('PAGE_SCRIPTS')


<script type="text/javascript">

var Login = function () {
    return { //main function to initiate the module
        init: function () {
             jQuery.validator.addMethod("mobileNo", function(value, element) {
               return this.optional(element) || /^[6-9][0-9]{9}$/.test(value);
            }, "Mobile Number is invalid"); 

            jQuery.validator.addMethod("noSpace", function(value, element) { 
              return value == '' || value.trim().length != 0;  
            }, "This field is required");

            jQuery.validator.addMethod("doubleSpace", function(value, element) {
               return this.optional(element) || /^\S((?!.*  ).*\S)?$/.test(value);
            }, "This field is required");  

            jQuery.validator.addMethod("emailAddress", function(value, element) {
               return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
            }, "Please Enter a Valid Email Address");  

            jQuery.validator.addMethod("lettersonly", function(value, element) 
            {
                return this.optional(element) || /^[A-Z," "]+$/i.test(value);
            }, "Letters and spaces only please");

            

            $('#auth-register-form').validate({
                // onkeypress : true ,
                        rules: {
                            'email': {
                                required: true,
                                
                                emailAddress:true,
                                

                            },
                            'name': {
                                required: true,
                                noSpace: true,
                                doubleSpace:true,
                                maxlength: 50,
                                minlength:3,
                                lettersonly:true
                            },
                            'mobileno':{
                                required: true,
                                
                                digits:true,
                                minlength:10,
                                mobileNo: true
                            }
                        },
                        messages: {
                            email: {
                                required: "The email address field is required",
                                email : "Please enter a valid email address",
                                emailAddress : "Please enter a valid email address",
                                noSpace: "Please enter a valid email address",
                                doubleSpace: "Please enter a valid email address"
                                
                            },  
                            name: {
                                required: "The name field is required",
                                noSpace: "Please enter a valid name",
                                doubleSpace: "Please enter a valid name",
                                lettersonly: "Please enter a valid name"
                            },  
                            mobileno: {
                                required: "The mobile number field is required",
                            },  
                        },
                        // });
                        submitHandler : function(form){
                        
                            $("#register").prop('disabled',true);
                            $("#register").html("Loading....");                        
                            $.ajax({
                                type: "POST",
                                url: $(form).attr('action'),
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                data: $(form).serialize(),
                                success: function(response) {
                                    if(response.status == 'errors') {
                                        $("#register").prop('disabled',false);
                                        $("#register").html("Register");
                                        $.each(response.errors, function (key, error) {
                                            $('form#auth-register-form').find("#" + key + "-icon-error").removeClass('borderRightError').addClass('borderRightError');
                                            $('form#auth-register-form').find("#" + key + "-icon-error").addClass('is-invalid');
                                            $('form#auth-register-form').find("#" + key).removeClass('error').addClass('error');
                                            $('form#auth-register-form').find("#" + key + "-error-add").css('display','block');
                                            $('form#auth-register-form').find("#" + key + "-error-add").text(error[0]);
                                        });
                                        
                                    }
                                    if(response.status == 'success') {
                                        setTimeout(function(){
                                            window.location.href = "{{route('assessor.registerSuccess')}}";
                                        }, 3000);
                                    }

                                    if(response.status == 'error') {
                                        $("#register").prop('disabled',false);
                                        $("#register").html("Register");
                                        JsUtility.showToastr('error', 'Assessor', response.message);
                                    }
                                },
                                error: function(response) {
                                    console.log(response)
                                    $("#register").prop('disabled',false);
                                    $("#register").html("Register");
                                    JsUtility.showToastr('error', 'Assessor', response.message);
                                }
                            });   
                            
                        }
                    }); 
                    
        }
    }
}();


jQuery(document).ready(function() {
    Login.init(); 

    $('input').blur(function(){
        $("#auth-register-form").validate().element(this);
    });

    $('.username-class').on('keyup',function(event){
        event.currentTarget.value = this.value.toUpperCase();
        $("#auth-register-form").validate().element(this);
    });

    $('input').on('keyup',function(event){
        $("#auth-register-form").validate().element(this);
    });
 
});
</script>

@endpush
