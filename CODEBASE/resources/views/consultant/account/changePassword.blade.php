@extends(ASSESSOR_THEME_NAME.'.layouts.app')

@section('content')
<div class="app-content content content-ps-stl">
    <div class="">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <section>
                    <div class="container">
                        <div class="row  change-password v-row">
                            <div class="col-lg-6 card offset-lg-3  mt-3 px-3 pt-3 pb-3 ">
                                <div id="changepassword" class="">
                                    <div class="content-header">
                                        <h4 class="mb-0 text-center">Change Password </h4>
                                    </div>
                                    <form class="auth-reset-password-form form-auth-div mt-2" id="auth-reset-password-form" action="{{route('assessor.changePassword.save')}}" method="POST">
                                        @csrf
                                        <div class="form-group">
                                            <div class="d-flex justify-content-between">
                                                <label for="current_password">Old Password</label>
                                            </div>
                                            <div class="input-group input-group-merge form-password-toggle">
                                                <input class="form-control form-control-merge" id="current_password" type="password" name="current_password" placeholder="············" aria-describedby="reset-password-old" autofocus="" tabindex="1" value="{{old('current_password')}}"/>
                                                <div class="input-group-append"><span class="input-group-text border-right-line cursor-pointer"><i data-feather="eye"></i></span></div>
                                                <span class="highlight">{{$errors->first('current_password')}}</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="d-flex justify-content-between">
                                                <label for="new_password">New Password</label>
                                            </div>
                                            <div class="input-group input-group-merge form-password-toggle">
                                                <input class="form-control form-control-merge" id="new_password" type="password" name="new_password" placeholder="············" aria-describedby="reset-password-new" autofocus="" tabindex="2" value="{{old('new_password')}}"/>
                                                <div class="input-group-append"><span class="input-group-text  border-right-line cursor-pointer"><i data-feather="eye"></i></span></div>
                                                <span class="highlight">{{$errors->first('new_password')}}</span>
                                                <span>Note: Your New Password must contain atleast 1 uppercase, lowercase, number and special character and should be of atleast 6 characters in length.</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="d-flex justify-content-between">
                                                <label for="confirmed">Confirm Password</label>
                                            </div>
                                            <div class="input-group input-group-merge form-password-toggle">
                                                <input class="form-control form-control-merge" id="confirmed" type="password" name="confirmed" placeholder="············" aria-describedby="reset-password-confirm" tabindex="3" value="{{old('confirmed')}}"/>
                                                <div class="input-group-append"><span class="input-group-text border-right-line cursor-pointer"><i data-feather="eye"></i></span></div>
                                                <span class="highlight">{{$errors->first('confirmed')}}</span>
                                            </div>
                                        </div>
                                        <button type="submit" id="submit" name="submit" value="submit" class="btn btn-primary btn-block" tabindex="3">Submit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
@endsection

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var ChangePassword = function () {
    return { //main function to initiate the module
        init: function () {
            $('.auth-reset-password-form').validate({
                rules: {
                    'current_password': {
                        required: true,
                    },
                    'new_password': {
                        required: true
                    },
                    'confirmed': {
                        required: true
                    }
                }
            });

            $('#auth-reset-password-form').submit(function () {   
                var myForm = $("form#auth-reset-password-form");   
                if (myForm) {   
                    $('#submit').prop('disabled', true); 
                    $('#submit').html('Loading...'); 
                    $(myForm).submit();   
                }   
            });   
        }
    }
}();

jQuery(document).ready(function() {
    ChangePassword.init();
});
</script>
@endpush
