
@extends(ASSESSOR_THEME_NAME.'.layouts.app')

@section('content')
<div class="app-content content content-ps-stl">
    <div class="">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <section class="profile-style2">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="card ">
                                <div class="d-flex v-row title-strip mb-0">
                                    <div class="col-6 pl-0">Login Information:</div>
                                    <div class="col-6 pr-0"> <a class="btn btn-primary float-right" href="{{route('assessor.changePassword.save')}}"><i data-feather="lock"></i>Change Password</a></div>
                                </div>
                                <div class="card-body ">
                                    <form class="auth-reset-password-form form-auth-div mt-2" action="#" method="POST">
                                        @csrf
                                        <div class="form-label-group">
                                            <input class="form-control border-right-line"  disabled id="email" type="text" name="email" placeholder="Username* (Email Address)"  value="{{old('email', $profile->email)}}" autocomplete="false" onfocus="this.removeAttribute('readonly');" />
                                            <label class="form-label ">Username* (Email Address)</label>
                                        </div>
                                        <!-- <div class="form-group text-center mt-1">
                                            <button class="btn btn-primary ">Submit</button>
                                        </div> -->
                                    </form>
                                </div>
                            </div>


                        </div>

                        <div class="col-lg-8">
                        <div class="card ">
                                <div class="d-flex v-row title-strip">
                                    <div class="col-6 pl-0">Contact information:</div>
                                </div>
                                <div class="card-body">
                                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 p-a0">
                                        <form class="auth-reset-password-form form-auth-div" action="{{route('assessor.profile')}}" method="POST">
                                            @csrf
                                            <div class="row ">
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="row ">
                                                        <div class="col-lg-12 col-md-12 pr-0">
                                                        </div>
                                                        <div class="col-lg-12 col-md-12">
                                                            <div class="form-label-group">
                                                                <input class="form-control border-right-line" autocomplete="off" type="text" name="name" id="name" placeholder="Name of the representative" maxlength ="60" required value="{{old('name', $profile->name)}}"/>
                                                                <label>Name of the representative * </label>
                                                                <span class="form-text text-danger">{{$errors->first('name')}}</span>
                                                            </div>
                                                            <!-- <span class="form-text text-danger">{{$errors->first('name')}}</span> -->
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-6 col-md-6">
                                                    <div class="row ">
                                                        <div class="col-lg-12 col-md-12">
                                                            <div class="form-label-group">
                                                                <input class="form-control border-right-line" autocomplete="off" type="text" placeholder="Designation" name="designation" id="designation" value="{{old('designation', $profile->designation)}}"/>
                                                                <label>Designation</label>
                                                                <span class="form-text text-danger">{{$errors->first('designation')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="form-label-group form-group d-flex">
                                                        <input type="text" class="form-control" maxlength="10" min="0" oninput="this.value=this.value.slice(0,this.maxLength)" id="mobile" name="mobile" placeholder="Mobile Number" value="{{$profile->mobile}}"/>
                                                        <label for="Mobile">Mobile Number</label>
                                                        <span class="form-text text-danger">{{$errors->first('mobile')}}</span>
                                                    </div>
                                                </div>
                                                
                                                <div class="text-center col-lg-12 col-md-6">
                                                    <button type="submit" name="submit" value="submit" class="btn btn-primary btn-block" tabindex="3">Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                </section>

            </div>
        </div>
    </div>
</div>
@endsection

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Profile = function () {
    return { //main function to initiate the module
        init: function () {
            $('.auth-reset-password-form').validate({
                rules: {
                    'email': {
                        required: true,
                        email: true
                    },
                    'name': {
                        required: true
                    },
                    'mobile': {
                        required: true
                    }
                }
            });
        }
    }
}();

jQuery(document).ready(function() {
    Profile.init();
});
</script>
@endpush
