<ul class="nav nav-pills tabs-pages tab-leftside">
    @if ($technical)
        <li class="nav-item ">
            <a class="nav-link @if($tabeActive=='technial') active @endif " href="{{route('assessor.application.technical_assessment',$application)}}">
                <span class="bs-stepper-box">
                </span>Technical Assessment</a>
        </li>
    @endif

    @if ($onsite)
            <li class="nav-item">
                <a class="nav-link @if($tabeActive=='onsite') active @endif  " href="{{route('assessor.application.onsite_assessment',$application)}}">
                    <span class="bs-stepper-box">
                    </span>On-site Assessment</a>
            </li>
    @endif



</ul>

<p class="edit-tab">Application Number:<span>{{($application->application_no==null)?'N/A':$application->application_no}}</span>
