<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
      <div class="navbar-header">

      <ul class="nav navbar-nav flex-row">
          <li class="nav-item me-auto"> <a href="index.html" class="brand-logo">
                <img class="img-fluid" src="{{url('app-assets/images/logo/logo-white.png')}}" alt="Login V2">

            </a><h2 class="brand-text">  <img class="img-fluid" src="{{url('app-assets/images/logo/glogon.png')}}" alt="Login V2"></h2></li>
          <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li>
        </ul>

      </div>

      <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="{{($activeMenu=='dashboard')?'active':''}} nav-item">
                <a class="d-flex align-items-center" href="{{route('assessor.dashboard')}}">
                    <i data-feather='airplay'></i><span class="menu-title" data-i18n="Email">Dashboard</span>
                </a>
            </li>

            <li class="{{($activeMenu=='assessor')?'active':''}} nav-item">
                <a class="d-flex align-items-center" href="{{route('assessor.profile')}}">
                    <i class="fal fa-id-card pr-0"></i><span class="menu-title" data-i18n="Calendar">Profile  Management </span></a>
            </li>


        </ul>
      </div>
    </div>
    <!-- END: Main Menu-->
