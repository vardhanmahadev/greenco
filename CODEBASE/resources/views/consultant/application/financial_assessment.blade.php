@extends(ASSESSOR_THEME_NAME.'.layouts.app')

@section('content')
<div class="app-content content content-ps-stl">
    <div class="">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <section class=" p-relative mt--1">
                    @include('assessor.components.application_nave')
                    <div class="col-12">
                        <section id="advanced-search-datatable" class="">
                            <div class="row">
                                <div class="col-12">
                                    <form method="POST" action="{{$route}}" class="form-validate">
                                        @csrf
                                        <div class="card">
                                            <div class=" sub-tabpages">
                                                <div class="form-div">
                                                    <div class="card-body">
                                                        @isset($master_inputs)
                                                            @foreach ($master_inputs as $input)
                                                                <div @if($input->visible==0) hidden @endif class="row  border-bb  pb-1 pt-1">
                                                                    <div class="col-lg-5">
                                                                        <div class="row ">
                                                                            <div class="col-lg-8 col-md-8 tab-assets-list">
                                                                                <div class="">
                                                                                    <label> {{$input->description}} : </label>
                                                                                </div>
                                                                                {{-- <p class="p-a0">{{$input->description}}</p> --}}
                                                                            </div>
                                                                            <div class="col-lg-4 col-md-4">
                                                                                <div class="v-row uploadedfile">
                                                                                    <div class="col-lg-10 pl-0"> <label>Score</label>
                                                                                        <input type="text" name="score[{{$input->param_field_name}}][value]" value="{{old('score['.$input->param_field_name.'][value]')}}" class="form-control"> </div>
                                                                                        <input type="hidden" name="score[{{$input->param_field_name}}][id]" value="{{$input->id}}"   >
                                                                                        <input type="hidden" name="score[{{$input->param_field_name}}][param_name]" value="{{$input->param_name}}"   >
                                                                                        <input type="hidden" name="score[{{$input->param_field_name}}][description]" value="{{$input->description}}"   >
                                                                                        <input type="hidden" name="score[{{$input->param_field_name}}][visible]" value="{{$input->visible}}"   >
                                                                                </div>
                                                                            </div>
                                                                            @error('score['.$input->param_field_name.'][value]')
                                                                                <div class="col-md-12" >
                                                                                    <span class="text-danger" > {{$message}}  </span>
                                                                                </div>
                                                                            @enderror

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-7 remark-title">
                                                                        <div class="v-row justify-content-between mb-50">
                                                                            <label>Remarks :</label>
                                                                        </div>
                                                                        <p class="remark-lists">{{$input->remark}}</p>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        @endisset

                                                        @isset($exist_inputs)
                                                        @foreach($exist_inputs as $input)

                                                            <div @if ($input->parameter_visible==0) hidden @endif  class="row  border-bb  pb-1 pt-1">
                                                                <div class="col-lg-5">
                                                                    <div class="row ">
                                                                        <div class="col-lg-8 col-md-8 tab-assets-list">
                                                                            <div class="">
                                                                                <label> {{$input->parameter_description}} : </label>
                                                                            </div>
                                                                            {{-- <p class="p-a0">{{$input->parameter_description}}</p> --}}
                                                                        </div>
                                                                        <div class="col-lg-4 col-md-4">
                                                                            <div class="v-row uploadedfile">
                                                                                <div class="col-lg-10 pl-0"> <label>Score</label>
                                                                                    <input type="text" name="score[{{$input->parameter_field_name}}][value]" value="{{old('score['.$input->parameter_field_name.'][value]',$input->score_inpute )}}" class="form-control"> </div>
                                                                                    <input type="hidden" name="score[{{$input->parameter_field_name}}][id]" value="{{$input->mst_assessment_id}}"   >
                                                                                    <input type="hidden" name="score[{{$input->parameter_field_name}}][param_name]" value="{{$input->parameter_title}}"   >
                                                                                    <input type="hidden" name="score[{{$input->parameter_field_name}}][description]" value="{{$input->parameter_description}}"   >
                                                                                    <input type="hidden" name="score[{{$input->parameter_field_name}}][description]" value="{{$input->parameter_description}}"   >
                                                                                    <input type="hidden" name="score[{{$input->parameter_field_name}}][visible]" value="{{$input->parameter_visible}}"   >
                                                                            </div>
                                                                        </div>
                                                                        @error('score['.$input->param_field_name.'][value]')
                                                                            <div class="col-md-12" >
                                                                                <span class="text-danger" > {{$message}}  </span>
                                                                            </div>
                                                                        @enderror

                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-7 remark-title">
                                                                    <div class="v-row justify-content-between mb-50">
                                                                        <label>Remarks :</label>
                                                                    </div>
                                                                    <p class="remark-lists">{{$input->remark}}</p>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    @endisset



                                                        <div class="m0-auto text-center">
                                                            <button class="btn btn-primary mt-1 text-center">Submit</button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </section>
                    </div>
                </section>
            </div>
        </div>
</div>
</div>
@endsection
