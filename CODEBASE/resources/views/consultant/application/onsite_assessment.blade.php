@extends(ASSESSOR_THEME_NAME.'.layouts.app')


@section('content')
<div class="app-content content content-ps-stl">
    <div class="">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <section class=" p-relative mt--1">
                    @include('assessor.components.application_nave')
                    <div class="col-12">
                        <section id="advanced-search-datatable" class="">
                            <div class="row">
                                <div class="col-12">
                                        <div class="card">
                                            <div class=" sub-tabpages">
                                                <div class="form-div">
                                                    <div>
                                                        <ul class="nav nav-pills tabs-pages tab-leftside">
                                                            @foreach ($plants as $plant)
                                                                <li class="nav-item">
                                                                    <a class="nav-link  @if ($plant->id == $select_plant->id) active @endif "
                                                                        href="{{ route('assessor.application.onsite_assessment', ['application' => $application, 'plant' => $plant->id]) }}">
                                                                        <span class="bs-stepper-box">
                                                                            <i class="fal fa-users"></i>
                                                                        </span>Plant {{ $plant->plant_sl }}</a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                        <p class="edit-tab">Plant Name: {{$plant_name}}</p>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="tab-pane " id="On-siteAssesssment" aria-labelledby="On-siteAssesssment-tab" role="tabpanel">
                                                            <div class="nav-vertical assets-tabs">
                                                                <ul class="nav nav-tabs nav-left flex-columntab-leftside" role="tablist">
                                                                    @foreach ($option_type as $index=>$lable)
                                                                        <li class="nav-item">
                                                                            <a class="nav-link @if ($index==$select_group) active @endif " id="ontime-tab-{{$index}}" data-toggle="tab" href="#ontime-tab-targ-{{$index}}" aria-controls="Engineeringcompliance" role="tab" aria-selected="false"> <span class="bs-stepper-box">
                                                                                </span>{{$index}}. {{@$lable['label']}}</a>
                                                                        </li>
                                                                    @endforeach

                                                                </ul>

                                                                <div class="tab-content">
                                                                    @foreach ($option_type as $index=> $tab)
                                                                        <div class="tab-pane  @if ($index==$select_group) active @endif " id="ontime-tab-targ-{{$index}}" aria-labelledby="ontime-tab-{{$index}}" role="tabpanel">
                                                                            <div class="form-div">
                                                                                <form method="POST" action="{{$route}}" enctype="multipart/form-data" class=""  >
                                                                                    @csrf
                                                                                    <input type="hidden" name="group" value="{{$index}}"  >
                                                                                    @if (@$tab['remark']!=null)
                                                                                        <div class="row mt-2">
                                                                                            <div class="col">
                                                                                                <div
                                                                                                    class="">
                                                                                                    <label>Remarks :</label>
                                                                                                </div>
                                                                                                <textarea class="form-control" maxlength="500" placeholder="Enter your remark" disabled name="remark" rows="1"> {{@$tab['remark']}} </textarea>
                                                                                            </div>
                                                                                        </div>
                                                                                    @endif

                                                                                    @foreach ($tab['inputs'] as $Input_index=>$input)

                                                                                    <div   @if ($input->visible===0) hidden @endif  class="row  border-bb  pb-1  pt-1">
                                                                                        <div class="col-lg-12 col-md-12 tab-assets-list">
                                                                                            <div class="">
                                                                                                <label>{{$loop->iteration}}. {{$input->description}} </label>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-lg-12 pl-4">
                                                                                            <div class="row mt-1">
                                                                                                <div class="col-md-6">
                                                                                                    <div class="">
                                                                                                        <input type="hidden" name="score[{{$input->param_field_name}}][id]" value="{{$input->id}}"   >
                                                                                                        <input type="hidden" name="score[{{$input->param_field_name}}][param_name]" value="{{$input->param_name}}"   >
                                                                                                        <input type="hidden" name="score[{{$input->param_field_name}}][description]" value="{{$input->description}}"   >
                                                                                                        <input type="hidden" name="score[{{$input->param_field_name}}][visible]" value="{{$input->visible}}" >
                                                                                                        <div>
                                                                                                            {{-- <span class="text-danger" > {{old($input->param_field_name)}} </span> --}}
                                                                                                        </div>
                                                                                                        @foreach ($input->common_config as $sl=> $check)
                                                                                                            <div class="form-check">
                                                                                                                <input class="form-check-input"  @if($tab['exist'])  disabled  @endif  type="radio"  @if (old($input->param_field_name,$input->assessor_input) == "$sl") checked @endif  value="{{$sl}}" name="score[{{$input->param_field_name}}][value]" id="checkBox{{$index}}-{{$Input_index}}-{{$sl}}">
                                                                                                                <label class="form-check-label" for="checkBox{{$index}}-{{$Input_index}}-{{$sl}}">
                                                                                                                    {{$check->label}}
                                                                                                                </label>
                                                                                                            </div>
                                                                                                        @endforeach
                                                                                                    </div>
                                                                                                    <div>
                                                                                                    @error($input->param_field_name)
                                                                                                        <div class="col-md-12" >
                                                                                                            <span class="text-danger" > {{$message}}  </span>
                                                                                                        </div>
                                                                                                    @enderror
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class=" col-md-6 ">
                                                                                                    {{-- <p class="remark-lists">{{$input->remark}}</p> --}}
                                                                                                    <div class="" >
                                                                                                        @if ($input->document_required==1 || $input->assessor_document != null )
                                                                                                            <div class="v-row mb-1 uploadedfile">
                                                                                                                <div class="col-lg-10 pl-0">
                                                                                                                    @if(!$tab['exist'])
                                                                                                                    <div class="custom-file">
                                                                                                                        <input type="file" name="document[{{$input->param_field_name}}]" class="custom-file-input" >
                                                                                                                        <label class="custom-file-label" >Choose file</label>
                                                                                                                    </div>
                                                                                                                    @else
                                                                                                                    <div>
                                                                                                                        <h6>Upload Files</h6>
                                                                                                                    </div>
                                                                                                                    @endif
                                                                                                                    <span class=""> {{basename($input->assessor_document)}} </span>
                                                                                                                </div>
                                                                                                            </div>

                                                                                                            @error('document['.$input->param_field_name.']')
                                                                                                                <div class="col-md-12" >
                                                                                                                    <span class="text-danger" > {{$message}}  </span>
                                                                                                                </div>
                                                                                                            @enderror
                                                                                                        @endif
                                                                                                        @if ($tab['exist'])
                                                                                                        <div>
                                                                                                            <label for="">Score</label>
                                                                                                            <input class="form-control" type="text" disabled value="{{$input->assessor_score}}"   >
                                                                                                        </div>
                                                                                                        @endif

                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    @endforeach


                                                                                    @if (!$tab['exist'])
                                                                                        <div class="m0-auto text-center mb-2">
                                                                                            <button class="btn btn-primary mt-1 text-center">Submit</button>
                                                                                        </div>
                                                                                        @else
                                                                                        <div class="row my-2 justify-content-end" >
                                                                                            <div class="col-md-4" >
                                                                                                <h5>Total score : <span>{{$tab['inputs']->sum('assessor_score')}}</span></h5>

                                                                                            </div>
                                                                                        </div>
                                                                                    @endif

                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    @endforeach


                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </section>
            </div>
        </div>
</div>
</div>
@endsection
