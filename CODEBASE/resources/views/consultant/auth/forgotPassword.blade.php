@extends(ASSESSOR_THEME_NAME.'.layouts.auth')

@section('content')
<div class="auth-wrapper auth-v2">
    <div class="auth-inner row m-0">
        <!-- Brand logo-->
        <!-- /Brand logo-->
        <!-- Left Text-->
        <div class="d-none d-lg-flex col-lg-8 ">
            <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
                <img class="img-fluid" src="{{ url('app-assets/images/pages/forgot.jpg') }}" alt="Forgot password V2" /></div>
        </div>
        <!-- /Left Text-->
        <!-- Forgot password-->
        <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
            <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                <h2 class="card-title font-weight-bold mb-1">Forgot Password? 🔒</h2>
                <p class="card-text mb-2">Enter your email address and we'll send you a system generated password.</p>
                <form class="auth-forgot-password-form auth-form-div mt-2" action="{{route('assessor.forgotPassword')}}" method="POST">
                    @csrf
           
                    <div class="form-label-group">
                        <div class="input-group">
                            <div class="input-group-append">
                                <span class="input-group-text border-left-line cursor-pointer"><i data-feather="user"></i></span>
                            </div>
                            <input class="form-control border-right-line border-left-none form-caontrol-merge" id="email" type="text" name="email" placeholder="Enter Your Email Address" aria-describedby="email" autocomplete="false" tabindex="1" value="{{old('email')}}" />
                            <label for="email" class="float-label">Email Address</label>
                        </div>
                        <span class="form-text text-danger">{{$errors->first('email')}}</span>
                    </div>

                    <button class="btn btn-primary btn-block" tabindex="2" name="submit" type="submit" id="submit" value="submit">Send Password</button>
                </form>
                <p class="text-center mt-2"><a href="{{route('assessor.login')}}"><i data-feather="chevron-left"></i> Back to login</a></p>
            </div>
        </div>
        <!-- /Forgot password-->
    </div>
</div>
@endsection

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var ForgotPassword = function () {
    return { //main function to initiate the module
        init: function () {
            $('.auth-forgot-password-form').validate({
                onkeyup: function (element) {
                    $(element).valid();
                },
                onfocusout: function (element) {
                    $(element).valid();
                },
                rules: {
                    'email': {
                        required: true,
                        email: true
                    }
                }
            });
        }
    }
}();

jQuery(document).ready(function() {
    ForgotPassword.init();
});
</script>
@endpush