@extends(COMPANY_THEME_NAME.'.layouts.auth')

@section('content')
<div class="auth-wrapper auth-v2">
    <div class="auth-inner row m-0">
        <div class="col-lg-8  width-img">
            <!-- <div class="login_slider owl-carousel owl-theme"> -->
                <div class="item pt-5 mt-3 text-center">
                    <div class="slider-block">
                        <img class="img-fluid" src="{{ url('app-assets/images/login_slider.png') }}" class="" alt="Login V2" />
                        <div class="carousel-caption">
                        <h3> The Confederation of Indian Industry (CII) works to create and sustain an environment conducive to the development of India, partnering industry, Government, and civil society, through advisory and consultative processes.</h3>
                        </div>
                    </div>
                </div>
            <!-- </div> -->
        </div>
        <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-2">
            <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                <a class="brand-logo" href="javascript:void(0);">
                    <h2 class="brand-text text-primary ">
                        <img class="img-fluid" src="{{ url('/app-assets/images/logo/logo.png') }}" width="150" alt="{{APP_NAME}}">
                    </h2>
                </a>
                <h2 class="card-title font-weight-bold mb-0 text-center">Welcome Assessor!</h2>
                <p class="card-text mb-3">&nbsp;</p>
                <form class="auth-register-form auth-form-div" action="{{route('assessor.register')}}" method="POST">
                    @csrf
                    <div class="form-label-group">
                        <div class="input-group">
                            <div class="input-group-append">
                                <span class="input-group-text border-left-line cursor-pointer"><i data-feather="user"></i></span>
                            </div>
                            <input class="form-control border-right-line border-left-none form-caontrol-merge" id="name" type="text" name="name" placeholder="Enter Your Name" aria-describedby="name" autocomplete="false" tabindex="1" value="{{old('name')}}" />
                            <label for="name" class="float-label">Name</label>
                        </div>
                        <span class="form-text text-danger">{{$errors->first('name')}}</span>
                    </div>
                   
                    <div class="form-label-group">
                        <div class="input-group">
                            <div class="input-group-append">
                                <span class="input-group-text border-left-line cursor-pointer"><i data-feather="at-sign"></i></span>
                            </div>
                            <input class="form-control border-right-line border-left-none form-caontrol-merge" id="email" type="text" name="email" placeholder="Enter Your Email Address" aria-describedby="email" autocomplete="false" tabindex="1" value="{{old('email')}}" />
                            <label for="email" class="float-label">Email Address</label>
                        </div>
                        <span class="form-text text-danger">{{$errors->first('email')}}</span>
                    </div>
                    <div class="form-label-group">
                        <div class="input-group">
                            <div class="input-group-append">
                                <span class="input-group-text border-left-line cursor-pointer"><i data-feather="phone"></i></span>
                            </div>
                            <input class="form-control border-right-line border-left-none form-caontrol-merge" maxlength="10" min="0" oninput="this.value=this.value.slice(0,this.maxLength)" id="mobileno" type="number" name="mobileno" value="{{old('mobileno')}}" placeholder="Enter Your Mobile No" aria-describedby="mobileno" autocomplete="false" tabindex="1" />
                            <label for="mobileno" class="float-label">Mobile No</label>
                        </div>
                        <span class="form-text text-danger">{{$errors->first('mobileno')}}</span>
                    </div>
                   
                    
                    <div class="col-lg-12 justify-content-center  d-flex">
                        <button class="btn btn-primary" tabindex="5" type="submit" id="register" name="register" value="register"><i class="fal fa-check"></i> Submit  </button>
                    </div>
    
                </form>
                
                <p class="text-center mt-2">
					<span>Already having account?</span>
					<a href="{{ route('assessor.login') }}">
						<span>Login</span>
					</a>
				</p>

            </div>
        </div>
        <!-- /Login-->
    </div>
</div>
@endsection

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Login = function () {
    return { //main function to initiate the module
        init: function () {
            $('.auth-register-form').validate({
                rules: {
                    'name': {
                        required: true,
                        lettersonly: true
                    },
                    'email': {
                        required: true,
                        email: true
                    },
                    'mobileno': {
                        required: true
                    }
                }
            });
        }
    }
}();

jQuery(document).ready(function() {
    Login.init();
});
</script>
@endpush
