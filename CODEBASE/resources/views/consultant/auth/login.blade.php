@extends(ASSESSOR_THEME_NAME.'.layouts.auth')

@section('content')
<div class="auth-wrapper auth-v2">
    <div class="auth-inner row m-0">
        <div class="col-lg-8  width-img">
            <!-- <div class="login_slider owl-carousel owl-theme"> -->
                <div class="item pt-5 mt-3 text-center">
                    <div class="slider-block">
                        <img class="img-fluid" src="{{ url('app-assets/images/login_slider.png') }}" class="" alt="Login V2" />
                        <div class="carousel-caption">
                        <h3> The Confederation of Indian Industry (CII) works to create and sustain an environment conducive to the development of India, partnering industry, Government, and civil society, through advisory and consultative processes.</h3>
                        </div>
                    </div>
                </div>
            <!-- </div> -->
        </div>
        <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-2">
            <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                <a class="brand-logo" href="javascript:void(0);">
                    <h2 class="brand-text text-primary ">
                        <img class="img-fluid" src="{{ url('/app-assets/images/logo/logo.png') }}" width="150" alt="{{APP_NAME}}">
                    </h2>
                </a>
                <h2 class="card-title font-weight-bold mb-0 text-center">Assessor Portal! 👋</h2>
                <p class="card-text mb-3">&nbsp;</p>
                <form class="auth-login-form auth-form-div" action="{{route('assessor.login')}}" method="POST">
                    @csrf
                    <div class="form-label-group">
                        <div class="input-group">
                            <div class="input-group-append">
                                <span class="input-group-text border-left-line cursor-pointer"><i data-feather="user"></i></span>
                            </div>
                            <input class="form-control border-right-line border-left-none form-caontrol-merge" id="email" type="text" name="email" placeholder="Enter Your Email Address" aria-describedby="email" value="{{old('email')}}" autocomplete="false" tabindex="1" />
                            <label for="email" class="float-label">Email Address</label>
                           
                        </div>
                        <span class="form-text text-danger">{{$errors->first('email')}}</span>
                    </div>
                    <div class="form-label-group mb-h5">
                        <div class="d-flex justify-content-end label-link position-absolute-right">
                            <a href="{{route('assessor.forgotPassword')}}" class="id-anchor">Forgot Password?</a>
                        </div>
                        <div class="input-group input-group-merge form-password-toggle">
                            <div class="input-group-append">
                                <span class="input-group-text  border-left-line cursor-pointer"><i data-feather="lock"></i></span>
                            </div>
                            <input type="password" class="form-control form-control-merge" id="password" name="password" tabindex="2" placeholder="Enter Your Password" aria-describedby="password" value="{{old('password')}}"/>
                            <label for="password" class="float-label">Password</label>
                            <span class="form-text text-danger">{{$errors->first('password')}}</span>
                            <div class="input-group-append">
                                <span class="input-group-text border-right-line  cursor-pointer"><i data-feather="eye"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="mt-2">
                        <button class="btn btn-primary btn-block" tabindex="4" type="submit" name="login" value="login">Sign in</button>
                    </div>
                   
                </form>
                
                <p class="text-center mt-2">
					<span>Don't having account? </span>
					<a href="{{route('assessor.register')}}">
						<span>Register</span>
					</a>
				</p>
            </div>
        </div>
        <!-- /Login-->
    </div>
</div>
@endsection

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Login = function () {
    return { //main function to initiate the module
        init: function () {
            $('.auth-login-form').validate({
                rules: {
                    'email': {
                        required: true,
                        email: true
                    },
                    'password': {
                        required: true
                    }
                }
            });
        }
    }
}();

jQuery(document).ready(function() {
    Login.init();
});
</script>
@endpush
