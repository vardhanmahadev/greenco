<div class="modal modal-slide-in float-labeldispaly fade" id="{{$id}}" tabindex="-1" role="dialog"
    aria-labelledby="editWarehouseTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="add-new-user modal-content pt-0">
            <div class="modal-header mb-1">
                <h5 class="modal-title">{{@$heading}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
            </div>
            <div class="modal-body flex-grow-1 pt-3">
                {{ $slot }}
            </div>
        </div>
</div>
</div>
