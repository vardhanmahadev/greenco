

    <div class="modal float-labeldispaly fade" id="{{$id}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
            <div class="add-new-user modal-content pt-0">
                <div class="modal-header mb-1">
                    <h5 class="modal-title">{{@$heading}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                </div>
                <div class="modal-body flex-grow-1">
                    {!! $slot !!}
                </div>
            </div>
        </div>
    </div>
