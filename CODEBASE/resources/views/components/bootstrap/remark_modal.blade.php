<div class="modal float-labeldispaly fade" id="{{$id}}" tabindex="-1" role="dialog" aria-labelledby="editWarehouseTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form class="add-new-user modal-content pt-0">
            <div class="modal-header mb-1">
                <h5 class="modal-title">{{@$heading}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
            </div>
            <div class="modal-body remark_histroy_modal flex-grow-1">
                <div class="form-label mb-2">
                    <div class="col-lg-12 order-detailspage commenthist-lists border-bb">
                        {!! $slot !!}
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
    
