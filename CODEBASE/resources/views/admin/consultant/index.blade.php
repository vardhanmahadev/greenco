@extends(ADMIN_THEME_NAME.'.layouts.app')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
    <div class="app-content content content-ps-stl">
        <div class="">
            <div class=" content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper">
                <div class="content-body">
                    <section class="">
                        <div class=" col-12">
                            <section id="advanced-search-datatable ">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card table-filters-design">
                                            <div class="d-flex v-row title-strip">
                                                <div class="col-6 pl-0">Facilitator / Consultant Management</div>
                                                <div class="col-6 pr-0">
                                                    <div class="filter-tab-btn card-option ">
                                                        <span class="minimize-card btn btn-primary "><i
                                                                data-feather="filter"></i>
                                                            Filters</span>
                                                        <span style="display:none"><i data-feather="filter"></i>
                                                            Filters</span></li>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--Search Form -->
                                            <div class="card-body pt-1" style="display:none;">
                                                <div method="POST">
                                                <div class="row">
                                                        <div class="col-3">
                                                            <div class="form-group">
                                                                <label>Name</label>
                                                                <input type="text" id="filterName"
                                                                    class="form-control dt-input dt-full-name"
                                                                    data-column="2" placeholder="Name" />
                                                            </div>
                                                        </div>

                                                        <div class="col-3">
                                                            <div class="form-group">
                                                                <label>Phone Number</label>
                                                                <input type="text" id="filterMobile"
                                                                    class="form-control dt-input dt-full-name"
                                                                    data-column="2" placeholder="Phone Number " />
                                                            </div>
                                                        </div>
                                                        <div class="col-3">
                                                            <div class="form-group">
                                                                <label>Email Address</label>
                                                                <input type="text" id="filterEmail"
                                                                    class="form-control dt-input dt-full-name"
                                                                    data-column="2" placeholder="Name " />
                                                            </div>
                                                        </div>

                                                        <div class="col-3">
                                                            <div class="form-group">
                                                                <label>Account Status</label>
                                                                <select id="filterStatus" class="form-control">
                                                                    <option value="">Select</option>
                                                                    <option value="0">In Active</option>
                                                                    <option value="1">Active </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-3">
                                                            <div class="form-group">
                                                                <label>Approval Status</label>
                                                                <select id="filterVerification_status" class="form-control">
                                                                    <option value="">Select</option>
                                                                    <option value="0">Pending</option>
                                                                    <option value="1">Approved</option>
                                                                    <option value="2">Disapproved </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-3">
                                                            <div class="form-group"style="margin-top: 7.7%;">
                                                                <button type="submit" class="btn btn-primary m0-auto"
                                                                    id="filterButton">Search</button>
                                                                <button type="submit" class="btn btn-secondary m0-auto "
                                                                    id="filterResetButton">Reset</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                            <div class="basic-datatable datatable-list">
                                                <table id="accessorTable"
                                                    class="dt-accessorlist table table-bordered table-responsive">
                                                    <thead>
                                                        <th></th>
                                                        <th>S.no</th>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Mobile</th>
                                                        <th>Account Status</th>
                                                        <th>Approval Status</th>
                                                        <th>Action</th>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    @component('components.bootstrap.side_modal', ['id' => 'addConsultantModal', 'heading' => 'Facilitator/Consultant Details'])
        <form id="addConsultantForm" method="POST" action="{{ route('admin.consultant.store') }}">

            <div class="form-label-group">
                <input type="text" name="name" maxlength="50" autocomplete="off"  class="form-control dt-input add-consultant-info-form-controls" id="addAccessorName" placeholder="Name*">
                <label>Name<span class="text-danger">*</span> </label>
                <span class="error" id="name-back-error"></span>
            </div>
            <div class="form-label-group">
                <input type="email" class="form-control add-consultant-info-form-controls" autocomplete="off" name="email" id="addAccessorEmail" placeholder="Email Address*">
                <label>Email Address<span class="text-danger">*</span> </label>
                <span class="error" id="email-back-error"></span>
            </div>
            <div class="form-label-group mb-1">
                <input class="form-control add-consultant-info-form-controls" onkeypress="$('#mobile-back-error').text(''); return /[0-9]/i.test(event.key)" maxlength="10" oninput="this.value=this.value.slice(0,this.maxLength)" name="mobile" id="addAccessorMobile" type="text" placeholder="Mobile Number*" autocomplete="false" tabindex="1">
                <label>Mobile Number<span class="text-danger">*</span> </label>
                <span class="error" id="mobile-back-error"></span>
            </div>
            <div class="form-label mb-2">
                <label class="font-small-4">Status<span class="text-danger">*</span></label>
                <div class=" mb-1 select-tag w-100">
                    <select id="addAccessorStatus" name="status" class="form-control add-consultant-info-form-controls">
                        <option value="">Select Status</option>
                        <option value="0">In Active</option>
                        <option value="1">Active </option>
                    </select>
                </div>
                <span class="error" id="status-back-error"></span>
            </div>
            <div class="v-row">
                <input type="checkbox" name="approveCheck" id="approveCheck" class="w-auto mr-50" value="1">
                <label for="approveCheck" class="font-small-4">Account Approve!</label>
            </div>
            <div class="form-label-group m0-auto text-center">
                <button type="submit" class="btn btn-primary m0-auto addAssessor_save_btn" id="AddConsultant">Submit</button>
            </div>
        </form>
    @endcomponent

    @component('components.bootstrap.side_modal', ['id' => 'editConsultantModal', 'heading' => 'Facilitator/Consultant Details'])
        <form action="" method="POST" id="editConsultantForm">
            @method("PUT")
            <div class="form-label-group ">
                <input type="text" maxlength="40" autocomplete="off" oninput="this.value = this.value.replace(/[^a-zA-Z ]/g, '').replace(/(\..*?)\..*/g, '$1');"  name="name" class="form-control edit-consultant-info-form-controls" id="editConsultantName" placeholder="Name*">
                <label class="font-small-4">Name<span class="text-danger">*</span> </label>
                <span class="error" id="name-back-error"></span>
            </div>
            <div class="form-label-group">
                <input type="email" class="form-control edit-consultant-info-form-controls" name="email" id="editConsultantEmail"
                    placeholder="Email Address*">
                <label class="font-small-4">Email Address<span class="text-danger">*</span> </label>
                <span class="error" id="email-back-error"></span>
            </div>
            <div class="form-label-group  mb-1">
                <input type="text" class="form-control edit-consultant-info-form-controls" onkeypress="$('#mobile-back-error').text(''); return /[0-9]/i.test(event.key)" maxlength="10" oninput="this.value=this.value.slice(0,this.maxLength)" name="mobile" id="editConsultantMobile" placeholder="Mobile Number*" autocomplete="false" tabindex="1">
                <label class="font-small-4">Mobile Number<span class="text-danger">*</span> </label>
                <span class="error" id="mobile-back-error"></span>
            </div>
            <div class="form-label mb-2">
                <label class="font-small-4">Status<span class="text-danger">*</span></label>
                <div class=" mb-1 select-tag w-100">
                    <select  id="editConsultantStatus" name="status" class="form-control edit-consultant-info-form-controls">
                        <option value="">Select Status</option>
                        <option value="0">In Active</option>
                        <option value="1">Active </option>
                    </select>
                </div>
                <span class="error" id="status-back-error"></span>
            </div>
            <div class="form-label-group m0-auto text-center">
                <button type="submit" class="btn btn-primary m0-auto addAssessor_save_btn" id="editConsultant">Submit</button>
            </div>
        </form>
    @endcomponent

    @component('components.bootstrap.remark_modal', ['id' => 'showRemarkModal', 'heading' => 'Remark'])
        <ul>
            <li> <div class="d-flex justify-content-between">
                    <div><span> Date </span> <label id="remarkDate"></label></div> <div><span> Status </span> <label class="badge mr-50" id="remarkstatus">Approve</label> </div></div>  </li>
            <li> <span>  Verified By </span><label id="remarkby"></label></li>
            <li class="bg-lightblue">
                <p id="remarkcontent" style="word-break: break-all;"></p>
            </li>
        </ul>
        <hr>
    @endcomponent

    @component('components.bootstrap.form_modal',['id'=>'changeStatusModal','heading'=>'Change Status'])
        <div id="changeStatusForm" action="">
            @csrf
            <div class="form-group">
                <label>Status</label>
                <select  id="changeAccessorStatus" name="status" class="form-control ">
                    <option value="">Select Status</option>
                    <option value="0">In Active</option>
                    <option value="1">Active </option>
                </select>
                <span id="status-error" class="error"></span>
            </div>
            <div class="col-12 text-center">
                <div class="form-group">
                    <button  type="submit" id="statusBulkUpdateSubmit" class="btn btn-primary m0-auto">Submit</button>
                </div>
            </div>
        </div>
    @endcomponent

@endsection


@push('PAGE_SCRIPTS')
    <script>
        var Accessor = function() {
            return {
                init: function() {
                    let dt_ajax_table = $('#accessorTable');
                    let export_url = "{{route('admin.consultant.bulk_export')}}?";
                    let main_export_url = export_url;
                    let dataTable = null;
                    let rows_selected = [];

                    if (dt_ajax_table.length) {

                        dataTable = dt_ajax_table.DataTable({
                            serverSide: true,
                            processing: true,
                            orderable: false,
                            ajax: {
                                url: "{{ route('admin.consultant.data') }}",
                                data: function(d) {
                                    d.name = $('#filterName').val();
                                    d.mobile = $('#filterMobile').val();
                                    d.email = $('#filterEmail').val();
                                    d.status = $('#filterStatus').val();
                                    d.verification_status = $('#filterVerification_status').val();
                                }
                            },
                            columns: [{
                                    data: 'id',
                                    name: 'id'
                                },
                                {
                                    data: 'DT_RowIndex',
                                    orderable: false,
                                    searchable: false
                                },
                                {
                                    data: 'name',
                                    name: 'name',
                                    orderable: false,
                                },
                                {
                                    data: 'email',
                                    name: 'email',
                                    orderable: false,
                                },
                                {
                                    data: 'mobile',
                                    name: 'mobile',
                                    orderable: false,
                                },
                                {
                                    data: 'status',
                                    name: 'status',
                                    orderable: false,
                                },
                                {
                                    data: 'verification_status',
                                    name: 'verification_status',
                                    orderable: false,
                                },
                                {
                                    data: 'id',
                                    name: 'id',
                                    orderable: false,
                                    searchable: false
                                }
                            ],

                            columnDefs: [

                                {
                                    targets: 0,
                                    orderable: false,
                                    render: function(data, t, a, s) {
                                        return "display" === t && (e =
                                            '<div class="custom-control custom-checkbox"> <input class=" selected-accessor custom-control-input dt-checkboxes" type="checkbox" value="' +
                                            data +
                                            '"  ><label class="custom-control-label"  for="checkbox2" ></label></div>'
                                        ), e
                                    },
                                    checkboxes: {
                                        selectRow: !0,
                                        selectAllRender: '<div class="custom-control custom-checkbox"> <input class="custom-control-input" type="checkbox" value="" id="checkboxSelectAll" /> <label class="custom-control-label" for="checkboxSelectAll"></label></div>'
                                    }
                                },
                                {
                                    // Label
                                    targets: 6,
                                    render: function(data, type, full, meta) {
                                        var $status_number = data;
                                        var $status = {
                                            0: {
                                                title: 'Pending',
                                                class: 'badge-light-warning'
                                            },
                                            1: {
                                                title: 'Approved',
                                                class: ' badge-light-success'
                                            },
                                            2: {
                                                title: 'Disapproved',
                                                class: ' badge-light-danger'
                                            },
                                        };
                                        if (typeof $status[$status_number] === 'undefined') {
                                            return data;
                                        }
                                        return ('<span class="badge badge-pill ' + $status[
                                                $status_number].class +
                                            '" target="_blank" >' +
                                            $status[$status_number].title + '</span>');
                                    }
                                },
                                {
                                    // Label
                                    targets: 5,
                                    render: function(data, type, full, meta) {
                                        var $status_number = full['status'];
                                        var $status = {
                                            1: {
                                                title: 'Active',
                                                class: 'badge-light-success'
                                            },
                                            0: {
                                                title: 'In Active',
                                                class: ' badge-light-danger'
                                            },
                                        };
                                        if (typeof $status[$status_number] === 'undefined') {
                                            return data;
                                        }
                                        return ('<span class="badge badge-pill ' + $status[
                                                $status_number].class +
                                            '" target="_blank" >' +
                                            $status[$status_number].title + '</span>');
                                    }
                                },

                                {
                                    // Actions
                                    targets: 7,
                                    title: 'Actions',
                                    orderable: false,
                                    render: function(data, type, full, meta) {

                                        let id = full['id'];

                                        let action='<div class="d-inline-flex">';

                                        action+='<a href="#" data-id="' + data +
                                            '" data-toggle = "tooltip" title data-original-title  = "View / Edit" class="action-icons  badge badge-light-primary mr-50 edit-accessor">' +  feather.icons['edit'].toSvg({
                                                class: 'font-small-4 '
                                            }) + '</a>';

                                        if(full['verification_status']==0){
                                            action+='<a href="#" data-id="' + data +
                                                '" data-toggle = "tooltip" title data-original-title = "Approve / Disapprove" class="action-icons badge badge-light-success mr-50 approve-consultant">'+ feather.icons['check-square'].toSvg({
                                                    class: 'font-small-4 '
                                                }) + '</a>';
                                        }

                                        if(full['verification_status']==2)
                                            {
                                                action+='<a href="#"  data-id="' + data +
                                                '" data-toggle = "tooltip" title data-original-title = "Approve" class="action-icons badge badge-light-success mr-50 approve-consultant1">'+ feather.icons['check-square'].toSvg({
                                                    class: 'font-small-4 '
                                                }) + '</a>' ;
                                            }

                                            if(full['verification_status']==2)
                                            {
                                                action+='<a href="#"  data-id="' + data +
                                                '" data-toggle = "tooltip" title data-original-title = "Remark" class="action-icons badge badge-light-danger mr-50 show-remark">'+ feather.icons['eye'].toSvg({
                                                    class: 'font-small-4 '
                                                }) + '</a>';
                                            }

                                        action+='</div>';

                                        return action;
                                    }
                                }

                            ],
                            dom: '<"row d-flex justify-content-between align-items-center m-1"' +
                                '<"col-lg-1 d-flex align-items-center"l>' + '<"col-lg-5 "f>' +
                                '<"col-lg-5 d-flex align-items-center justify-content-lg-end flex-wrap  p-0"<"dt-action-buttons text-right"B>>' +
                                '>t' + '<"d-flex justify-content-between mx-2 my-2"' +
                                '<"col-sm-12 col-md-6"i>' + '<"col-sm-12 col-md-6"p>' + '>',
                            // orderCellsTop: !0,
                            select: {
                                style: "multi",
                                selector: "td:first-child",
                                items: "row"
                            },
                            language: {
                                sLengthMenu: 'Show _MENU_',
                                search: '',
                                searchPlaceholder: 'Search',
                                paginate: {
                                    previous: '&nbsp;',
                                    next: '&nbsp;'
                                }
                            },
                            buttons: [{
                                extend: 'collection',
                                className: 'btn pr-1 btn-outline-secondary dropdown-toggle ml-1',
                                text: '<i class="fas fa-tasks "></i>' + 'Bulk Actions',
                                buttons: [{
                                        text: feather.icons['edit'].toSvg({
                                                class: 'font-small-4 mr-50'
                                        }) + 'Update Status ',
                                        className: 'dropdown-item',
                                        attr: {
                                            id: 'statusBulkUpdate'
                                        },
                                        action: function(e, dt, node, config) {
                                            var that = this;
                                            // show sweetalert ...
                                            if (rows_selected.length  > 0 ) {
                                            $("#changeStatusModal").modal('show');
                                            $('#statusBulkUpdateSubmit').attr('disabled',false);
                                            $('#statusBulkUpdateSubmit').html('Submit');
                                            }else{
                                                JsUtility.showToastr('error', 'Facilitator/Consultant', 'Please Select a Consultant');
                                            }
                                        }
                                    },
                                    {
                                        text: feather.icons['file-text'].toSvg({
                                            class: 'font-small-4 mr-50'
                                        }) + 'Export',
                                        className: 'dropdown-item',
                                        action:function(e, dt, node, config) {
                                            let redirect=encodeURI(export_url);
                                            window.location=redirect;
                                            // console.log(export_url)
                                        }
                                    }
                                ],
                            }, {
                                text: feather.icons['plus'].toSvg({
                                    class: 'mr-50 font-small-4'
                                }) + 'Add Consultant',
                                className: 'btn btn-primary btn-add-record ml-2',
                                attr: {
                                    "data-toggle": "modal",
                                    "data-target": "#addConsultantModal"
                                },
                                init: function(api, node, config) {
                                    $(node).removeClass('btn-secondary');
                                }
                            }
                            ],

                        });

                    }

                    dataTable.on( 'draw', function () {
                        $('.dt-checkboxes').prop('checked',false)
                        $('#checkboxSelectAll').prop('checked',false)
                    } );

                    $("#filterButton").click(function(e) {
                        e.preventDefault();
                        dataTable.draw();
                        export_url=main_export_url;
                        let parameter="";
                        if ($("#filterName").val()!="") {
                            export_url +=("name="+$("#filterName").val())
                            parameter="name";
                        }

                        if ($("#filterMobile").val()!="") {

                            if (parameter=="") {
                                export_url +=("mobile="+$("#filterMobile").val())
                            } else {
                                export_url +=("&mobile="+$("#filterMobile").val())
                            }
                            parameter="mobile";
                        }

                        if ($("#filterEmail").val()!="") {

                            if (parameter=="") {
                                export_url +=("email="+$("#filterEmail").val())
                            } else {
                                export_url +=("&email="+$("#filterEmail").val())
                            }
                            parameter="email";
                        }

                        if ($("#filterStatus").val()!="") {

                            if (parameter=="") {
                                export_url +=("status="+$("#filterStatus").val())
                            } else {
                                export_url +=("&status="+$("#filterStatus").val())
                            }
                            parameter="status";
                        }

                        if ($("#filterVerification_status").val()!="") {

                            if (parameter=="") {
                                export_url +=("verificationStatus="+$("#filterVerification_status").val())
                            } else {
                                export_url +=("&verificationStatus="+$("#filterVerification_status").val())
                            }
                            parameter="verificationStatus";
                        }
                    });

                    $("#filterResetButton").click(function(e) {
                        e.preventDefault();
                        $("#filterName").val('')
                        $("#filterMobile").val('')
                        $("#filterEmail").val('')
                        $("#filterStatus").val('')
                        $("#filterVerification_status").val('')

                        dataTable.draw();
                    });

                    // Custom Validations
                    jQuery.validator.addMethod("emailAddress", function(value, element) {
                    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
                    }, "Please Enter a Valid Email Address");

                    jQuery.validator.addMethod("noSpace", function(value, element) {
                    return value == '' || value.trim().length != 0;
                    }, "This field is required");

                    jQuery.validator.addMethod("doubleSpace", function(value, element) {
                    return this.optional(element) || /^\S((?!.*  ).*\S)?$/.test(value);
                    }, "This field is required");

                    jQuery.validator.addMethod("lettersonly", function(value, element)
                    {
                        return this.optional(element) || /^[A-Z," "]+$/i.test(value);
                    }, "Letters and spaces only please");

                    jQuery.validator.addMethod("mobileNo", function(value, element) {
                    return this.optional(element) || /^[6-9][0-9]{9}$/.test(value);
                    }, "Mobile Number is invalid");


                    $(document).on('click','#statusBulkUpdateSubmit' ,function () {
                        $('#statusBulkUpdateSubmit').attr('disabled',true);
                        $('#statusBulkUpdateSubmit').html('Loading...');
                            let update_url ="{{ route('admin.consultant.bulk_update') }}";
                            let status=$("#changeAccessorStatus").val();
                             if (rows_selected.length  > 0 ) {
                                $.ajax({
                                    type: "put",
                                    url: update_url,
                                    data: {
                                        consultant_id:rows_selected,
                                        status:status
                                    },
                                    success: function (response) {
                                        if (response.success) {
                                            $("#changeStatusModal").modal('hide');
                                            rows_selected=[];
                                            dataTable.draw();
                                            $("#changeAccessorStatus").val();
                                            JsUtility.showToastr('success', 'Assessor', response.message);
                                            $('#changeStatusForm').find(".error").html('');
                                        } else {
                                            $('#statusBulkUpdateSubmit').attr('disabled',false);
                                            $('#statusBulkUpdateSubmit').html('Submit');
                                            $.each(response.errors, function (key, error) {
                                                $('#changeStatusForm').find("#" + key + "-error").text(error[0]);
                                            });
                                        }
                                    }
                                });
                             }else{
                                JsUtility.showToastr('error', 'Assessor Update', 'Please Select accessor');
                             }

                    });

                    $(document).on('click', '.edit-accessor', function(e) {
                        e.preventDefault();
                        let id = $(this).data('id');
                        let url = "{{ route('admin.consultant.show', '') }}"
                        let form_url = "{{ route('admin.consultant.update', '') }}"
                        $('span.error').html('');
                        url += '/' + id;
                        form_url += '/' + id;
                        $.get(url, function(data) {
                            $("#editConsultantName").val(data.name);
                            $("#editConsultantEmail").val(data.email);
                            $("#editConsultantMobile").val(data.mobile);
                            $("#editConsultantStatus").val(data.status);
                            $("#editConsultantModal").modal('show');
                            $("#editConsultantForm").attr('action', form_url);
                        });

                    });

                    $('#editConsultantForm').validate({
                        rules:
                        {
                            'name': {
                                required: true,
                                noSpace: true,
                                doubleSpace:true,
                                maxlength: 50,
                                minlength:3
                            },
                            'email': {
                                required: true,
                                emailAddress: true
                            },
                            'mobile':{
                                required: true,
                                noSpace: true,
                                digits:true,
                                minlength:10,
                                mobileNo: true
                            },
                            // 'status':{
                            //     required: true,
                            // }
                        },

                        messages:
                        {
                            name : {
                                required:"The Name field is required",
                                noSpace: "Please Enter a Valid Name",
                                doubleSpace: "Please Enter a Valid Name"
                            },
                            email: {
                                required:"The Email Address field is required",
                                email : "Please Enter a Valid Email Address",
                                noSpace: "Please enter a valid email address",
                                doubleSpace: "Please enter a valid email address"
                            },
                            mobile: {
                                required:"The Mobile Number field is required",
                            },
                            // status: {
                            //     required:"The Status field is required",
                            // }
                        },

                        submitHandler : function(form){
                            $('#editConsultant').attr('disabled',true);
                            $('#editConsultant').html('Loading...');
                            $('form#editConsultantForm').find("-back-error").css('display','none');
                            let url = $('form#editConsultantForm').attr('action');

                            $.ajax({
                                method: "post",
                                url: $('form#editConsultantForm').attr('action'),
                                data: $(form).serialize()
                            }).done(function(response) {
                                    $('#editConsultant').attr('disabled',false);
                                    $('#editConsultant').html('Submit');
                                if (response.success) {
                                    dataTable.draw();
                                    $('form#editConsultantForm').find("-back-error").css('display','none');
                                    $('form#editConsultantForm').find("-back-error").html('');
                                    $('#editConsultant').attr('disabled',false);
                                    $('#editConsultant').html('Submit');
                                    $("#editConsultantModal").modal('hide');
                                    JsUtility.showToastr('success', 'Facilitator / Consultant', response.message);
                                } else {
                                    form.find('span.error').html('');
                                    $('form#editConsultantForm').find("-back-error").css('display','none');
                                    $.each(response.errors, function(index, item) {
                                        $('form#editConsultantForm').find("#" + index).removeClass('error').addClass('error');
                                        $('form#editConsultantForm').find("#" + index + "-back-error").text(item[0]);
                                        $('form#editConsultantForm').find("#" + index + "-back-error").css('display','block');
                                    });
                                    // JsUtility.showToastr('error', 'Assessor', 'Some error occured!');
                                }
                            }).fail(function(jqXHR, response) {
                                $('form#editConsultantForm').find("-back-error").css('display','none');
                                $("#AddConsultant").attr("disabled",false);
                                $("#AddConsultant").html("Submit");
                                JsUtility.showToastr('error', 'Facilitator / Consultant', 'Some error occured!');
                            });
                        }
                    });

                    $('#addConsultantForm').validate({
                        rules: {

                            'name': {
                                required: true,
                                noSpace: true,
                                doubleSpace:true,
                                maxlength: 50,
                                minlength:3
                            },
                            'email': {
                                required: true,
                                emailAddress: true
                            },
                            'mobile':{
                                required: true,
                                noSpace: true,
                                digits:true,
                                minlength:10,
                                mobileNo: true
                            },
                            // 'status':{
                            //     required: true,
                            // }
                        },

                        messages: {
                            name : {
                                required:"The Name field is required",
                                noSpace: "Please Enter a Valid Name",
                                doubleSpace: "Please Enter a Valid Name"
                            },
                            email: {
                                required:"The Email Address field is required",
                                email : "Please Enter a Valid Email Address",
                                noSpace: "Please enter a valid email address",
                                doubleSpace: "Please enter a valid email address"
                            },
                            mobile: {
                                required:"The Mobile Number field is required",
                            },
                            // status: {
                            //     required:"The Status field is required",
                            // }
                        },


                        submitHandler : function(form){
                            $("#AddConsultant").attr("disabled",true);
                            $("#AddConsultant").html("Loading...");
                            $('form#addConsultantForm').find("-back-error").css('display','none');
                            let url = $('form#addConsultantForm').attr('action');

                            $.ajax({
                                method: "post",
                                url: $('form#addConsultantForm').attr('action'),
                                data: $(form).serialize()
                            }).done(function(response) {
                                $("#AddConsultant").attr("disabled",false);
                                $("#AddConsultant").html("Submit");
                                if (response.success) {
                                    dataTable.draw();
                                    $("#addConsultantForm").trigger("reset");
                                    $("#addConsultantModal").modal('hide');
                                    $("#addConsultantForm span").html('');
                                    JsUtility.showToastr('success', 'Facilitator / Consultant', response.message);
                                } else {
                                    form.find('span.error').html('');
                                    $('form#addConsultantForm').find("-back-error").css('display','none');
                                    $.each(response.errors, function(index, item) {
                                        $('form#addConsultantForm').find("#" + index).removeClass('error').addClass('error');
                                        $('form#addConsultantForm').find("#" + index + "-back-error").text(item[0]);
                                        $('form#addConsultantForm').find("#" + index + "-back-error").css('display','block');
                                    });
                                    // JsUtility.showToastr('error', 'Assessor', 'Some error occured!');
                                }
                            }).fail(function(jqXHR, response) {
                                $('form#addConsultantForm').find("-back-error").css('display','none');
                                $("#AddConsultant").attr("disabled",false);
                                $("#AddConsultant").html("Submit");
                                JsUtility.showToastr('error', 'Facilitator / Consultant', 'Some error occured!');
                            });
                        }
                    });

                    $(document).on('click', '.show-remark', function(e) {
                        e.preventDefault();
                        let id = $(this).data('id');
                        let url = "{{ route('admin.assessors.logs', '') }}";
                        url += '/' + id;
                        $.get(url, function(data) {
                            const d = new Date(data.verified_at)
                            const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d)
                            const mo = new Intl.DateTimeFormat('en', { month: 'long' }).format(d)
                            const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d)
                            document.getElementById("remarkDate").innerHTML = da+' '+ mo + ','+ ye;
                            document.getElementById("remarkby").innerHTML = data.name;
                            if(data.verified_status==2)
                            {
                                document.getElementById("remarkstatus").innerHTML = 'Disapproved';
                                $("#remarkstatus").addClass('badge-light-danger');
                            }
                            else{
                                document.getElementById("remarkstatus").innerHTML = 'Approved';
                                $("#remarkstatus").addClass('badge-light-success');
                            }
                            document.getElementById("remarkcontent").innerHTML = data.verified_remark;
                            $("#showRemarkModal").modal('show');
                        });
                    });

                    $(document).on('click','.approve-consultant1',function (e) {
                        e.preventDefault();
                        let vender_id = $(this).data('id');
                        let approve_url="{{route('admin.consultant.approve','')}}"
                        approve_url+='/'+vender_id;
                        Swal.fire({
                            title: 'Do you want to Approve?',
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Yes, Approve it!',
                            showDenyButton: false,
                            showClass: {
                                popup: 'animate__animated animate__fadeIn'
                            },
                            customClass: {
                                confirmButton: 'btn btn-primary',
                                cancelButton: 'btn btn-outline-danger ml-1',
                            },
                            buttonsStyling: false
                        }).then((result) => {
                            /* Read more about isConfirmed, isDenied below */
                            if (result.isConfirmed) {
                                $.get(approve_url, {status:1},
                                    function (responce) {
                                        if (responce.success) {
                                            Swal.fire({
                                                icon: 'success',
                                                title: 'Approved!',
                                                confirmButtonText: 'Okay',
                                                text: 'Your Consultant has been Approved.',
                                                showClass: {
                                                    popup: 'animate__animated animate__fadeIn'
                                                },
                                                customClass: {
                                                    confirmButton: 'btn btn-primary'
                                                },
                                                buttonsStyling: false
                                            }).then((result2) => {
                                            if (result2.isConfirmed) {
                                                dataTable.draw();
                                            location.reload();
                                            }
                                            else if(Swal.DismissReason.backdrop) {
                                                dataTable.draw();
                                                location.reload();
                                                }
                                            })
                                        }else{
                                            Swal.fire('Approve!',responce.message, 'error');
                                        }
                                    }
                                );

                            } else if (result.isDenied) {
                                Swal.fire({
                                    title: 'Reason for Rejection!',
                                    input: 'textarea',
                                    showCancelButton: true,
                                    confirmButtonText: 'Submit',
                                    showClass: {
                                        popup: 'animate__animated animate__fadeIn'
                                    },
                                    customClass: {
                                        confirmButton: 'btn btn-primary',
                                        cancelButton: 'btn btn-outline-danger ml-1',
                                        denyButton: 'btn btn-secondary ml-1',
                                    },
                                    buttonsStyling: false,
                                    preConfirm: (remark) => {
                                        $.get(approve_url, {status:2,remark:remark},
                                        function (responce) {
                                            if (responce.success) {
                                                    Swal.fire('Disapproved!', responce.message, 'success');
                                                    dataTable.draw();
                                                }else{
                                                    Swal.fire('Disapproved!', responce.message, 'error');
                                                }
                                            }
                                        );
                                    },
                                })
                            }
                        })

                    });

                    $(document).on('click','.approve-consultant',function (e) {
                        e.preventDefault();
                        let vender_id = $(this).data('id');
                        let approve_url="{{route('admin.consultant.approve','')}}"
                        approve_url+='/'+vender_id;
                        Swal.fire({
                            title: 'Do you want to Approve?',
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Yes, Approve it!',
                            showDenyButton: true,
                            denyButtonText: 'No, Disapprove it',
                            showClass: {
                                popup: 'animate__animated animate__fadeIn'
                            },
                            customClass: {
                                confirmButton: 'btn btn-primary',
                                cancelButton: 'btn btn-outline-danger ml-1',
                                denyButton: 'btn btn-secondary ml-1',
                            },
                            buttonsStyling: false
                        }).then((result) => {
                            /* Read more about isConfirmed, isDenied below */
                            if (result.isConfirmed) {
                                $.get(approve_url, {status:1},
                                    function (responce) {
                                        if (responce.success) {
                                            Swal.fire({
                                                icon: 'success',
                                                title: 'Approved!',
                                                confirmButtonText: 'Okay',
                                                text: 'Your Consultant has been Approved.',
                                                showClass: {
                                                    popup: 'animate__animated animate__fadeIn'
                                                },
                                                customClass: {
                                                    confirmButton: 'btn btn-primary'
                                                },
                                                buttonsStyling: false
                                            }).then((result2) => {
                                            if (result2.isConfirmed) {
                                                dataTable.draw();
                                            location.reload();
                                            }
                                            else if(Swal.DismissReason.backdrop) {
                                                dataTable.draw();
                                                location.reload();
                                                }
                                            })
                                        }else{
                                            Swal.fire('Approve!',responce.message, 'error');
                                        }
                                    }
                                );

                            } else if (result.isDenied) {
                                Swal.fire({
                                    title: 'Reason for Disapproval!',
                                    input: 'textarea',
                                    showCancelButton: true,
                                    confirmButtonText: 'Submit',
                                    showClass: {
                                        popup: 'animate__animated animate__fadeIn'
                                    },
                                    customClass: {
                                        confirmButton: 'btn btn-primary',
                                        cancelButton: 'btn btn-outline-danger ml-1',
                                        denyButton: 'btn btn-secondary ml-1',
                                    },
                                    buttonsStyling: false,
                                    preConfirm: (remark) => {
                                        $.get(approve_url, {status:2,remark:remark},
                                        function (responce) {
                                            if (responce.success) {
                                                    Swal.fire('Disapproved!', responce.message, 'success');
                                                    dataTable.draw();
                                                }else{
                                                    Swal.fire('Disapproved!', responce.message, 'error');
                                                }
                                            }
                                        );
                                    },
                                })
                            }
                        })

                    });

                    $(document).on('change', '.selected-accessor', function() {
                        var id = $(this).val();
                        // Determine whether row ID is in the list of selected row IDs
                        var index = $.inArray(id, rows_selected);
                        // If checkbox is checked and row ID is not in list of selected row IDs
                        if (this.checked && index === -1) {
                            rows_selected.push(id);
                        } else if (!this.checked && index !== -1) {
                            rows_selected.splice(index, 1);
                        }
                    })




                    // $(document).on('click', '#exportExceleData', function(e) {
                    //     e.preventDefault();

                    //     var url = "{{ route('admin.assessors.bulk_export') }}?export=1";

                    //     if ($('body').find('div.dataTables_filter input[type="search"]').val() != '') url +=
                    //         "&fsearch=" + encodeURIComponent($('#fsearch').val());

                    //     window.location.href = url;
                    // });

                }

            }

        }();

        jQuery(document).ready(function() {
            Accessor.init();

            //Add Facilitator Form
            $('.add-consultant-info-form-controls').blur(function(){
                $("#addConsultantForm").validate().element(this);
            });

            $('.add-consultant-info-form-controls').on('keyup',function(event){
                $("#addConsultantForm").validate().element(this);
            });

            //Edit Facilitator Form
            $('.edit-consultant-info-form-controls').blur(function(){
                $("#editConsultantForm").validate().element(this);
            });

            $('.edit-consultant-info-form-controls').on('keyup',function(event){
                $("#editConsultantForm").validate().element(this);
            });

            // capitalize name input text fields
            $('input[type=text]').on('keyup',function(event){
                event.currentTarget.value = this.value.toUpperCase();
            });
        });
    </script>
@endpush
