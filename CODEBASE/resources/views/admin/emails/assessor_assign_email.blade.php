<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OTP Email</title>
    <!--bootstrap css -->
    <link rel="stylesheet" type="text/css" href="{{url('app-assets/mail-assets/css/bootstrap.min.css')}}">
    <!--fontawesome icons-->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <!--Custom css-->
    <style>
        * {
            font-family: Century Gothic !important;
            color: #000;
        }

        @font-face {
            font-family: Century Gothic !important;
            src: url('app-assets/mail-assets/fonts/GOTHIC.TTF');
        }

        li {
            display: inline-block;
            margin-left: 0;
            border: 1px solid #ccc;
            padding: 2px;
            border-radius: 50%;
            width: 35px;
            height: 35px;
            margin-right: 6px;
        }

        ul {
            padding-left: 0;
            margin: 0;
        }
        button
        {
            font-size: 16px;
    color: #fff;
    text-decoration: underline;
    padding: 9px 25px;
    border-radius: 30px;
    background: #b57d53;
    border: none;
    opacity: 1 !important;
        }
    </style>
</head>

<body>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f0f4f7" class="gwfw">
        <tbody>
            <tr>
                <td align="center" valign="top" class="py-50 mpy-10 px-10" style="padding-top: 30px; padding-bottom: 30px; padding-left: 10px; padding-right: 10px;">
                    <table width="550" border="0" cellspacing="0" cellpadding="0" class="m-shell" style="box-shadow: 0 1rem 3rem rgba(0,0,0,.175)">
                        <tbody>
                            <tr>
                                <td class="td" style="width:600px; min-width:600px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
                                    <!-- Header -->
                                
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="background-body" style="    background: #a06942;
    padding-bottom: 2px;">
                                        <tbody>
                                            <tr>
                                                <td class="es-p30t es-p30b">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                                                        <tbody>
                                                            <tr>
                                                                <td class="py-50 px-50 mpy-30 mpx-20" style="
                                                                        padding-top: 0px;
                                                                        padding-bottom: 35px;
                                                                    padding-left: 50px; padding-right: 50px;">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                        <tr>
                                                                                <td class="text-14 t-color a-center pb-18 tt-u fw-b" style="font-family:'Montserrat', Arial, sans-serif; font-size:17px; line-height:31px; min-width:auto !important; color:#e75e83; text-align:left; padding-bottom: 18px; font-weight:400;color:#2b2929"> Dear {{$user->name}},</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-14 t-color a-center pb-18 tt-u fw-b" style="font-family:'Montserrat', Arial, sans-serif; font-size:17px; line-height:31px; min-width:auto !important; color:#e75e83; text-align:left; padding-bottom: 18px; font-weight:400;color:#2b2929"> Greetings !</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-14 t-color a-center pb-18 tt-u fw-b" style="font-family:'Montserrat', Arial, sans-serif; font-size:17px; line-height:31px; min-width:auto !important; color:#e75e83; text-align:left; padding-bottom: 18px; font-weight:400;color:#2b2929"> Following Sites have been allotted to you.</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-14 t-color a-center pb-18 tt-u fw-b" style="font-family:'Montserrat', Arial, sans-serif; font-size:17px; line-height:31px; min-width:auto !important; color:#e75e83; text-align:left; padding-bottom: 18px; font-weight:400;color:#2b2929">
                                                                                    <table>
                                                                                        <tbody style="width: 100%;border: 1px solid;" >
                                                                                            <tr>
                                                                                                <th>Plant: </th>
                                                                                                <td> {{$plant->plant_size}} </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-14 t-color a-center pb-18 tt-u fw-b" style="font-family:'Montserrat', Arial, sans-serif; font-size:17px; line-height:31px; min-width:auto !important; color:#e75e83; text-align:left; padding-bottom: 18px; font-weight:400;color:#2b2929"> Kindly confirm your acceptance for completing the onsite assessment.</td>
                                                                            </tr>
                                                                            
                                                                            <tr>
                                                                                <td class="text-14 t-color a-center pb-18 tt-u fw-b" style="font-family:'Montserrat', Arial, sans-serif; font-size:17px; line-height:31px; min-width:auto !important; color:#e75e83; text-align:center; padding-bottom: 18px; font-weight:400;color:#2b2929"> Best regards </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-14 t-color a-center pb-18 tt-u fw-b" style="font-family:'Montserrat', Arial, sans-serif; font-size:17px; line-height:31px; min-width:auto !important; color:#e75e83; text-align:center; padding-bottom: 18px; font-weight:400;color:#2b2929"> Greenco Team </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- Footer -->
                    <table width="100%" border="0" bgcolor="#ffffff" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td align="center" valign="top">
                                    <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
                                        <tbody>
                                            <tr>
                                                <td class="plr-15" style="padding:0px;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>

                                                            <tr>
                                                                <td>
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="    padding-top: 25px;">
                                                                        <tbody>
                                                                            <tr>
                                                                                <th class="column-top" width="54%" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="    padding-bottom: 0px;">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td class="text-14 lh-28 c-grey mt-center" style="font-family:'Montserrat', Arial, sans-serif; font-size:14px; text-align:left; min-width:auto !important; line-height:28px; color:#555555;">
                                                                                                    <p class="mb-2" style="font-size: 19px;    text-align: left;margin-bottom:15px;">Follow us on</p>
                                                                                                    <div class="">
                                                                                                        <ul class="contact-details">
                                                                                                            <li><a href="#"><img src="http://13.126.160.234/greenco/app-assets/mail-assets/images/instagram.png" width="25px" alt="" style=" padding: 6px; width: 35px;"></a></li>
                                                                                                            <li> <a href="#"><img src="http://13.126.160.234/greenco/app-assets/mail-assets/images/facebook.png" width="25px" alt="" style=" padding:6px; width: 35px;"></a></li>
                                                                                                            <li> <a href="#"><img src="http://13.126.160.234/greenco/app-assets/mail-assets/images/twitter.png" width="25px" alt="" style="padding: 6px; width: 24px;"></a></li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </th>
                                                                                <th class="column mpb-20" width="4%" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
                                                                                </th>
                                                                                <th class="column-bottom" width="42%" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; ">
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td class="text-14 lh-28 c-grey mt-center" style="font-family:'Montserrat', Arial, sans-serif; font-size:14px;  min-width:auto !important; line-height:28px; color:#555555;">
                                                                                                    <p style="font-size: 19px;    text-align: right;margin-bottom:15px;"> Contact Us </p>
                                                                                                    <p style="text-align:right;"> +91 80728 86122 <br> contact@cii.com </p>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </th>
                                                                            </tr>

                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="background-body">
                                        <tbody>
                                            <tr>
                                                <td class="pb-2">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="border-radius: 4px 4px 0px 0px;">
                                                        <tbody>
                                                            <tr>
                                                                <td class="py-25 px-50 mpx-20" style="padding-top: 15px; padding-bottom: 15px; padding-left: 50px; padding-right: 50px;">
                                                                    <table border="0" cellpadding="0" cellspacing="0" class="body" style="border:0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; ">
                                                                        <tr style="background:#fff;border-top:0;border-bottom:0">
                                                                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
                                                                            <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 750px; width: 750px;">
                                                                                <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 750px;">
                                                                                    <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;margin:0 auto;padding:12px 12px; background: #ffffff; border-radius: 3px;">
                                                                                        <tr>
                                                                                            <div style="  text-align: center; padding: 0px; color: #fff;">
                                                                                            <a href="#"><button class="visit-sitelink">Visit Website <span class='arrow'></span></button></a>

                                                                                            </div>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- END Footer -->
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>
