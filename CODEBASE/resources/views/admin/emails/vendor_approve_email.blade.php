<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OTP Email</title>
    <!--bootstrap css -->
    <link rel="stylesheet" type="text/css" href="{{url('app-assets/mail-assets/css/bootstrap.min.css')}}">
    <!--fontawesome icons-->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <!--Custom css-->
    <style>
        * {
            font-family: Century Gothic !important;
            color: #000;
        }

        @font-face {
            font-family: Century Gothic !important;
            src: url('app-assets/mail-assets/fonts/GOTHIC.TTF');
        }

        li {
            display: inline-block;
            margin-left: 0;
            border: 1px solid #ccc;
            padding: 2px;
            border-radius: 50%;
            width: 35px;
            height: 35px;
            margin-right: 6px;
        }

        ul {
            padding-left: 0;
            margin: 0;
        }
        button
        {
            font-size: 16px;
    color: #fff;
    text-decoration: underline;
    padding: 9px 25px;
    border-radius: 30px;
    background: #b57d53;
    border: none;
    opacity: 1 !important;
        }
    </style>
</head>

<body>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f0f4f7" class="gwfw">
        <tbody>
            <tr>
                <td align="center" valign="top" class="py-50 mpy-10 px-10" style="padding-top: 30px; padding-bottom: 30px; padding-left: 10px; padding-right: 10px;">
                    <table width="550" border="0" cellspacing="0" cellpadding="0" class="m-shell" style="box-shadow: 0 1rem 3rem rgba(0,0,0,.175)">
                        <tbody>
                            <tr>
                                <td class="td" style="width:600px; min-width:600px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
                                    <!-- Header -->
                                    
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="background-body" style="    background: #a06942;
    padding-bottom: 2px;">
                                        <tbody>
                                            <tr>
                                                <td class="es-p30t es-p30b">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                                                        <tbody>
                                                            <tr>
                                                                <td class="py-50 px-50 mpy-30 mpx-20" style="
    padding-top: 0px;
    padding-bottom: 35px;
 padding-left: 50px; padding-right: 50px;">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tbody>
                                                                        @if ($status==1)
                                                                            <tr>
                                                                                <td class="text-14 t-color a-center pb-18 tt-u fw-b" style="font-family:'Montserrat', Arial, sans-serif; font-size:17px; line-height:31px; min-width:auto !important; color:#e75e83; text-align:center; padding-bottom: 18px; font-weight:400;color:#2b2929">Dear {{$company_name}},</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-14 t-color a-center pb-18 tt-u fw-b" style="font-family:'Montserrat', Arial, sans-serif; font-size:17px; line-height:31px; min-width:auto !important; color:#e75e83; text-align:center; padding-bottom: 18px; font-weight:400;color:#2b2929">Greetings from CII!</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-14 t-color a-center pb-18 tt-u fw-b" style="font-family:'Montserrat', Arial, sans-serif; font-size:17px; line-height:31px; min-width:auto !important; color:#e75e83; text-align:center; padding-bottom: 18px; font-weight:400;color:#2b2929"> Your profile has been approved by Admin. </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-14 t-color a-center pb-18 tt-u fw-b" style="font-family:'Montserrat', Arial, sans-serif; font-size:17px; line-height:31px; min-width:auto !important; color:#e75e83; text-align:center; padding-bottom: 18px; font-weight:400;color:#2b2929"> You can now login the (portal) by using the following credentials.</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-14 t-color a-center pb-18 tt-u fw-b" style="font-family:'Montserrat', Arial, sans-serif; font-size:17px; line-height:31px; min-width:auto !important; color:#e75e83; text-align:center; padding-bottom: 18px; font-weight:400;color:#2b2929"> Email: {{$email}}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-14 t-color a-center pb-18 tt-u fw-b" style="font-family:'Montserrat', Arial, sans-serif; font-size:25px; line-height:31px; min-width:auto !important; color:#e75e83; text-align:center; padding-bottom: 18px; font-weight:400;color:#2b2929">We look forward to receiving your application for the rating.</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-14 t-color a-center pb-18 tt-u fw-b" style="font-family:'Montserrat', Arial, sans-serif; font-size:17px; line-height:31px; min-width:auto !important; color:#e75e83; text-align:center; padding-bottom: 18px; font-weight:400;color:#2b2929"> Best regards </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-14 t-color a-center pb-18 tt-u fw-b" style="font-family:'Montserrat', Arial, sans-serif; font-size:17px; line-height:31px; min-width:auto !important; color:#e75e83; text-align:center; padding-bottom: 18px; font-weight:400;color:#2b2929"> Greenco Team </td>
                                                                            </tr>
                                                                        @elseif($status==2)
                                                                            <tr>
                                                                                <td class="text-14 t-color a-center pb-18 tt-u fw-b" style="font-family:'Montserrat', Arial, sans-serif; font-size:17px; line-height:31px; min-width:auto !important; color:#e75e83; text-align:center; padding-bottom: 18px; font-weight:400;color:#2b2929">Dear {{$company_name}},</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-14 t-color a-center pb-18 tt-u fw-b" style="font-family:'Montserrat', Arial, sans-serif; font-size:17px; line-height:31px; min-width:auto !important; color:#e75e83; text-align:center; padding-bottom: 18px; font-weight:400;color:#2b2929">Greetings from CII!</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-14 t-color a-center pb-18 tt-u fw-b" style="font-family:'Montserrat', Arial, sans-serif; font-size:17px; line-height:31px; min-width:auto !important; color:#e75e83; text-align:center; padding-bottom: 18px; font-weight:400;color:#2b2929"> Kindly contact Mr Sivagurunathan S (+91-9717753520, s.sivagurunathan@cii.in) for completing the registration of your profile in (portal). </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-14 t-color a-center pb-18 tt-u fw-b" style="font-family:'Montserrat', Arial, sans-serif; font-size:17px; line-height:31px; min-width:auto !important; color:#e75e83; text-align:center; padding-bottom: 18px; font-weight:400;color:#2b2929"> Best regards </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="text-14 t-color a-center pb-18 tt-u fw-b" style="font-family:'Montserrat', Arial, sans-serif; font-size:17px; line-height:31px; min-width:auto !important; color:#e75e83; text-align:center; padding-bottom: 18px; font-weight:400;color:#2b2929"> Greenco Team </td>
                                                                            </tr>
                                                                        @endif
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- Footer -->

                    <!-- END Footer -->
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>