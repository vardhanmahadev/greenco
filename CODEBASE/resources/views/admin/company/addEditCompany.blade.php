,@extends(ADMIN_THEME_NAME.'.layouts.app')

@push('PAGE_ASSETS_CSS')
<link rel="stylesheet" type="text/css" href="{{url('app-assets/vendors/css/forms/wizard/bs-stepper.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('app-assets/css/plugins/forms/form-validation.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('app-assets/css/plugins/forms/form-wizard.min.css')}}">
<style>
    .txt-c{
        text-align:center;
    }
</style>
@endpush

@section('content')
<div class="app-content content content-ps-stl pt-4">
    <div class="">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <section class="p-relative">
                    <ul class="nav nav-pills tabs-pages tab-leftside">
                        <li class="nav-item">
                            <a class="nav-link active" href="{{route('admin.company.index')}}">
                                <span class="bs-stepper-box">
                                    <i class="fal fa-long-arrow-left"></i>
                                </span>Company Management </a>
                        </li>
                    </ul>
                    <p class="edit-tab">@if(isset($company))Edit Company? @else Add Company? @endif <a href="#" onclick="goBack()"><i class="fal fa-times"></i></a></p>
                </section>
                <section class="col-lg-12 horizontal-wizard">
                    <div class="bs-stepper horizontal-wizard-example">
                        <div class="bs-stepper-header company-profile-info" role="tablist">
                            <div class="step" data-target="#contact-details" role="tab" id="contact-details-trigger">
                                <button type="button" class="step-trigger">
                                    <span class="bs-stepper-box">1</span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">Basic Info</span>
                                    </span>
                                </button>
                            </div>
                            <div class="line">
                                <i data-feather="chevron-right" class="font-medium-2"></i>
                            </div>
                            <div class="step" data-target="#industry-details" role="tab" id="industry-details-trigger">
                                <button type="button" class="step-trigger">
                                    <span class="bs-stepper-box">2</span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">Industry Details</span>
                                    </span>
                                </button>
                            </div>
                            <div class="line">
                                <i data-feather="chevron-right" class="font-medium-2"></i>
                            </div>
                            <div class="step" data-target="#infrastructure-step" role="tab" id="infrastructure-step-trigger">
                                <button type="button" class="step-trigger">
                                    <span class="bs-stepper-box">3</span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">Infrastructure Details</span>
                                    </span>
                                </button>
                            </div>
                            <div class="line">
                                <i data-feather="chevron-right" class="font-medium-2"></i>
                            </div>
                            <div class="step" data-target="#income-details" role="tab" id="income-details-trigger">
                                <button type="button" class="step-trigger">
                                    <span class="bs-stepper-box">4</span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">Income Details</span>
                                    </span>
                                </button>
                            </div>
                            <!-- <div class="line">
                                <i data-feather="chevron-right" class="font-medium-2"></i>
                            </div>
                            <div class="step" data-target="#document-upload" role="tab" id="document-upload-trigger">
                                <button type="button" class="step-trigger">
                                    <span class="bs-stepper-box">5</span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">Document Upload</span>
                                    </span>
                                </button>
                            </div> -->
                        </div>
                        <div class="bs-stepper-content">
                            <div id="contact-details" class="content mt-2" role="tabpanel" aria-labelledby="contact-details-trigger">
                                <form id="basic_info_form" class="basic_info_form" method="post" onsubmit="return false" action="{{isset($company)?route('admin.company.update',$company->id):route('admin.company.store')}}">
                                    @csrf
                                    @isset($company)
                                        @method('PUT')
                                    @endisset
                                    <input type="hidden" name="tab" id="tab" value="1">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label class="form-label font-small-4">Name of the Company<span class="text-danger">*</span></label>
                                                <input type="text" maxlength="50" value="{{isset($company)?$company->name:''}}"  placeholder="Company Name*" name="company_name"  id="company_name" class="ui-autocomplete-input form-control form-caontrol-merge border-right-line basic-info-form-controls" autocomplete="off">
                                                <span class="error" id="company_name-back-error"></span>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label font-small-4">Phone Number<span class="text-danger">*</span></label>
                                                <input class="form-control form-caontrol-merge border-right-line basic-info-form-controls" value="{{isset($company)?$company->mobile:''}}" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="company_mobile" id="company_mobile" maxlength="10" min="0"  oninput="this.value=this.value.slice(0,this.maxLength)" type="number" placeholder="Phone Number*" autocomplete="false" />
                                                <span class="error" id="company_mobile-back-error"></span>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label font-small-4">Email Address<span class="text-danger">*</span></label>
                                                <input class="form-control form-caontrol-merge border-right-line basic-info-form-controls" onkeypress=" $('#company_email-back-error').text('');" value="{{isset($company)?$company->email:''}}" name="company_email" id="company_email" type="email" placeholder="Email Address*" autocomplete="false" />
                                                <span class="error" id="company_email-back-error"></span>
                                            </div>
                                            <!-- <div class="form-group mt--1">
                                                <label class="form-label float-label font-small-4" for="state">State<span class="text-danger">*</span></label>
                                                <select class="form-control basic-info-form-controls" name="state" id="state" data-placeholder="Select State">
                                                    <option value="">Select State</option>
                                                    @foreach($states as $state)
                                                        <option value="{{$state->id}}" @if(isset($company))@if($company->state == $state->id) selected @endif @endif>{{$state->name}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="error" id="state-back-error"></span>
                                            </div> -->
                                          @if(isset($company))

                                          @else
                                            <div class="form-label-group">
                                                <div class="mb-1">
                                                    <div class="custom-control custom-checkbox">
                                                        <input class="custom-control-input basic-info-form-controls" id="account_approve" name="account_approve" type="checkbox" tabindex="3" >
                                                        <label class="custom-control-label font-small-4" for="account_approve"> Account Approve</label>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label class="form-label select-control-label" for="state">State<span class="text-danger">*</span></label>
                                                <select class="form-control basic-info-form-controls" name="state" id="state" data-placeholder="Select State">
                                                    <option value="">Select State</option>
                                                    @foreach($states as $state)
                                                        <option value="{{$state->id}}" @if(isset($company))@if($company->state == $state->id) selected @endif @endif>{{$state->name}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="error" id="state-back-error"></span>
                                            </div>


                                            <div class="form-group">
                                                <label class="form-label font-small-4" for="company_location">City<span class="text-danger">*</span></label>
                                                <input class="form-control border-right-line basic-info-form-controls" onkeypress="return /[a-zA-Z\s]/i.test(event.key)" maxlength="50" value="{{isset($company)?$company->location:''}}" name="company_location" id="company_location" type="text"  placeholder="City*" autocomplete="false" />
                                                <span class="error" id="company_location-back-error"></span>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label select-control-label font-small-4">Account Status<span class="text-danger">*</span></label>
                                                <select class="form-control basic-info-form-controls" name="account_status" id="account_status" placeholder="Select Status">
                                                    <option value="">Select Status</option>
                                                    <option value="1"@if(isset($company))@if($company->account_status=="1") selected @endif @endif>Active</option>
                                                    <option value="0"@if(isset($company))@if($company->account_status=="0") selected @endif @endif>In-Active</option>
                                                </select>
                                                <span class="error" id="account_status-back-error"></span>
                                            </div>

                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label class="form-label" for="postal_address">Postal Address<span class="text-danger">*</span></label>
                                                <textarea placeholder="Postal Address*" class="form-control border-right-line basic-info-form-controls" name="postal_address" id="postal_address" height="135" rows="3">{{isset($company)?$company->postal_address:''}}</textarea>
                                                <span class="error" id="postal_address-back-error"></span>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label" for="postal_address_pincode">Postal Address Pincode<span class="text-danger">*</span></label>
                                                <input class="form-control border-right-line basic-info-form-controls" value="{{isset($company)?$company->postal_address_pincode:''}}" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="6" min="0"  oninput="this.value=this.value.slice(0,this.maxLength)" id="postal_address_pincode" name="postal_address_pincode" type="number"  placeholder="Postal Address Pincode*" autocomplete="false" />
                                                <span class="error" id="postal_address_pincode-back-error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label class="form-label" for="billing_address">Billing Address<span class="text-danger">*</span> </label>
                                                <textarea placeholder="Billing Address*" class="form-control border-right-line basic-info-form-controls" name="billing_address" id="billing_address" height="135" rows="3" rows="3">{{isset($company)?$company->billing_address:''}}</textarea>
                                                <span class="error" id="billing_address-back-error"></span>
                                            </div>
                                            <div class="form-group">
                                                <label class="form-label" for="billing_address_pincode">Billing Address Pincode<span class="text-danger">*</span></label>
                                                <input class="form-control border-right-line basic-info-form-controls" value="{{isset($company)?$company->billing_address_pincode:''}}" maxlength="6" min="0"  oninput="this.value=this.value.slice(0,this.maxLength)" id="billing_address_pincode" onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="billing_address_pincode" type="number"  placeholder="Billing Address Pincode*" autocomplete="false" />
                                                <span class="error" id="billing_address_pincode-back-error"></span>
                                            </div>
                                        </div>
                                    </div>
                                <div class="d-flex justify-content-end">
                                    <!-- <button class="btn btn-primary btn-next profile-info-btn" type="submit" id="basic_details_submit">Save & Continue</button> -->

                                    <button type ="submit" id="basic_details_submit" class="btn btn-primary btn-next profile-info-btn">
                                        <span class="align-middle d-sm-inline-block d-none">Save & Continue</span>
                                        <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                    </button>
                                </div>
                                </form>
                            </div>
                            <div id="industry-details" class="content" role="tabpanel" aria-labelledby="industry-details-trigger">
                                <form id="industry_info_form" class="industry_info_form" action="{{isset($company)?route('admin.company.update',$company->id):route('admin.company.store')}}">
                                    @csrf
                                @isset($company)
                                        @method('PUT')
                                    @endisset
                                    <input type="hidden" name="tab" id="tab" value="2">
                                    <input type="hidden" name="company_id" id="company_id">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group ">
                                                <label for="login-password" class="font-small-4">Type of the Industry<span class="text-danger">*</span></label>
                                                <select class="form-control industry-details-form-controls" name="industry" id="industry" data-placeholder="select">
                                                    <option value="">Select Industry</option>
                                                    @foreach($industries as $industry)
                                                    <option value="{{$industry->id}}"@if(isset($company))@if($company->mst_industry_id==$industry->id) selected @endif @endif>{{$industry->name}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="error" id="industry-back-error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="login-password" class="font-small-4">Which type of Entity you are ?<span class="text-danger">*</span></label>
                                                <select class="form-control industry-details-form-controls" name="entity" id="entity" data-placeholder="select">
                                                    <option value="">Select Entity</option>
                                                    @foreach($entities as $entity)
                                                    <option value="{{$entity->id}}"@if(isset($company))@if($company->mst_entity_id==$entity->id) selected @endif @endif>{{$entity->name}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="error" id="entity-back-error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group ">
                                                <label for="login-password" class="font-small-4">Type of Sector<span class="text-danger">*</span></label>
                                                <select class="form-control industry-details-form-controls" id="sector" name="sector" data-placeholder="select">
                                                    <option value="">Select Sector</option>
                                                    @foreach($sectors as $sector)
                                                    <option value="{{$sector->id}}"@if(isset($company))@if($company->mst_sector_id==$sector->id) selected @endif @endif>{{$sector->name}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="error" id="sector-back-error"></span>
                                            </div>
                                        </div>
                                    </div>
                                <button  id='prev1' type="button"  class="btn btn-outline-secondary">
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                </button>
                                <span class="float-right">
                                    <button type="submit" id="industry_details_submit" class="btn btn-primary btn-next profile-info-btn">
                                        <span class="align-middle d-sm-inline-block d-none">Save & Continue</span>
                                        <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                    </button>
                                </span>
                                </form>
                            </div>
                            <div id="infrastructure-step" class="content mt-2" role="tabpanel" aria-labelledby="infrastructure-step-trigger">
                                <form id="infrastructure_info_form" class="infrastructure_info_form" method="post" action="{{isset($company)?route('admin.company.update',$company->id):route('admin.company.store')}}">
                                    @csrf
                                @isset($company)
                                        @method('PUT')
                                    @endisset
                                    <input type="hidden" name="tab" id="tab" value="2">
                                    <input type="hidden" name="company_id" id="company_id2">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="form-label">Name of the Products manufactured / Services offered<span class="text-danger">*</span></label>
                                                <input class="form-control form-caontrol-merge border-right-line infrastructure-details-form-controls" value="{{isset($company)?$company->product_name:''}}" type="text" placeholder="Name of the Products manufactured / Services offered*" name="name_of_product" id="name_of_product"/>
                                                <span class="error" id="name_of_product-back-error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="form-label">Latest turnover of the unit/ facility, in Rs Crores only<span class="text-danger">*</span></label>
                                                <input class="form-control form-caontrol-merge border-right-line infrastructure-details-form-controls txtQty" min="0" onkeypress="return /[0-9.]/i.test(event.key)" value="{{isset($company)?$company->turnover:''}}" type="number" placeholder="Enter latest turnover of the unit/ facility, in Rs Crores only*" name="turnover" id="turnover"/>
                                                <span class="error" id="turnover-back-error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="form-label">Total no of permanent employees<span class="text-danger">*</span></label>
                                                <input class="form-control form-caontrol-merge border-right-line infrastructure-details-form-controls" value="{{isset($company)?$company->no_of_employees:''}}" type="number" min="0" onkeypress="return /[0-9]/i.test(event.key)" placeholder="Total no of permanent employees*" name="no_of_permanent_employees" id="no_of_permanent_employees"/>
                                                <span class="error" id="no_of_permanent_employees-back-error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="form-label">Total no of contract employees<span class="text-danger">*</span></label>
                                                <input class="form-control form-caontrol-merge border-right-line infrastructure-details-form-controls" value="{{isset($company)?$company->contract_employees:''}}" min="0" onkeypress="return /[0-9]/i.test(event.key)"  type="number" placeholder="Total no of contract employees*" name="no_of_contract_employees" id="no_of_contract_employees" />
                                                <span class="error" id="no_of_contract_employees-back-error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="form-label">Yearly Electrical Energy Consumption in KWH / year<span class="text-danger">*</span></label>
                                                <input class="form-control form-caontrol-merge border-right-line infrastructure-details-form-controls txtQty" type="number"  onkeypress="return /[0-9.]/i.test(event.key)" min ="0" value="{{isset($company)?$company->electrical_consumption:''}}" placeholder="Yearly Electrical Energy Consumption in KWH / year*" name="electrical_energy" id="electrical_energy" />
                                                <span class="error" id="electrical_energy-back-error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="form-label">Yearly Thermal Energy Consumption in Kcal / year<span class="text-danger">*</span></label>
                                                <input class="form-control form-caontrol-merge border-right-line infrastructure-details-form-controls txtQty" type="number"  onkeypress="return /[0-9.]/i.test(event.key)" min="0" value="{{isset($company)?$company->thermal_consumption:''}}" placeholder="Yearly Thermal Energy Consumption in Kcal / year*" name="thermal_energy" id="thermal_energy" />
                                                <span class="error" id="thermal_energy-back-error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="form-label">Yearly water consumption in Kilo Litres/ Cubic meters<span class="text-danger">*</span></label>
                                                <input class="form-control form-caontrol-merge border-right-line infrastructure-details-form-controls txtQty" type="number"  onkeypress="return /[0-9.]/i.test(event.key)" min="0" value="{{isset($company)?$company->water_consumption:''}}" placeholder="Yearly water consumption in Kilo Litres/ Cubic meteres*" name="water_consumption" id="water_consumption"/>
                                                <span class="error" id="water_consumption-back-error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label class="form-label">Total area of the unit / facility ( including Built up space, open area, colony or township etc ) in Acres only<span class="text-danger">*</span></label>
                                                <input class="form-control form-caontrol-merge border-right-line infrastructure-details-form-controls txtQty" type="number" onkeypress="return /[0-9.]/i.test(event.key)" value="{{isset($company)?$company->area:''}}" placeholder="Total area of the unit / facility ( including Built up space, open area, colony or township etc ) in Acres only*" name="total_area" id="total_area" />
                                                <span class="error" id="total_area-back-error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="form-label">Total Budget Allocation in Rs Crores only<span class="text-danger">*</span></label>
                                                <input class="form-control form-caontrol-merge border-right-line infrastructure-details-form-controls txtQty" type="number" min="0" onkeypress="return /[0-9.]/i.test(event.key)" id="budget_allocation" name="budget_allocation" value="{{ old('budget_allocation', $company->budget_allocation) }}" placeholder="Total Budget Allocation in Rs Crores only*"  />
                                                <span class="error" id="budget_allocation-error-back"></span>
                                            </div>
                                        </div>
                                    </div>
                                <button  id='prev2' type="button"  class="btn btn-outline-secondary">
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                </button>
                                <span class="float-right">
                                    <button type="submit" class="btn btn-primary btn-next profile-info-btn" id="infrastructure_details_submit">
                                        <span class="align-middle d-sm-inline-block d-none">Save & Continue</span>
                                        <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                    </button>
                                </span>
                                </form>
                            </div>
                            <div id="income-details" class="content mt-2" role="tabpanel" aria-labelledby="income-details-trigger">
                            <form id="income_info_form" class="income_info_form" method="post" action="{{isset($company)?route('admin.company.update',$company->id):route('admin.company.store')}}">
                                @csrf
                            @isset($company)
                                        @method('PUT')
                                    @endisset
                            <input type="hidden" name="tab" id="tab" value="4">
                                <input type="hidden" name="company_id" id="company_id3">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="form-label">TAN NO (Tax dedcution Account Number)<span class="text-danger">*</span></label>
                                                <input class="form-control form-caontrol-merge border-right-line income-details-form-controls" onkeypress="return /[a-zA-Z0-9]/i.test(event.key)" maxlength="10" value="{{isset($company)?$company->tanno:''}}" type="text"  placeholder="TAN NO (Tax dedcution Account Number)*" id="tanno" name="tanno"/>
                                                <span class="error" id="tanno-back-error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                            <label class="form-label">PAN NO (Permanent Account Number)<span class="text-danger">*</span></label>
                                                <input class="form-control form-caontrol-merge border-right-line income-details-form-controls" onkeypress="return /[a-zA-Z0-9]/i.test(event.key)" maxlength="10" value="{{isset($company)?$company->panno:''}}" type="text" placeholder="PAN NO (Permanent Account Number)*" id="panno" name="panno"/>
                                                <span class="error" id="panno-back-error"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="form-label font-small-3">GSTIN NO (Goods and Services Tax Identification Number )<span class="text-danger">*</span></label>
                                                <input class="form-control form-caontrol-merge border-right-line income-details-form-controls" onkeypress="return /[a-zA-Z0-9]/i.test(event.key)" maxlength="15" value="{{isset($company)?$company->gstno:''}}" type="text" placeholder="GSTIN NO (Goods and Services Tax Identification Number)*" id="gstinno" name="gstinno" />
                                                <span class="error" id="gstinno-back-error"></span>
                                            </div>
                                        </div>
                                    </div>
                                <button  id='prev3' type="button"  class="btn btn-outline-secondary">
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                </button>
                                <span class="float-right">
                                    <button type="submit" class="btn btn-primary btn-next profile-info-btn" id="income_details_submit">
                                        <span class="align-middle d-sm-inline-block d-none">Save & Submit</span>
                                        <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                    </button>
                                </span>
                                </form>
                            </div>
                            <!-- <div id="document-upload" class="content" role="tabpanel" aria-labelledby="document-upload-trigger">
                                <form>
                                <input type="hidden" name="company_id" id="company_id4">
                                    <div class="row  pb-1 mt--1">
                                        <div class="col-lg-6">
                                            <div class="card ">
                                                <div class="d-flex v-row title-strip mb-2">
                                                    <div class="col-6 pl-0">Proposal Document</div>
                                                </div>
                                                <div class="card-body ">
                                                    <div class="row  pb-1 mt--1">
                                                        <div class="col-lg-12">
                                                            <div class="row v-row mb-2">
                                                                <div class="col-lg-2 col-md-2">
                                                                    <label>Uploaded File :</label>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4">
                                                                    <div class="v-row  uploadedfile">
                                                                        <span class="">DC Bill of material.pdf</span> <button class="btn btn-outline-secondary waves-effect ml-1"> <i data-feather="download"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12  mt-50">
                                                                    <div class="custom-file">
                                                                        <input type="file" class="custom-file-input" id="customFile">
                                                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 offset-lg-2">
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="card ">
                                                <div class="d-flex v-row title-strip mb-2">
                                                    <div class="col-6 pl-0">PO Document</div>
                                                </div>
                                                <div class="card-body ">
                                                    <div class="row  pb-1 mt--1">
                                                        <div class="col-lg-12">
                                                            <div class="row v-row mb-2">
                                                                <div class="col-lg-2 col-md-2">
                                                                    <label>Uploaded File :</label>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4">
                                                                    <div class="v-row  uploadedfile">
                                                                        <span class="">DC Bill of material.pdf</span> <button class="btn btn-outline-secondary waves-effect ml-1"> <i data-feather="download"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12  mt-50">
                                                                    <div class="custom-file">
                                                                        <input type="file" class="custom-file-input" id="customFile">
                                                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 offset-lg-2">
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="d-flex justify-content-between">
                                    <button class="btn btn-outline-secondary btn-prev">
                                        <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                        <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                    </button>
                                    <button class="btn btn-success btn-submit mr-1">
                                        <i data-feather="check" class="align-middle ms-sm-25 ms-0"></i>Save & Submit</button>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </section>
                <!-- /Horizontal Wizard -->
            </div>
        </div>
    </div>
</div>

@endsection

@push('PAGE_SCRIPTS')
<script src="{{url('app-assets/vendors/js/forms/repeater/jquery.repeater.min.js')}}"></script>
<script src="{{url('app-assets/js/scripts/forms/form-repeater.js')}}"></script>
<script src="{{url('app-assets/vendors/js/forms/wizard/bs-stepper.min.js')}}"></script>
<!-- BEGIN: Page JS-->
<script src="{{url('app-assets/js/scripts/forms/form-wizard.min.js')}}"></script>
<script src="{{url('app-assets/vendors/js/jquery-ui.js')}}"></script>

<script>

  $("#company_name").autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ route('company.companieslist') }}",
                type: 'get',
                dataType: "json",
                data:{name:request.term},
                success: function( data ) {
                response( data );
                }
            });
        },

        response: function(event, ui) {
            if (ui.content.length === 0) {
                $('#manufacturer_id').val('-1');
            }
        },

        select: function(event, ui) {
            $('#manufacturer_id').val('-1');
            $(this).val(ui.item.value);
            $('#manufacturer_id').val(ui.item.id);
        },
        minLength: 2,
        autoFocus: true
    });


//stepper code

var stepper = new Stepper(document.querySelector('.bs-stepper'))

    $("#prev1").click(function(){
        stepper.to(1);
    });

    $("#prev2").click(function(){
        stepper.to(2);
    });

    $("#prev3").click(function(){
        stepper.to(3);
    });

var Accessor = function() {
    return {
        init: function() {

            jQuery.validator.addMethod("emailAddress", function(value, element) {
               return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
            }, "Please Enter a Valid Email Address");

            jQuery.validator.addMethod("noSpace", function(value, element) {
              return value == '' || value.trim().length != 0;
            }, "This field is required");

            jQuery.validator.addMethod("doubleSpace", function(value, element) {
               return this.optional(element) || /^\S((?!.*  ).*\S)?$/.test(value);
            }, "This field is required");

            jQuery.validator.addMethod("lettersonly", function(value, element)
            {
                return this.optional(element) || /^[A-Z," "]+$/i.test(value);
            }, "Letters and spaces only please");

            jQuery.validator.addMethod("mobileNo", function(value, element) {
               return this.optional(element) || /^[6-9][0-9]{9}$/.test(value);
            }, "Mobile Number is invalid");

            jQuery.validator.addMethod("decimalTwo", function(value, element) {
               return this.optional(element) || /^\d+(?:\.\d{1,2})?$/.test(value);
            }, "The format must be in two decimal");

            jQuery.validator.addMethod("decimalOnly", function(value, element) {
               return this.optional(element) || /^[+-]?([0-9]{1,})[.,]([0-9]{1,})$/.test(value);
            }, "Allow Decimal Only");

            //Basic Info form
            $('#basic_info_form').validate({
                rules: {
                    'company_name': {
                        required: true,
                        noSpace: true,
                        maxlength: 50,
                        minlength:3
                    },
                    'company_email': {
                        required: true,
                        // email: true,
                        emailAddress: true
                    },
                    'company_mobile':{
                        required: true,
                        digits:true,
                        minlength:10,
                        mobileNo:true
                    },
                    'company_location':{
                        required: true,
                        doubleSpace:true,
                        noSpace: true,
                        minlength:3,
                        lettersonly:true,
                    },
                    'account_status':{
                        required: true,
                    },
                    'state':{
                        required: true,
                    },
                    'postal_address':{
                        required: true,
                        noSpace: true,
                        doubleSpace:true,
                        maxlength: 150,
                        minlength:10
                    },
                    'postal_address_pincode':{
                        required: true,
                        digits:true,
                        minlength:6,
                    },
                    'billing_address':{
                        required: true,
                        noSpace: true,
                        doubleSpace:true,
                        maxlength: 150,
                        minlength:10
                    },
                    'billing_address_pincode':{
                        required: true,
                        digits:true,
                        minlength:6,
                    }
                },

                messages: {

                    company_name: {
                        required: "The Company Name field is required",
                        noSpace: "Please Enter a Valid Company Name",
                        doubleSpace: "Please Enter a Valid Company Name"
                    },

                    company_email: {
                        required: "The Company Email Address field is required",
                        email : "Please Enter a Valid Email Address",
                        noSpace: "Please enter a valid email address",
                        doubleSpace: "Please enter a valid email address"
                    },

                    company_mobile: {
                        required: "The Company Mobile Number field is required",
                    },

                    company_location : {
                        required: "The City field is required",
                        noSpace: "Please Enter a Valid City",
                        doubleSpace: "Please Enter a Valid City",
                        lettersonly: "Please Enter a Valid City"
                    },

                    state : {
                        required : "The State field is required"
                    },

                    account_status: {
                        required : "The Account Status field is required"
                    },

                    postal_address : {
                        required: "The Postal Address field is required",
                        noSpace: "Please Enter a Valid Postal Address",
                        doubleSpace: "Please Enter a Valid Postal Address",
                        maxlength: "Please do not enter more than 150 characters"
                    },

                    postal_address_pincode : {
                        required: "The Postal Address Pincode field is required",
                    },

                    billing_address: {
                        required: "The Billing Address field is required",
                        noSpace: "Please Enter a Valid Billing Address",
                        doubleSpace: "Please Enter a Valid Billing Address",
                        maxlength: "Please do not enter more than 150 characters"
                    },

                    billing_address_pincode: {
                        required: "The Billing Address Pincode is required",
                    }

                },


                submitHandler : function(form){
                   Swal.fire({
                        title: 'Are you sure?',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Continue',
                        showClass: {
                            popup: 'animate__animated animate__fadeIn'
                        },
                        customClass: {
                            confirmButton: 'btn btn-primary',
                            cancelButton: 'btn btn-outline-danger ml-1'
                        },
                        buttonsStyling: false
                    }).then(function(result) {
                        if (result.value) {
                            $('form.basic_info_form').find("-back-error").css('display','none');
                            $("#basic_details_submit").attr('disabled',true);
                            $("#basic_details_submit").html("Loading....");
                            $.ajax({
                                type: "POST",
                                url: $('form.basic_info_form').attr('action'),
                                data: $(form).serialize(),
                                success: function(response) {
                                    if(response.status == 'validations') {
                                        $("#basic_details_submit").attr('disabled',false);
                                        $("#basic_details_submit").html("Save & Continue" + " " + "<i class='fal fa-arrow-right rightArrow' class='align-middle ms-sm-25 ms-0'></i>");
                                        $('form.basic_info_form').find("-back-error").css('display','none');
                                        $.each(response.errors, function (key, error) {
                                            $('form.basic_info_form').find("#" + key).removeClass('error').addClass('error');
                                            $('form.basic_info_form').find("#" + key + "-back-error").text(error[0]);
                                            $('form.basic_info_form').find("#" + key + "-back-error").css('display','block');
                                        });
                                    }

                                    if(response.status == 'success') {
                                        stepper.to(2);
                                        $('#company_id').val(response.id);
                                        $('form.basic_info_form').find("-back-error").css('display','none');
                                        $("#basic_details_submit").attr('disabled',false);
                                        $("#basic_details_submit").html("Save & Continue" + " " + "<i class='fal fa-arrow-right rightArrow' class='align-middle ms-sm-25 ms-0'></i>");
                                    }

                                    if(response.status == 'error') {
                                        $('form.basic_info_form').find("-back-error").css('display','none');
                                        $("#basic_details_submit").attr('disabled',false);
                                        $("#basic_details_submit").html("Save & Continue" + " " + "<i class='fal fa-arrow-right rightArrow' class='align-middle ms-sm-25 ms-0'></i>");
                                        JsUtility.showToastr('error', 'Company', response.message);
                                    }
                                },
                                error: function() {
                                    JsUtility.showToastr('error', 'Company', response.message);
                                }
                            });
                        }
                    });
                }
            });


            //Industry Details Form
            $('#industry_info_form').validate({
                rules: {
                    'industry': {
                        required: true,
                    },
                    'entity': {
                        required: true,
                    },
                    'sector':{
                        required: true,
                    }
                },

                messages: {
                    industry:{
                        required: "The Industry field is required",
                    },
                    entity: {
                        required: "The Entity field is required",
                    },
                    sector: {
                        required: "The Sector field is required",
                    }
                },

                submitHandler : function(form){
                    $("#industry_details_submit").attr('disabled',true);
                    $("#industry_details_submit").html("Loading....");
                    $.ajax({
                        type: "POST",
                        url: $('form.industry_info_form').attr('action'),
                        data: $(form).serialize(),
                        success: function(response) {
                            if(response.status == 'validations') {
                                $("#industry_details_submit").attr('disabled',false);
                                $("#industry_details_submit").html("Save & Continue" + " " + "<i class='fal fa-arrow-right rightArrow' class='align-middle ms-sm-25 ms-0'></i>");
                                $.each(response.errors, function (key, error) {
                                    $('form.industry_info_form').find("#" + key).removeClass('error').addClass('error');
                                    $('form.industry_info_form').find("#" + key + "-error").text(error[0]);
                                });
                            }

                            if(response.status == 'success') {
                                stepper.to(3);
                                $('#company_id2').val(response.id);
                                $("#industry_details_submit").attr('disabled',false);
                                $("#industry_details_submit").html("Save & Continue" + " " + "<i class='fal fa-arrow-right rightArrow' class='align-middle ms-sm-25 ms-0'></i>");
                            }

                            if(response.status == 'error') {
                                $("#industry_details_submit").attr('disabled',false);
                                $("#industry_details_submit").html("Save & Continue" + " " + "<i class='fal fa-arrow-right rightArrow' class='align-middle ms-sm-25 ms-0'></i>");
                                JsUtility.showToastr('error', 'Company', response.message);
                            }
                        },
                        error: function() {
                            JsUtility.showToastr('error', 'Company', response.message);
                        }
                    });
                }
            });


            //Infrastructure Details Form
            $('#infrastructure_info_form').validate({
                rules: {
                    'name_of_product': {
                        required: true,
                        doubleSpace: true,
                        noSpace: true,
                        minlength:3
                    },
                    'turnover': {
                        required: true,
                        decimalTwo: true,
                    },
                    'no_of_permanent_employees': {
                        required: true,
                    },
                     'no_of_contract_employees': {
                        required: true,
                    },
                    'electrical_energy': {
                        required: true,
                        decimalTwo: true,
                        // decimalOnly: true
                    },
                    'thermal_energy': {
                        required: true,
                        decimalTwo: true,
                    },
                     'water_consumption': {
                        required: true,
                        decimalTwo: true,
                    },
                    'total_area': {
                        required: true,
                        decimalTwo: true,
                    },
                    'budget_allocation':{
                        required: true,
                        decimalTwo: true,
                    },
                },

                messages: {
                    name_of_product: {
                        required: "The Name of the Products or Services field is required",
                        noSpace: "Please Enter a Valid Name of the Products or Services",
                        doubleSpace: "Please Enter a Valid Name of the Products or Services",
                    },
                    turnover: {
                        required: "The Latest Turnover field is required",
                    },
                    no_of_permanent_employees: {
                        required: "The Total no of permanent employees field is required",
                    },
                    no_of_contract_employees: {
                        required: "The Total no of contract employees field is required",
                    },
                    electrical_energy: {
                        required: "The Yearly Electrical Energy Consumption field is required",
                        decimalOnly: "the input field should be not a dot value"
                    },
                    thermal_energy: {
                        required: "The Yearly Thermal Energy Consumption field is required",
                    },
                    water_consumption: {
                        required: "The Yearly Water Consumption field is required",
                    },
                    total_area: {
                        required: "The Total Area field is required",
                    },
                    budget_allocation: {
                        required: "The Total Budget Allocation field is required",
                    },
                },


                submitHandler : function(form){
                    $("#infrastructure_details_submit").attr('disabled',true);
                    $("#infrastructure_details_submit").html("Loading....");
                    $.ajax({
                        type: "POST",
                        url: $('form.infrastructure_info_form').attr('action'),
                        data: $(form).serialize(),
                        success: function(response) {
                            if(response.status == 'validations') {
                                $("#infrastructure_details_submit").attr('disabled',false);
                                $("#infrastructure_details_submit").html("Save & Continue" + " " + "<i class='fal fa-arrow-right rightArrow' class='align-middle ms-sm-25 ms-0'></i>");
                                $.each(response.errors, function (key, error) {
                                    $('form.infrastructure_info_form').find("#" + key).removeClass('error').addClass('error');
                                    $('form.infrastructure_info_form').find("#" + key + "-error").text(error[0]);
                                });
                            }

                            if(response.status == 'success') {
                                stepper.to(4);
                                $('#company_id3').val(response.id);
                                $("#infrastructure_details_submit").attr('disabled',false);
                                $("#infrastructure_details_submit").html("Save & Continue" + " " + "<i class='fal fa-arrow-right rightArrow' class='align-middle ms-sm-25 ms-0'></i>");
                            }

                            if(response.status == 'error') {
                                $("#infrastructure_details_submit").attr('disabled',false);
                                $("#infrastructure_details_submit").html("Save & Continue" + " " + "<i class='fal fa-arrow-right rightArrow' class='align-middle ms-sm-25 ms-0'></i>");
                                JsUtility.showToastr('error', 'Company', response.message);
                            }
                        },
                        error: function() {
                            JsUtility.showToastr('error', 'Company', response.message);
                        }
                    });
                }
            });

            // // Form submit start
            // $('#income_info_form').submit(function(e){
            //     e.preventDefault();
            //     var formData = new FormData($(this)[0]);
            //     $("#income_details_submit").attr('disabled',true);
            //     $("#income_details_submit").html("Loading....");
            //     $.ajax({
            //         type: "POST",
            //         url: $('form.income_info_form').attr('action'),
            //         data: formData,
            //         success: function(response) {
            //             if(response.status == 'validations') {
            //                 $("#income_details_submit").attr('disabled',false);
            //                 $("#income_details_submit").html("Save & Continue");
            //                 $.each(response.errors, function (key, error) {
            //                     $('form.income_info_form').find("#" + key).removeClass('error').addClass('error');
            //                     $('form.income_info_form').find("#" + key + "-error").text(error[0]);
            //                 });
            //             }

            //             if(response.status == 'success') {
            //                 JsUtility.showToastr('success', 'Company', response.message);
            //                 setTimeout(function(){
            //                     window.location = "{{route('admin.company.index')}}";
            //                      }, 5000);
            //             }

            //             if(response.status == 'error') {
            //                 $("#infrastructure_details_submit").attr('disabled',false);
            //                 $("#infrastructure_details_submit").html("Save & Continue");
            //                 JsUtility.showToastr('error', 'Company', response.message);
            //             }
            //         },
            //         error: function() {
            //             JsUtility.showToastr('error', 'Company', response.message);
            //         }
            //     });
            // });


            //Income Details Form
            $('#income_info_form').validate({
                rules: {
                    'tanno': {
                        required: true,
                        noSpace: true,
                        minlength:10,
                    },
                    'panno': {
                        required: true,
                        noSpace: true,
                        minlength:10
                    },
                    'gstinno':{
                        required: true,
                        noSpace: true,
                        minlength:15
                    }
                },

                messages: {
                    tanno: {
                        required:"The Tax Deduction Account Number field is required",
                        noSpace:"Please enter a valid Tax Deduction Account Number"
                    },
                    panno: {
                        required:"The Permanent Account Number field is required",
                        noSpace:"Please enter a valid Permanent Account Number"
                    },
                    gstinno: {
                        required:"The Goods and Services Tax Identification Number field is required",
                        noSpace:"Please enter a valid Goods and Services Tax Identification Number"
                    }
                },

                submitHandler : function(form){
                    $("#income_details_submit").attr('disabled',true);
                    $("#income_details_submit").html("Loading....");
                    $.ajax({
                        type: "POST",
                        url: $('form.income_info_form').attr('action'),
                        data: $(form).serialize(),
                        success: function(response) {
                            if(response.status == 'validations') {
                                $("#income_details_submit").attr('disabled',false);
                                $("#income_details_submit").html("Save & Continue" + " " + "<i class='fal fa-arrow-right rightArrow' class='align-middle ms-sm-25 ms-0'></i>");
                                $.each(response.errors, function (key, error) {
                                    $('form.income_info_form').find("#" + key).removeClass('error').addClass('error');
                                    $('form.income_info_form').find("#" + key + "-error").text(error[0]);
                                });
                            }

                            if(response.status == 'success') {
                                JsUtility.showToastr('success', 'Company', response.message);
                                setTimeout(function(){
                                    window.location = "{{route('admin.company.index')}}";
                                    }, 3000);
                            }

                            if(response.status == 'error') {
                                $("#income_details_submit").attr('disabled',false);
                                $("#income_details_submit").html("Save & Continue" + " " + "<i class='fal fa-arrow-right rightArrow' class='align-middle ms-sm-25 ms-0'></i>");
                                JsUtility.showToastr('error', 'Company', response.message);
                            }
                        },
                        error: function() {
                            JsUtility.showToastr('error', 'Company', response.message);
                        }
                    });
                }
            });
        }
    }
}();

jQuery(document).ready(function() {
    Accessor.init();

    $('.txtQty').on('blur',function(event) {
        var amt = parseFloat(this.value);
        $(this).val(amt.toFixed(2));
    });

    //Basic Info
    $('.basic-info-form-controls').blur(function(){
        $("#basic_info_form").validate().element(this);
    });

    $('.basic-info-form-controls').on('keyup',function(event){
        $("#basic_info_form").validate().element(this);
    });

    //Industry Details
    $('.industry-details-form-controls').blur(function(){
        $("#industry_info_form").validate().element(this);
    });

    $('.industry-details-form-controls').on('keyup',function(event){
        $("#industry_info_form").validate().element(this);
    });

    //Infrastructure Details
    $('.infrastructure-details-form-controls').blur(function(){
        $("#infrastructure_info_form").validate().element(this);
    });

    $('.infrastructure-details-form-controls').on('keyup',function(event){
        $("#infrastructure_info_form").validate().element(this);
    });

    //Income Details
    $('.income-details-form-controls').blur(function(){
        $("#income_info_form").validate().element(this);
    });

    $('.income-details-form-controls').on('keyup',function(event){
        $("#income_info_form").validate().element(this);
    });

    // capitalize name input text fields
    $('input[type=text], textarea').on('keyup',function(event){
        event.currentTarget.value = this.value.toUpperCase();
    });
});
</script>
@endpush
