@extends(ADMIN_THEME_NAME.'.layouts.app')

@push('PAGE_ASSETS_CSS')
@endpush

@section('content')
    <div class="app-content content content-ps-stl">
        <div class="">
            <div class=" content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper">
                <div class="content-body">
                    <section class="">
                        <div class=" col-12">
                            <section id="advanced-search-datatable ">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card table-filters-design">
                                            <div class="d-flex v-row title-strip">
                                                <div class="col-6 pl-0">Company Management</div>
                                                <div class="col-6 pr-0">
                                                    <div class="filter-tab-btn card-option ">
                                                        <span class="minimize-card btn btn-primary "><i
                                                                data-feather="filter"></i>
                                                            Filters</span>
                                                        <span style="display:none"><i data-feather="filter"></i>
                                                            Filters</span></li>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--Search Form -->
                                            <div class="card-body pt-1" style="display:none;">
                                                <div method="POST">
                                                    <div class="row">
                                                        <div class="col-3">
                                                            <div class="form-group">
                                                                <label>Registration Id</label>
                                                                <input type="text" id="filterRegId"
                                                                    class="form-control dt-input dt-full-name"
                                                                    data-column="2" placeholder="Registration Id" />
                                                            </div>
                                                        </div>
                                                        <div class="col-3">
                                                            <div class="form-group">
                                                                <label>Name</label>
                                                                <input type="text" id="filterName"
                                                                    class="form-control dt-input dt-full-name"
                                                                    data-column="2" placeholder="Name" />
                                                            </div>
                                                        </div>

                                                        <div class="col-3">
                                                            <div class="form-group">
                                                                <label>Phone Number</label>
                                                                <input type="text" id="filterMobile"
                                                                    class="form-control dt-input dt-full-name"
                                                                    data-column="2" placeholder="Phone Number " />
                                                            </div>
                                                        </div>
                                                        <div class="col-3">
                                                            <div class="form-group">
                                                                <label>Email Address</label>
                                                                <input type="text" id="filterEmail"
                                                                    class="form-control dt-input dt-full-name"
                                                                    data-column="2" placeholder="Name " />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-3">
                                                            <div class="form-group">
                                                                <label>Account Status</label>
                                                                <select id="filterStatus" class="form-control">
                                                                    <option value="">Select</option>
                                                                    <option value="0">In Active</option>
                                                                    <option value="1">Active </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-3">
                                                            <div class="form-group">
                                                                <label>Approval Status</label>
                                                                <select id="filterVerification_status" class="form-control">
                                                                    <option value="">Select</option>
                                                                    <option value="0">Pending</option>
                                                                    <option value="1">Approved</option>
                                                                    <option value="2">Disapproved </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-3">
                                                            <div class="form-group"style="margin-top: 7.7%;">
                                                                <button type="submit" class="btn btn-primary m0-auto"
                                                                    id="filterButton">Search</button>
                                                                <button type="submit" class="btn btn-secondary m0-auto "
                                                                    id="filterResetButton">Reset</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                            <div class="basic-datatable datatable-list">
                                                <table id="accessorTable"
                                                    class="dt-companylist table table-bordered table-responsive">
                                                    <thead>
                                                        <th></th>
                                                        <th>S.no</th>
                                                        <th>REG ID</th>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Mobile</th>
                                                        <th>Account Status</th>
                                                        <th>Approval Status</th>
                                                        <th>Action</th>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    @component('components.bootstrap.form_modal',['id'=>'changeStatusModal','heading'=>'Change Status'])
        <div id="changeStatusForm" action="">
            @csrf
            <div class="form-group">
                <label>Status</label>
                <select  id="changeCompanyStatus" name="status" class="form-control ">
                    <option value="">Select Status</option>
                    <option value="0">In Active</option>
                    <option value="1">Active </option>
                </select>
                <span id="status-error" class="error"></span>
            </div>

            <div class="col-12 text-center">
                <div class="form-group">
                    <button  type="submit" id="statusBulkUpdateSubmit" class="btn btn-primary m0-auto">Submit</button>
                </div>
            </div>

        </div>
    @endcomponent

@endsection

@push('PAGE_SCRIPTS')
<script type="text/javascript">
   var Category = function () {
      return { //main function to initiate the module
        init: function() {
                    let dt_ajax_table = $('.dt-companylist');
                    // let export_url = "{{route('admin.assessors.bulk_export')}}?";
                    // let main_export_url = export_url;
                    let dataTable = null;
                    let rows_selected = [];

                    if (dt_ajax_table.length) {

                        dataTable = dt_ajax_table.DataTable({
                            serverSide: true,
                            processing: true,
                            orderable: false,
                            ajax: {
                                url: "{{ route('admin.company.data') }}",
                                data: function(d) {
                                    d.name = $('#filterName').val();
                                    d.reg_id = $('#filterRegId').val();
                                    d.mobile = $('#filterMobile').val();
                                    d.email = $('#filterEmail').val();
                                    d.status = $('#filterStatus').val();
                                    d.verification_status = $('#filterVerification_status').val();
                                }
                            },
                            columns: [{
                                    data: 'id',
                                    name: 'id'
                                },
                                {
                                    data: 'DT_RowIndex',
                                    orderable: false,
                                    searchable: false
                                },
                                {
                                    data: 'reg_id',
                                    name: 'reg_id',
                                    orderable: false,
                                },
                                {
                                    data: 'name',
                                    name: 'name',
                                    orderable: false,
                                },
                                {
                                    data: 'email',
                                    name: 'email',
                                    orderable: false,
                                },
                                {
                                    data: 'mobile',
                                    name: 'mobile',
                                    orderable: false,
                                },
                                {
                                    data: 'account_status',
                                    name: 'account_status',
                                    orderable: false,
                                },
                                {
                                    data: 'verified_status',
                                    name: 'verified_status',
                                    orderable: false,
                                },
                                {
                                    data: 'id',
                                    name: 'id',
                                    orderable: false,
                                    searchable: false
                                }
                            ],

                            columnDefs: [

                                {
                                    targets: 0,
                                    orderable: false,
                                    render: function(data, t, a, s) {
                                        return "display" === t && (e =
                                            '<div class="custom-control custom-checkbox"> <input class=" selected-accessor custom-control-input dt-checkboxes" type="checkbox" value="' +
                                            data +
                                            '"  ><label class="custom-control-label"  for="checkbox2" ></label></div>'
                                        ), e
                                    },
                                    checkboxes: {
                                        selectRow: !0,
                                        selectAllRender: '<div class="custom-control custom-checkbox"> <input class="custom-control-input" type="checkbox" value="" id="checkboxSelectAll" /> <label class="custom-control-label" for="checkboxSelectAll"></label></div>'
                                    }
                                },
                                {
                                    // Label
                                    targets: 2,
                                    render: function(data, type, full, meta) {
                                        var ids = full['id'];

                                    let add_url = "{{route('admin.company.update','')}}/"+ids;
                                        var $reg_id = full['reg_id'];
                                        let id = '-';
                                        if($reg_id){
                                            id = '<a href='+add_url+' data-id="' + data +
                                                '" >' + $reg_id +'</a>';
                                        }

                                        return id;
                                    }
                                },
                                {
                                    // Label
                                    targets: 6,
                                    render: function(data, type, full, meta) {
                                        var $status_number = full['account_status'];
                                        var $status = {
                                            1: {
                                                title: 'Active',
                                                class: 'badge-light-success'
                                            },
                                            0: {
                                                title: 'In Active',
                                                class: ' badge-light-danger'
                                            },
                                        };
                                        if (typeof $status[$status_number] === 'undefined') {
                                            return data;
                                        }
                                        return ('<span class="badge badge-pill ' + $status[
                                                $status_number].class +
                                            '" target="_blank" >' +
                                            $status[$status_number].title + '</span>');
                                    }
                                },
                                {
                                    // Label
                                    targets: 7,
                                    render: function(data, type, full, meta) {
                                        var $status_number = data;
                                        var $status = {
                                            0: {
                                                title: 'Pending',
                                                class: 'badge-light-warning'
                                            },
                                            1: {
                                                title: 'Approved',
                                                class: ' badge-light-success'
                                            },
                                            2: {
                                                title: 'Disapproved',
                                                class: ' badge-light-danger'
                                            },
                                        };
                                        if (typeof $status[$status_number] === 'undefined') {
                                            return data;
                                        }
                                        return ('<span class="badge badge-pill ' + $status[$status_number].class +'" target="_blank" >' + $status[$status_number].title + '</span>');
                                    }
                                },
                                {
                                    // Actions
                                    targets: 8,
                                    title: 'Actions',
                                    orderable: false,
                                    render: function(data, type, full, meta) {

                                        var id = full['id'];

                                        let action='<div class="d-inline-flex">';
                                        let add_url = "{{route('admin.company.update','')}}/"+id;
                                        var $approval_status = full['verified_status'];
                                            action+='<a href='+add_url+'  data-id="' + data +
                                            '" data-toggle = "tooltip" title data-original-title  = "View / Edit" class="action-icons  badge badge-light-primary mr-50 edit-company">' +  feather.icons['edit'].toSvg({
                                                class: 'font-small-4 '
                                            }) + '</a>';

                                            if($approval_status==0)
                                            {
                                                action+='<a href="#"  data-id="' + data +
                                                '" data-toggle = "tooltip" title data-original-title = "Approve / Disapprove" class="action-icons badge badge-light-success mr-50 active-record">'+ feather.icons['check-square'].toSvg({
                                                    class: 'font-small-4 '
                                                }) + '</a>' ;
                                            }

                                            if($approval_status==2)
                                            {
                                                action+='<a href="#"  data-id="' + data +
                                                '" data-toggle = "tooltip" title data-original-title = "Approve" class="action-icons badge badge-light-success mr-50 active-record1">'+ feather.icons['check-square'].toSvg({
                                                    class: 'font-small-4 '
                                                }) + '</a>' ;
                                            }

                                            if($approval_status==2)
                                            {
                                                action+='<a href="#"  data-id="' + data +
                                                '" data-toggle = "tooltip" title data-original-title = "Remark" class="action-icons badge badge-light-danger mr-50 show-remark">'+ feather.icons['eye'].toSvg({
                                                    class: 'font-small-4 '
                                                }) + '</a>';
                                            }

                                            action+='</div>';

                                            return action;

                                    }
                                }

                            ],
                            dom: '<"row d-flex justify-content-between align-items-center m-1"' +
                                '<"col-lg-1 d-flex align-items-center"l>' + '<"col-lg-5 "f>' +
                                '<"col-lg-5 d-flex align-items-center justify-content-lg-end flex-wrap  p-0"<"dt-action-buttons text-right"B>>' +
                                '>t' + '<"d-flex justify-content-between mx-2 my-2"' +
                                '<"col-sm-12 col-md-6"i>' + '<"col-sm-12 col-md-6"p>' + '>',
                            // orderCellsTop: !0,
                            select: {
                                style: "multi",
                                selector: "td:first-child",
                                items: "row"
                            },
                            language: {
                                sLengthMenu: 'Show _MENU_',
                                search: '',
                                searchPlaceholder: ' Search',
                                paginate: {
                                    previous: '&nbsp;',
                                    next: '&nbsp;'
                                }
                            },
                            buttons: [{
                                extend: 'collection',
                                className: 'btn pr-1 btn-outline-secondary dropdown-toggle ml-1',
                                text: '<i class="fas fa-tasks "></i>' + 'Bulk Actions',
                                buttons: [{
                                        text: feather.icons['edit'].toSvg({
                                                class: 'font-small-4 mr-50'
                                        }) + 'Update Status ',
                                        className: 'dropdown-item',
                                        attr: {
                                            id: 'statusBulkUpdate'
                                        },
                                        action: function(e, dt, node, config) {
                                            var that = this;
                                            // show sweetalert ...
                                            if (rows_selected.length  > 0 ) {
                                            $("#changeStatusModal").modal('show');
                                            $('#statusBulkUpdateSubmit').attr('disabled',false);
                                            $('#statusBulkUpdateSubmit').html('Submit');
                                            }else{
                                                JsUtility.showToastr('error', 'Company', 'Please select a Company');
                                            }
                                        }
                                    },
                                    {
                                        text: feather.icons['file-text'].toSvg({
                                            class: 'font-small-4 mr-50'
                                        }) + 'Export',
                                        className: 'dropdown-item',
                                        action:function(e, dt, node, config) {
                                            let redirect=encodeURI(export_url);
                                            window.location=redirect;
                                            // console.log(export_url)
                                        }
                                    }
                                ],

                            }, {
                                text: feather.icons['plus'].toSvg({
                                    class: 'mr-50 font-small-4'
                                }) + 'Add Company',
                                className: 'btn btn-primary btn-add-record ml-2',
                                action:function(e, dt, node, config) {
                                            let add_url = "{{route('admin.company.addEditCompany')}}"
                                            let redirect=encodeURI(add_url);
                                            window.location=redirect;
                                            // console.log(export_url)
                                        }
                            }
                            ],

                        });

                    }

                    dataTable.on( 'draw', function () {
                        $('.dt-checkboxes').prop('checked',false)
                        $('#checkboxSelectAll').prop('checked',false)
                    } );

                    // $(document).on('click', '.edit-company', function(e) {
                    //     e.preventDefault();
                    //     let id = $(this).data('id');
                    //     let url = "{{route('admin.company.addEditCompany')}}"
                    //     let form_url = "{{ route('admin.assessors.update', '') }}"
                    //     $('span.error').html('');
                    //     url += '/' + id;
                    //     form_url += '/' + id;
                    //     $.get(url, function(data) {
                    //         $("#editAccessorName").val(data.name);
                    //         $("#editAccessorEmail").val(data.email);
                    //         $("#editAccessorMobile").val(data.mobile);
                    //         $("#editAccessorState").val(data.state_id);
                    //         $("#editAccessorStatus").val(data.status);
                    //         $("#editAccessorModal").modal('show');
                    //         $('#editAssessorSubmit').attr('disabled',false);
                    //         $('#editAssessorSubmit').html('Submit');
                    //         $("#editAccessorForm").attr('action', form_url);
                    //     });

                    // });

                    $("#filterButton").click(function(e) {
                        e.preventDefault();
                        dataTable.draw();
                    });

                    $("#filterResetButton").click(function(e) {
                        e.preventDefault();
                        $('#filterRegId').val('')
                        $("#filterName").val('')
                        $("#filterMobile").val('')
                        $("#filterEmail").val('')
                        $("#filterStatus").val('')
                        $("#filterVerification_status").val('')
                        $("#filterState").val('')
                        $("#filterRegion").val('')

                        dataTable.draw();
                    });

                    $(document).on('click','#statusBulkUpdateSubmit' ,function () {
                        $('#statusBulkUpdateSubmit').attr('disabled',true);
                        $('#statusBulkUpdateSubmit').html('Loading...');
                            let update_url ="{{ route('admin.company.status_change') }}";
                            let status=$("#changeCompanyStatus").val();
                             if (rows_selected.length  > 0 ) {
                                $.ajax({
                                    type: "put",
                                    url: update_url,
                                    data: {
                                        company_id:rows_selected,
                                        status:status
                                    },

                                    success: function (response) {
                                        console.log(response)
                                        if (response.success) {
                                            $("#changeStatusModal").modal('hide');
                                            rows_selected=[];
                                            dataTable.draw();
                                            $("#changeCompanyStatus").val();
                                            JsUtility.showToastr('success', 'Assessor', response.message);
                                        } else {
                                            $('#statusBulkUpdateSubmit').attr('disabled',false);
                                            $('#statusBulkUpdateSubmit').html('Submit');
                                            $.each(response.errors, function (key, error) {
                                                $('#changeStatusForm').find("#" + key + "-error").text(error[0]);
                                            });
                                        }
                                    }
                                });
                             }else{
                                JsUtility.showToastr('error', 'Assessor Update', 'Please Select accessor');
                             }



                    });


                    $(document).on('click','.active-record',function (e) {
                        e.preventDefault();

                        let vender_id = $(this).data('id');
                        let approve_url="{{route('admin.company.approve','')}}"
                        approve_url+='/'+vender_id;
                        Swal.fire({
                            title: 'Do you want to Approve?',
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Yes, Approve it!',
                            showDenyButton: true,
                            denyButtonText: 'No, Reject it',
                            showClass: {
                                popup: 'animate__animated animate__fadeIn'
                            },
                            customClass: {
                                confirmButton: 'btn btn-primary',
                                cancelButton: 'btn btn-outline-danger ml-1',
                                denyButton: 'btn btn-secondary ml-1',
                            },
                            buttonsStyling: false
                        }).then((result) => {
                            /* Read more about isConfirmed, isDenied below */
                            if (result.isConfirmed) {
                                $.get(approve_url, {status:1},
                                    function (responce) {
                                        if (responce.success) {
                                            Swal.fire({
                                                icon: 'success',
                                                title: 'Approved!',
                                                confirmButtonText: 'Okay',
                                                text: 'Your Company has been Approved.',
                                                showClass: {
                                                    popup: 'animate__animated animate__fadeIn'
                                                },
                                                customClass: {
                                                    confirmButton: 'btn btn-primary'
                                                },
                                                buttonsStyling: false
                                            }).then((result2) => {
                                            if (result2.isConfirmed) {
                                                dataTable.draw();
                                            location.reload();
                                            }
                                            else if(Swal.DismissReason.backdrop) {
                                                dataTable.draw();
                                                location.reload();
                                                }
                                            })
                                        }else{
                                            Swal.fire('Approved!',responce.message, 'error');
                                        }
                                    }
                                );

                            } else if (result.isDenied) {
                                Swal.fire({
                                    title: 'Reason for Rejection!',
                                    input: 'textarea',
                                    showCancelButton: true,
                                    confirmButtonText: 'Submit',
                                    showClass: {
                                        popup: 'animate__animated animate__fadeIn'
                                    },
                                    customClass: {
                                        confirmButton: 'btn btn-primary',
                                        cancelButton: 'btn btn-outline-danger ml-1',
                                        denyButton: 'btn btn-secondary ml-1',
                                    },
                                    buttonsStyling: false,
                                    preConfirm: (remark) => {
                                        $.get(approve_url, {status:2,remark:remark},
                                        function (responce) {
                                            if (responce.success) {
                                                    Swal.fire('Disapproved!', responce.message, 'success');
                                                    dataTable.draw();
                                                }else{
                                                    Swal.fire('Disapproved!', responce.message, 'error');
                                                }
                                            }
                                        );
                                    },
                                })
                            }
                        })

                    });

                    $(document).on('click','.active-record1',function (e) {
                        e.preventDefault();

                        let vender_id = $(this).data('id');
                        let approve_url="{{route('admin.company.approve','')}}"
                        approve_url+='/'+vender_id;
                        Swal.fire({
                            title: 'Do you want to Approve?',
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Yes, Approve it!',
                            showDenyButton: false,
                            showClass: {
                                popup: 'animate__animated animate__fadeIn'
                            },
                            customClass: {
                                confirmButton: 'btn btn-primary',
                                cancelButton: 'btn btn-outline-danger ml-1',
                            },
                            buttonsStyling: false
                        }).then((result) => {
                            /* Read more about isConfirmed, isDenied below */
                            if (result.isConfirmed) {
                                $.get(approve_url, {status:1},
                                    function (responce) {
                                        if (responce.success) {
                                            Swal.fire({
                                                icon: 'success',
                                                title: 'Approved!',
                                                confirmButtonText: 'Okay',
                                                text: 'Your Company has been Approved.',
                                                showClass: {
                                                    popup: 'animate__animated animate__fadeIn'
                                                },
                                                customClass: {
                                                    confirmButton: 'btn btn-primary'
                                                },
                                                buttonsStyling: false
                                            }).then((result2) => {
                                            if (result2.isConfirmed) {
                                                dataTable.draw();
                                            location.reload();
                                            }
                                            else if(Swal.DismissReason.backdrop) {
                                                dataTable.draw();
                                                location.reload();
                                                }
                                            })
                                        }else{
                                            Swal.fire('Approved!',responce.message, 'error');
                                        }
                                    }
                                );

                            } else if (result.isDenied) {
                                Swal.fire({
                                    title: 'Reason for Rejection!',
                                    input: 'textarea',
                                    showCancelButton: true,
                                    confirmButtonText: 'Submit',
                                    showClass: {
                                        popup: 'animate__animated animate__fadeIn'
                                    },
                                    customClass: {
                                        confirmButton: 'btn btn-primary',
                                        cancelButton: 'btn btn-outline-danger ml-1',
                                        denyButton: 'btn btn-secondary ml-1',
                                    },
                                    buttonsStyling: false,
                                    preConfirm: (remark) => {
                                        $.get(approve_url, {status:2,remark:remark},
                                        function (responce) {
                                            if (responce.success) {
                                                    Swal.fire('Disapproved!', responce.message, 'success');
                                                    dataTable.draw();
                                                }else{
                                                    Swal.fire('Disapproved!', responce.message, 'error');
                                                }
                                            }
                                        );
                                    },
                                })
                            }
                        })

                    });

                    $(document).on('change', '.selected-accessor', function() {
                        var id = $(this).val();
                        // Determine whether row ID is in the list of selected row IDs
                        var index = $.inArray(id, rows_selected);
                        // If checkbox is checked and row ID is not in list of selected row IDs
                        if (this.checked && index === -1) {
                            rows_selected.push(id);
                        } else if (!this.checked && index !== -1) {
                            rows_selected.splice(index, 1);
                        }
                    })

                    // $(document).on('click', '#exportExceleData', function(e) {
                    //     e.preventDefault();

                    //     var url = "{{ route('admin.assessors.bulk_export') }}?export=1";

                    //     if ($('body').find('div.dataTables_filter input[type="search"]').val() != '') url +=
                    //         "&fsearch=" + encodeURIComponent($('#fsearch').val());

                    //     window.location.href = url;
                    // });

                    $("#filterButton").click(function(e) {
                        e.preventDefault();
                        dataTable.draw();
                        export_url=main_export_url;
                        let parameter="";
                        if ($("#filterName").val()!="") {
                            export_url +=("name="+$("#filterName").val())
                            parameter="name";
                        }

                        if ($("#filterMobile").val()!="") {

                            if (parameter=="") {
                                export_url +=("mobile="+$("#filterMobile").val())
                            } else {
                                export_url +=("&mobile="+$("#filterMobile").val())
                            }
                            parameter="mobile";
                        }

                        if ($("#filterEmail").val()!="") {

                            if (parameter=="") {
                                export_url +=("email="+$("#filterEmail").val())
                            } else {
                                export_url +=("&email="+$("#filterEmail").val())
                            }
                            parameter="email";
                        }

                        if ($("#filterStatus").val()!="") {

                            if (parameter=="") {
                                export_url +=("status="+$("#filterStatus").val())
                            } else {
                                export_url +=("&status="+$("#filterStatus").val())
                            }
                            parameter="status";
                        }

                        if ($("#filterVerification_status").val()!="") {

                            if (parameter=="") {
                                export_url +=("verificationStatus="+$("#filterVerification_status").val())
                            } else {
                                export_url +=("&verificationStatus="+$("#filterVerification_status").val())
                            }
                            parameter="verificationStatus";
                        }
                    });

                }
   }
}();

jQuery(document).ready(function() {
   Category.init();
});
</script>
@endpush
