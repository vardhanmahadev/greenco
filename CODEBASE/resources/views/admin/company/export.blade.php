<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<!-- Head -->

<head>
    <meta charset="utf-8" />
    <title>Company</title>
    <meta name="description" content="blank page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
    <!-- Page Content -->
    <div class="page-content">
        <!-- Page Body -->
        <div class="page-body">
            <div id="to_print">
                <table class="tabletable-bordered">
                    <thead>
                        <tr style="background:#c2c2c2;">
                            <th> # </th>
                            <th> Name </th>
                            <th> Email </th>
                            <th> Phone Number </th>
                            <th> Approve Status </th>
                            <th> Status </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($assessors))
                        @foreach($assessors as $index => $assessor)
                            <tr>
                                <td>{{++$index}}</td>
                                <td>{{$assessor->name}}</td>
                                <td>{{$assessor->email}}</td>
                                <td>{{$assessor->mobile}}</td>
                                @switch($assessor->verification_status)
                                    @case(0)
                                        <td>Pending</td>
                                        @break
                                    @case(1)
                                        <td>Approved </td>
                                    @break
                                    @case(2)
                                        <td>Disapproved </td>
                                    @break
                                @endswitch

                                @switch($assessor->status)
                                    @case(0)
                                        <td>In Active</td>
                                        @break
                                    @case(1)
                                        <td>Active </td>
                                    @break
                                @endswitch
                            </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /Page Body -->
    </div>
    <!-- /Page Content -->

</body>
<!--  /Body -->

</html>
