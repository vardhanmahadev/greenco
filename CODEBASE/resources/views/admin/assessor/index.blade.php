@extends(ADMIN_THEME_NAME.'.layouts.app')

@push('PAGE_ASSETS_CSS')
<style>
    .btn-group [class*='btn-']:not([class*='btn-outline-']):not([class*='btn-flat-']):not([class*='btn-gradient-']):not([class*='btn-relief-']) {
    border-right: none!important;
    border-left: none!important;
}
table td:nth-child(3){
        min-width:105px;
        word-break:break-all !important;
        white-space: break-spaces!important;
    }
    table td:nth-child(4){
        min-width:150px;
        word-break:break-all !important;
        white-space: break-spaces!important;
    }
    #swal2-validation-message{
        display:none!important;
    }
</style>
@endpush

@section('content')
    <div class="app-content content content-ps-stl">
        <div class="">
            <div class=" content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper">
                <div class="content-body">
                    <section class="">
                        <div class=" col-12">
                            <section id="advanced-search-datatable ">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card table-filters-design">
                                            <div class="d-flex v-row title-strip">
                                                <div class="col-6 pl-0">Assessor Management</div>
                                                <div class="col-6 pr-0">
                                                    <div class="filter-tab-btn card-option ">
                                                        <span class="minimize-card btn btn-primary "><i
                                                                data-feather="filter"></i>
                                                            Filters</span>
                                                        <span style="display:none"><i data-feather="filter"></i>
                                                            Filters</span></li>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--Search Form -->
                                            <div class="card-body pt-1" style="display:none;">
                                                <div method="POST">
                                                    <div class="row">
                                                        <div class="col-3">
                                                            <div class="form-group">
                                                                <label>Name</label>
                                                                <input type="text" id="filterName"
                                                                    class="form-control dt-input dt-full-name"
                                                                    data-column="2" placeholder="Name" />
                                                            </div>
                                                        </div>
                                                        <div class="col-3">
                                                            <div class="form-group">
                                                                <label>Phone Number</label>
                                                                <input type="text" id="filterMobile"
                                                                    class="form-control dt-input dt-full-name"
                                                                    data-column="2" placeholder="Phone Number " />
                                                            </div>
                                                        </div>
                                                        <div class="col-3">
                                                            <div class="form-group">
                                                                <label>Email Address</label>
                                                                <input type="email" id="filterEmail"
                                                                    class="form-control dt-input dt-full-name"
                                                                    data-column="2" placeholder="Email"/>
                                                            </div>
                                                        </div>
                                                        <div class="col-3">
                                                            <div class="form-group">
                                                                <label>Industry Category</label>
                                                                <select id="filterCategory" class="form-control">
                                                                    <option value="">All</option>
                                                                    @foreach($categories as $category)
                                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-3">
                                                            <div class="form-group">
                                                                <label>State</label>
                                                                <select id="filterState" class="form-control">
                                                                    <option value="">All</option>
                                                                    @foreach($states as $state)
                                                                    <option value="{{$state->id}}">{{$state->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-3">
                                                            <div class="form-group">
                                                                <label>Account Status</label>
                                                                <select id="filterStatus" class="form-control">
                                                                    <option value="">All</option>
                                                                    <option value="0">In Active</option>
                                                                    <option value="1">Active </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-3">
                                                            <div class="form-group">
                                                                <label>Approval Status</label>
                                                                <select id="filterVerification_status" class="form-control">
                                                                    <option value="">All</option>
                                                                    <option value="0">Pending</option>
                                                                    <option value="1">Approved</option>
                                                                    <option value="2">Disapproved </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-3">
                                                            <div class="form-group">
                                                                <label>Profile Status</label>
                                                                <select id="filterProfile_status" class="form-control">
                                                                    <option value="">All</option>
                                                                    <option value="0">Pending</option>
                                                                    <option value="1">Completed</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-3">
                                                            <div class="form-group"style="margin-top: 7.7%;">
                                                                <button type="submit" class="btn btn-primary m0-auto"
                                                                    id="filterButton">Search</button>
                                                                <button type="submit" class="btn btn-secondary m0-auto "
                                                                    id="filterResetButton">Reset</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                            <div class="basic-datatable datatable-list">
                                                <table id="accessorTable" class="dt-accessorlist table table-bordered table-responsive my-datatable">
                                                    <thead>
                                                        <th></th>
                                                        <th>S.No</th>
                                                        <th style="width:80px">Name</th>
                                                        <th>Email</th>
                                                        <th>Mobile</th>
                                                        <th>State</th>
                                                        <th>Account Status</th>
                                                        <th>Approval Status</th>
                                                        <th>Profile Status</th>
                                                        <th>Action</th>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <!---Start Modal Popup-->
<div class="modal float-labeldispaly fade" id="viewRemark" tabindex="-1" role="dialog" aria-labelledby="editWarehouseTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form class="add-new-user modal-content pt-0">
            <div class="modal-header mb-1">
                <h5 class="modal-title">Change Account Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
            </div>
            <div class="modal-body flex-grow-1">
                <div class="form-label mb-2">
                    <label class="">Status</label>
                    <div class="mb-1 d-flex select-tag w-100">
                        <select class="form-control ">
                            <option value="Select">Select Status</option>
                            <option value="Active">Active</option>
                            <option value="InActive"> InActive</option>
                        </select>
                    </div>
                </div>
                <div class="form-label-group m0-auto text-center">
                    <button type="submit" class="btn btn-primary m0-auto  ">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!---end Modal Popup-->


    @component('components.bootstrap.side_modal', ['id' => 'addAccessorModal', 'heading' => 'Add Assessor'])
        <form id="addAccessorForm" action="{{ route('admin.assessors.store') }}">

            <div class="form-label-group ">
                <input type="text" name="name" autocomplete="off" onkeypress="$('#name-back-error').text(''); return /[a-zA-Z\s]/i.test(event.key);"  class="form-control" id="name" placeholder="Name*">
                <label class="font-small-4">Name <span class="text-danger">*</span> </label>
                <span class="error" id="name-back-error"></span>
                @error('name')
                    <span class="error" id="name-error-add">{{ $message }}</span>
                @enderror
            </div>
            <div class="form-label-group">
                <input type="email" class="form-control" autocomplete="off" name="email" id="email" placeholder="Email Address*">
                <label class="font-small-4">Email Address <span class="text-danger">*</span> </label>
                <span class="error" id="email-back-error"></span>
                @error('email')
                    <span class="error" id="email-error-add">{{ $message }}</span>
                @enderror
            </div>
            <div class="form-label-group  mb-1">
                <input type="text" autocomplete="off" maxlength="10" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" class="form-control" name="mobile" id="mobile" placeholder="Mobile Number*">
                <label class="font-small-4">Mobile Number <span class="text-danger">*</span> </label>
                <span class="error" id="mobile-back-error"></span>
                @error('mobile')
                    <span class="error" id="mobile-error-add">{{ $message }}</span>
                @enderror
            </div>
            <input type="hidden" name="status" value="1">
            <!-- <div class="form-label mb-2">
                <label class="font-small-4">Status <span class="text-danger">*</span> </label>
                <div class="select-tag w-100">
                    <select id="status" name="status" class="form-control ">
                        <option value="">Select Status</option>
                        <option value="0">In Active</option>
                        <option value="1">Active </option>
                    </select>
                </div>
                <span class="error" id="status-back-error"></span>
                @error('status')
                    <span class="error" id="status-error-add">{{ $message }}</span>
                @enderror
            </div> -->
            <div class="form-label-group m0-auto text-center">
                <button type="submit" class="btn btn-primary m0-auto addAssessor_save_btn" id="AddAssessor" >Submit</button>
            </div>
        </form>
    @endcomponent

    @component('components.bootstrap.side_modal', ['id' => 'editAccessorModal', 'heading' => 'Edit Assessor'])
        <form action="" method="POST" id="editAccessorForm">
            @method("PUT")
            <div class="form-label-group ">
                <input type="text" maxlength="40" autocomplete="off" oninput="this.value = this.value.replace(/[^a-zA-Z ]/g, '').replace(/(\..*?)\..*/g, '$1');"  name="name" class="form-control name" id="editAccessorName" placeholder="Name*">
                <label class="font-small-4">Name <span class="text-danger">*</span> </label>
                <span class="error" id="name-back-error"></span>
                @error('name')
                    <span class="error" id="name-error-edit">{{ $message }}</span>
                @enderror
            </div>
            <div class="form-label-group">
                <input type="email" class="form-control email" name="email" id="editAccessorEmail"
                    placeholder="Email Address*">
                <label class="font-small-4">Email Address <span class="text-danger">*</span> </label>
                <span class="error" id="email-back-error"></span>
                @error('email')
                    <span class="error" id="email-error-edit">{{ $message }}</span>
                @enderror
            </div>
            <div class="form-label-group  mb-1">
                <input type="text"  class="form-control mobile" name="mobile" id="editAccessorMobile" placeholder="Mobile Number*">
                <label class="font-small-4">Mobile Number <span class="text-danger">*</span> </label>
                <span class="error" id="mobile-back-error"></span>
                @error('mobile')
                    <span class="error" id="mobile-error-edit">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-label mb-2">
                <label class="font-small-4">Status <span class="text-danger">*</span></label>
                <div class="select-tag w-100">
                    <select  id="editAccessorStatus" name="status" class="form-control" data-placeholder="Select Status">
                        <option value="">Select Status</option>
                        <option value="0">In Active</option>
                        <option value="1">Active </option>
                    </select>
                </div>
                <span class="error" id="status-back-error"></span>
                @error('status')
                    <span class="error" id="name-error-edit">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-label-group m0-auto text-center">
                <button type="submit" class="btn btn-primary m0-auto" id="editAssessorSubmit">Submit</button>
            </div>
        </form>
    @endcomponent

    @component('components.bootstrap.remark_modal', ['id' => 'showRemarkModal', 'heading' => 'Remark'])
        <ul>
            <li> <div class="d-flex justify-content-between">
                    <div><span> Date </span> <label id="remarkDate"></label></div> <div><span> Status </span> <label class="badge mr-50" id="remarkstatus">Approve</label> </div></div>  </li>
            <li> <span>  Verified By </span><label id="remarkby"></label></li>
            <li class="bg-lightblue">
                <p id="remarkcontent" style="word-break: break-all;"></p>
            </li>
        </ul>
    @endcomponent

    @component('components.bootstrap.form_modal',['id'=>'changeStatusModal','heading'=>'Change Status'])
        <div id="changeStatusForm" action="">
            @csrf
            <div class="form-group">
                <label>Status</label>
                <select  id="changeAccessorStatus" name="status" class="form-control ">
                    <option value="">Select Status</option>
                    <option value="0">In Active</option>
                    <option value="1">Active </option>
                </select>
                <span id="status-error" class="error"></span>
            </div>

            <div class="col-12 text-center">
                <div class="form-group">
                    <button  type="submit" id="statusBulkUpdateSubmit" class="btn btn-primary m0-auto">Submit</button>
                </div>
            </div>

        </div>
    @endcomponent

@endsection


@push('PAGE_SCRIPTS')
    <script>
        var Accessor = function() {
            return {
                init: function() {
                    let dt_ajax_table = $('#accessorTable');
                    let export_url = "{{route('admin.assessors.bulk_export')}}?";
                    let main_export_url = export_url;
                    let dataTable = null;
                    let rows_selected = [];

                    if (dt_ajax_table.length) {

                        dataTable = dt_ajax_table.DataTable({
                            serverSide: true,
                            processing: true,
                            ajax: {
                                url: "{{ route('admin.assessors.data') }}",
                                data: function(d) {
                                    d.name = $('#filterName').val();
                                    d.mobile = $('#filterMobile').val();
                                    d.email = $('#filterEmail').val();
                                    d.status = $('#filterStatus').val();
                                    d.state = $('#filterState').val();
                                    d.verification_status = $('#filterVerification_status').val();
                                    d.profile_status = $('#filterProfile_status').val();
                                    d.category = $('#filterCategory').val();
                                    d.region = $('#filterRegion').val();
                                }
                            },
                            columns: [{
                                    data: 'id',
                                    name: 'id'
                                },
                                {
                                    data: 'DT_RowIndex',
                                    orderable: false,
                                    searchable: false,
                                },
                                {
                                    data: 'name',
                                    name: 'name',
                                    orderable: false,
                                },
                                {
                                    data: 'email',
                                    name: 'email',
                                    orderable: false,
                                },
                                {
                                    data: 'mobile',
                                    name: 'mobile',
                                    orderable: false,
                                },
                                {
                                    data: 'state',
                                    name: 'state',
                                    orderable: false,
                                },
                                {
                                    data: 'status',
                                    name: 'status',
                                    orderable: false,
                                },
                                {
                                    data: 'verification_status',
                                    name: 'verification_status',
                                    orderable: false,
                                },
                                {
                                    data: 'profile_updated',
                                    name: 'profile_updated',
                                    orderable: false,
                                },
                                {
                                    data: 'id',
                                    name: 'id',
                                    orderable: false,
                                    searchable: false
                                },


                            ],
                            scrollX:        true,
                            scrollCollapse: true,
                            columnDefs: [

                                {
                                    targets: 0,
                                    orderable: true,
                                    render: function(data, t, a, s) {
                                        return "display" === t && (e =
                                            '<div class="custom-control custom-checkbox"> <input class=" selected-accessor custom-control-input dt-checkboxes" type="checkbox" value="' +
                                            data +
                                            '"  ><label class="custom-control-label"  for="checkbox2" ></label></div>'
                                        ), e
                                    },
                                    checkboxes: {
                                        selectRow: !0,
                                        selectAllRender: '<div class="custom-control custom-checkbox"> <input class="custom-control-input" type="checkbox" value="" id="checkboxSelectAll" /> <label class="custom-control-label" for="checkboxSelectAll"></label></div>'
                                    }
                                },
                                {
                                    // Label
                                    targets: 2,
                                    render: function(data, type, full, meta) {
                                        var $name = full['name'];
                                        return ('<span class="word-wrap">' +
                                            $name + '</span>');
                                    }
                                },
                                {
                                    // Label
                                    targets: 3,
                                    width: '42px',
                                },
                                {
                                    // Label
                                    targets: 6,
                                    render: function(data, type, full, meta) {
                                        var $status_number = full['status'];
                                        var $status = {
                                            1: {
                                                title: 'Active',
                                                class: 'badge-light-success'
                                            },
                                            0: {
                                                title: 'In Active',
                                                class: ' badge-light-danger'
                                            },
                                        };
                                        if (typeof $status[$status_number] === 'undefined') {
                                            return data;
                                        }
                                        return ('<span class="badge badge-pill ' + $status[
                                                $status_number].class +
                                            '" target="_blank" >' +
                                            $status[$status_number].title + '</span>');
                                    }
                                },
                                {
                                    // Label
                                    targets: 7,
                                    render: function(data, type, full, meta) {
                                        var $status_number = data;
                                        var $status = {
                                            0: {
                                                title: 'Pending',
                                                class: 'badge-light-warning'
                                            },
                                            1: {
                                                title: 'Approved',
                                                class: ' badge-light-success'
                                            },
                                            2: {
                                                // title: 'Rejected',
                                                title: 'Disapproved',
                                                class: ' badge-light-danger'
                                            },
                                        };
                                        if (typeof $status[$status_number] === 'undefined') {
                                            return data;
                                        }
                                        let remark_data = '<span class="badge badge-pill ' + $status[
                                                            $status_number].class +
                                                        '" target="_blank" >' +
                                                        $status[$status_number].title + '</span>';

                                        return remark_data;
                                    }
                                },
                                {
                                    // Label
                                    targets: 8,
                                    render: function(data, type, full, meta) {
                                        var $status_number = data;
                                        var $status = {
                                            0: {
                                                title: 'Pending',
                                                class: 'badge-light-warning'
                                            },
                                            1: {
                                                title: 'Completed',
                                                class: ' badge-light-success'
                                            },
                                        };
                                        if (typeof $status[$status_number] === 'undefined') {
                                            return data;
                                        }
                                        let remark_data = '<span class="badge badge-pill ' + $status[
                                                            $status_number].class +
                                                        '" target="_blank" >' +
                                                        $status[$status_number].title + '</span>';

                                        return remark_data;
                                    }
                                },
                                {
                                    // Actions
                                    targets: 9,
                                    title: 'Actions',
                                    orderable: false,
                                    render: function(data, type, full, meta) {

                                        var status_id = full['id'];
                                        let add_url = "{{route('admin.assessors.addEditAssessor','')}}/"+status_id;
                                        let action='<div class="d-inline-flex">';
                                        var $approval_status = full['verification_status'];
                                                action+='<a href='+add_url+'  data-id="' + data +
                                                '" data-toggle = "tooltip" title data-original-title  = "View / Edit" class="action-icons  badge badge-light-primary mr-50">' +  feather.icons['edit'].toSvg({
                                                    class: 'font-small-4 '
                                                }) + '</a>';

                                            if($approval_status==0 && full['profile_updated']==1)
                                            {
                                                action+='<a href="#"  data-id="' + data +
                                                '" data-toggle = "tooltip" title data-original-title = "Approve / Disapprove" class="action-icons badge badge-light-success mr-50 approve-accessor">'+ feather.icons['check-square'].toSvg({
                                                    class: 'font-small-4 '
                                                }) + '</a>' ;
                                            }

                                            if($approval_status==2 && full['profile_updated']==1)
                                            {
                                                action+='<a href="#"  data-id="' + data +
                                                '" data-toggle = "tooltip" title data-original-title = "Approve" class="action-icons badge badge-light-success mr-50 approve-accessor1">'+ feather.icons['check-square'].toSvg({
                                                    class: 'font-small-4 '
                                                }) + '</a>' ;
                                            }

                                            if($approval_status>0 && full['profile_updated']==1)
                                            {
                                                action+='<a href="#"  data-id="' + data +
                                                '" data-toggle = "tooltip" title data-original-title = "Remark" class="action-icons badge badge-light-danger mr-50 show-remark">'+ feather.icons['eye'].toSvg({
                                                    class: 'font-small-4 '
                                                }) + '</a>';
                                            }

                                            action+='</div>';

                                            return action;
                                        // return data
                                    }
                                }

                            ],
                            dom: '<"row d-flex justify-content-between align-items-center m-1"' +
                                '<"col-lg-1 d-flex align-items-center"l>' + '<"col-lg-5 "f>' +
                                '<"col-lg-5 d-flex align-items-center justify-content-lg-end flex-wrap  p-0"<"dt-action-buttons text-right"B>>' +
                                '>t' + '<"d-flex justify-content-between  mx-2 my-2"' +
                                '<"col-sm-12 col-md-6"i>' + '<"col-sm-12 col-md-6"p>' + '>',
                            // orderCellsTop: !0,
                            select: {
                                style: "multi",
                                selector: "td:first-child",
                                items: "row"
                            },
                            language: {
                                sLengthMenu: 'Show _MENU_',
                                search: '',
                                searchPlaceholder: 'Search',
                                paginate: {
                                    previous: '&nbsp;',
                                    next: '&nbsp;'
                                }
                            },
                            buttons: [{
                                extend: 'collection',
                                className: 'btn pr-1 btn-outline-secondary dropdown-toggle ml-1',
                                text: '<i class="fas fa-tasks "></i>' + 'Bulk Actions',
                                buttons: [{
                                        text: feather.icons['edit'].toSvg({
                                                class: 'font-small-4 mr-50'
                                        }) + 'Update Status ',
                                        className: 'dropdown-item',
                                        attr: {
                                            id: 'statusBulkUpdate'
                                        },
                                        action: function(e, dt, node, config) {
                                            var that = this;
                                            // show sweetalert ...
                                            if (rows_selected.length  > 0 ) {
                                            $("#changeStatusModal").modal('show');
                                            $('#statusBulkUpdateSubmit').attr('disabled',false);
                                            $('#statusBulkUpdateSubmit').html('Submit');
                                            }else{
                                                JsUtility.showToastr('error', 'Assessor', 'Please Select Assessor');
                                            }
                                        }
                                    },
                                    {
                                        text: feather.icons['file-text'].toSvg({
                                            class: 'font-small-4 mr-50'
                                        }) + 'Export',
                                        className: 'dropdown-item',
                                        action:function(e, dt, node, config) {
                                            let redirect=encodeURI(export_url);
                                            window.location=redirect;
                                            // console.log(export_url)
                                        }
                                    }
                                ],
                            }, {
                                text: feather.icons['plus'].toSvg({
                                    class: 'mr-50 font-small-4'
                                }) + 'Add Assessor',
                                className: 'btn btn-primary btn-add-record ml-2',
                                attr: {
                                    "data-toggle": "modal",
                                    "data-target": "#addAccessorModal"
                                },
                                init: function(api, node, config) {
                                    $(node).removeClass('btn-secondary');
                                }
                            }
                            ],
                            drawCallback: function() {
                                $(document).find('[data-toggle="tooltip"]').tooltip();
                            }
                        });
                    }

                    dataTable.on( 'draw', function () {
                        $('.dt-checkboxes').prop('checked',false)
                        $('#checkboxSelectAll').prop('checked',false)
                    } );

                    $("#filterButton").click(function(e) {
                        e.preventDefault();
                        dataTable.draw();
                    });

                    $("#filterResetButton").click(function(e) {
                        e.preventDefault();
                        $("#filterName").val('')
                        $("#filterMobile").val('')
                        $("#filterEmail").val('')
                        $("#filterStatus").val('')
                        $('#filterState').val('')
                        $("#filterVerification_status").val('')
                        $("#filterCategory").val('')
                        $("#filterRegion").val('')

                        dataTable.draw();
                    });

                    $(document).on('click','#statusBulkUpdateSubmit' ,function () {
                        $('#statusBulkUpdateSubmit').attr('disabled',true);
                        $('#statusBulkUpdateSubmit').html('Loading...');
                            let update_url ="{{ route('admin.assessors.bulk_update') }}";
                            let status=$("#changeAccessorStatus").val();
                             if (rows_selected.length  > 0 ) {
                                $.ajax({
                                    type: "put",
                                    url: update_url,
                                    data: {
                                        assessor_id:rows_selected,
                                        status:status
                                    },

                                    success: function (response) {
                                        console.log(response)
                                        if (response.success) {
                                            $("#changeStatusModal").modal('hide');
                                            rows_selected=[];
                                            dataTable.draw();
                                            $("#changeAccessorStatus").val();
                                            JsUtility.showToastr('success', 'Assessor', response.message);
                                        } else {
                                            $('#statusBulkUpdateSubmit').attr('disabled',false);
                                            $('#statusBulkUpdateSubmit').html('Submit');
                                            $.each(response.errors, function (key, error) {
                                                $('#changeStatusForm').find("#" + key + "-error").text(error[0]);
                                            });
                                        }
                                    }
                                });
                             }else{
                                JsUtility.showToastr('error', 'Assessor Update', 'Please Select accessor');
                             }
                    });

                    $(document).on('click', '.edit-accessor', function(e) {
                        e.preventDefault();
                        let id = $(this).data('id');
                        let url = "{{ route('admin.assessors.show', '') }}"
                        let form_url = "{{ route('admin.assessors.update', '') }}"
                        $('span.error').html('');
                        url += '/' + id;
                        form_url += '/' + id;
                        $.get(url, function(data) {
                            $("#editAccessorForm").attr('action', form_url);
                            $("#editAccessorName").val(data.name);
                            $("#editAccessorEmail").val(data.email);
                            $("#editAccessorMobile").val(data.mobile);
                            $("#editAccessorState").val(data.state_id);
                            $("#editAccessorStatus").val(data.status);
                            $("#editAccessorModal").modal('show');
                            $('#editAssessorSubmit').attr('disabled',false);
                            $('#editAssessorSubmit').html('Submit');
                        });
                    });

                    $(document).on('click', '.show-remark', function(e) {
                        e.preventDefault();
                        let id = $(this).data('id');
                        let url = "{{ route('admin.assessors.logs', '') }}";
                        url += '/' + id;
                        $.get(url, function(data) {
                            const d = new Date(data.verified_at)
                            const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d)
                            const mo = new Intl.DateTimeFormat('en', { month: 'long' }).format(d)
                            const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d)
                            document.getElementById("remarkDate").innerHTML = da+' '+ mo + ', '+ ye;
                            document.getElementById("remarkby").innerHTML = data.name;
                            if(data.verified_status==2)
                            {
                                document.getElementById("remarkstatus").innerHTML = 'Disapproved';
                                $("#remarkstatus").addClass('badge-light-danger');
                            }
                            else{
                                document.getElementById("remarkstatus").innerHTML = 'Approved';
                                $("#remarkstatus").addClass('badge-light-success');
                            }
                            document.getElementById("remarkcontent").innerHTML = data.verified_remark;
                            $("#showRemarkModal").modal('show');
                        });
                    });

                    // $("#editAccessorForm").submit(function(e) {
                    //     e.preventDefault();
                    //     $('#editAssessorSubmit').attr('disabled',true);
                    //     $('#editAssessorSubmit').html('Loading...');
                    //     let _FORM = $(this);
                    //     let url = $(this).attr('action');

                    //     $.ajax({
                    //         method: "PUT",
                    //         url: url,
                    //         data: $(this).serialize()
                    //     }).done(function(response) {
                    //         if (response.success) {
                    //             $("#editAccessorModal").modal('hide');
                    //             dataTable.draw();
                    //             JsUtility.showToastr('success', 'Assessor', response.message);
                    //         } else {
                    //             $('#editAssessorSubmit').attr('disabled',false);
                    //             $('#editAssessorSubmit').html('Submit');
                    //             _FORM.find('span.error').html('');
                    //             // _FORM.find("input.").removeClass('error');
                    //             $.each(response.errors, function(index, item) {
                    //                 _FORM.find("." + index).removeClass('error').addClass('error');
                    //                 _FORM.find('span#' + index + '-error-edit').html(item[
                    //                     0]);
                    //             });
                    //         }
                    //     }).fail(function(jqXHR, response) {
                    //         $('#editAssessorSubmit').attr('disabled',false);
                    //         $('#editAssessorSubmit').html('Submit');
                    //         // console.log(response);
                    //         JsUtility.showToastr('error', 'Assessor', 'Some error occurred!');
                    //     });
                    // });

                    $('#editAccessorForm').validate({
                        rules: {
                            'name': {
                                required: true,
                                noSpace: true,
                                doubleSpace:true,
                                maxlength: 50,
                                minlength:3
                            },
                            'email': {
                                required: true,
                                // email: true,
                                emailAddress:true
                            },
                            'mobile':{
                                required: true,
                                noSpace: true,
                                digits:true,
                                minlength:10,
                                mobileNo: true
                            },
                            'status':{
                                required: true,
                            }
                        },
                        submitHandler : function(form){
                            $('#editAssessorSubmit').attr('disabled',true);
                            $('#editAssessorSubmit').html('Loading...');
                            $('form#editAccessorForm').find("-back-error").css('display','none');
                            let url = $('form#editAccessorForm').attr('action');

                            $.ajax({
                                method: "post",
                                url: $('form#editAccessorForm').attr('action'),
                                data: $(form).serialize()
                            }).done(function(response) {
                                $('#editAssessorSubmit').attr('disabled',false);
                                $('#editAssessorSubmit').html('Submit');
                                if (response.success) {
                                    dataTable.draw();
                                    $('form#editAccessorForm').find("-back-error").css('display','none');
                                    $('form#editAccessorForm').find("-back-error").html('');
                                    $('#editAssessorSubmit').attr('disabled',false);
                                    $('#editAssessorSubmit').html('Submit');
                                    $("#editAccessorModal").modal('hide');
                                    JsUtility.showToastr('success', 'Assessor', response.message);
                                } else {
                                    form.find('span.error').html('');
                                    $.each(response.errors, function(index, item) {
                                        $('form#editAccessorForm').find("." + index).removeClass('error').addClass('error');
                                        $('form#editAccessorForm').find("#" + index + "-back-error").text(item[0]);
                                        $('form#editAccessorForm').find("#" + index + "-back-error").css('display','block');
                                    });
                                    // JsUtility.showToastr('error', 'Assessor', 'Some error occured!');
                                }
                            }).fail(function(jqXHR, response) {
                                $('form#editAccessorForm').find("-back-error").css('display','none');
                                $('#editAssessorSubmit').attr('disabled',false);
                                $('#editAssessorSubmit').html('Submit');
                                JsUtility.showToastr('error', 'Assessor', 'Some error occured!');
                            });
                        }
                    });
                    jQuery.validator.addMethod("mobileNo", function(value, element) {
                    return this.optional(element) || /^[6-9][0-9]{9}$/.test(value);
                    }, "Mobile Number is invalid");

                    jQuery.validator.addMethod("noSpace", function(value, element) {
                    return value == '' || value.trim().length != 0;
                    }, "This field is required");

                    jQuery.validator.addMethod("doubleSpace", function(value, element) {
                    return this.optional(element) || /^\S((?!.*  ).*\S)?$/.test(value);
                    }, "Format entered is Invalid");

                    jQuery.validator.addMethod("emailAddress", function(value, element) {
                    return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
                    }, "Please Enter a Valid Email Address");

                    $('#addAccessorForm').validate({
                        rules: {
                            'name': {
                                required: true,
                                noSpace: true,
                                doubleSpace:true,
                                maxlength: 50,
                                minlength:3
                            },
                            'email': {
                                required: true,
                                doubleSpace:true,
                                // email: true,
                                emailAddress:true
                            },
                            'mobile':{
                                required: true,
                                digits:true,
                                minlength:10,
                                mobileNo: true
                            },
                            // 'status':{
                            //     required: true,
                            // }
                        },
                        submitHandler : function(form){
                            $("#AddAssessor").attr("disabled",true);
                            $("#AddAssessor").html("Loading...");
                            $('form#addAccessorForm').find("-back-error").css('display','none');
                            let url = $('form#addAccessorForm').attr('action');

                            $.ajax({
                                method: "post",
                                url: $('form#addAccessorForm').attr('action'),
                                data: $(form).serialize(),
                                success: function(response) {
                                    if(response.status == 'validations') {
                                        $("#AddAssessor").attr("disabled",false);
                                        $("#AddAssessor").html("Submit");
                                        $('form#addAccessorForm').find('span.error').html('');
                                        $.each(response.errors, function(index, item) {
                                            $('form#addAccessorForm').find("#" + index).removeClass('error').addClass('error');
                                            $('form#addAccessorForm').find("#" + index + "-back-error").text(item[0]);
                                            $('form#addAccessorForm').find("#" + index + "-back-error").css('display','block');
                                        });
                                    }
                                    if(response.status == 'success') {
                                        $("#AddAssessor").attr("disabled",false);
                                        $("#AddAssessor").html("Submit");
                                        $('form#addAccessorForm').find("-back-error").css('display','none');
                                        dataTable.draw();
                                        $("#addAccessorForm").trigger("reset");
                                        $("#addAccessorModal").modal('hide');
                                        $("#addAccessorForm span").html('');
                                        JsUtility.showToastr('success', 'Assessor', response.message);
                                    }
                                    if(response.status == 'error') {
                                        $("#AddAssessor").attr("disabled",false);
                                        $("#AddAssessor").html("Submit");
                                        $('form#addAccessorForm').find("-back-error").css('display','none');
                                        JsUtility.showToastr('error', 'Assessor', response.message);
                                    }
                                },
                                error: function() {
                                    $("#AddAssessor").attr("disabled",false);
                                    $("#AddAssessor").html("Submit");
                                    $('form#addAccessorForm').find("-back-error").css('display','none');
                                    JsUtility.showToastr('error', 'Assessor', response.message);
                                }
                            })
                        }
                    });


                    $(document).on('click','.approve-accessor',function (e) {
                        e.preventDefault();

                        let vender_id = $(this).data('id');
                        let approve_url="{{route('admin.assessors.approve','')}}"
                        approve_url+='/'+vender_id;
                        Swal.fire({
                            title: 'Do you want to Approve?',
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Yes, Approve it!',
                            showDenyButton: true,
                            // denyButtonText: 'No, Reject it',
                            denyButtonText: 'No, Disapprove it',
                            showClass: {
                                popup: 'animate__animated animate__fadeIn'
                            },
                            customClass: {
                                confirmButton: 'btn btn-primary',
                                cancelButton: 'btn btn-outline-danger ml-1',
                                denyButton: 'btn btn-secondary ml-1',
                            },
                            buttonsStyling: false
                        }).then((result) => {
                            /* Read more about isConfirmed, isDenied below */
                            if (result.isConfirmed) {
                                $.get(approve_url, {status:1},
                                    function (responce) {
                                        if (responce.success) {
                                            Swal.fire({
                                                icon: 'success',
                                                title: 'Approved!',
                                                confirmButtonText: 'Okay',
                                                text: 'Your Assessor has been Approved.',
                                                showClass: {
                                                    popup: 'animate__animated animate__fadeIn'
                                                },
                                                customClass: {
                                                    confirmButton: 'btn btn-primary'
                                                },
                                                buttonsStyling: false
                                            }).then((result2) => {
                                            if (result2.isConfirmed) {
                                                dataTable.draw();
                                            location.reload();
                                            }
                                            else if(Swal.DismissReason.backdrop) {
                                                dataTable.draw();
                                                location.reload();
                                                }
                                            })
                                        }else{
                                            Swal.fire('Approved!',responce.message, 'error');
                                        }
                                    }
                                );

                            } else if (result.isDenied) {
                                Swal.fire({
                                    title: 'Reason for Disapproval!',
                                    input: 'textarea',
                                    inputAttributes: {
                                        maxlength: 150,
                                        required: true,
                                    },
                                    showCancelButton: true,
                                    confirmButtonText: 'Submit',
                                    showClass: {
                                        popup: 'animate__animated animate__fadeIn'
                                    },
                                    customClass: {
                                        confirmButton: 'btn btn-primary',
                                        cancelButton: 'btn btn-outline-danger ml-1',
                                        denyButton: 'btn btn-secondary ml-1',
                                    },
                                    buttonsStyling: false,
                                    preConfirm: (remark) => {
                                        $.get(approve_url, {status:2,remark:remark},
                                        function (responce) {
                                            if (responce.success) {
                                                    Swal.fire('Disapproved!', responce.message, 'success');
                                                    dataTable.draw();
                                                }else{
                                                    Swal.fire('Disapproved!', responce.message, 'error');
                                                }
                                            }
                                        );
                                    },
                                })
                            }
                        })

                    });

                    $(document).on('click','.approve-accessor1',function (e) {
                        e.preventDefault();

                        let vender_id = $(this).data('id');
                        let approve_url="{{route('admin.assessors.approve','')}}"
                        approve_url+='/'+vender_id;
                        Swal.fire({
                            title: 'Do you want to Approve?',
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Yes, Approve it!',
                            showDenyButton: false,
                            // denyButtonText: 'No, Reject it',
                            showClass: {
                                popup: 'animate__animated animate__fadeIn'
                            },
                            customClass: {
                                confirmButton: 'btn btn-primary',
                                cancelButton: 'btn btn-outline-danger ml-1',
                            },
                            buttonsStyling: false
                        }).then((result) => {
                            /* Read more about isConfirmed, isDenied below */
                            if (result.isConfirmed) {
                                $.get(approve_url, {status:1},
                                    function (responce) {
                                        if (responce.success) {
                                            Swal.fire({
                                                icon: 'success',
                                                title: 'Approved!',
                                                confirmButtonText: 'Okay',
                                                text: 'Your Assessor has been Approved.',
                                                showClass: {
                                                    popup: 'animate__animated animate__fadeIn'
                                                },
                                                customClass: {
                                                    confirmButton: 'btn btn-primary'
                                                },
                                                buttonsStyling: false
                                            }).then((result2) => {
                                            if (result2.isConfirmed) {
                                                dataTable.draw();
                                            location.reload();
                                            }
                                            else if(Swal.DismissReason.backdrop) {
                                                dataTable.draw();
                                                location.reload();
                                                }
                                            })
                                        }else{
                                            Swal.fire('Approved!',responce.message, 'error');
                                        }
                                    }
                                );

                            } else if (result.isDenied) {
                                Swal.fire({
                                    title: 'Reason for Rejection!',
                                    input: 'textarea',
                                    showCancelButton: true,
                                    confirmButtonText: 'Submit',
                                    showClass: {
                                        popup: 'animate__animated animate__fadeIn'
                                    },
                                    customClass: {
                                        confirmButton: 'btn btn-primary',
                                        cancelButton: 'btn btn-outline-danger ml-1',
                                        denyButton: 'btn btn-secondary ml-1',
                                    },
                                    buttonsStyling: false,
                                    preConfirm: (remark) => {
                                        $.get(approve_url, {status:2,remark:remark},
                                        function (responce) {
                                            if (responce.success) {
                                                    Swal.fire('Disapproved!', responce.message, 'success');
                                                    dataTable.draw();
                                                }else{
                                                    Swal.fire('Disapproved!', responce.message, 'error');
                                                }
                                            }
                                        );
                                    },
                                })
                            }
                        })

                    });

                    $(document).on('change', '.selected-accessor', function() {
                        var id = $(this).val();
                        // Determine whether row ID is in the list of selected row IDs
                        var index = $.inArray(id, rows_selected);
                        // If checkbox is checked and row ID is not in list of selected row IDs
                        if (this.checked && index === -1) {
                            rows_selected.push(id);
                        } else if (!this.checked && index !== -1) {
                            rows_selected.splice(index, 1);
                        }
                    })

                    // $(document).on('click', '#exportExceleData', function(e) {
                    //     e.preventDefault();

                    //     var url = "{{ route('admin.assessors.bulk_export') }}?export=1";

                    //     if ($('body').find('div.dataTables_filter input[type="search"]').val() != '') url +=
                    //         "&fsearch=" + encodeURIComponent($('#fsearch').val());

                    //     window.location.href = url;
                    // });

                    $("#filterButton").click(function(e) {
                        e.preventDefault();
                        dataTable.draw();
                        export_url=main_export_url;
                        let parameter="";
                        if ($("#filterName").val()!="") {
                            export_url +=("name="+$("#filterName").val())
                            parameter="name";
                        }

                        if ($("#filterMobile").val()!="") {

                            if (parameter=="") {
                                export_url +=("mobile="+$("#filterMobile").val())
                            } else {
                                export_url +=("&mobile="+$("#filterMobile").val())
                            }
                            parameter="mobile";
                        }

                        if ($("#filterEmail").val()!="") {

                            if (parameter=="") {
                                export_url +=("email="+$("#filterEmail").val())
                            } else {
                                export_url +=("&email="+$("#filterEmail").val())
                            }
                            parameter="email";
                        }

                        if ($("#filterStatus").val()!="") {

                            if (parameter=="") {
                                export_url +=("status="+$("#filterStatus").val())
                            } else {
                                export_url +=("&status="+$("#filterStatus").val())
                            }
                            parameter="status";
                        }

                        if ($("#filterState").val()!="") {

                           if (parameter=="") {
                               export_url +=("state="+$("#filterState").val())
                           } else {
                               export_url +=("&state="+$("#filterState").val())
                           }
                           parameter="state";
                       }

                       if ($("#filterCategory").val()!="") {

                           if (parameter=="") {
                               export_url +=("category="+$("#filterCategory").val())
                           } else {
                               export_url +=("&category="+$("#filterCategory").val())
                           }
                           parameter="category";
                       }


                        if ($("#filterVerification_status").val()!="") {

                            if (parameter=="") {
                                export_url +=("verificationStatus="+$("#filterVerification_status").val())
                            } else {
                                export_url +=("&verificationStatus="+$("#filterVerification_status").val())
                            }
                            parameter="verificationStatus";
                        }

                        if ($("#filterProfile_status").val()!="") {

                            if (parameter=="") {
                                export_url +=("profileStatus="+$("#filterProfile_status").val())
                            } else {
                                export_url +=("&profileStatus="+$("#filterProfile_status").val())
                            }
                            parameter="profileStatus";
                        }


                    });

                }

            }

        }();

        jQuery(document).ready(function() {
            Accessor.init();

        });
        $('input[type=text]').on('keyup',function(event){
        event.currentTarget.value = this.value.toUpperCase();
        });
    </script>
@endpush
