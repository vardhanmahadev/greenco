<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<!-- Head -->

<head>
    <meta charset="utf-8" />
    <title>Assessor</title>
    <meta name="description" content="blank page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
    <!-- Page Content -->
    <div class="page-content">
        <!-- Page Body -->
        <div class="page-body">
            <div id="to_print">
                <table class="tabletable-bordered">
                    <thead>
                        <tr style="background:#c2c2c2;">
                            <th> # </th>
                            <th> Name </th>
                            <th> Email Address </th>
                            <th> Mobile Number </th>
                            <th> Industry Category </th>
                            <th> Alternate Mobile Number</th>
                            <th> Address Line 1 </th>
                            <th> Address Line 2 </th>
                            <th> City </th>
                            <th> State </th>
                            <th> Pincode </th>
                            <th> PAN Number </th>
                            <th> GST Number </th>
                            <th> Assessor Grade </th>
                            <th> Enrollment Date </th>
                            <th> Lead Assessor </th>
                            <th> Emergency Contact Name </th>
                            <th> Emergency Contact Mobile Number </th>
                            <th> Emergency Contact Address Line 1 </th>
                            <th> Emergency Contact Address Line 2 </th>
                            <th> Emergency Contact City </th>
                            <th> Emergency Contact State </th>
                            <th> Emergency Contact Pincode </th>
                            <th> Bank Name </th>
                            <th> Account Number </th>
                            <th> Branch Name </th>
                            <th> IFSC Code </th>
                            <th> Account Status </th>
                            <th> Approve Status </th>
                            <th> Profile Status </th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(!empty($assessors))
                        @foreach($assessors as $index => $assessor)
                            <tr>
                                <td>{{++$index}}</td>
                                <td>{{$assessor->name?$assessor->name:'N/A'}}</td>
                                <td>{{$assessor->email?$assessor->email:'N/A'}}</td>
                                <td>{{$assessor->mobile?$assessor->mobile:'N/A'}}</td>
                                @php $cdata = json_decode($assessor->category,TRUE); 
                                $categories =[];
                                @endphp
                                @if($cdata)
                                    @foreach((array)$cdata as $category)
                                        @php $category_name = \App\Models\AssessorCategories::where('id',$category)->first(); @endphp
                                        @php array_push($categories,$category_name->name) @endphp
                                    @endforeach
                                    <td>
                                        @foreach($categories as $c_name)
                                            {{$c_name}},
                                        @endforeach
                                    </td>
                                @else
                                    <td>N/A</td>
                                @endif
                                <td>{{$assessor->alt_mobile?$assessor->alt_mobile:'N/A'}}</td>
                                <td>{{$assessor->address_1?$assessor->address_1:'N/A'}}</td>
                                <td>{{$assessor->address_2?$assessor->address_2:'N/A'}}</td>
                                <td>{{$assessor->city?$assessor->city:'N/A'}}</td>
                                <td>{{$assessor->state_name?$assessor->state_name:'N/A'}}</td>
                                <td>{{$assessor->pincode?$assessor->pincode:'N/A'}}</td>
                                <td>{{$assessor->pan_number?$assessor->pan_number:'N/A'}}</td>
                                <td>{{$assessor->gst_number?$assessor->gst_number:'N/A'}}</td>
                                @php $grade = \App\Models\AssessorGrade::where('id',$assessor->assessor_grade)->first(); @endphp
                                <td>{{$grade?$grade->name:'N/A'}}</td>
                                <td>{{$assessor->enrollment_date? date('d/m/Y', strtotime($assessor->enrollment_date)):'N/A' }}</td>
                                @switch($assessor->lead_assessor)
                                    @case(0)
                                        <td>No</td>
                                        @break
                                    @case(1)
                                        <td>Yes </td>
                                    @break
                                @endswitch
                                <td>{{$assessor->em_contact_name?$assessor->em_contact_name:'N/A'}}</td>
                                <td>{{$assessor->em_contact_no?$assessor->em_contact_no:'N/A'}}</td>
                                <td>{{$assessor->em_address_1?$assessor->em_address_1:'N/A'}}</td>
                                <td>{{$assessor->em_address_2?$assessor->em_address_2:'N/A'}}</td>
                                <td>{{$assessor->em_city?$assessor->em_city:'N/A'}}</td>
                                @php $em_state_name = \App\Models\States::where('id',$assessor->em_state)->first(); @endphp
                                <td>{{$em_state_name?$em_state_name->name:'N/A'}}</td>
                                <td>{{$assessor->em_pincode?$assessor->em_pincode:'N/A'}}</td>
                                <td>{{$assessor->bank_name?$assessor->bank_name:'N/A'}}</td>
                                <td>{{$assessor->acc_no?$assessor->acc_no:'N/A'}}</td>
                                <td>{{$assessor->branch_name?$assessor->branch_name:'N/A'}}</td>
                                <td>{{$assessor->ifsc_code?$assessor->ifsc_code:'N/A'}}</td>

                                @switch($assessor->status)
                                    @case(0)
                                        <td>In Active</td>
                                        @break
                                    @case(1)
                                        <td>Active </td>
                                    @break
                                @endswitch
                                
                                @switch($assessor->verification_status)
                                    @case(0)
                                        <td>Pending</td>
                                        @break
                                    @case(1)
                                        <td>Approved </td>
                                    @break
                                    @case(2)
                                        <td>Disapproved </td>
                                    @break
                                @endswitch
                                
                                @switch($assessor->profile_updated)
                                    @case(0)
                                    <td>Pending</td>
                                    @break
                                    @case(1)
                                    <td>Completed</td>
                                    @break
                                @endswitch
                            </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /Page Body -->
    </div>
    <!-- /Page Content -->

</body>
<!--  /Body -->

</html>
