@extends(ADMIN_THEME_NAME.'.layouts.app')

@push('PAGE_ASSETS_CSS')
<link rel="stylesheet" type="text/css" href="{{url('app-assets/vendors/css/forms/wizard/bs-stepper.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('app-assets/css/plugins/forms/form-validation.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('app-assets/css/plugins/forms/form-wizard.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
select.form-control:disabled{
        background-color:#e9e9e9!important;
    }
    [type="radio"]:checked + label:after{
        background: #78A2CC!important;
    }
</style>
@endpush
@section('content')
<div class="app-content content content-ps-stl">
    <div class="">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <ul class="nav nav-tabs tabs-pages tab-leftside" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link tab-links active mini-border" href="{{route('admin.assessors.addEditAssessor',$assessor)}}" aria-selected="true">
                            <i data-feather="user"></i>Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link tab-links mini-border" href="{{route('admin.assessors.nda',$assessor)}}" aria-controls="changepasswordIcon" aria-selected="false">
                            <i data-feather="lock"></i>Yearly NDA's</a>
                    </li>
                </ul>
                <section class=" p-relative">
                    <div class="col-12">
                        <section id="advanced-search-datatable ">
                        <form action="{{ route('admin.assessors.update',$assessor) }}" class="editAccessorForm" id="editAccessorForm" method="POST" enctype="multipart/form-data">
                                            @method('PUT')
                                            @csrf
                            <div class="card ">
                                <div class="row">
                                    <div class="col-lg-12 col-xl-12 col-md-12 col-12" style="margin-bottom: -2%;">
                                            <div class="d-flex v-row title-strip">
                                                <div class="col-6 pl-0">Basic Details</div>
                                                <div class="col-6 pl-0" style="text-align: end;">Profile Status: <p style="margin-top:8px" class="font-small-3 badge badge-light-{{$assessor->profile_updated =='1' ? 'success' : 'warning' }} badge-pill ">{{$assessor->profile_updated =='1' ? "Completed" : "Pending"}}</p></div>
                                            </div>
                                            <div class="col-lg-12 mt-1 pl-1">
                                            <ul class="info-form">
                                                <li>
                                                <span>Note:</span> The Applicant has to fill Basic Details along with Biodata file upload to be Assessor.  
                                            </li>
                                            </ul>
                                        </div>
                                            
                                            <div class="card-body ">
                                                <div class="">
                                                    <div class="row ">
                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Name <span class="text-danger">*</span></label>
                                                            </div>
                                                            <input type="text" class="form-control dt-input bg-white" required  placeholder="Name" name="name" id="name" value="{{isset($assessor->name)?$assessor->name:''}}">
                                                            <span class="error" id="name_error" ></span>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Email Address <span class="text-danger">*</span></label>
                                                            </div>
                                                            <input type="email" class="form-control dt-input bg-white" required placeholder="Email Address" name="email" id="email" value="{{isset($assessor->email)?$assessor->email:''}}">
                                                            <span class="error" id="email_error" ></span>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Mobile Number <span class="text-danger">*</span></label>
                                                            </div>
                                                            <input type="text" class="form-control dt-input bg-white" required placeholder="Mobile" name="mobile" id="mobile" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="10" value="{{isset($assessor->mobile)?$assessor->mobile:''}}">
                                                            <span class="error" id="mobile_error" ></span>
                                                            <!-- <span id="mobile_syntax_error" style="color:red;display:none">Please enter a valid mobile number.</span> -->
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 selectcheck-div">
                                                            <div class="form-label-group form-group select-checkvarlist ">
                                                                <label  class="ind-category-label font-small-4">Industry Category <span class="text-danger">*</span></label>
                                                                <div class="select-tag ">
                                                                    <select class="select2 form-control form-control-md" required="true" multiple data-placeholder="Select Industry Category" name="ind_category[]" id="ind_category">
                                                                        
                                                                        @php $cdata = json_decode($assessor->category,TRUE); @endphp
                                                                        @foreach($categories as $category)
                                                                            <!-- <option value="{{$category->id}}" @if(isset($profile))@if($profile->category == $category->id) selected @endif @endif>{{$category->name}}</option> -->
                                                                            @if($cdata && in_array($category->id,$cdata))
                                                                                <option value="{{$category->id}}" selected>{{$category->name}}</option> 
                                                                                @else
                                                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <span class="error" id="ind_category_error" ></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Alternate Mobile Number</label>
                                                            </div>
                                                            <input type="text" class="form-control dt-input bg-white" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="10" placeholder="Alternate Mobile" name="alt_mobile" id="alt_mobile" value="{{isset($assessor->alt_mobile)?$assessor->alt_mobile:''}}">
                                                            <span class="error" id="alt_mobile_error" ></span>
                                                            <!-- <span id="mobile_syntax_error" style="color:red;display:none">Please enter a valid mobile number.</span> -->
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Address Line 1 <span class="text-danger">*</span></label>
                                                            </div>
                                                            <input type="text" class="form-control dt-input bg-white" required placeholder="Address Line 1" name="address_1" id="address_1" value="{{isset($assessor->address_1)?$assessor->address_1:''}}">
                                                            <span class="error" id="address_1_error" ></span>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Address Line 2</label>
                                                            </div>
                                                            <input type="text" class="form-control dt-input bg-white" placeholder="Address Line 2" name="address_2" id="address_2" value="{{isset($assessor->address_2)?$assessor->address_2:''}}">
                                                            <span class="error" id="address_2_error" ></span>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">City <span class="text-danger">*</span></label>
                                                            </div>
                                                            <input type="text" class="form-control dt-input bg-white" onkeypress="$('#city_error').text(''); return /[a-zA-Z\s]/i.test(event.key);"  placeholder="City" name="city" id="city" value="{{isset($assessor->city)?$assessor->city:''}}">
                                                            <span class="error" id="city_error" ></span>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">State <span class="text-danger">*</span></label>
                                                            </div>
                                                            <select class="form-control" name="state"  id="state">
                                                                <option value="">Select State</option>
                                                                @foreach($states as $state)
                                                                    <option value="{{$state->id}}" @if(isset($assessor))@if($assessor->state == $state->id) selected @endif @endif>{{$state->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="error" id="state_error" ></span>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Pincode <span class="text-danger">*</span></label>
                                                            </div>
                                                            <input type="text" class="form-control dt-input bg-white"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="6" placeholder="Pincode" name="pincode" id="pincode" value="{{isset($assessor->pincode)?$assessor->pincode:''}}">
                                                            <span class="error" id="pincode_error" ></span>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">PAN Number <span class="text-danger">*</span></label>
                                                            </div>
                                                            <input type="text" class="form-control dt-input bg-white" maxlength="10" onkeypress="$('#pan_number_error').text(''); return /[A-Z0-9]/i.test(event.key);" placeholder="PAN Number" name="pan_number" id="pan_number" value="{{isset($assessor->pan_number)?$assessor->pan_number:''}}">
                                                            <span class="error" id="pan_number_error" ></span>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">GST Number <span class="text-danger">*</span></label>
                                                            </div>
                                                            <input type="text" class="form-control dt-input bg-white" maxlength="15" onkeypress="$('#gst_number_error').text(''); return /[A-Z0-9]/i.test(event.key);" placeholder="GST Number" name="gst_number" id="gst_number" value="{{isset($assessor->gst_number)?$assessor->gst_number:''}}">
                                                            <span class="error" id="gst_number_error" ></span>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Assessor Grade <span class="text-danger">*</span></label>
                                                            </div>
                                                            <select class="form-control" name="assessor_grade" id="assessor_grade">
                                                                <option value="">Select Grade</option>
                                                                @foreach($grades as $grade)
                                                                    <option value="{{$grade->id}}" @if(isset($assessor))@if($assessor->assessor_grade == $grade->id) selected @endif @endif>{{$grade->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            <span class="error" id="assessor_grade_error" ></span>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Enrollment Date <span class="text-danger">*</span></label>
                                                            </div>
                                                            <input type="text" class="form-control dt-date flatpickr-human-friendly dt-input bg-white" id="enrollment_date" name="enrollment_date" data-column="2" placeholder="Enrollment Date" value="{{$assessor->enrollment_date?date('d-m-Y',strtotime($assessor->enrollment_date)):''}}"/>
                                                            <span class="error" id="enrollment_date_error" ></span>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Lead Assessor <span class="text-danger">*</span></label>
                                                            </div>
                                                            <div class="col-lg-5 d-flex">
                                                                <div class="demo-inline-spacing" style="margin-top:7px">
                                                                    <div class="radio-custom  mr-1">
                                                                        <input type="radio" id="Yes" name="lead_assessor" class="custom-inline" value="1" @if($assessor->lead_assessor==1) checked @endif/>
                                                                        <label class="custom-control-label" for="Yes">Yes</label>
                                                                    </div>
                                                                    <div class="radio-custom">
                                                                        <input type="radio" id="No" name="lead_assessor" class="custom-inline" value="0" @if($assessor->lead_assessor==0) checked @endif/>
                                                                        <label class="custom-control-label" for="No">No</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <span class="error" id="lead_assessor_error" ></span>
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 form-group">
                                                            <div class=""><label class="font-small-4">Account Status <span class="text-danger">*</span></label>
                                                            </div>
                                                            <select class="form-control" name="status" id="status">
                                                                <option value="">Select Status</option>
                                                                <option value="1" @if(isset($assessor))@if($assessor->status==1) selected @endif @endif>Active</option>
                                                                <option value="0" @if(isset($assessor))@if($assessor->status==0) selected @endif @endif>In-Active</option>
                                                            </select>
                                                            <span class="error" id="status_error" ></span>
                                                        </div>
                                                    </div>
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-xl-12 col-md-12 col-12" style="margin-bottom: -2%;">
                                            <div class="d-flex v-row title-strip">
                                                <div class="col-6 pl-0">Emergency Contact Details</div>
                                            </div>
                                                <div class="card-body ">
                                                    <div class="">
                                                        <div class="row ">
                                                            <div class="col-lg-4 col-md-4 form-group">
                                                                <div class=""><label class="font-small-4">Contact Name <span class="text-danger">*</span></label>
                                                                </div>
                                                                <input type="text" class="form-control dt-input bg-white" onkeypress="$('#em_contact_name_error').text(''); return /^[a-zA-Z\s]+$/i.test(event.key);"  minlength="3" maxlength="30" placeholder="Name" name="em_contact_name" id="em_contact_name" value="{{isset($assessor->em_contact_name)?$assessor->em_contact_name:''}}">
                                                                <span class="error" id="em_contact_name_error"></span>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 form-group">
                                                                <div class=""><label class="font-small-4">Mobile Number <span class="text-danger">*</span></label>
                                                                </div>
                                                                <input type="text" class="form-control dt-input bg-white"  placeholder="Mobile Number" onkeypress='return event.charCode >= 48 && event.charCode <= 57' minlength="10" maxlength="10" name="em_contact_no" id="em_contact_no" value="{{isset($assessor->em_contact_no)?$assessor->em_contact_no:''}}">
                                                                <span class="error" id="em_contact_no_error"></span>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 form-group">
                                                                <div class=""><label class="font-small-4">Address Line 1 <span class="text-danger">*</span></label>
                                                                </div>
                                                                <input type="text" class="form-control dt-input bg-white"  placeholder="Address Line 1" name="em_address_1" id="em_address_1" value="{{isset($assessor->em_address_1)?$assessor->em_address_1:''}}">
                                                                <span class="error" id="em_address_1_error" ></span>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 form-group">
                                                                <div class=""><label class="font-small-4">Address Line 2</label>
                                                                </div>
                                                                <input type="text" class="form-control dt-input bg-white" placeholder="Address Line 2" name="em_address_2" id="em_address_2" value="{{isset($assessor->em_address_2)?$assessor->em_address_2:''}}">
                                                                <span class="error" id="em_address_2_error" ></span>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 form-group">
                                                                <div class=""><label class="font-small-4">City <span class="text-danger">*</span></label>
                                                                </div>
                                                                <input type="text" class="form-control dt-input bg-white" onkeypress=" return /[a-zA-Z\s]/i.test(event.key);"  placeholder="City" name="em_city" id="em_city" value="{{isset($assessor->em_city)?$assessor->em_city:''}}">
                                                                <span class="error" id="em_city_error" ></span>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 v">
                                                                <div class=""><label class="font-small-4">State <span class="text-danger">*</span></label>
                                                                </div>
                                                                <select class="form-control" name="em_state"  id="em_state">
                                                                    <option value="">Select State</option>
                                                                    @foreach($states as $state)
                                                                        <option value="{{$state->id}}" @if(isset($assessor))@if($assessor->em_state == $state->id) selected @endif @endif>{{$state->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <span class="error" id="em_state_error"></span>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 form-group">
                                                                <div class=""><label class="font-small-4">Pincode <span class="text-danger">*</span></label>
                                                                </div>
                                                                <input type="text" class="form-control dt-input bg-white"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="6" placeholder="Pincode" name="em_pincode" id="em_pincode" value="{{isset($assessor->em_pincode)?$assessor->em_pincode:''}}">
                                                                <span class="error" id="em_pincode_error" ></span>
                                                            </div>
                                                        </div>
                                                        <br>
                                                    </div>
                                                </div>
                                    </div>
                                    <div class="col-lg-12 col-xl-12 col-md-12 col-12" style="margin-bottom: -2%;">
                                            <div class="d-flex v-row title-strip">
                                                <div class="col-6 pl-0">Bank Details</div>
                                            </div>
                                                <div class="card-body ">
                                                    <div class="">
                                                        <div class="row ">
                                                            <div class="col-lg-4 col-md-4 form-group">
                                                                <div class=""><label class="font-small-4">Bank Name <span class="text-danger">*</span></label>
                                                                </div>
                                                                <input type="text" class="form-control dt-input bg-white" maxlength="30" onkeypress="$('#bank_name_error').text(''); return /[a-zA-Z\s]/i.test(event.key);" placeholder="Bank Name" name="bank_name" id="bank_name" value="{{isset($assessor->bank_name)?$assessor->bank_name:''}}">
                                                                <span class="error" id="bank_name_error"></span>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 form-group">
                                                                <div class=""><label class="font-small-4">Account Number <span class="text-danger">*</span></label>
                                                                </div>
                                                                <input type="text" class="form-control dt-input bg-white" maxlength="20" onkeypress="$('#acc_no_error').text(''); return /[a-zA-Z0-9]/i.test(event.key);" placeholder="Account Number" name="acc_no" id="acc_no" value="{{isset($assessor->acc_no)?$assessor->acc_no:''}}">
                                                                <span class="error" id="acc_no_error"></span>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 form-group">
                                                                <div class=""><label class="font-small-4">Branch Name <span class="text-danger">*</span></label>
                                                                </div>
                                                                <input type="text" class="form-control dt-input bg-white" maxlength="30" placeholder="Branch Name" name="branch_name" id="branch_name" value="{{isset($assessor->branch_name)?$assessor->branch_name:''}}">
                                                                <span class="error" id="branch_name_error"></span>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 form-group">
                                                                <div class=""><label class="font-small-4">IFSC Code <span class="text-danger">*</span></label>
                                                                </div>
                                                                <input type="text" class="form-control dt-input bg-white" maxlength="11" placeholder="IFSC Code" name="ifsc_code" id="ifsc_code" onkeypress="$('#ifsc_code_error').text(''); return /[a-zA-Z0-9]/i.test(event.key);" value="{{isset($assessor->ifsc_code)?$assessor->ifsc_code:''}}">
                                                                <span class="error" id="ifsc_code_error"></span>
                                                            </div>
                                                        </div>
                                                        <br>
                                                    </div>
                                                </div>
                                       
                                    </div>
                                    <div class="col-lg-12 col-xl-12 col-md-12 col-12" style="margin-bottom: -2%;">
                                        <div class="d-flex v-row title-strip">
                                            <div class="col-6 pl-0">Upload Documents(*pdf, *jpg, *png, *jpeg)</div>
                                        </div>
                                        <div class="card-body ">
                                            <div class="">
                                                <div class="row ">
                                                    <div class="col-lg-12 form-group">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-4">
                                                                        <div class="form-group font-small-4">1. Biodata <span class="text-danger">*</span></label>
                                                                        </div>
                                                                    </div>
                                                                    @if(isset($assessor->biodata))
                                                                    <div class="col-lg-7 col-md-8">
                                                                        <div class="v-row uploadedfile border-bb mb-1">
                                                                            <div class="col-lg-8 pl-0"  style="word-break: break-all">
                                                                                <label class="mr-1 ">
                                                                                    <p class="mb-0">Uploaded File</p>
                                                                                    @php
                                                                                    $application_name7 = basename($assessor->biodata);
                                                                                    @endphp
                                                                                    <span>{{$application_name7}}</span>
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-lg-4 text-right pr-0">
                                                                            @if($assessor->biodata_status==1)
                                                                                    <a href="{{url($assessor->biodata)}}" target="_blank" class="btn btn-outline-secondary waves-effect" tooltip='View' data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                    <a href="{{url($assessor->biodata)}}" download class="btn btn-outline-secondary waves-effect ml-1" data-toggle="tooltip" title data-original-title="Download"><i data-feather="download"></i></a>
                                                                                @else   
                                                                                    <a href="{{url($assessor->biodata)}}" target="_blank" class="btn btn-outline-secondary waves-effect" tooltip='View' data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    @else
                                                                    <div class="col-lg-7 col-md-8">
                                                                        <div class="custom-file">
                                                                            <input type="file" class="custom-file-input" id="biodata" name="biodata">
                                                                            <label class="custom-file-label" for="biodata">Choose file</label>
                                                                            <span id="biodata_error" class="error"></span>
                                                                            <span id="biodata_file_error" style="color:red; display:none">Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only</span>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                </div>
                                                                    <div class="row">
                                                                    @if(isset($assessor->biodata) && $assessor->profile_updated==1)
                                                                        <div class="col-lg-5 col-md-4">
                                                                            <div class="form-group font-small-4">Approval Status : </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-7 col-md-8">
                                                                            <div class="form-group">
                                                                                @if($assessor->biodata_status==1)
                                                                                    @if($assessor->biodata_status==1)
                                                                                    <p class="font-small-4 badge badge-light-success badge-pill">Accepted</p>
                                                                                    @endif
                                                                                @endif
                                                                                @if($assessor->biodata_status==2)
                                                                                    @if($assessor->biodata==2)
                                                                                    <p class="font-small-4 badge badge-light-danger badge-pill">Not Accepted</p>
                                                                                    @endif
                                                                                @endif
                                                                                @if($assessor->biodata_status==0 || $assessor->biodata_status==NULL)
                                                                                <select name="biodata_status" id="biodata_status" onchange="biodataChange()" @if(isset($assessor->biodata_status))@if($assessor->biodata_status==2) disabled @endif @endif class="form-control">
                                                                                    <option value="">Select</option>
                                                                                    <option value="0" @if(isset($assessor->biodata_status))@if($assessor->biodata_status==0) selected @endif @endif>Under Review</option>
                                                                                    <option value="1" @if(isset($assessor->biodata_status))@if($assessor->biodata_status==1) selected @endif @endif>Accepted</option>
                                                                                    <option value="2" @if(isset($assessor->biodata_status))@if($assessor->biodata_status==2) selected @endif @endif>Not Accepted</option>
                                                                                </select>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        @else
                                                                        @endif
                                                                    </div>
                                                            </div>
                                                            @if(isset($assessor->biodata) && $assessor->profile_updated==1)
                                                            <div class="col-lg-6 remark-title7 remark-title" @if($assessor->biodata_status==1) style="display:none"@endif>
                                                                <label>Remarks :</label>
                                                                <textarea name="biodata_remark" id="biodata_remark" maxlength="150" class="form-control" rows="2">{{$assessor->biodata_remark}}</textarea>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 form-group">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-4">
                                                                        <div class="form-group font-small-4">2. Vendor Registration Form <span class="text-danger">*</span></label>
                                                                        </div>
                                                                    </div>
                                                                    @if(isset($assessor->vendor_stamp))
                                                                    <div class="col-lg-7 col-md-8">
                                                                        <div class="v-row uploadedfile border-bb mb-1">
                                                                            <div class="col-lg-8 pl-0" style="word-break: break-all">
                                                                                <label class="mr-1 ">
                                                                                    <p class="mb-0">Uploaded File</p>
                                                                                    @php
                                                                                    $application_name2 = basename($assessor->vendor_stamp);
                                                                                    @endphp
                                                                                    <span>{{$application_name2}}</span>
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-lg-4 text-right pr-0">
                                                                                @if($assessor->vendor_stamp_status==1)
                                                                                    <a href="{{url($assessor->vendor_stamp)}}" target="_blank" class="btn btn-outline-secondary waves-effect" tooltip='View' data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                    <a href="{{url($assessor->vendor_stamp)}}" download class="btn btn-outline-secondary waves-effect ml-1" data-toggle="tooltip" title data-original-title="Download"><i data-feather="download"></i></a>
                                                                                @else   
                                                                                    <a href="{{url($assessor->vendor_stamp)}}" target="_blank" class="btn btn-outline-secondary waves-effect" tooltip='View' data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    @else
                                                                    <div class="col-lg-7 col-md-8">
                                                                        <div class="custom-file">
                                                                            <input type="file" class="custom-file-input" id="vendor_stamp" name="vendor_stamp">
                                                                            <label class="custom-file-label" for="vendor_stamp">Choose file</label>
                                                                            <span id="vendor_stamp_error" class="error"></span>
                                                                            <span id="vendor_stamp_file_error" style="color:red; display:none">Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only</span>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                </div>
                                                                    <div class="row">
                                                                    @if(isset($assessor->vendor_stamp) && $assessor->profile_updated==1)
                                                                        <div class="col-lg-5 col-md-4">
                                                                            <div class="form-group font-small-4">Approval Status : </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-7 col-md-8">
                                                                            <div class="form-group">
                                                                                @if($assessor->vendor_stamp_status==1)
                                                                                    @if($assessor->vendor_stamp_status==1)
                                                                                    <p class="font-small-4 badge badge-light-success badge-pill">Accepted</p>
                                                                                    @endif
                                                                                @endif
                                                                                @if($assessor->vendor_stamp_status==2)
                                                                                    @if($assessor->vendor_stamp_status==2)
                                                                                    <p class="font-small-4 badge badge-light-danger badge-pill">Not Accepted</p>
                                                                                    @endif
                                                                                @endif
                                                                                @if($assessor->vendor_stamp_status==0 || $assessor->vendor_stamp_status==NULL)
                                                                                <select name="vendor_stamp_status"  id="vendor_stamp_status" onchange="vendorChange()" class="form-control" @if(isset($assessor->vendor_stamp_status))@if($assessor->vendor_stamp_status==2) disabled @endif @endif>
                                                                                    <option value="">Select</option>
                                                                                    <option value="0" @if(isset($assessor->vendor_stamp_status))@if($assessor->vendor_stamp_status==0) selected @endif @endif>Under Review</option>
                                                                                    <option value="1" @if(isset($assessor->vendor_stamp_status))@if($assessor->vendor_stamp_status==1) selected @endif @endif>Accepted</option>
                                                                                    <option value="2" @if(isset($assessor->vendor_stamp_status))@if($assessor->vendor_stamp_status==2) selected @endif @endif>Not Accepted</option>
                                                                                </select>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        @else
                                                                        @endif
                                                                    </div>
                                                            </div>
                                                            @if(isset($assessor->vendor_stamp) && $assessor->profile_updated==1)
                                                            <div class="col-lg-6 remark-title3 remark-title" @if($assessor->vendor_stamp_status==1) style="display:none" @endif>
                                                                <label>Remarks :</label>
                                                                <textarea name="vendor_stamp_remark" id="vendor_stamp_remark" maxlength="150" class="form-control" rows="2">{{$assessor->vendor_stamp_remark}}</textarea>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 form-group">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-4">
                                                                        <div class="form-group font-small-4">3. Non-Disclosure Agreement <span class="text-danger">*</span></label>
                                                                        </div>
                                                                    </div>
                                                                    @if(isset($assessor->ndc_form))
                                                                    <div class="col-lg-7 col-md-8">
                                                                        <div class="v-row uploadedfile border-bb mb-1">
                                                                            <div class="col-lg-8 pl-0"  style="word-break: break-all">
                                                                                <label class="mr-1 ">
                                                                                    <p class="mb-0">Uploaded File</p>
                                                                                    @php
                                                                                    $application_name3 = basename($assessor->ndc_form);
                                                                                    @endphp
                                                                                    <span>{{$application_name3}}</span>
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-lg-4 text-right pr-0">
                                                                                @if($assessor->ndc_form_status==1)
                                                                                    <a href="{{url($assessor->ndc_form)}}" target="_blank" class="btn btn-outline-secondary waves-effect" tooltip='View' data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                    <a href="{{url($assessor->ndc_form)}}" download class="btn btn-outline-secondary waves-effect ml-1" data-toggle="tooltip" title data-original-title="Download"><i data-feather="download"></i></a>
                                                                                @else   
                                                                                    <a href="{{url($assessor->ndc_form)}}" target="_blank" class="btn btn-outline-secondary waves-effect" tooltip='View' data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    @else
                                                                    <div class="col-lg-7 col-md-8">
                                                                        <div class="custom-file">
                                                                            <input type="file" class="custom-file-input" id="ndc_form" name="ndc_form">
                                                                            <label class="custom-file-label" for="ndc_form">Choose file</label>
                                                                            <span id="ndc_form_error" class="error"></span>
                                                                            <span id="ndc_form_file_error" style="color:red; display:none">Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only</span>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                </div>
                                                                    <div class="row">
                                                                    @if(isset($assessor->ndc_form) && $assessor->profile_updated==1)
                                                                        <div class="col-lg-5 col-md-4">
                                                                            <div class="form-group font-small-4">Approval Status : </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-7 col-md-8">
                                                                            <div class="form-group">
                                                                                @if($assessor->ndc_form_status==1)
                                                                                    @if($assessor->ndc_form_status==1)
                                                                                    <p class="font-small-4 badge badge-light-success badge-pill">Accepted</p>
                                                                                    @endif
                                                                                @endif
                                                                                @if($assessor->ndc_form_status==2)
                                                                                    @if($assessor->ndc_form_status==2)
                                                                                    <p class="font-small-4 badge badge-light-danger badge-pill">Not Accepted</p>
                                                                                    @endif
                                                                                @endif
                                                                                @if($assessor->ndc_form_status==0 || $assessor->ndc_form_status==NULL)
                                                                                <select name="ndc_form_status" id="ndc_form_status" onchange="ndcChange()" @if(isset($assessor->ndc_form_status))@if($assessor->ndc_form_status==2) disabled @endif @endif class="form-control">
                                                                                    <option value="">Select</option>
                                                                                    <option value="0" @if(isset($assessor->ndc_form_status))@if($assessor->ndc_form_status==0) selected @endif @endif>Under Review</option>
                                                                                    <option value="1" @if(isset($assessor->ndc_form_status))@if($assessor->ndc_form_status==1) selected @endif @endif>Accepted</option>
                                                                                    <option value="2" @if(isset($assessor->ndc_form_status))@if($assessor->ndc_form_status==2) selected @endif @endif>Not Accepted</option>
                                                                                </select>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        @else
                                                                        @endif
                                                                    </div>
                                                            </div>
                                                            @if(isset($assessor->ndc_form) && $assessor->profile_updated==1)
                                                            <div class="col-lg-6 remark-title4 remark-title" @if($assessor->ndc_form_status==1) style="display:none"@endif>
                                                                <label>Remarks :</label>
                                                                <textarea name="ndc_form_remark" id="ndc_form_remark" maxlength="150" class="form-control" rows="2">{{$assessor->ndc_form_remark}}</textarea>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 form-group">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-4">
                                                                        <div class="form-group font-small-4">4. Health Declaration <span class="text-danger">*</span></label>
                                                                        </div>
                                                                    </div>
                                                                    @if(isset($assessor->health_doc))
                                                                    <div class="col-lg-7 col-md-8">
                                                                        <div class="v-row uploadedfile border-bb mb-1">
                                                                            <div class="col-lg-8 pl-0"  style="word-break: break-all">
                                                                                <label class="mr-1 ">
                                                                                    <p class="mb-0">Uploaded File</p>
                                                                                    @php
                                                                                    $application_name = basename(@$assessor->health_doc);
                                                                                    @endphp
                                                                                    <span>{{@$application_name}}</span>
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-lg-4 text-right pr-0">
                                                                                @if($assessor->health_doc_status==1)
                                                                                    <a href="{{url($assessor->health_doc)}}" target="_blank" class="btn btn-outline-secondary waves-effect" tooltip='View' data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                    <a href="{{url($assessor->health_doc)}}" download class="btn btn-outline-secondary waves-effect  ml-1" data-toggle="tooltip" title data-original-title="Download"><i data-feather="download"></i></a>
                                                                                @else   
                                                                                    <a href="{{url($assessor->health_doc)}}" target="_blank" class="btn btn-outline-secondary waves-effect" tooltip='View' data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    @else
                                                                    <div class="col-lg-7 col-md-8">
                                                                        <div class="custom-file">
                                                                            <input type="file" class="custom-file-input" id="health_doc" name="health_doc">
                                                                            <label class="custom-file-label" for="health_doc">Choose file</label>
                                                                            <span id="health_doc_error" class="error"></span>
                                                                            <span id="health_doc_file_error" style="color:red; display:none">Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only</span>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                </div>
                                                                    <div class="row">
                                                                    @if(isset($assessor->health_doc) && $assessor->profile_updated==1)
                                                                        <div class="col-lg-5 col-md-4">
                                                                            <div class="form-group font-small-4">Approval Status : </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-7 col-md-8">
                                                                            <div class="form-group">
                                                                                @if($assessor->health_doc_status==1)
                                                                                    @if($assessor->health_doc_status==1)
                                                                                    <p class="font-small-4 badge badge-light-success badge-pill">Accepted</p>
                                                                                    @endif
                                                                                @endif
                                                                                @if($assessor->health_doc_status==2)
                                                                                    @if($assessor->health_doc_status==2)
                                                                                    <p class="font-small-4 badge badge-light-danger badge-pill">Not Accepted</p>
                                                                                    @endif
                                                                                @endif
                                                                                @if($assessor->health_doc_status==0 || $assessor->health_doc_status==NULL)
                                                                                <select name="health_card_status" id="health_card_status" onchange="healthChange()"  @if(isset($assessor->health_doc_status))@if($assessor->health_doc_status==2) disabled @endif @endif class="form-control">
                                                                                    <option value="">Select</option>
                                                                                    <option value="0" @if(isset($assessor->health_doc_status))@if($assessor->health_doc_status==0) selected @endif @endif>Under Review</option>
                                                                                    <option value="1" @if(isset($assessor->health_doc_status))@if($assessor->health_doc_status==1) selected @endif @endif>Accepted</option>
                                                                                    <option value="2" @if(isset($assessor->health_doc_status))@if($assessor->health_doc_status==2) selected @endif @endif>Not Accepted</option>
                                                                                </select>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    @else
                                                                    @endif
                                                                    </div>
                                                                </div>
                                                                @if(isset($assessor->health_doc) && $assessor->profile_updated==1)
                                                            <div class="col-lg-6 remark-title1 remark-title" @if($assessor->health_doc_status==1) style="display:none"@endif>
                                                                <label>Remarks :</label>
                                                                <textarea name="health_card_remark" id="health_card_remark" maxlength="150" class="form-control" rows="2">{{$assessor->health_doc_remark}}</textarea>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 form-group">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-4">
                                                                        <div class="form-group font-small-4">5. GST Declaration <span class="text-danger">*</span></label>
                                                                        </div>
                                                                    </div>
                                                                    @if(isset($assessor->gst_form))
                                                                    <div class="col-lg-7 col-md-8">
                                                                        <div class="v-row uploadedfile border-bb mb-1">
                                                                            <div class="col-lg-8 pl-0"  style="word-break: break-all">
                                                                                <label class="mr-1 ">
                                                                                    <p class="mb-0">Uploaded File</p>
                                                                                    @php
                                                                                    $application_name1 = basename($assessor->gst_form);
                                                                                    @endphp
                                                                                    <span>{{$application_name1}}</span>
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-lg-4 text-right pr-0">
                                                                                @if($assessor->gst_form_status==1)
                                                                                    <a href="{{url($assessor->gst_form)}}" target="_blank" class="btn btn-outline-secondary waves-effect" tooltip='View' data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                    <a href="{{url($assessor->gst_form)}}" download class="btn btn-outline-secondary waves-effect ml-1" data-toggle="tooltip" title data-original-title="Download"><i data-feather="download"></i></a>
                                                                                @else   
                                                                                    <a href="{{url($assessor->gst_form)}}" target="_blank" class="btn btn-outline-secondary waves-effect" tooltip='View' data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    @else
                                                                    <div class="col-lg-7 col-md-8">
                                                                        <div class="custom-file">
                                                                            <input type="file" class="custom-file-input" id="gst_form" name="gst_form">
                                                                            <label class="custom-file-label" for="gst_form">Choose file</label>
                                                                            <span id="gst_form_error" class="error"></span>
                                                                            <span id="gst_form_file_error" style="color:red; display:none">Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only</span>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                </div>
                                                                    <div class="row">
                                                                    @if(isset($assessor->gst_form) && $assessor->profile_updated==1)
                                                                        <div class="col-lg-5 col-md-4">
                                                                            <div class="form-group font-small-4">Approval Status : </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-7 col-md-8">
                                                                            <div class="form-group">
                                                                                @if($assessor->gst_form_status==1)
                                                                                    @if($assessor->gst_form_status==1)
                                                                                    <p class="font-small-4 badge badge-light-success badge-pill">Accepted</p>
                                                                                    @endif
                                                                                @endif
                                                                                @if($assessor->gst_form_status==2)
                                                                                    @if($assessor->gst_form_status==2)
                                                                                    <p class="font-small-4 badge badge-light-danger badge-pill">Not Accepted</p>
                                                                                    @endif
                                                                                @endif
                                                                                @if($assessor->gst_form_status==0 || $assessor->gst_form_status==NULL)
                                                                                <select name="gst_form_status"  id="gst_form_status" onchange="gstChange()"  @if(isset($assessor->gst_form_status))@if($assessor->gst_form_status==2) disabled @endif @endif class="form-control" >
                                                                                    <option value="">Select</option>
                                                                                    <option value="0" @if(isset($assessor->gst_form_status))@if($assessor->gst_form_status==0) selected @endif @endif>Under Review</option>
                                                                                    <option value="1" @if(isset($assessor->gst_form_status))@if($assessor->gst_form_status==1) selected @endif @endif>Accepted</option>
                                                                                    <option value="2" @if(isset($assessor->gst_form_status))@if($assessor->gst_form_status==2) selected @endif @endif>Not Accepted</option>
                                                                                </select>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        @else
                                                                        @endif
                                                                    </div>
                                                            </div>
                                                            @if(isset($assessor->gst_form) && $assessor->profile_updated==1)
                                                            <div class="col-lg-6 remark-title2 remark-title" @if($assessor->gst_form_status==1) style="display:none"@endif>
                                                                <label>Remarks :</label>
                                                                <textarea name="gst_form_remark" id="gst_form_remark" maxlength="150" class="form-control" rows="2">{{$assessor->gst_form_remark}}</textarea>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 form-group">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-4">
                                                                        <div class="form-group font-small-4">6. PAN Card <span class="text-danger">*</span></label>
                                                                        </div>
                                                                    </div>
                                                                    @if(isset($assessor->pan))
                                                                    <div class="col-lg-7 col-md-8">
                                                                        <div class="v-row uploadedfile border-bb mb-1">
                                                                            <div class="col-lg-8 pl-0"  style="word-break: break-all">
                                                                                <label class="mr-1 ">
                                                                                    <p class="mb-0">Uploaded File</p>
                                                                                    @php
                                                                                    $application_name3 = basename($assessor->pan);
                                                                                    @endphp
                                                                                    <span>{{$application_name3}}</span>
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-lg-4 text-right pr-0">
                                                                                @if($assessor->pan_status==1)
                                                                                    <a href="{{url($assessor->pan)}}" target="_blank" class="btn btn-outline-secondary waves-effect" tooltip='View' data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                    <a href="{{url($assessor->pan)}}" download class="btn btn-outline-secondary waves-effect ml-1" data-toggle="tooltip" title data-original-title="Download"><i data-feather="download"></i></a>
                                                                                @else   
                                                                                    <a href="{{url($assessor->pan)}}" target="_blank" class="btn btn-outline-secondary waves-effect" tooltip='View' data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    @else
                                                                    <div class="col-lg-7 col-md-8">
                                                                        <div class="custom-file">
                                                                            <input type="file" class="custom-file-input" id="pan" name="pan">
                                                                            <label class="custom-file-label" for="pan">Choose file</label>
                                                                            <span id="pan_error" class="error"></span>
                                                                            <span id="pan_file_error" style="color:red; display:none">Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only</span>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                </div>
                                                                    <div class="row">
                                                                    @if(isset($assessor->pan) && $assessor->profile_updated==1)
                                                                        <div class="col-lg-5 col-md-4">
                                                                            <div class="form-group font-small-4">Approval Status : </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-7 col-md-8">
                                                                            <div class="form-group">
                                                                                @if($assessor->pan_status==1)
                                                                                    @if($assessor->pan_status==1)
                                                                                    <p class="font-small-4 badge badge-light-success badge-pill">Accepted</p>
                                                                                    @endif
                                                                                @endif
                                                                                @if($assessor->pan_status==2)
                                                                                    @if($assessor->pan_status==2)
                                                                                    <p class="font-small-4 badge badge-light-danger badge-pill">Not Accepted</p>
                                                                                    @endif
                                                                                @endif
                                                                                @if($assessor->pan_status==0 || $assessor->pan_status==NULL)
                                                                                <select name="pan_status" id="pan_status" onchange="panChange()" @if(isset($assessor->pan_status))@if($assessor->pan_status==2) disabled @endif @endif class="form-control">
                                                                                    <option value="">Select</option>
                                                                                    <option value="0" @if(isset($assessor->pan_status))@if($assessor->pan_status==0) selected @endif @endif>Under Review</option>
                                                                                    <option value="1" @if(isset($assessor->pan_status))@if($assessor->pan_status==1) selected @endif @endif>Accepted</option>
                                                                                    <option value="2" @if(isset($assessor->pan_status))@if($assessor->pan_status==2) selected @endif @endif>Not Accepted</option>
                                                                                </select>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        @else
                                                                        @endif
                                                                    </div>
                                                            </div>
                                                            @if(isset($assessor->pan) && $assessor->profile_updated==1)
                                                            <div class="col-lg-6 remark-title5 remark-title" @if($assessor->pan_status==1) style="display:none" @endif>
                                                                <label>Remarks :</label>
                                                                <textarea name="pan_remark" id="pan_remark" class="form-control" maxlength="150" rows="2">{{$assessor->pan_remark}}</textarea>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 form-group">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-4">
                                                                        <div class="form-group font-small-4">7. Cancelled Cheque <span class="text-danger">*</span></label>
                                                                        </div>
                                                                    </div>
                                                                    @if(isset($assessor->cancelled_check))
                                                                    <div class="col-lg-7 col-md-8">
                                                                        <div class="v-row uploadedfile border-bb mb-1">
                                                                            <div class="col-lg-8 pl-0"  style="word-break: break-all">
                                                                                <label class="mr-1 ">
                                                                                    <p class="mb-0">Uploaded File</p>
                                                                                    @php
                                                                                    $application_name6 = basename($assessor->cancelled_check);
                                                                                    @endphp
                                                                                    <span>{{$application_name6}}</span>
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-lg-4 text-right pr-0">
                                                                            @if($assessor->cancelled_check_status==1)
                                                                                    <a href="{{url($assessor->cancelled_check)}}" target="_blank" class="btn btn-outline-secondary waves-effect" tooltip='View' data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                    <a href="{{url($assessor->cancelled_check)}}" download class="btn btn-outline-secondary waves-effect ml-1" data-toggle="tooltip" title data-original-title="Download"><i data-feather="download"></i></a>
                                                                                @else   
                                                                                    <a href="{{url($assessor->cancelled_check)}}" target="_blank" class="btn btn-outline-secondary waves-effect" tooltip='View' data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    @else
                                                                    <div class="col-lg-7 col-md-8">
                                                                        <div class="custom-file">
                                                                            <input type="file" class="custom-file-input" id="cancelled_check" name="cancelled_check">
                                                                            <label class="custom-file-label" for="cancelled_check">Choose file</label>
                                                                            <span id="cancelled_check_error" class="error"></span>
                                                                            <span id="cancelled_check_file_error" style="color:red; display:none">Allowed Extensions are : *.pdf, *.jpg, *.jpeg, *.png only</span>
                                                                        </div>
                                                                    </div>
                                                                    @endif
                                                                </div>
                                                                    <div class="row">
                                                                    @if(isset($assessor->cancelled_check) && $assessor->profile_updated==1)
                                                                        <div class="col-lg-5 col-md-4">
                                                                            <div class="form-group font-small-4">Approval Status : </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-lg-7 col-md-8">
                                                                            <div class="form-group">
                                                                                @if($assessor->cancelled_check_status==1)
                                                                                    @if($assessor->cancelled_check_status==1)
                                                                                    <p class="font-small-4 badge badge-light-success badge-pill">Accepted</p>
                                                                                    @endif
                                                                                @endif
                                                                                @if($assessor->cancelled_check_status==2)
                                                                                    @if($assessor->cancelled_check_status==2)
                                                                                    <p class="font-small-4 badge badge-light-danger badge-pill">Not Accepted</p>
                                                                                    @endif
                                                                                @endif
                                                                                @if($assessor->cancelled_check_status==0 || $assessor->cancelled_check_status==NULL)
                                                                                <select name="cancelled_check_status" id="cancelled_check_status" onchange="cancelledChequeChange()" @if(isset($assessor->cancelled_check_status))@if($assessor->cancelled_check_status==2) disabled @endif @endif class="form-control">
                                                                                    <option value="">Select</option>
                                                                                    <option value="0" @if(isset($assessor->cancelled_check_status))@if($assessor->cancelled_check_status==0) selected @endif @endif>Under Review</option>
                                                                                    <option value="1" @if(isset($assessor->cancelled_check_status))@if($assessor->cancelled_check_status==1) selected @endif @endif>Accepted</option>
                                                                                    <option value="2" @if(isset($assessor->cancelled_check_status))@if($assessor->cancelled_check_status==2) selected @endif @endif>Not Accepted</option>
                                                                                </select>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                        @else
                                                                        @endif
                                                                    </div>
                                                            </div>
                                                            @if(isset($assessor->cancelled_check) && $assessor->profile_updated==1)
                                                            <div class="col-lg-6 remark-title6 remark-title" @if($assessor->cancelled_check_status==1) style="display:none"@endif>
                                                                <label>Remarks :</label>
                                                                <textarea name="cancelled_check_remark" id="cancelled_check_remark" maxlength="150" class="form-control" rows="2">{{$assessor->cancelled_check_remark}}</textarea>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 m0-auto text-center">
                                                        @if(isset($assessor->pan_number) && isset($assessor->gst_number) && isset($assessor->name) && isset($assessor->email) && isset($assessor->mobile) && isset($assessor->status) && isset($assessor->health_doc) && isset($assessor->gst_form) && isset($assessor->vendor_stamp) && isset($assessor->ndc_form) && isset($assessor->cancelled_check) && isset($assessor->biodata) && isset($assessor->bank_name) && isset($assessor->acc_no) && isset($assessor->ifsc_code) && isset($assessor->branch_name) && isset($assessor->category) && isset($assessor->state) && isset($assessor->em_contact_name) && isset($assessor->pan) && $assessor->profile_updated==0)
                                                            <input type="hidden" name="final_submit_value" id="final_submit_value" value="1">    
                                                            <button type="submit" class="btn btn-primary mr-1 waves-effect waves-float waves-light page_save_btn" id="final_submit" name="final_submit" value="final_submit">Final Submit</button>
                                                        @else
                                                            @if($assessor->profile_updated==1)
                                                            <input type="hidden" name="final_submit_value" id="final_submit_value" value="2">
                                                            @else
                                                            <input type="hidden" name="final_submit_value" id="final_submit_value">
                                                            @endif
                                                            <button type="submit" class="btn btn-primary mr-1 waves-effect waves-float waves-light page_save_btn" id="save" name="save">Save</button>
                                                        @endif
                                                        <button type="button" class="btn btn-outline-secondary waves-effect waves-float waves-light cancelpage_btn" id="cancelpage" name="cancelpage">Cancel</button>
                                                    </div>
                                                </div>
                                                <br>
                                            </div>
                                        </div>  
                                    </div>

                                </div>
                            </div>
                        </form>
                        </section>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

@endsection

@push('PAGE_SCRIPTS')
<script>
    function healthChange(){
        if(document.getElementById('health_card_status').value==2){
            $('.remark-title1').css('display','block');
            document.getElementById("health_card_remark").required = true;
        }
        else{
            $('.remark-title1').css('display','none');
            document.getElementById("health_card_remark").required = false;
        }
    }
    function vendorChange(){
        if(document.getElementById('vendor_stamp_status').value==2){
            $('.remark-title3').css('display','block');
            document.getElementById("vendor_stamp_remark").required = true;
        }
        else{
            $('.remark-title3').css('display','none');
            document.getElementById("vendor_stamp_remark").required = false;
        }
    }
    function panChange(){
        if(document.getElementById('pan_status').value==2){
            $('.remark-title5').css('display','block');
            document.getElementById("pan_remark").required = true;
        }
        else{
            $('.remark-title5').css('display','none');
            document.getElementById("pan_remark").required = false;
        }
    }
    function gstChange(){
        if(document.getElementById('gst_form_status').value==2){
            $('.remark-title2').css('display','block');
            document.getElementById("gst_form_remark").required = true;
        }
        else{
            $('.remark-title2').css('display','none');
            document.getElementById("gst_form_remark").required = false;
        }
    }
    function ndcChange(){
        if(document.getElementById('ndc_form_status').value==2){
            $('.remark-title4').css('display','block');
            document.getElementById("ndc_form_remark").required = true;
        }
        else{
            $('.remark-title4').css('display','none');
            document.getElementById("ndc_form_remark").required = false;
        }
    }
    function cancelledChequeChange(){
        if(document.getElementById('cancelled_check_status').value==2){
            $('.remark-title6').css('display','block');
            document.getElementById("cancelled_check_remark").required = true;
        }
        else{
            $('.remark-title6').css('display','none');
            document.getElementById("cancelled_check_remark").required = false;
        }
    }
    function biodataChange(){
        if(document.getElementById('biodata_status').value==2){
            $('.remark-title7').css('display','block');
            document.getElementById("biodata_remark").required = true;
        }
        else{
            $('.remark-title7').css('display','none');
            document.getElementById("biodata_remark").required = false;
        }
    }
    function mobileValidate(){
        var text = document.getElementById("mobile").value;
        var regx = /^[6-9]\d{9}$/ ;
        if(regx.test(text))
            $('#mobile_syntax_error').attr('display','block');
        else
            $('#mobile_syntax_error').attr('display','none');
    }
</script>
<script src="{{ url('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
<script src="{{ url('app-assets/vendors/js/pickers/flatpickr/custom-flatpicker.min.js')}}"></script>
<script>
    var Accessor = function() {
        return {
            init: function() {
                $(".select-placeholder").select2({
                    placeholder: "Select Industry Category",
                });

                $("#cancelpage").click(function(){
                    Swal.fire({
                            title: 'Do you want to Cancel?',
                            text: 'You will lose all the changes done!',
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            showDenyButton: false,
                            // denyButtonText: 'No, Reject it',
                            showClass: {
                                popup: 'animate__animated animate__fadeIn'
                            },
                            customClass: {
                                confirmButton: 'btn btn-primary',
                                cancelButton: 'btn btn-outline-danger ml-1',
                            },
                            buttonsStyling: false
                        }).then((result) => {
                            /* Read more about isConfirmed, isDenied below */
                            if (result.isConfirmed) {
                                window.location = "{{route('admin.assessors.index')}}";
                            }
                        })
                });

                $('#editAccessorForm').validate({
                    ignore: [],
                        rules: {
                            'name': {
                                required: true,
                                noSpace: true,
                                doubleSpace:true,
                                maxlength: 50,
                                minlength:3
                            },
                            'email': {
                                required: true,
                                emailAddress:true,
                                email: true,
                            },
                            'ind_category': {
                                required: true,
                            },
                            'mobile':{
                                noSpace: true,
                                digits:true,
                                minlength:10,
                                mobileNo: true
                            },
                            'alt_mobile':{
                            
                                noSpace: true,
                                digits:true,
                                minlength:10,
                                mobileNo: true
                            },
                            'address_1': {
                                required: true,
                                doubleSpace:true,
                                noSpace: true,
                                minlength:3
                            },
                            'address_2': {
                                doubleSpace:true,
                                noSpace: true,
                                minlength:3
                            },
                            'city': {
                                required: true,
                                doubleSpace:true,
                                noSpace: true,
                                minlength:3
                            },
                            'state':{
                                required:true,
                            },
                            'pincode': {
                                required: true,
                                
                                digits:true,
                                minlength:6
                            },
                            'pan_number': {
                                required: true,
                                minlength:10,
                                maxlength:10
                            },
                            'gst_number': {
                                required: true,
                                minlength:15,
                                maxlength:15
                            },
                            'assessor_grade':{
                               
                            },
                            'enrollment_date':{
                               
                            },
                            'lead_assessor':{
                               
                            },
                            'em_contact_name': { 
                                noSpace: true,
                                maxlength: 50,
                                minlength:3
                            },
                            'em_contact_no': {
                                noSpace: true,
                                digits:true,
                                minlength:10,
                                mobileNo: true
                            },
                            // 'em_contact_address': {
                            'em_address_1':{
                                doubleSpace:true,
                                noSpace: true,
                                minlength:3
                            },
                            'em_address_2': {
                                doubleSpace:true,
                                noSpace: true,
                                minlength:3
                            },
                            'em_city': {
                                doubleSpace:true,
                                noSpace: true,
                                minlength:3
                            },
                            'em_pincode': {
                                digits:true,
                                minlength:6
                            },
                            'em_state': {
                              
                            },
                            'bank_name': {
                                doubleSpace:true,
                                noSpace: true,
                                maxlength: 50,
                                minlength:3
                            },
                            'branch_name': {
                                doubleSpace:true,
                                noSpace: true,
                                maxlength: 50,
                                minlength:3
                            },
                            'ifsc_code': {
                                doubleSpace:true,
                                noSpace: true,
                                maxlength: 50,
                                minlength:3
                            },
                            'acc_no': {
                                doubleSpace:true,
                                noSpace: true,
                                maxlength: 50,
                                minlength:10
                            },
                        }, 
                        submitHandler : function(form){
                            $('#save').html('Loading....');
                            $('#save').attr('disabled',true);
                            $('#cancelpage').attr('disabled',true);
                            let formdata = new FormData(form);
                            // console.log(formdata)
                            $.ajax({
                                method:"post",
                                url: $('form#editAccessorForm').attr('action'),
                                data: formdata,
                                contentType: false,
                                processData: false,
                                success: function(response) {
                                    // console.log(response)
                                    if(response.status == 'validations') {
                                        $('#save').html('Save');
                                        $('#save').attr('disabled',false);
                                        $('#cancelpage').attr('disabled',false);
                                        $.each(response.errors, function (key, error) {
                                            $('.error').css('display','block');
                                            $('form#editAccessorForm').find("#" + key).removeClass('error').addClass('error');
                                            $('form#editAccessorForm').find("#" + key + "_error").html(error[0]);
                                        });
                                    }
                                    if(response.status == 'success') {
                                        $('.error').css('display','none');
                                        $('#save').html('Save');
                                        $('#save').attr('disabled',false);
                                        $('#cancelpage').attr('disabled',false);
                                        if(response.final_submit){
                                            JsUtility.showToastr('success', 'Assessor', response.message);
                                            setTimeout(function(){
                                            window.location = "{{route('admin.assessors.index')}}";
                                            }, 3000);
                                        }
                                        else{
                                            JsUtility.showToastr('success', 'Assessor', response.message);
                                            setTimeout(function(){
                                            location.reload();
                                            }, 3000);
                                            
                                        }
                                    }
                                    if(response.status == 'error') {
                                        $('#save').html('Save');
                                        $('#save').attr('disabled',false);
                                        $('#cancelpage').attr('disabled',false);
                                        JsUtility.showToastr('error', 'Assessor', response.message);
                                    } 
                                },
                                error: function() {
                                    $('#save').html('Save');
                                    $('#save').attr('disabled',false);
                                    $('#cancelpage').attr('disabled',false);
                                    JsUtility.showToastr('error', 'Assessor', response.message);
                                }
                            })
                        }
                });

                    $('.select2').on('change', function(e) {
                        $('.select2').removeClass('error');
                        $('#ind_category_error').css('display','none');
                        $('#ind_category-error').css('display','none');
                        if ($('.select2').val() === '') {
                            // Add error class when select2 element is required
                            $('.select2').addClass('error');
                            $('#ind_category_error').css('display','block');
                            // Stop submiting
                            e.preventDefault();
                            return false;
                        }
                    });

            //
            $('#health_doc').on('change', function(e) {
                var extension = e.target.files[0].name.split('.').pop().toLowerCase()
                var reader = new FileReader();
                reader.onload = function(e) {
                    if (extension == 'png' || extension == 'jpeg' || extension == 'jpg' || extension == 'pdf') {
                        $("#health_doc_file_error").css("display", "none");
                        $('#save').attr('disabled',false);
                        return true;
                    }
                    else{
                        $("#health_doc_file_error").css("display", "block");
                        document.getElementById('health_doc').value= null;
                        $('#save').attr('disabled',true);
                        return false;
                    }
                }
                reader.readAsDataURL(e.target.files[0]);
            });
            $('#gst_form').on('change', function(e) {
                var extension = e.target.files[0].name.split('.').pop().toLowerCase()
                var reader = new FileReader();
                reader.onload = function(e) {
                    if (extension == 'png' || extension == 'jpeg' || extension == 'jpg' || extension == 'pdf') {
                        $("#gst_form_file_error").css("display", "none");
                        $('#save').attr('disabled',false);
                        return true;
                    }
                    else{
                        $("#gst_form_file_error").css("display", "block");
                        document.getElementById('gst_form').value= null;
                        $('#save').attr('disabled',true);
                        return false;
                    }
                }
                reader.readAsDataURL(e.target.files[0]);
            });
            $('#vendor_stamp').on('change', function(e) {
                var extension = e.target.files[0].name.split('.').pop().toLowerCase()
                var reader = new FileReader();
                reader.onload = function(e) {
                    if (extension == 'png' || extension == 'jpeg' || extension == 'jpg' || extension == 'pdf') {
                        $("#vendor_stamp_file_error").css("display", "none");
                        $('#save').attr('disabled',false);
                        return true;
                    }
                    else{
                        $("#vendor_stamp_file_error").css("display", "block");
                        document.getElementById('vendor_stamp').value= null;
                        $('#save').attr('disabled',true);
                        return false;
                    }
                }
                reader.readAsDataURL(e.target.files[0]);
            });
            $('#ndc_form').on('change', function(e) {
                var extension = e.target.files[0].name.split('.').pop().toLowerCase()
                var reader = new FileReader();
                reader.onload = function(e) {
                    if (extension == 'png' || extension == 'jpeg' || extension == 'jpg' || extension == 'pdf') {
                        $("#ndc_form_file_error").css("display", "none");
                        $('#save').attr('disabled',false);
                        return true;
                    }
                    else{
                        $("#ndc_form_file_error").css("display", "block");
                        document.getElementById('ndc_form').value= null;
                        $('#save').attr('disabled',true);
                        return false;
                    }
                }
                reader.readAsDataURL(e.target.files[0]);
            });
            $('#pan').on('change', function(e) {
                var extension = e.target.files[0].name.split('.').pop().toLowerCase()
                var reader = new FileReader();
                reader.onload = function(e) {
                    if (extension == 'png' || extension == 'jpeg' || extension == 'jpg' || extension == 'pdf') {
                        $("#pan_file_error").css("display", "none");
                        $('#save').attr('disabled',false);
                        return true;
                    }
                    else{
                        $("#pan_file_error").css("display", "block");
                        document.getElementById('pan').value= null;
                        $('#save').attr('disabled',true);
                        return false;
                    }
                }
                reader.readAsDataURL(e.target.files[0]);
            });
            $('#cancelled_check').on('change', function(e) {
                var extension = e.target.files[0].name.split('.').pop().toLowerCase()
                var reader = new FileReader();
                reader.onload = function(e) {
                    if (extension == 'png' || extension == 'jpeg' || extension == 'jpg' || extension == 'pdf') {
                        $("#cancelled_check_file_error").css("display", "none");
                        $('#save').attr('disabled',false);
                        return true;
                    }
                    else{
                        $("#cancelled_check_file_error").css("display", "block");
                        document.getElementById('cancelled_check').value= null;
                        $('#save').attr('disabled',true);
                        return false;
                    }
                }
                reader.readAsDataURL(e.target.files[0]);
            });
            $('#biodata').on('change', function(e) {
                var extension = e.target.files[0].name.split('.').pop().toLowerCase()
                var reader = new FileReader();
                reader.onload = function(e) {
                    if (extension == 'png' || extension == 'jpeg' || extension == 'jpg' || extension == 'pdf') {
                        $("#biodata_file_error").css("display", "none");
                        $('#save').attr('disabled',false);
                        return true;
                    }
                    else{
                        $("#biodata_file_error").css("display", "block");
                        document.getElementById('biodata').value= null;
                        $('#save').attr('disabled',true);
                        return false;
                    }
                }
                reader.readAsDataURL(e.target.files[0]);
            });
            //
        }
    }
}();

jQuery(document).ready(function() {
Accessor.init();

});

// capitalize input all text fields with jquery
$('input[type=text]').on('keyup',function(event){
    event.currentTarget.value = this.value.toUpperCase();
});



</script>
@endpush