@extends(ADMIN_THEME_NAME.'.layouts.app')

@push('PAGE_ASSETS_CSS')
<link rel="stylesheet" type="text/css" href="{{url('app-assets/vendors/css/forms/wizard/bs-stepper.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('app-assets/css/plugins/forms/form-validation.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('app-assets/css/plugins/forms/form-wizard.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
select.form-control:disabled{
        background-color:#e9e9e9!important;
    }
    [type="radio"]:checked + label:after{
        background: #78A2CC!important;
    }
</style>
@endpush
@section('content')
<div class="app-content content content-ps-stl">
    <div class="">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <ul class="nav nav-tabs tabs-pages tab-leftside" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link tab-links mini-border" href="{{route('admin.assessors.addEditAssessor',$assessor)}}" aria-selected="true">
                            <i data-feather="user"></i>Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link tab-links active mini-border" href="{{route('admin.assessors.nda',$assessor)}}"  aria-selected="false">
                            <i data-feather="doc"></i>Yearly NDA's</a>
                    </li>
                </ul>
                <section class=" p-relative">
                    <div class="col-12">
                        <section id="advanced-search-datatable ">
                            <form action="#" onSubmit="return false" class="profileUpdate" id="profileUpdate" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="card ">
                                        <div class="col-lg-12 col-xl-12 col-md-12 col-12 " >
                                        </div>
                                        <input type='hidden' name="assessorid" id="assessorid">
                                        <input type='hidden' name="profile_updated_status" id="profile_updated_status">
                                        <input type='hidden' name="verification_status" id="verification_status">
                                        <div class="row">
                                        <div class="col-lg-12 col-xl-12 col-md-12 col-12">
                                            <div class="d-flex v-row title-strip">
                                                <div class="col-6 pl-0">Non Disclosure Aggrements List</div>
                                            </div>
                                            <div class="card-body ">
                                                <div class="row ">
                                                    <div class="col-lg-12 form-group">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="col-lg-6 d-flex">
                                                                    <div class="form-group" style="font-size: 15px;text-align:center;font-weight: 400;margin-left: 14%;"><strong>Year</strong></label>
                                                                    </div>
                                                                    <div class="form-group"style="font-size: 15px;text-align:center;font-weight: 400;margin-left: 44%;"><strong>Document</strong></label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-4">
                                                                        <div class="form-group font-small-4" style="text-align:center">2021</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-7 col-md-8">
                                                                        <div class="v-row uploadedfile border-bb mb-1">
                                                                            <div class="col-lg-8 pl-0"  style="word-break: break-all">
                                                                                <label class="mr-1 ">
                                                                                    <p class="mb-0">Uploaded File</p>
                                                                                    <span>abcabcabc</span>
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-lg-4 text-right pr-0">
                                                                                <a href="#" target="_blank" class="btn btn-outline-secondary waves-effect" tooltip='View' data-toggle="tooltip" title data-original-title="View"><i data-feather="eye"></i></a>
                                                                                <a href="#" download class="btn btn-outline-secondary waves-effect ml-1" data-toggle="tooltip" title data-original-title="Download"><i data-feather="download"></i></a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-4">
                                                                    </div>  
                                                                    <div class="col-lg-7 col-md-8">
                                                                        <div class="custom-file">
                                                                            <input type="file" class="custom-file-input"  accept="application/pdf,image/jpg,image/jpeg,image/png" id="ndc_form" name="ndc_form" >
                                                                            <label class="custom-file-label" for="ndc_form">Choose file</label>
                                                                            <span id="ndc_form-error-add" class="error"></span>
                                                                        </div>
                                                                    </div>    
                                                                </div>  
                                                                <div class="row">
                                                                    <div class="col-lg-5 col-md-4">
                                                                        <div class="form-group font-small-4" style="text-align:center">Approval Status : </label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-7 col-md-8">
                                                                        <div class="form-group">
                                                                            
                                                                                <!-- <p class="font-small-4 badge badge-light-success badge-pill">Accepted</p>
                                                                                <p class="font-small-4 badge badge-light-danger badge-pill">Not Accepted</p> -->
                                                                            <select name="cancelled_check_status" id="cancelled_check_status" class="form-control">
                                                                                <option value="">Select</option>
                                                                                <option value="0">Under Review</option>
                                                                                <option value="1">Accepted</option>
                                                                                <option value="2">Not Accepted</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 remark-title6 remark-title" style="margin-top:2%" @if($assessor->cancelled_check_status==1) style="display:none"@endif>
                                                                <div class="row">
                                                                    <div class="col-lg-10 col-md-4">
                                                                        <label>Remarks :</label>
                                                                        <textarea name="cancelled_check_remark" id="cancelled_check_remark" maxlength="150" class="form-control" rows="2">{{$assessor->cancelled_check_remark}}</textarea>
                                                                    </div>  
                                                                    <div class="col-lg-2 col-md-8" style="padding:30px">
                                                                    <button type="submit" class="btn btn-primary mr-1 waves-effect waves-float waves-light">Save</button>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </section>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

@endsection

@push('PAGE_SCRIPTS')

@endpush