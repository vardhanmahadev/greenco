@extends(ADMIN_THEME_NAME.'.layouts.app')

@section('content')
<div class="app-content content content-ps-stl">
    <div class="">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <section class="page-center">
                    <div class="col-12">
                        <section id="advanced-search-datatable ">
                            <div class="row v-row justify-content-center">
                            <h1 class="text-center w-100 mt-2">Welcome To Dashboard</h1>
                                <img class="img-fluid" src="{{URL('app-assets/images/logo/greenco.png')}}"  alt="Login V2">
                            </div>
                        </section>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
@endsection

