<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="description" content="GreenCo Admin">
    <meta name="keywords" content="GreenCo Admin">
    <meta name="author" content="{{APP_NAME}}">
    <title>{{APP_NAME}} | {{ $pageTitle ?? ''}} </title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('/app-assets/images/logo/greenco.png') }}">
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/vendors/css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/vendors/css/extensions/toastr.min.css') }}">
    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/components.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/themes/dark-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/themes/bordered-layout.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/plugins/extensions/ext-component-toastr.css') }}">
    <!-- END: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/owl.carousel.min.css') }}">
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/plugins/forms/form-validation.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/pages/page-auth.min.css') }}">
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/style.css') }}">
    <style>
        .borderRightError{
            border-color: #EA5455!important;
        }
    </style>
</head>
<!--- END HEAD-->
<body class="vertical-layout vertical-menu-modern blank-page navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="blank-page">
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>

        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">

                @yield('content')

            </div>
        </div>
    </div>

    <!-- END: Content-->
    <!-- BEGIN: Vendor JS-->
    <script src="{{ url('/app-assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{ url('/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ url('/app-assets/vendors/js/forms/validation/jquery.validate.min.js') }}"></script>
    <script src="{{ url('/app-assets/js/scripts/owl.carousel.min.js') }}"></script>
    <script src="{{ url('/app-assets/js/core/app-menu.min.js') }}"></script>
    <script src="{{ url('/app-assets/js/core/app.js') }}"></script>
    <script src="{{ url('/assets/js/jsutility.js') }}"></script>
    <script src="{{ url('/assets/js/scripts.js') }}"></script>
    <!-- END: Page JS-->
    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>
    <script>
        $(function () {
            @if(Session::has('toast') && Session::has('status'))
                JsUtility.showToastr("{{session('status')}}", "{{session('title')}}", "{{session('message')}}");
            @endif
        });
    </script>
    <script>
            jQuery.validator.addMethod("mobileNo", function(value, element) {
                return this.optional(element) || /^[6-9][0-9]{9}$/.test(value);
                }, "Mobile Number is invalid"); 

                jQuery.validator.addMethod("noSpace", function(value, element) { 
                return value == '' || value.trim().length != 0;  
                }, "This field is required");

                jQuery.validator.addMethod("doubleSpace", function(value, element) {
                return this.optional(element) || /^\S((?!.*  ).*\S)?$/.test(value);
                }, "Mobile Number is invalid");  

                jQuery.validator.addMethod("emailAddress", function(value, element) {
                return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
                }, "Please Enter a Valid Email Address");  
        </script>

    @stack('PAGE_SCRIPTS')

</body>
<!-- END: Body-->

</html>