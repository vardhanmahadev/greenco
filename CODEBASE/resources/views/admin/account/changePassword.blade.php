@extends(ADMIN_THEME_NAME.'.layouts.app')

@section('content')
<div class="app-content content content-ps-stl pt-4">
    <div class="">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <ul class="nav nav-tabs tabs-pages tab-leftside" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link tab-links mini-border" href="{{route('admin.profile')}}" aria-selected="true">
                            <i data-feather="user"></i>Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link tab-links active mini-border" href="{{route('admin.changePassword')}}" aria-controls="changepasswordIcon" aria-selected="false">
                            <i data-feather="lock"></i>Change Password</a>
                    </li>
                </ul>
                <div class="col-lg-6 pb-3">
                    <div class="tab-content">
                        <div class="tab-pane active" id="changepasswordIcon" aria-labelledby="changepassword-tab" role="tabpanel">
                            <section>
                                <div class="container">
                                    <div class="row  change-password v-row">
                                        <div class="col-lg-10 offset-lg-1  mt-2 pb-3 ">
                                            <div id="changepassword" class="">
                                            <form class="auth-reset-password-form border-boxdiv form-auth-div mt-2" id="auth-reset-password-form"  onSubmit="return false" action="{{route('admin.changePassword')}}" method="POST">
                                                @csrf
                                                <div class="form-group">
                                                    <div class="d-flex justify-content-between">
                                                        <label for="current_password" class="font-small-4">Old Password <span class="text-danger">*</span> </label>
                                                    </div>
                                                    <div class="input-group input-group-merge form-password-toggle">
                                                        <input class="form-control form-control-merge" id="current_password" type="password" name="current_password" onkeypress=" $('#current_password-error-back').text('');  return /[a-zA-Z0-9@]/i.test(event.key);" placeholder="" aria-describedby="reset-password-old"  tabindex="1" value="{{old('current_password')}}"/>
                                                        <div class="input-group-append"><span class="input-group-text border-right-line cursor-pointer" id="current_password-icon-error"><i data-feather="eye"></i></span></div>

                                                    </div>
                                                    <span id="current_password-error-back" class="error"></span>
                                                </div>
                                                <div class="form-group">
                                                    <div class="d-flex justify-content-between">
                                                        <label for="new_password" class="font-small-4">New Password <span class="text-danger">*</span></label>
                                                    </div>
                                                    <div class="input-group input-group-merge form-password-toggle">
                                                        <input class="form-control form-control-merge" id="new_password" type="password" name="new_password" onkeypress=" $('#new_password-error-back').text(''); return /[a-zA-Z0-9@$!%*?&]/i.test(event.key);" placeholder="" aria-describedby="reset-password-new"  tabindex="2" value="{{old('new_password')}}"/>
                                                        <div class="input-group-append"><span class="input-group-text  border-right-line cursor-pointer"  id="new_password-icon-error"><i data-feather="eye"></i></span></div>

                                                    </div>
                                                    <span id="new_password-error-back" class="error"></span>
                                                    <div style="margin-top:5px;">
                                                    <span><label class="text-danger">Note:</label> Your New Password must contain atleast 1 uppercase, lowercase, number and special character and should be of atleast 6 characters in length.</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="d-flex justify-content-between">
                                                        <label for="confirmed" class="font-small-4">Confirm Password <span class="text-danger">*</span></label>
                                                    </div>
                                                    <div class="input-group input-group-merge form-password-toggle">
                                                        <input class="form-control form-control-merge" id="confirmed" type="password" name="confirmed" onkeypress=" $('#confirmed-error-back').text(''); return /[a-zA-Z0-9@$!%*?&]/i.test(event.key);" placeholder="" aria-describedby="reset-password-confirm" tabindex="3" value="{{old('confirmed')}}"/>
                                                        <div class="input-group-append"><span class="input-group-text border-right-line cursor-pointer" id="confirmed-icon-error"><i data-feather="eye"></i></span></div>

                                                    </div>
                                                    <span id="confirmed-error-back" class="error"></span>
                                                </div>
                                                    <button type="submit" id="submit" name="submit" value="submit" class="btn btn-primary btn-block mr-1 waves-effect waves-float waves-light" tabindex="3">Submit</button>
                                                    <!-- <button type="submit" class="btn btn-primary mr-1 waves-effect waves-float waves-light page_save_btn" id="page_save" name="page_save">Save</button> -->
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
                <!-- Modern Horizontal Wizard -->
            </div>
        </div>
    </div>
</div>
@endsection

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var ChangePassword = function () {
    return { //main function to initiate the module
        init: function () {

            jQuery.validator.addMethod("noSpace", function(value, element) {
              return value == '' || value.trim().length != 0;
            }, "This field is required");

            jQuery.validator.addMethod("passwordFormat", function(value, element) {
               return this.optional(element) || /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}$/.test(value);
            }, "Please Enter a Valid Password");

            $('#auth-reset-password-form').validate({
                rules: {
                    'current_password': {
                        required: true,
                        noSpace: true,
                        minlength:6
                    },
                    'new_password': {
                        required: true,
                        minlength:6,
                        passwordFormat:true,
                    },
                    'confirmed':{
                        required: true,
                        minlength:6,
                        equalTo:new_password,
                        passwordFormat:true
                    },

                },
                messages: {
                    current_password: {
                        required: "The Old Password field is required",
                        noSpace: "Please enter a valid password"
                    },
                    new_password: {
                        required: "The New Password field is required",
                        passwordFormat: "Please Enter a Valid Password"

                    },
                    confirmed: {
                        required: "The Confirmed Password field is required",
                        equalTo : "The Confirmed and New Password should be same",
                        passwordFormat: "Please Enter a Valid Password"
                    },
                },
                submitHandler : function(form){
                    // alert($("#state").val())
                    $("#submit").attr('disabled',true);
                    $("#submit").html("Loading....");
                    $.ajax({
                        type: "POST",
                        url: $('form#auth-reset-password-form').attr('action'),
                        data: $('form#auth-reset-password-form').serialize(),
                        success: function(response) {
                            if(response.status == 'errors') {
                                $.each(response.errors, function (key, error) {
                                    $('form#auth-reset-password-form').find("#" + key + "-icon-error").removeClass('borderRightError').addClass('borderRightError');
                                    $('form#auth-reset-password-form').find("#" + key).removeClass('error').addClass('error');
                                    $('form#auth-reset-password-form').find("#" + key + "-error-back").css('display','block');
                                    $('form#auth-reset-password-form').find("#" + key + "-error-back").text(error[0]);

                                });
                                $('#submit').prop("disabled",false);
                                $('#submit').html("Submit");
                            }
                            if(response.status == 'success') {
                                $('#submit').prop("disabled",false);
                                $('#submit').html("Submit");
                                JsUtility.showToastr('success', 'Change Password', response.message);
                                setTimeout(function(){
                                        location.reload();
                                     }, 3000);
                            }
                            if(response.status == 'error') {
                                $('#submit').prop("disabled",false);
                                $('#submit').html("Submit");
                                JsUtility.showToastr('error', 'Change Password', response.message);
                            }
                        },
                        error: function() {
                            $('#submit').prop("disabled",false);
                            $('#submit').html("Submit");
                            JsUtility.showToastr('error', 'Change Password', 'Some error occurred!');
                        }
                    });
                }
            });
        }
    }
}();

jQuery(document).ready(function() {
    ChangePassword.init();
    $('input').blur(function(){
        $("#auth-reset-password-form").validate().element(this);
    });
});

</script>
@endpush