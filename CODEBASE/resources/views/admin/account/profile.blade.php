@extends(ADMIN_THEME_NAME.'.layouts.app')

@section('content')
<div class="app-content content content-ps-stl pt-4">
    <div class="">
        <div class="content-overlay"></div>
        <!-- <div class="header-navbar-shadow"></div> -->
        <div class="content-wrapper">
            <div class="content-body">
                <ul class="nav nav-tabs tabs-pages tab-leftside" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link tab-links active mini-border" href="{{route('admin.profile')}}" aria-selected="true">
                            <i data-feather="user"></i>Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link tab-links mini-border" href="{{route('admin.changePasswordPage')}}" aria-controls="changepasswordIcon" aria-selected="false">
                            <i data-feather="lock"></i>Change Password</a>
                    </li>
                </ul>
                <div class="col-lg-5 pb-3 ">
                    <div class="tab-content">
                        <div class="tab-pane active" id="profileIcon" aria-labelledby="profile-tab" role="tabpanel">
                            <div class="container">
                                <div class="row profile v-row">
                                    <div class="col-lg-10 offset-lg-1 mt-2 pb-3 ">
                                        <div id="profile" class="">
                                        <form class="auth-reset-password-form form-auth-div mt-2" action="{{route('admin.profile')}}" method="POST" id="profileUpdate">
                                            @csrf
                     
                                            <div class="form-label-group">
                                                <label class="form-label float-label" for="name">Username</label> 
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text border-left-line cursor-pointer" id="user-icon-error"><i data-feather="user"></i></span>
                                                    </div>
                                                    <input class="form-control form-caontrol-merge border-right-line" value="{{old('name', isset($user_profile)?$user_profile->name:'')}}" onkeypress="return /[a-zA-Z\s]/i.test(event.key)" maxlength="50"  id="name" autocomplete="name" type="text" name="name" placeholder="Username*" autocomplete="false"/>
                                                    <span id="name-error" class="error"></span>
                                                </div>
                                            </div>

                                            <div class="form-label-group">
                                                <label class="form-label float-label" for="email">Email Address</label>
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text border-left-line cursor-pointer" id="email-icon-error"><i data-feather="at-sign"></i></span>
                                                    </div>
                                                    <input class="form-control border-right-line" id="email" type="text"  value="{{old('email', isset($user_profile)?$user_profile->email:'')}}" name="email" placeholder="Email Address*" autocomplete="false"/>
                                                    <span id="email-error" class="error"></span>
                                                </div>
                                            </div>
                                            <div class="form-label-group">
                                                <label class="form-label float-label" for="mobile">Mobile Number</label>
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text border-left-line cursor-pointer" id="mobile-icon-error"><i data-feather="phone"></i></span>
                                                    </div>
                                                    <input class="form-control border-right-line" id="mobile" type="text"  maxlength="10" min="0" oninput="this.value=this.value.slice(0,this.maxLength)" value="{{old('mobile', isset($user_profile)?$user_profile->mobile:'')}}"  onkeypress='return event.charCode >= 48 && event.charCode <= 57' name="mobile" placeholder="Mobile Number*"/>
                                                    <span id="mobile-error" class="error"></span>
                                                </div>
                                            </div>
                                            <div class="row d-flex justify-content-center col-lg-12"> 
                                                <button type="submit" name="submit" id="submit" value="submit" class="btn btn-primary addAssessor_save_btn" tabindex="3">Submit</button>
                                            </div>
                                            <!-- <button type="submit" name="submit" value="submit" id="submit" class="btn btn-primary" tabindex="3">Submit</button> -->
                                        </form>





                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modern Horizontal Wizard -->
            </div>
        </div>
    </div>
</div>
@endsection

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Profile = function () {
    return { //main function to initiate the module
        init: function () {
            // $('.auth-reset-password-form').validate({
            //     rules: {
            //         'email': {
            //             required: true,
            //             email: true
            //         },
            //         'name': {
            //             required: true
            //         },
            //         'mobile': {
            //             required: true
            //         }
            //     }
            // });
            $('#profileUpdate').submit(function(e){
                $('#submit').prop("disabled",true);
                $('#submit').html("Loading...");
                e.preventDefault();
                var formData = new FormData($(this)[0]);
                $("#profileUpdate :input").prop("disabled", true);
                console.log(formData);
                $.ajax({
                    type: "POST",
                    url: $('form#profileUpdate').attr('action'),
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        if(response.status == 'errors') {
                            $.each(response.errors, function (key, error) {
                                $('form#profileUpdate').find("#" + key + "-icon-error").removeClass('borderRightError').addClass('borderRightError');
                                $('form#profileUpdate').find("#" + key).removeClass('error').addClass('error');
                                $('form#profileUpdate').find("#" + key + "-error").text(error[0]);
                            });
                            $('#submit').prop("disabled",false);
                            $('#submit').html("Submit");
                            $("#profileUpdate :input").prop("disabled", false);
                        }

                        if(response.status == 'success') {
                            $('#submit').prop("disabled",false);
                            $('#submit').html("Submit");
                            $("#profileUpdate :input").prop("disabled", false);
                            JsUtility.showToastr('success', 'Profile', response.message);
                            setTimeout(function(){
                                    location.reload();
                                 }, 5000);
                        }

                        if(response.status == 'error') {
                            $('#submit').prop("disabled",false);
                            $('#submit').html("Submit");
                            $("#profileUpdate :input").prop("disabled", false);
                            JsUtility.showToastr('error', 'Profile', response.message);
                        }
                    },
                    error: function() {
                        $('#submit').prop("disabled",false);
                        $('#submit').html("Submit");
                        $("#profileUpdate :input").prop("disabled", false);
                        JsUtility.showToastr('error', 'Profile', response.message);
                    }
                });
                return false;
            });
        }
    }
}();

jQuery(document).ready(function() {
    Profile.init();
});
</script>
@endpush

