<div class="deznav">
    <div class="deznav-scroll">
        <ul class="metismenu" id="menu">
            <li class="{{($activeMenu=='dashboard')?'active':''}}"><a class="ai-icon"  href="{{route('admin.dashboard')}}" aria-expanded="false">
                <i data-feather="airplay"></i><span class="menu-title " data-i18n="Dashboards"> Dashboard </span></a>
            </li>
            <li class="{{($activeMenu=='assessor')?'active':''}}"><a class="ai-icon" href="{{route('admin.assessors.index')}}" aria-expanded="false">
                <i class="fal fa-users pr-0"></i><span class="menu-title " data-i18n="Invoice">  Assessor Management </span></a>
            </li>
            <li class="{{($activeMenu=='company')?'active':''}}"><a class="ai-icon" href="{{route('admin.company.index')}}" aria-expanded="false">
                <i class="fal fa-id-card pr-0"></i><span class="menu-title " data-i18n="Invoice">  Company Management </span></a>
            </li>
            <li class="{{($activeMenu=='consultant')?'active':''}}"><a class="ai-icon" href="{{route('admin.consultant.index')}}" aria-expanded="false">
                <i class="fal fa-id-card pr-0"></i><span class="menu-title " data-i18n="Invoice">  Facilitator Management </span></a>
            </li>
        </ul>
    </div>
</div>
