<div class="row" >
    <div class="col-md-8 col-lg-9" >
        <ul class="nav nav-pills tabs-pages tab-leftside">
            <li class="nav-item">
                <a class="nav-link @if($tabeActive=='quick_view') active @endif" href="{{route('admin.application.quickview',$application)}}">
                    <span class="bs-stepper-box">
                        <i data-feather="eye" class="font-medium-3"></i>
                    </span>Quick view </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if($tabeActive=='application') active @endif " href="{{route('admin.application.application',$application)}}">
                    <span class="bs-stepper-box">
                        <i data-feather="file-text" class="font-medium-3"></i>
                    </span>Application </a>
            </li>
                <li class="nav-item">
                <a class="nav-link @if($tabeActive=='payments') active @endif " href="{{route('admin.application.payments',$application)}}">
                    <span class="bs-stepper-box">
                        <i class="fal fa-money-bill" class="font-medium-3"></i>
                    </span>Payments </a>
            </li>
            @if ($application->status==1 || $application->status>2)
                @if ($application->checkPrePayment())
                    <li class="nav-item">
                        <a class="nav-link @if($tabeActive=='listplants') active @endif " href="{{route('admin.application.listplants',$application)}}">
                            <span class="bs-stepper-box">
                            <i data-feather='list'  class="font-medium-3"></i>
                            </span>List of plants  </a>
                    </li>
                @endif

                @if ($application->final_documents->count())
                    <li class="nav-item">
                        <a class="nav-link @if($tabeActive=='reg_doc') active @endif " href="{{route('admin.application.registion.doc',$application)}}">
                            <span class="bs-stepper-box">
                            <i data-feather='file-text'  class="font-medium-3"></i>
                            </span>Registration Document</a>
                    </li>
                @endif

                @if($application->assignPlants->count())

                <li class="nav-item">
                    <a class="nav-link @if($tabeActive=='technical') active @endif " href="{{route('admin.application.technical',$application)}}">
                        <span class="bs-stepper-box">
                        <i data-feather='file-text'  class="font-medium-3"></i>
                        </span>Technical Assessment</a>
                </li>
                @if($application->assignPlants->count())
                <li class="nav-item">
                    <a class="nav-link @if($tabeActive=='onsite_assessment') active @endif " href="{{route('admin.application.onsite_assessment',$application)}}">
                        <span class="bs-stepper-box">
                            <i class='fal fa-images' class="font-medium-3"></i>
                        </span>Onsite Documents </a>
                </li>
                @endif
                <li class="nav-item">
                    <a class="nav-link @if($tabeActive=='financial') active @endif " href="{{route('admin.application.financial',$application)}}">
                        <span class="bs-stepper-box">
                        <i data-feather='file-text'  class="font-medium-3"></i>
                        </span>Financial Assessment</a>
                </li>
                @endif
                @php
                $fin_accept = \App\Models\FinancialAssessment::where('application_id',$application->id)->count();
                $tec_accept = \App\Models\TechnicalAssessment::where('application_id',$application->id)->count();
                @endphp
                @if($fin_accept>0 && $tec_accept>0)
                <li class="nav-item">
                    <a class="nav-link @if($tabeActive=='jury') active @endif " href="{{route('admin.application.jury',$application)}}">
                        <span class="bs-stepper-box">
                            <i class='fal fa-images' class="font-medium-3"></i>
                        </span>Jury Review</a>
                </li>
                @endif
                @if($application->jury_tech_score && $application->adv_tech_score)
                <li class="nav-item">
                    <a class="nav-link @if($tabeActive=='certificateview') active @endif " href="{{route('admin.application.certificateview',$application)}}">
                        <span class="bs-stepper-box">
                        <i class="fal fa-file-certificate"></i>
                        </span>Certificate View </a>
                </li>
                @endif
            @endif
        </ul>
    </div>
    <div class="col-md-4 col-lg-3" >
        <p class="p-0 m-0 text-bold text-primary" style="font-size: 1rem;"><strong>Vendor Name:<span> {{$application->vendor->name}}  </strong></span>
        <p class="p-0 m-0 text-bold text-primary" style="font-size: 1rem;"><strong>Application Number:<span> {{($application->application_no==null)?'N/A':$application->application_no}}  </strong></span>
        <p class="p-0 m-0 text-bold text-primary" style="font-size: 1rem;"><strong>CII Reference:<span> {{($application->cii_ref_no==null)?'N/A':$application->cii_ref_no}}   </strong></span>
    </div>
</div>


