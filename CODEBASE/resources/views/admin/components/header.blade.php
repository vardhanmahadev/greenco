<nav class="header-navbar navbar navbar-expand-lg align-items-center navbar-light navbar-shadow fixed-top">
    <div class="navbar-container d-flex content">
        <ul class="nav navbar-nav align-items-center">
            <li class="nav-item d-none d-lg-block">
                <a class="nav-link nav-link-expand"><i class="ficon" data-feather="maximize"></i></a>
            </li>
        </ul>
        <ul class="nav navbar-nav align-items-center ml-auto">
            {{-- <li class="nav-item dropdown dropdown-notification mr-25">
                <a class="nav-link" href="javascript:void(0);" data-toggle="dropdown"><i class="ficon" data-feather="bell"></i><span class="badge badge-pill badge-danger badge-up">5</span></a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                    <li class="dropdown-menu-header">
                        <div class="dropdown-header d-flex">
                            <h4 class="notification-title mb-0 mr-auto">Notifications</h4>
                            <div class="badge badge-pill badge-light-primary">6 New</div>
                        </div>
                    </li>
                    <li class="scrollable-container media-list">
                        <a class="d-flex" href="javascript:void(0)">
                            <div class="media d-flex align-items-start">
                                <div class="media-left">
                                    <div class="avatar"><img src="{{ url('/app-assets/images/avatars/avatar-s-11.jpg') }}" alt="avatar" width="32" height="32"></div>
                                </div>
                                <div class="media-body">
                                    <p class="media-heading"><span class="font-weight-bolder">Congratulation Sam 🎉</span>winner!</p>
                                    <small class="notification-text"> Won the monthly best seller badge.</small>
                                </div>
                            </div>
                        </a>
                        <a class="d-flex" href="javascript:void(0)">
                            <div class="media d-flex align-items-start">
                                <div class="media-left">
                                    <div class="avatar"><img src="{{ url('app-assets/images/avatars/avatar-s-11.jpg') }}" alt="avatar" width="32" height="32"></div>
                                </div>
                                <div class="media-body">
                                    <p class="media-heading"><span class="font-weight-bolder">New message</span>&nbsp;received</p>
                                    <small class="notification-text"> You have 10 unread messages</small>
                                </div>
                            </div>
                        </a>
                        <a class="d-flex" href="javascript:void(0)">
                            <div class="media d-flex align-items-start">
                                <div class="media-left">
                                    <div class="avatar bg-light-danger">
                                        <div class="avatar-content">MD</div>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <p class="media-heading"><span class="font-weight-bolder">Revised Order 👋</span>&nbsp;checkout</p>
                                    <small class="notification-text"> MD Inc. order updated</small>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li class="dropdown-menu-footer"><a class="btn btn-primary btn-block" href="../support/notifications.html">Read all notifications</a></li>
                </ul>
            </li> --}}
            @php
                
                $str=Auth::user()->name;
                if(!$str)
                    $str = 'A';
                $strA = explode(' ', $str);
                $string = "";
                foreach($strA as $words)
                {
                    $string = $string . $words[0];
                }

            @endphp

            <li class="nav-item dropdown dropdown-user">
                <a class="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="user-nav d-sm-flex d-none"><span class="font-medium-2 user-name font-weight-bolder">{{Auth::user()->name}}</span><span class="user-status font-small-2">Admin</span></div>
                    <!-- <span class="avatar"><img class="round" src="{{url('app-assets/images/avatars/profile.png')}}" alt="avatar" height="40" width="40"><span class="avatar-status-online"></span></span> -->
                    <span class="avatar"><div class="circle">{{strtoupper($string)}}</div><span class="avatar-status-online"></span></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-user">
                    <a class="dropdown-item {{($pageTitle=='Profile')?'active':''}}" href="{{route('admin.profile')}}"><i class="mr-50" data-feather="user"></i> Profile</a>
                    <a class="dropdown-item {{($pageTitle=='Change Password')?'active':''}} " href="{{route('admin.changePasswordPage')}}"><i class="mr-50" data-feather="unlock"></i> Change Password</a>
                    <a class="dropdown-item" href="{{route('admin.logout')}}"><i class="mr-50" data-feather="power"></i> Logout</a>
                </div>
            </li>
        </ul>
    </div>
</nav>
<div class="nav-header">
    <a href="{{route('admin.dashboard')}}" class="brand-logo pb-1 pt-1 pl-1 pr-1">
        <img class="img-fluid" src="{{url('app-assets/images/logo/logo-white1.png')}}" alt="Login V2">
        
    </a>
    <div class="nav-control">
        <div class="hamburger">
            <span class="line"></span><span class="line"></span><span class="line"></span>
        </div>
    </div>
</div>
