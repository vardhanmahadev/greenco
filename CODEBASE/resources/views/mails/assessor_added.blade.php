<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Register Success</title>
        <!--bootstrap css -->
        <link rel="stylesheet" type="text/css" href="{{url('app-assets/mail-assets/css/bootstrap.min.css')}}" />
        <!--fontawesome icons-->
        <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
        <!--Custom css-->
        <style>
            * {
                font-family: Century Gothic !important;
                color: #000;
            }

            @font-face {
                font-family: Century Gothic !important;
                src: url("app-assets/mail-assets/fonts/GOTHIC.TTF");
            }

            li {
                display: inline-block;
                margin-left: 0;
                border: 1px solid #ccc;
                padding: 2px;
                border-radius: 50%;
                width: 35px;
                height: 35px;
                margin-right: 6px;
            }

            ul {
                padding-left: 0;
                margin: 0;
            }
            button {
                font-size: 16px;
                color: #fff;
                text-decoration: underline;
                padding: 9px 25px;
                border-radius: 30px;
                background: #8bc34a;
                border: none;
                opacity: 1 !important;
            }
        </style>
    </head>

    <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f0f4f7" class="gwfw">
            <tbody>
                <tr>
                    <td align="center" valign="top" class="py-50 mpy-10 px-10" style="padding-top: 30px; padding-bottom: 30px; padding-left: 10px; padding-right: 10px;">
                        <table width="550" border="0" cellspacing="0" cellpadding="0" class="m-shell" style="box-shadow: 0 1rem 3rem rgba(0, 0, 0, 0.175);">
                            <tbody>
                                <tr>
                                    <td class="td" style="width: 600px; min-width: 600px; font-size: 0pt; line-height: 0pt; padding: 0; margin: 0; font-weight: normal;">
                                        <!-- Header -->
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="background-body" style="background: #8bc34a;">
                                            <tbody>
                                                <tr>
                                                    <td class="pb-2" style="padding-bottom: 2px;">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="border-radius: 4px 4px 0px 0px;">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="py-25 px-50 mpx-20" style="padding-top: 15px; padding-right: 50px;">
                                                                        <table border="0" cellpadding="0" cellspacing="0" class="body" style="border: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                                                                            <tr style="background: #fff; border-top: 0; border-bottom: 0;">
                                                                                <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
                                                                                <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; margin: 0 auto; max-width: 750px; width: 750px;">
                                                                                    <div class="content" style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 750px;">
                                                                                        <table
                                                                                            class="main"
                                                                                            style="
                                                                                                border-collapse: separate;
                                                                                                mso-table-lspace: 0pt;
                                                                                                mso-table-rspace: 0pt;
                                                                                                width: auto;
                                                                                                margin: 0 auto;
                                                                                                padding: 12px 12px;
                                                                                                background: #ffffff;
                                                                                                border-radius: 3px;
                                                                                            "
                                                                                        >
                                                                                            <tr>
                                                                                                <div style="text-align: left; padding: 0px; color: #fff;">
                                                                                                    <img style="width:75px; margin-bottom: 0px;"  src="http://13.126.160.234/greenco/app-assets/images/logo/logo.png" alt="Green-co"  /><!-- <h3 style="text-align: center; color: #47536b; font-size: 17px; text-transform: uppercase; text-decoration: underline;">Green</h3> -->
                                                                                                </div>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="background-body">
                                            <tbody>
                                                <tr>
                                                    <td class="pb-2">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="border-radius: 4px 4px 0px 0px;">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="py-25 px-50 mpx-20" style="padding-top: 15px; padding-bottom: 15px; padding-left: 50px; padding-right: 50px;">
                                                                        <table border="0" cellpadding="0" cellspacing="0" class="body" style="border: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                                                                            <tr style="background: #fff; border-top: 0; border-bottom: 0;">
                                                                                <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
                                                                                <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; margin: 0 auto; max-width: 750px; width: 750px;">
                                                                                    <div class="content" style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 750px;">
                                                                                        <table
                                                                                            class="main"
                                                                                            style="
                                                                                                border-collapse: separate;
                                                                                                mso-table-lspace: 0pt;
                                                                                                mso-table-rspace: 0pt;
                                                                                                width: auto;
                                                                                                margin: 0 auto;
                                                                                                padding: 12px 12px;
                                                                                                background: #ffffff;
                                                                                                border-radius: 3px;
                                                                                            "
                                                                                        >
                                                                                            <tr>

                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="background-body" style="background: #8bc34a; padding-bottom: 2px;">
                                            <tbody>
                                                <tr>
                                                    <td class="es-p30t es-p30b">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="py-50 px-50 mpy-30 mpx-20" style="padding-top: 0px; padding-left: 50px; padding-right: 50px;">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td
                                                                                        class="text-14 t-color a-center pb-18 tt-u fw-b"
                                                                                        style="
                                                                                            font-family: 'Montserrat', Arial, sans-serif;
                                                                                            font-size: 31px;
                                                                                            line-height: 24px;
                                                                                            min-width: auto !important;
                                                                                            color: #e75e83;
                                                                                            text-align: center;
                                                                                            padding-bottom: 18px;
                                                                                            font-weight: 400;
                                                                                            color: #2b2929;
                                                                                        "
                                                                                    >
                                                                                    Dear {{$user->name}},
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td
                                                                                        class="text-14 t-color a-center pb-18 tt-u fw-b"
                                                                                        style="
                                                                                            font-family: 'Montserrat', Arial, sans-serif;
                                                                                            font-size: 17px;
                                                                                            line-height: 31px;
                                                                                            min-width: auto !important;
                                                                                            color: #8bc34a;
                                                                                                text-align: center;
                                                                                                padding-bottom: 8px;
                                                                                            font-weight: 400;
                                                                                        "
                                                                                    >
                                                                                    Your account has been approved!
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td
                                                                                        class="text-14 t-color a-center pb-18 tt-u fw-b"
                                                                                        style="
                                                                                              box-sizing: border-box;
                                                                            font-family: 'Montserrat',Arial,sans-serif;
                                                                            font-size: 16px;
                                                                            line-height: 23px;
                                                                            min-width: auto!important;
                                                                            color: #2b2929;
                                                                            /* margin-bottom: 25px; */
                                                                            text-align: center;
                                                                            padding-bottom: 18px;
                                                                            font-weight: 400;
                                                                                        "
                                                                                    >
                                                                                     You will now able to Login, Kindly login the portal with the following credentials.<br>
                                                                                        Username: <strong>{{$user->email}}</strong><br>
                                                                                        Password: <strong>{{$password}}</strong><br>
                                                                <a href="{{route('assessor.login')}}"><button style="background: #2eeab7;color:#000 !important;margin-top:5px !important" class="visit-sitelink">Log In <span class="arrow"></span></button></a>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="center">
                                                                                        <!-- Button -->
                                                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td
                                                                                                        class="text-btn btn-1"
                                                                                                        style="
                                                                                                            mso-padding-alt: 14px 20px;
                                                                                                            font-family: 'Montserrat', Arial, sans-serif;
                                                                                                            font-size: 14px;
                                                                                                            line-height: 18px;
                                                                                                            text-align: center;
                                                                                                            font-weight: bold;
                                                                                                            border-radius: 3px;
                                                                                                            min-width: auto !important;
                                                                                                        "
                                                                                                    >
                                                                                                        <a
                                                                                                            href="#"
                                                                                                            target="_blank"
                                                                                                            class="link btn-1-c"
                                                                                                            style="padding: 14px 20px; display: block; text-decoration: none; color: #ffffff; font-size: 26px;"
                                                                                                        >
                                                                                                            <span class="link btn-1-c" style="text-decoration: none; color: #945e37; letter-spacing: 5px; font-weight: 500;"></span>
                                                                                                        </a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                        <!-- END Button -->
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- Footer -->
                        <table width="550" border="0" bgcolor="#ffffff" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td align="center" valign="top">
                                        <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
                                            <tbody>
                                                <tr>
                                                    <td class="plr-15" style="padding: 0px;">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-top: 25px;">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <th class="column-top" width="54%" style="font-size: 0pt; line-height: 0pt; padding: 0; margin: 0; font-weight: normal; vertical-align: top;">
                                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-bottom: 0px;">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td
                                                                                                        class="text-14 lh-28 c-grey mt-center"
                                                                                                        style="
                                                                                                            font-family: 'Montserrat', Arial, sans-serif;
                                                                                                            font-size: 14px;
                                                                                                            text-align: left;
                                                                                                            min-width: auto !important;
                                                                                                            line-height: 28px;
                                                                                                            color: #555555;
                                                                                                        "
                                                                                                    >
                                                                                                        <p class="mb-2" style="font-size: 19px; text-align: left; margin-bottom: 15px;">Follow us on</p>
                                                                                                        <div class="">
                                                                                                            <ul class="contact-details">
                                                                                                                <li>
                                                                                                                    <a href="">
                                                                                                                        <img
                                                                                                                            src="http://13.126.160.234/greenco/app-assets/mail-assets/images/instagram.png"
                                                                                                                            width="25px"
                                                                                                                            alt=""
                                                                                                                            style="padding: 6px; width: 24px;"
                                                                                                                        />
                                                                                                                    </a>
                                                                                                                </li>
                                                                                                                <li>
                                                                                                                    <a href="">
                                                                                                                        <img
                                                                                                                            src="http://13.126.160.234/greenco/app-assets/mail-assets/images/facebook.png"
                                                                                                                            width="25px"
                                                                                                                            alt=""
                                                                                                                            style="padding: 6px; width: 24px;"
                                                                                                                        />
                                                                                                                    </a>
                                                                                                                </li>
                                                                                                                <li>
                                                                                                                    <a href="">
                                                                                                                        <img
                                                                                                                            src="http://13.126.160.234/greenco/app-assets/mail-assets/images/twitter.png"
                                                                                                                            width="25px"
                                                                                                                            alt=""
                                                                                                                            style="padding: 6px; width: 24px;"
                                                                                                                        />
                                                                                                                    </a>
                                                                                                                </li>
                                                                                                            </ul>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </th>
                                                                                    <th class="column mpb-20" width="4%" style="font-size: 0pt; line-height: 0pt; padding: 0; margin: 0; font-weight: normal;"></th>
                                                                                    <th class="column-bottom" width="42%" style="font-size: 0pt; line-height: 0pt; padding: 0; margin: 0; font-weight: normal;">
                                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td
                                                                                                        class="text-14 lh-28 c-grey mt-center"
                                                                                                        style="font-family: 'Montserrat', Arial, sans-serif; font-size: 14px; min-width: auto !important; line-height: 28px; color: #555555;"
                                                                                                    >
                                                                                                        <p style="font-size: 19px; text-align: right; margin-bottom: 15px;">Contact Us</p>
                                                                                                        <p style="text-align: right;">
                                                                                                            +91 99123 08160 <br />
                                                                                                            info.ciigreenco@gmail.com
                                                                                                        </p>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </th>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="background-body">
                                            <tbody>
                                                <tr>
                                                    <td class="pb-2">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="border-radius: 4px 4px 0px 0px;">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="py-25 px-50 mpx-20" style="padding-top: 15px; padding-bottom: 15px; padding-left: 50px; padding-right: 50px;">
                                                                        <table border="0" cellpadding="0" cellspacing="0" class="body" style="border: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                                                                            <tr style="background: #fff; border-top: 0; border-bottom: 0;">
                                                                                <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
                                                                                <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; margin: 0 auto; max-width: 750px; width: 750px;">

                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- END Footer -->
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>