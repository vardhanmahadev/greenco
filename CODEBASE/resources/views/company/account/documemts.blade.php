
@extends(COMPANY_THEME_NAME.'.layouts.app')


@section('content')
<div class="app-content content content-ps-stl">
    <div class="">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
       
        </div>
    </div>
</div>

@endsection

@push('PAGE_SCRIPTS')

<script type="text/javascript">

var stepper = new Stepper(document.querySelector('.bs-stepper'))

/// Will navigate to the second step

    $("#prev1").click(function(){
        stepper.to(1);
    });

    $("#prev2").click(function(){
        stepper.to(2);
        
    });

    $("#prev3").click(function(){
        stepper.to(3);
        
    });

    

var Profile = function () {
    return { //main function to initiate the module
        init: function () {
            
            $('#add_company_form').submit(function(e){
                e.preventDefault();
                var formData = new FormData($(this)[0]);

                $("#step1").attr('disabled',true);
                $(".back").css('pointer','none');
                $("#step1").html("Loading....");

                $.ajax({
                    type: "POST",
                    url: $('form#add_company_form').attr('action'),
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        if(response.status == 'validations') {
                            $.each(response.errors, function (key, error) {
                                $("#step1").attr('disabled',false);
                                $("#step1").html("Save & Continue");
                                $('form#add_company_form').find("#" + key).removeClass('error').addClass('error');
                                $('form#add_company_form').find("#" + key + "-error").text(error[0]);
                            });
                        }

                        if(response.status == 'success') {
                           
                            stepper.to(2);
                            $("#step1").attr('disabled',false);
                            $("#step1").html("Save & Continue");
                               
                            // JsUtility.showToastr('success', 'Company', response.message);
                            
                        }

                        if(response.status == 'error') {
                            $("#step1").attr('disabled',false);
                            $("#step1").html("Save & Continue");
                            JsUtility.showToastr('error', 'Company', response.message);
                        }
                    },
                    error: function() {
                            $("#step1").attr('disabled',false);
                            $("#step1").html("Save & Continue");
                        JsUtility.showToastr('error', 'Company', response.message);
                    }
                }); 
                
                    

                return false;
            });

            $('#add_company_industry_form').submit(function(e){
                e.preventDefault();
                var formData = new FormData($(this)[0]);
                $("#step2").attr('disabled',true);
                $(".back").css('pointer','none');
                $("#step2").html("Loading....");

                $.ajax({
                    type: "POST",
                    url: $('form#add_company_industry_form').attr('action'),
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        if(response.status == 'validations') {
                            $.each(response.errors, function (key, error) {
                                $("#step2").attr('disabled',false);
                                $("#step2").html("Save & Continue");
                                $('form#add_company_industry_form').find("#" + key).removeClass('error').addClass('error');
                                $('form#add_company_industry_form').find("#" + key + "-error").text(error[0]);
                            });
                        }

                        if(response.status == 'success') {
                            
                            stepper.to(3);
                            // $("#industry-details").attr('data-toggle','tab');
                            // JsUtility.showToastr('success', 'Company', response.message);
                            
                        }

                        if(response.status == 'error') {
                            $("#step2").attr('disabled',false);
                            $("#step2").html("Save & Continue");
                            JsUtility.showToastr('error', 'Company', response.message);
                        }
                    },
                    error: function() {
                            $("#step2").attr('disabled',false);
                            $("#step2").html("Save & Continue");
                        JsUtility.showToastr('error', 'Company', response.message);
                    }
                }); 
                
                    

                return false;
            });

            $('#add_company_infrastructure_form').submit(function(e){
                e.preventDefault();
                var formData = new FormData($(this)[0]);
                $("#step3").attr('disabled',true);
                $(".back").css('pointer','none');
                $("#step3").html("Loading....");

                $.ajax({
                    type: "POST",
                    url: $('form#add_company_infrastructure_form').attr('action'),
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        if(response.status == 'validations') {
                            $.each(response.errors, function (key, error) {
                                $("#step3").attr('disabled',false);
                                $("#step3").html("Save & Continue");
                                $('form#add_company_infrastructure_form').find("#" + key).removeClass('error').addClass('error');
                                $('form#add_company_infrastructure_form').find("#" + key + "-error").text(error[0]);
                            });
                        }

                        if(response.status == 'success') {
                            
                            stepper.to(4);
                            // $("#industry-details").attr('data-toggle','tab');
                            // JsUtility.showToastr('success', 'Company', response.message);
                            
                        }

                        if(response.status == 'error') {
                            $("#step3").attr('disabled',false);
                            $("#step3").html("Save & Continue");
                            JsUtility.showToastr('error', 'Company', response.message);
                        }
                    },
                    error: function() {
                            $("#step3").attr('disabled',false);
                            $("#step3").html("Save & Continue");
                        JsUtility.showToastr('error', 'Company', response.message);
                    }
                }); 
                
                    

                return false;
            });

            $('#add_company_income_form').submit(function(e){
                e.preventDefault();
                var formData = new FormData($(this)[0]);
                $("#step4").attr('disabled',true);
                $(".back").css('pointer','none');
                $("#step4").html("Loading....");

                $.ajax({
                    type: "POST",
                    url: $('form#add_company_income_form').attr('action'),
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        if(response.status == 'validations') {
                            $.each(response.errors, function (key, error) {
                                $("#step4").attr('disabled',false);
                                $("#step4").html("Save & Submit");
                                $('form#add_company_income_form').find("#" + key).removeClass('error').addClass('error');
                                $('form#add_company_income_form').find("#" + key + "-error").text(error[0]);
                            });
                        }

                        if(response.status == 'success') {
                            // stepper.to(4);
                            // $("#industry-details").attr('data-toggle','tab');
                            JsUtility.showToastr('success', 'Company', response.message);
                            setTimeout(function(){
                                    location.reload();
                                 }, 5000);
                            
                        }

                        if(response.status == 'error') {
                            $("#step4").attr('disabled',false);
                            $("#step4").html("Save & Submit");
                            JsUtility.showToastr('error', 'Company', response.message);
                        }
                    },
                    error: function() {
                            $("#step4").attr('disabled',false);
                            $("#step4").html("Save & Submit");
                        JsUtility.showToastr('error', 'Company', response.message);
                    }
                }); 
                
                    

                return false;
            });

            
            
        }
    }
}();

jQuery(document).ready(function() {
    Profile.init();
});
</script>
@endpush
