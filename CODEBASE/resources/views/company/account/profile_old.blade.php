
@extends(COMPANY_THEME_NAME.'.layouts.app')

@section('content')
<div class="app-content content content-ps-stl">
    <div class="">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <section class="profile-style2">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="card ">
                                    <div class="d-flex v-row title-strip mb-0">
                                        <div class="col-6 pl-0">Login Information:</div>
                                        <div class="col-6 pr-0"> <a class="btn btn-primary float-right" href="{{route('company.changePassword')}}"><i data-feather="lock"></i>Change Password</a></div>
                                    </div>
                                    <div class="card-body ">
                                        <form id="add_company_form" class="auth-reset-password-form form-auth-div mt-2" action="{{route('company.updateprofile')}}" method="POST">
                                            <div class="form-label-group mb-1">
                                                <input class="form-control border-right-line" disabled id="register-email" type="text" name="register-email" placeholder="Username* (Email Address)" value="{{$profile->email}}" autocomplete="false" onfocus="this.removeAttribute('readonly');" />
                                                <label class="form-label ">Username (Email Address)</label>
                                            </div>
                                        
                                    </div>
                                </div>
                                <div class="card ">
                                    <div class="d-flex v-row title-strip mb-0">
                                        <div class="col-6 pl-0">Industry Details</div>
                                    </div>
                                    <div class="card-body ">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group mt--1">
                                                    <label for="login-password">Type of the Industry</label>
                                                    <select class="select2 form-control" data-placeholder="select" name="industry" id="industry" >
                                                        <option value="">Select Sector</option>
                                                        @foreach($industries as $industry)
                                                            <option value="{{$industry->id}}" @if(isset($profile))@if($profile->mst_industry_id == $industry->id) selected @endif @endif>{{$industry->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group mt--1">
                                                    <label for="login-password">Type of Entity</label>
                                                    <select class="select2 form-control" data-placeholder="select" id="entity" name="entity" >
                                                        <option value="">Select Entity</option>
                                                        @foreach($entities as $entity)
                                                            <option value="{{$entity->id}}" @if(isset($profile))@if($profile->mst_entity_id == $entity->id) selected @endif @endif>{{$entity->name}}</option>
                                                        @endforeach  
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group ">
                                                    <label for="login-password">Type of Sector</label>
                                                    <select class="select2 form-control" data-placeholder="select" id="sector" name="sector" >
                                                        <option value="">Select Sector</option>
                                                        @foreach($sectors as $sector)
                                                            <option value="{{$sector->id}}"@if(isset($profile))@if($profile->mst_sector_id == $sector->id) selected @endif @endif>{{$sector->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="card ">
                                    <div class="d-flex v-row title-strip mb-2">
                                        <div class="col-6 pl-0">Contact Details</div>
                                    </div>
                                    <div class="card-body ">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-label-group">
                                                    <input class="form-control form-caontrol-merge border-right-line" id="companyname" autocomplete="companyname" type="text" name="companyname" placeholder="Name of the Company" autocomplete="false" value="{{isset($profile)?$profile->name : old('companyname') }}"  />
                                                    <label class="form-label float-label">Name of the Company</label>
                                                    <span class="error" id="companyname-error"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-label-group">
                                                    <input class="form-control form-caontrol-merge border-right-line " id="email" autocomplete="email" type="email" name="email" placeholder="Email Id" autocomplete="false" value="{{isset($profile)?$profile->email : old('email') }}"   />
                                                    <label class="form-label float-label ">Email</label>
                                                    <span class="error" id="email-error"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-label-group">
                                                    <input class="form-control form-caontrol-merge border-right-line" id="mobileno" maxlength="10" min="0" step="1"   onkeypress="return event.charCode >= 48 && event.charCode <= 57"   oninput="this.value=this.value.slice(0,this.maxLength)" 
                                                    autocomplete="mobileno" type="number" name="mobileno" placeholder="Mobile No" autocomplete="false"  value="{{isset($profile)?$profile->mobile : old('mobileno') }}" />
                                                    <label class="form-label float-label">Mobile No</label>
                                                    <span class="error" id="mobileno-error"></span>      
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-label-group">
                                                    <input class="form-control border-right-line" id="location" type="text" name="location" placeholder="City" autocomplete="false"  value="{{isset($profile)?$profile->location : old('location') }}"   />
                                                    <label class="form-label float-label" for="location">City</label>
                                                    <span class="error" id="location-error"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-label-group">
                                                    <textarea placeholder="Postal Address with Pincode" class="form-control border-right-line" height="135" rows="4" name='postaladdress' id='postaladdress' >{{isset($profile)?$profile->postal_address : old('postaladdress') }}</textarea>
                                                    <label class="form-label float-label" for="email">Postal Address with Pincode</label>
                                                    <span class="error" id="postaladdress-error"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-label-group">
                                                    <textarea placeholder="Billing Address with Pincode " class="form-control border-right-line" height="135" rows="4" name='billingaddress' id='billingaddress' >{{isset($profile)?$profile->billing_address : old('billingaddress') }}</textarea>
                                                    <label class="form-label float-label" for="email">Billing Address with Pincode</label>
                                                    <span class="error" id="billingaddress-error"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="card ">
                                    <div class="d-flex v-row title-strip mb-2">
                                        <div class="col-6 pl-0">Infrastructure Details</div>
                                    </div>
                                    <div class="card-body ">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-label-group">
                                                    <input class="form-control form-caontrol-merge border-right-line" type="text" id="productsorservicesname" name="productsorservicesname" value="{{isset($profile)?$profile->product_name : old('productsorservicesname') }}" placeholder="Name of the Products manufactured / Services offered"  />
                                                    <label class="form-label float-label">Name of the Products manufactured / Services offered</label>
                                                    <span class="error" id="productsorservicesname-error"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-label-group">
                                                     <input class="form-control form-caontrol-merge border-right-line" type="number" min="0"  id="latestturnover" name="latestturnover" value="{{isset($profile)?$profile->turnover : old('latestturnover') }}" placeholder="Enter latest turnover of the unit/ facility, in Rs Crores only"  />
                                                    <label class="form-label float-label">Latest turnover of the unit/ facility, in Rs Crores only</label>
                                                    <span class="error" id="latestturnover-error"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-label-group">
                                                     <input class="form-control form-caontrol-merge border-right-line" type="number" min="0"  id="totalpermanentemployees" name="totalpermanentemployees" value="{{isset($profile)?$profile->no_of_employees : old('totalpermanentemployees') }}" placeholder="Total no of employees permanent employees"  />
                                                    <label class="form-label float-label">Total no of employees permanent employees</label>
                                                    <span class="error" id="totalpermanentemployees-error"></span> 
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-label-group">
                                                    <input class="form-control form-caontrol-merge border-right-line" min="0" type="number" id="totalcontractemployees" name="totalcontractemployees" value="{{isset($profile)?$profile->contract_employees : old('totalcontractemployees') }}"  placeholder="Total no of contract employees"  />
                                                    <label class="form-label float-label">Total no of contract employees</label>
                                                    <span class="error" id="totalcontractemployees-error"></span>  
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-label-group">
                                                    <input class="form-control form-caontrol-merge border-right-line" type="number" min ="0" id="electricalenergyconsumption" name="electricalenergyconsumption" value="{{isset($profile)?$profile->electrical_consumption : old('electricalenergyconsumption') }}"  placeholder="Yearly Electrical Energy Consumption in KWH / year"  />
                                                    <label class="form-label float-label">Yearly Electrical Energy Consumption in KWH / year</label>
                                                    <span class="error" id="electricalenergyconsumption-error"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-label-group">
                                                    <input class="form-control form-caontrol-merge border-right-line" type="number" min="0" id="thermalenergyconsumption" name="thermalenergyconsumption" value="{{isset($profile)?$profile->thermal_consumption : old('thermalenergyconsumption') }}"  placeholder="Yearly Thermal Energy Consumption in Kcal / year"  />
                                                    <label class="form-label float-label">Yearly Thermal Energy Consumption in Kcal / year</label>
                                                    <span class="error" id="thermalenergyconsumption-error"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-label-group">
                                                    <input class="form-control form-caontrol-merge border-right-line" type="number" min="0" id="waterconsumption" name="waterconsumption" value="{{isset($profile)?$profile->water_consumption : old('waterconsumption') }}"  placeholder="Yearly water consumption in Kilo Litres/ Cubic meteres"  />
                                                    <label class="form-label float-label">Yearly water consumption in Kilo Litres/ Cubic meteres</label>
                                                    <span class="error" id="waterconsumption-error"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="form-label-group">
                                                    <input class="form-control form-caontrol-merge border-right-line" id="totalarea" name="totalarea" value="{{isset($profile)?$profile->area : old('totalarea') }}" type="number" placeholder="Total area of the unit / facility ( including Built up space, open area, colony or township etc ) in Acres only"  />
                                                    <label class="form-label float-label">Total area of the unit / facility ( including Built up space, open area, colony or township etc ) in Acres only</label>
                                                    <span class="error" id="totalarea-error"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-label-group">
                                                    <input class="form-control form-caontrol-merge border-right-line" type="text" id="budget_allocation" name="budget_allocation" value="{{isset($profile)?$profile->budget_allocation : old('budget_allocation') }}" placeholder="Budget Allocation"  />
                                                    <label class="form-label float-label">Budget Allocation</label>
                                                    <span class="error" id="budget_allocation-error"></span>
                                                </div>
                                            </div>
                                       
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="card ">
                                    <div class="d-flex v-row title-strip mb-2">
                                        <div class="col-6 pl-0">Income Details</div>
                                    </div>
                                    <div class="card-body ">
                                        <div class="row">
                                           
                                            <div class="col-lg-4">
                                                <div class="form-label-group">
                                                    <input class="form-control form-caontrol-merge border-right-line" type="text" id="tanno" name="tanno" value="{{isset($profile)?$profile->tanno : old('tanno') }}" placeholder="TAN NO (Tax dedcution Account Number)"  />
                                                    <label class="form-label float-label">TAN NO (Tax dedcution Account Number)</label>
                                                    <span class="error" id="tanno-error"></span>
                                                 </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-label-group">
                                                    <input class="form-control form-caontrol-merge border-right-line" type="text" id="panno" name="panno" value="{{isset($profile)?$profile->panno : old('panno') }}" placeholder="PAN NO (Permanent Account Number)"  />
                                                    <label class="form-label float-label">PAN NO (Permanent Account Number)</label>
                                                    <span class="error" id="panno-error"></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-label-group">
                                                    <input class="form-control form-caontrol-merge border-right-line" id="gstinno" name="gstinno" value="{{isset($profile)?$profile->gstno : old('gstinno') }}" type="text" placeholder="GSTIN NO (Goods and Services Tax Identification Number )"  />
                                                    <label class="form-label float-label">GSTIN NO (Goods and Services Tax Identification Number )</label>
                                                    <span class="error" id="gstinno-error"></span>
                                                </div>
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="card ">
                                                <div class="d-flex v-row title-strip mb-2">
                                                    <div class="col-6 pl-0">Proposal Document</div>
                                                </div>
                                                <div class="card-body ">
                                                    <div class="row  pb-1 mt--1">
                                                        <div class="col-lg-12">
                                                            <div class="row v-row mb-2">
                                                                @if(isset($profile->proposal_document))
                                                                <div class="col-lg-2 col-md-2">
                                                                    <label>Uploaded File :</label>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4">
                                                                    <div class="v-row uploadedfile">
                                                                        <span class="">{{url($profile->proposal_document)}}</span> <button class="btn btn-outline-secondary waves-effect ml-1"> <i data-feather="download"></i></button>
                                                                    </div>
                                                                </div>
                                                                @endif
                                                                <div class="col-lg-2 col-md-2">
                                                                    <label>Upload File :</label>
                                                                </div>
                                                                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12  mt-50">
                                                                    <div class="custom-file">
                                                                        <input type="file" class="custom-file-input" name="proposal_doc" id="customFile">
                                                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                                                    </div>
                                                                    <span class="error" id="proposal_doc-error"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="card ">
                                                <div class="d-flex v-row title-strip mb-2">
                                                    <div class="col-6 pl-0">PO Document</div>
                                                </div>
                                                <div class="card-body ">
                                                    <div class="row  pb-1 mt--1">
                                                        <div class="col-lg-12">
                                                            <div class="row v-row mb-2">
                                                                @if(isset($profile->po_doc))
                                                                <div class="col-lg-2 col-md-2">
                                                                    <label>Uploaded File :</label>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4">
                                                                    <div class="v-row  uploadedfile">
                                                                        <span class=""></span> <button class="btn btn-outline-secondary waves-effect ml-1"> <i data-feather="download"></i></button>
                                                                    </div>
                                                                </div> 
                                                                @endif
                                                                <div class="col-lg-2 col-md-2">
                                                                    <label>Upload File :</label>
                                                                </div>
                                                                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12  mt-50">
                                                                    <div class="custom-file">
                                                                        <input type="file" class="custom-file-input" name="po_doc" id="customFile">
                                                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                                                    </div>
                                                                    <span class="error" id="po_doc-error"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                               

                            <div class="col-lg-12 justify-content-center  d-flex">
                                <button type="submit" class="btn btn-primary" id="submitButton" name="doSubmit" value="Submit" tabindex="4">Submit <i class="fal fa-long-arrow-right"></i> </button>
                            </div>

                        </form>
                </section>
            </div>
        </div>
    </div>
</div>

@endsection

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Profile = function () {
    return { //main function to initiate the module
        init: function () {
            
            $('#add_company_form').submit(function(e){
                e.preventDefault();
                var formData = new FormData($(this)[0]);

                $("#update").attr('disabled',true);
                $(".back").css('pointer','none');
                $("#update").html("Loading....");

                $.ajax({
                    type: "POST",
                    url: $('form#add_company_form').attr('action'),
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        if(response.status == 'validations') {
                            $.each(response.errors, function (key, error) {
                                $("#update").attr('disabled',false);
                                $("#update").html("Submit");
                                $('form#add_company_form').find("#" + key).removeClass('error').addClass('error');
                                $('form#add_company_form').find("#" + key + "-error").text(error[0]);
                            });
                        }

                        if(response.status == 'success') {
                            JsUtility.showToastr('success', 'Company', response.message);
                            
                        }

                        if(response.status == 'error') {
                            $("#update").attr('disabled',false);
                            $("#update").html("Submit");
                            JsUtility.showToastr('error', 'Company', response.message);
                        }
                    },
                    error: function() {
                            $("#update").attr('disabled',false);
                            $("#update").html("Submit");
                        JsUtility.showToastr('error', 'Company', response.message);
                    }
                }); 
                
                    

                return false;
            });
        }
    }
}();

jQuery(document).ready(function() {
    Profile.init();
});
</script>
@endpush
