
@extends(COMPANY_THEME_NAME.'.layouts.app')

@section('content')

<link rel="stylesheet" type="text/css" href="{{ url('app-assets/vendors/css/forms/wizard/bs-stepper.min.css') }}">
<!-- <link rel="stylesheet" type="text/css" href="{{ url('app-assets/css/plugins/forms/form-validation.css') }}"> -->
<link rel="stylesheet" type="text/css" href="{{ url('app-assets/css/plugins/forms/form-wizard.min.css') }}">
<style>
body.swal2-height-auto {
    height: inherit!important;
}
</style>
<div class="app-content content content-ps-stl">
    <div class="">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <section class="col-lg-12 horizontal-wizard">
                <div class="bs-stepper horizontal-wizard-example">
                    <div class="bs-stepper-header company-profile-info" role="tablist">
                        <div class="step" data-target="#contact-details" role="tab" id="contact-details-trigger">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">1</span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">Basic Info</span>
                                </span>
                            </button>
                        </div>
                        <div class="line">
                            <i data-feather="chevron-right" class="font-medium-2"></i>
                        </div>
                        <div class="step" data-target="#industry-details" role="tab" id="industry-details-trigger">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">2</span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">Industry Details</span>
                                </span>
                            </button>
                        </div>
                        <div class="line">
                            <i data-feather="chevron-right" class="font-medium-2"></i>
                        </div>
                        <div class="step" data-target="#infrastructure-step" role="tab" id="infrastructure-step-trigger">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">3</span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">Infrastructure Details</span>
                                </span>
                            </button>
                        </div>
                        <div class="line">
                            <i data-feather="chevron-right" class="font-medium-2"></i>
                        </div>
                        <div class="step" data-target="#income-details" role="tab" id="income-details-trigger">
                            <button type="button" class="step-trigger">
                                <span class="bs-stepper-box">4</span>
                                <span class="bs-stepper-label">
                                    <span class="bs-stepper-title">Income Details</span>
                                </span>
                            </button>
                        </div>
                    </div>
                    <div class="bs-stepper-content">
                        <div id="contact-details" class="content mt-2" role="tabpanel" aria-labelledby="contact-details-trigger">
                            <form id="add_company_form" class="form-auth-div mt-2" action="{{route('company.updateprofile')}}" onSubmit="return false" method="POST">
                                @csrf
                                <input type="hidden" name="tab" id="tab" value="1">
                                <input type='hidden' name='profile_updated_status' id='profile_updated_status' value='{{$profile->profile_update}}'>
                                <input type='hidden' name="userid" id="userid" value="{{isset($profile)?$profile->id:''}}">
                                <div class="row" id='basicinfo'>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <!-- onkeypress="return /[a-zA-Z\s]/i.test(event.key)" -->
                                            <label class="form-label">Name of the Company<span class="text-danger">*</span></label>
                                            <input type="text" maxlength="50" placeholder="Company Name*" name="company_name"  disabled='true' value="{{old('company_name', isset($profile)?$profile->name:'')}}" id="company_name" class="ui-autocomplete-input form-control form-caontrol-merge border-right-line remarks_data" autocomplete="off">
                                            <span class="error" id="company_name-error-back"></span>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Mobile Number<span class="text-danger">*</span></label>
                                            <input class="form-control form-caontrol-merge border-right-line remarks_data" disabled='true' id="mobileno" name="mobileno" type="text"  maxlength="10" min="0"  oninput="this.value=this.value.slice(0,this.maxLength)" placeholder="Mobile Number*" value="{{old('mobileno', isset($profile)?$profile->mobile:'')}}" autocomplete="false" />
                                            <span class="error" id="mobileno-error-back"></span>
                                        </div>
                                        <div class="form-group mb-50">
                                            <label class="form-label">Email Address<span class="text-danger">*</span></label>
                                            <input class="form-control form-caontrol-merge border-right-line remarks_data" disabled='true' id="email" autocomplete="email" type="email" name="email" placeholder="Email Address*" autocomplete="false"  value="{{old('email', isset($profile)?$profile->email:'')}}"   />
                                            <span class="error" id="email-error-back"></span>
                                        </div>
                                        <input type='hidden' name='old_email' id="old_email" value="{{$profile->email}}"  >



                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-label">State <span class="text-danger">*</span></label>
                                            <select class="form-control basic-info-form-controls" data-placeholder="Select State" id="state" name='state'>
                                                <option value="">Select State</option>
                                                @foreach($states as $state)
                                                    <option value="{{$state->id}}" @if(isset($profile))@if($profile->state == $state->id) selected @endif @endif>{{$state->name}}</option>
                                                @endforeach

                                            </select>
                                            <span class="error" id="state-error-back"></span>

                                        </div>

                                        <div class="form-group">
                                        <div class="">
                                            <label class="form-label" for="location">City<span class="text-danger">*</span></label>
                                            <input class="form-control border-right-line basic-info-form-controls" onkeypress="return /[a-zA-Z\s]/i.test(event.key)" maxlength="50" id="location" type="text" name="location" placeholder="City*" autocomplete="false"  value="{{old('location', isset($profile)?$profile->location:'')}}"   />
                                            <span class="error" id="location-error-back"></span>
                                        </div>
                                        </div>


                                    </div>

                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-label" for="postaladdress">Postal Address<span class="text-danger">*</span></label>
                                            <textarea placeholder="Postal Address*" class="form-control border-right-line basic-info-form-controls" height="125" rows="3" name='postaladdress' id='postaladdress' >{{  old('postaladdress', $profile->postal_address) }}</textarea>
                                            <span class="error" id="postaladdress-error-back"></span>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Postal Address Pincode<span class="text-danger">*</span></label>
                                            <input class="form-control border-right-line basic-info-form-controls" type="text" onkeypress="return /[0-9]/i.test(event.key)" maxlength="6" min="0"  oninput="this.value=this.value.slice(0,this.maxLength)"  name='postal_address_pincode' id='postal_address_pincode'  placeholder="Postal Address Pincode*" value="{{  old('postal_address_pincode', $profile->postal_address_pincode) }}" autocomplete="false" />
                                            <span class="error" id="postal_address_pincode-error-back"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-label" for="billingaddress">Billing Address<span class="text-danger">*</span></label>
                                            <textarea placeholder="Billing Address*" class="form-control border-right-line basic-info-form-controls" height="125" rows="3" name='billingaddress' id='billingaddress' >{{  old('billingaddress', $profile->postal_address) }}</textarea>
                                            <span class="error" id="billingaddress-error-back"></span>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Billing Address Pincode<span class="text-danger">*</span></label>
                                            <input class="form-control border-right-line basic-info-form-controls" type="text" onkeypress="return /[0-9]/i.test(event.key)" maxlength="6" min="0"  oninput="this.value=this.value.slice(0,this.maxLength)" name='billing_address_pincode' id='billing_address_pincode'   value="{{  old('billing_address_pincode', $profile->billing_address_pincode) }}" placeholder="Billing Address Pincode*" autocomplete="false" />
                                            <span class="error" id="billing_address_pincode-error-back"></span>
                                        </div>
                                    </div>
                                </div>
                                @if($profile->profile_update == 1)
                                <div class="d-flex justify-content-end">
                                    <button type ="button" id="next1" name="next1"  class="btn btn-primary btn-next profile-info-btn">
                                        <span class="align-middle d-sm-inline-block d-none">Next</span>
                                        <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                    </button>
                                </div>
                                @else
                                 <div class="d-flex justify-content-end">
                                    <button type ="submit" id="step1" name="step1"  class="btn btn-primary btn-next profile-info-btn">
                                        <span class="align-middle d-sm-inline-block d-none">Save & Continue</span>
                                        <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                    </button>
                                </div>
                                @endif

                            </form>
                        </div>


                        <div id="industry-details" class="content" role="tabpanel" aria-labelledby="industry-details-trigger">

                            <form id="add_company_industry_form" class="form-auth-div mt-2" action="{{route('company.updateprofile')}}" onSubmit="return false" method="POST">
                                @csrf
                                <input type="hidden" name="tab" id="tab" value="2">
                                <input type='hidden' name="userid" id="userid" value="{{isset($profile)?$profile->id:''}}">

                                <div class="row" id='industryinfo'>
                                    <div class="col-lg-4">
                                        <div class="form-group ">
                                            <label for="login-password" class="font-small-4">Type of the Industry<span class="text-danger">*</span></label>
                                            <select class="form-control industry-details-form-controls" data-placeholder="select" name="industry" id="industry" >
                                                <option value="">Select Industry</option>
                                                @foreach($industries as $industry)
                                                    <option value="{{$industry->id}}" @if(isset($profile))@if($profile->mst_industry_id == $industry->id) selected @endif @endif>{{$industry->name}}</option>
                                                @endforeach
                                            </select>
                                            <span class="error" id="industry-error-back"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="login-password" class="font-small-4">Type of Entity<span class="text-danger">*</span></label>
                                            <select class="form-control industry-details-form-controls" data-placeholder="select" id="entity" name="entity" >
                                                <option value="">Select Entity</option>
                                                @foreach($entities as $entity)
                                                    <option value="{{$entity->id}}" @if(isset($profile))@if($profile->mst_entity_id == $entity->id) selected @endif @endif>{{$entity->name}}</option>
                                                @endforeach
                                            </select>
                                            <span class="error" id="entity-error-back"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group ">
                                            <label for="login-password" class="font-small-4">Type of Sector<span class="text-danger">*</span></label>
                                            <select class="form-control industry-details-form-controls" data-placeholder="select" id="sector" name="sector" >
                                                <option value="">Select Sector</option>
                                                @foreach($sectors as $sector)
                                                    <option value="{{$sector->id}}"@if(isset($profile))@if($profile->mst_sector_id == $sector->id) selected @endif @endif>{{$sector->name}}</option>
                                                @endforeach
                                            </select>
                                            <span class="error" id="sector-error-back"></span>
                                        </div>
                                    </div>
                                </div>

                                <button id='prev1' type="button" class="btn btn-outline-secondary">
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                </button>
                                @if($profile->profile_update == 1)
                                <span class="float-right">
                                    <div class="d-flex justify-content-end">
                                        <button type ="button" id="next2" name="next2"  class="btn btn-primary btn-next profile-info-btn">
                                            <span class="align-middle d-sm-inline-block d-none">Next</span>
                                            <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                        </button>
                                    </div>
                                </span>
                                @else
                                <span class="float-right">
                                    <button  id="step2" name="step2" type="submit" class="btn btn-primary btn-next profile-info-btn">
                                        <span class="align-middle d-sm-inline-block d-none">Save & Continue</span>
                                        <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                    </button>
                                </span>
                                @endif

                            </form>
                        </div>

                        <div id="infrastructure-step" class="content" role="tabpanel" aria-labelledby="infrastructure-step-trigger">
                            <form id="add_company_infrastructure_form" class="form-auth-div mt-2" action="{{route('company.updateprofile')}}" method="POST">
                                @csrf
                                <input type="hidden" name="tab" id="tab" value="3">
                                <input type='hidden' name="userid" id="userid" value="{{isset($profile)?$profile->id:''}}">

                                <div class="row" id='infradata'>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-label">Name of the Products manufactured / Services offered<span class="text-danger">*</span></label>
                                            <input class="form-control form-caontrol-merge border-right-line infrastructure-details-form-controls" type="text"  onkeypress="return /[a-zA-Z\s]/i.test(event.key)"  id="productsorservicesname" name="productsorservicesname" value="{{ old('productsorservicesname',$profile->product_name) }}" placeholder="Name of the Products manufactured / Services offered*"  />
                                            <span class="error" id="productsorservicesname-error-back"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-label">Latest turnover of the unit/ facility, in Rs Crores only<span class="text-danger">*</span></label>
                                            <input class="form-control form-caontrol-merge border-right-line infrastructure-details-form-controls txtQty" type="number" min="0" onkeypress="return /[0-9.]/i.test(event.key)"  id="latestturnover" name="latestturnover" value="{{ old('latestturnover', $profile->turnover) }}" placeholder="Enter latest turnover of the unit/ facility, in Rs Crores only*"  />
                                            <span class="error" id="latestturnover-error-back"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-label">Total no of permanent employees<span class="text-danger">*</span></label>
                                            <input class="form-control form-caontrol-merge border-right-line infrastructure-details-form-controls" type="number" min="0" onkeypress="return /[0-9]/i.test(event.key)" id="totalpermanentemployees" name="totalpermanentemployees" value="{{ old('totalpermanentemployees', $profile->no_of_employees) }}" placeholder="Total no of permanent employees*"  />
                                            <span class="error" id="totalpermanentemployees-error-back"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-label">Total no of contract employees<span class="text-danger">*</span></label>
                                            <input class="form-control form-caontrol-merge border-right-line infrastructure-details-form-controls" min="0" onkeypress="return /[0-9]/i.test(event.key)"  type="number" id="totalcontractemployees" name="totalcontractemployees" value="{{ old('totalcontractemployees', $profile->contract_employees) }}"  placeholder="Total no of contract employees*"  />
                                            <span class="error" id="totalcontractemployees-error-back"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-label">Yearly Electrical Energy Consumption in KWH / year<span class="text-danger">*</span></label>
                                            <input class="form-control form-caontrol-merge border-right-line infrastructure-details-form-controls txtQty" type="number"  onkeypress="return /[0-9.]/i.test(event.key)" min ="0" id="electricalenergyconsumption" name="electricalenergyconsumption" value="{{  old('electricalenergyconsumption', $profile->electrical_consumption) }}"  placeholder="Yearly Electrical Energy Consumption in KWH / year*"  />
                                            <span class="error" id="electricalenergyconsumption-error-back"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-label">Yearly Thermal Energy Consumption in Kcal / year<span class="text-danger">*</span></label>
                                            <input class="form-control form-caontrol-merge border-right-line infrastructure-details-form-controls txtQty" type="number"  onkeypress="return /[0-9.]/i.test(event.key)" min="0" id="thermalenergyconsumption" name="thermalenergyconsumption" value="{{ old('thermalenergyconsumption', $profile->thermal_consumption) }}"  placeholder="Yearly Thermal Energy Consumption in Kcal / year*"  />
                                            <span class="error" id="thermalenergyconsumption-error-back"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-label">Yearly water consumption in Kilo Litres/ Cubic meters<span class="text-danger">*</span></label>
                                            <input class="form-control form-caontrol-merge border-right-line infrastructure-details-form-controls txtQty" type="number"  onkeypress="return /[0-9.]/i.test(event.key)" min="0" id="waterconsumption" name="waterconsumption" value="{{  old('waterconsumption' , $profile->water_consumption ) }}"  placeholder="Yearly water consumption in Kilo Litres/ Cubic meters*"  />
                                            <span class="error" id="waterconsumption-error-back"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <label class="form-label">Total area of the unit / facility ( including Built up space, open area, colony or township etc ) in Acres only<span class="text-danger">*</span></label>
                                            <input class="form-control form-caontrol-merge border-right-line infrastructure-details-form-controls txtQty" id="totalarea" onkeypress="return /[0-9.]/i.test(event.key)"  name="totalarea" value="{{ old('totalarea', $profile->area) }}" type="number" placeholder="Total area of the unit / facility ( including Built up space, open area, colony or township etc ) in Acres only*"  />
                                            <span class="error" id="totalarea-error-back"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-label">Total Budget Allocation in Rs Crores only<span class="text-danger">*</span></label>
                                            <input class="form-control form-caontrol-merge border-right-line infrastructure-details-form-controls txtQty" type="number" min="0" onkeypress="return /[0-9.]/i.test(event.key)" id="budget_allocation" name="budget_allocation" value="{{ old('budget_allocation', $profile->budget_allocation) }}" placeholder="Total Budget Allocation in Rs Crores only*"  />
                                            <span class="error" id="budget_allocation-error-back"></span>
                                        </div>
                                    </div>

                                </div>
                                <button id='prev2' type="button" class="btn btn-outline-secondary btn-prev2">
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                </button>

                                 @if($profile->profile_update == 1)
                                <span class="float-right">
                                    <div class="d-flex justify-content-end">
                                        <button type ="button" id="next3" name="next3"  class="btn btn-primary btn-next profile-info-btn">
                                            <span class="align-middle d-sm-inline-block d-none">Next</span>
                                            <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                        </button>
                                    </div>
                                </span>
                                @else
                                <span class="float-right">
                                    <button  id="step3" name="step3" type="submit" class="btn btn-primary btn-next profile-info-btn">
                                        <span class="align-middle d-sm-inline-block d-none">Save & Continue</span>
                                        <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                    </button>
                                </span>
                                @endif

                            </form>
                        </div>

                        <div id="income-details" class="content" role="tabpanel" aria-labelledby="income-details-trigger">
                            <form id="add_company_income_form" class="form-auth-div mt-2" action="{{route('company.updateprofile')}}" method="POST">
                                @csrf
                                <input type="hidden" name="tab" id="tab" value="4">
                                <input type='hidden' name="userid" id="userid" value="{{isset($profile)?$profile->id:''}}">

                                <div class="row" id='incomeinfo'>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-label">TAN NO (Tax dedcution Account Number)<span class="text-danger">*</span></label>
                                            <input class="form-control form-caontrol-merge border-right-line income-details-form-controls" onkeypress="return /[a-zA-Z0-9]/i.test(event.key)"  maxlength="10"  oninput="this.value=this.value.slice(0,this.maxLength)" type="text" id="tanno" name="tanno" value="{{ old('tanno', $profile->tanno ) }}" placeholder="TAN NO (Tax dedcution Account Number)*"  />
                                            <span class="error" id="tanno-error-back"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-label">PAN NO (Permanent Account Number)<span class="text-danger">*</span></label>
                                            <input class="form-control form-caontrol-merge border-right-line income-details-form-controls" onkeypress="return /[a-zA-Z0-9]/i.test(event.key)"  maxlength="10"  oninput="this.value=this.value.slice(0,this.maxLength)" type="text" id="panno" name="panno" value="{{ old('panno', $profile->panno ) }}" placeholder="PAN NO (Permanent Account Number)*"  />
                                            <span class="error" id="panno-error-back"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-label font-small-3">GSTIN NO (Goods and Services Tax Identification Number)<span class="text-danger">*</span></label>
                                            <input class="form-control form-caontrol-merge border-right-line income-details-form-controls" onkeypress="return /[a-zA-Z0-9]/i.test(event.key)"  maxlength="15"  oninput="this.value=this.value.slice(0,this.maxLength)"  id="gstinno" name="gstinno" value="{{ old('gstinno', $profile->gstno ) }}" type="text" placeholder="GSTIN NO (Goods and Services Tax Identification Number)*"  />
                                            <span class="error" id="gstinno-error-back"></span>
                                        </div>
                                    </div>

                                </div>

                                <button id='prev3' type="button"  class="btn btn-outline-secondary btn-prev3">
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                </button>
                                @if($profile->profile_update != 1)
                                <div class="btn-group float-right">
                                    <span class="float-right mr-2">
                                        <div class="d-flex justify-content-end">
                                            <button id="step4" name="step4" type="submit" class="btn btn-primary btn-next profile-info-btn">
                                                <span class="align-middle d-sm-inline-block d-none">Save</span>
                                                <!-- <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i> -->
                                            </button>
                                        </div>
                                    </span>

                                    <span class="float-right">
                                        <button  id="finalsubmit" type="button" name="finalsubmit" class="btn btn-primary profile-info-btn">
                                            <span class="align-middle d-sm-inline-block d-none">Final Submit</span>
                                            <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                        </button>
                                    </span>
                                </div>
                                @endif
                            </form>
                        </div>

                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection

@push('PAGE_SCRIPTS')
<script>
  var stepper = new Stepper(document.querySelector('.bs-stepper'))

    $("#prev1").click(function(){
        stepper.to(1);
    });

     $("#next1").click(function(){
        stepper.to(2);
    });

    $("#prev2").click(function(){
        stepper.to(2);
    });

     $("#next2").click(function(){
        stepper.to(3);
    });

    $("#prev3").click(function(){
        stepper.to(3);
    });

     $("#next3").click(function(){
        stepper.to(4);
    });

  $("#company_name").autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ route('company.companieslist') }}",
                type: 'get',
                dataType: "json",
                data:{name:request.term},
                success: function( data ) {
                response( data );
                }
            });
        },

        response: function(event, ui) {
            if (ui.content.length === 0) {
                $('#manufacturer_id').val('-1');
            }
        },

        select: function(event, ui) {
            $('#manufacturer_id').val('-1');
            $(this).val(ui.item.value);
            $('#manufacturer_id').val(ui.item.id);
        },
        minLength: 2,
        autoFocus: true
    });

</script>
<script type="text/javascript">

    var Profile = function () {
        return { //main function to initiate the module
            init: function () {
            jQuery.validator.addMethod("noSpace", function(value, element) {
              return value == '' || value.trim().length != 0;
            }, "This field is required");

            jQuery.validator.addMethod("doubleSpace", function(value, element) {
               return this.optional(element) || /^\S((?!.*  ).*\S)?$/.test(value);
            }, "This field is required");

            jQuery.validator.addMethod("lettersonly", function(value, element)
            {
                return this.optional(element) || /^[A-Z," "]+$/i.test(value);
            }, "Letters and spaces only please");

            jQuery.validator.addMethod("mobileNo", function(value, element) {
               return this.optional(element) || /^[6-9][0-9]{9}$/.test(value);
            }, "Mobile Number is invalid");

            jQuery.validator.addMethod("decimalTwo", function(value, element) {
               return this.optional(element) || /^\d+(?:\.\d{1,2})?$/.test(value);
            }, "The format must be in two decimal");


            //Basic Info
            $('#add_company_form').validate({
                rules: {
                    'company_name': {
                        required: true,
                        noSpace: true,
                        doubleSpace:true,
                        maxlength: 50,
                        minlength:3
                    },
                    'email': {
                        required: true,
                        email: true,
                        // maxlength: 50,
                        // minlength:3
                    },
                    'mobileno':{
                        required: true,
                        digits:true,
                        minlength:10,
                        // maxlength:10,
                        mobileNo:true
                    },
                    'location':{
                        required: true,
                        doubleSpace:true,
                        noSpace: true,
                        minlength:3,
                        lettersonly:true,
                    },
                    'state':{
                        required: true,
                    },
                    'postaladdress':{
                        required: true,
                        noSpace: true,
                        doubleSpace:true,
                        maxlength: 150,
                        minlength:10
                    },
                    'postal_address_pincode':{
                        required: true,
                        digits:true,
                        minlength:6,
                        // maxlength:6
                    },
                    'billingaddress':{
                        required: true,
                        noSpace: true,
                        doubleSpace:true,
                        maxlength: 150,
                        minlength:10
                    },
                    'billing_address_pincode':{
                        required: true,
                        digits:true,
                        minlength:6,
                        // maxlength:6
                    }
                },
                messages:{
                    company_name: {
                        required: "The Company Name field is required",
                        noSpace: "Please Enter a Valid Company Name",
                        doubleSpace: "Please Enter a Valid Company Name"
                    },
                    state : {
                        required : "The State field is required"
                    },
                    location : {
                        required: "The City field is required",
                        noSpace: "Please Enter a Valid City",
                        doubleSpace: "Please Enter a Valid City",
                        lettersonly: "Please Enter a Valid City"
                    },
                    postaladdress : {
                        required: "The Postal Address field is required",
                        noSpace: "Please Enter a Valid Postal Address",
                        doubleSpace: "Please Enter a Valid Postal Address",
                        maxlength: "Please do not enter more than 150 characters"
                    },
                    billingaddress: {
                        required: "The Billing Address field is required",
                        noSpace: "Please Enter a Valid Billing Address",
                        doubleSpace: "Please Enter a Valid Billing Address",
                        maxlength: "Please do not enter more than 150 characters"
                    },
                    postal_address_pincode : {
                        required: "The Postal Address Pincode field is required",
                    },
                    billing_address_pincode: {
                        required: "The Billing Address Pincode is required",
                    }


                },
                submitHandler : function(form){

                    $("#step1").attr('disabled',true);
                    $(".back").css('pointer','none');
                    $("#step1").html("Loading....");
                    $.ajax({
                        type: "POST",
                        url: $('form#add_company_form').attr('action'),
                        data: $('form#add_company_form').serialize(),
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        // contentType: false,
                        // processData: false,
                        success: function(response) {
                            if(response.status == 'validations') {
                                $.each(response.errors, function (key, error) {
                                    $("#step1").attr('disabled',false);
                                    $("#step1").html("Save & Continue" + " " + "<i class='fal fa-arrow-right rightArrow' class='align-middle ms-sm-25 ms-0'></i>");
                                    $('form#add_company_form').find("#" + key).removeClass('error').addClass('error');
                                    $('form#add_company_form').find("#" + key + "-error-back").css('display','block');
                                    $('form#add_company_form').find("#" + key + "-error-back").text(error[0]);
                                });
                            }

                            if(response.status == 'success') {

                                stepper.to(2);
                                $("#step1").attr('disabled',false);
                                $("#step1").html("Save & Continue" + " " + "<i class='fal fa-arrow-right rightArrow' class='align-middle ms-sm-25 ms-0'></i>");

                                // JsUtility.showToastr('success', 'Company', response.message);

                            }

                            if(response.status == 'error') {
                                $("#step1").attr('disabled',false);
                                $("#step1").html("Save & Continue" + " " + "<i class='fal fa-arrow-right rightArrow' class='align-middle ms-sm-25 ms-0'></i>");
                                JsUtility.showToastr('error', 'Company', response.message);
                            }
                            if(response.status =='errorlogout')
                            {
                                setTimeout(function(){
                                    window.location.href = "{{route('company.login')}}";
                                    }, 3000);
                                JsUtility.showToastr('error', 'Company', response.message);
                            }
                        },
                        error: function() {
                                $("#step1").attr('disabled',false);
                                $("#step1").html("Save & Continue" + " " + "<i class='fal fa-arrow-right rightArrow' class='align-middle ms-sm-25 ms-0'></i>");
                            JsUtility.showToastr('error', 'Company', response.message);
                        }
                    });

                }

            });

            //Industry Details
            $('#add_company_industry_form').validate({
                rules: {
                    'industry': {
                        required: true,
                    },
                    'entity': {
                        required: true,
                    },
                    'sector':{
                        required: true,
                    },
                },
                messages: {
                    industry:{
                        required: "The Industry field is required",
                    },
                    entity: {
                        required: "The Entity field is required",
                    },
                    sector: {
                        required: "The Sector field is required",
                    }
                },
                submitHandler : function(form){

                    $("#step2").attr('disabled',true);
                    $(".back").css('pointer','none');
                    $("#step2").html("Loading....");
                    $("#prev1").attr('disabled',true);

                    $.ajax({
                        type: "POST",
                        url: $('form#add_company_industry_form').attr('action'),
                        data: $('form#add_company_industry_form').serialize(),
                         headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        // contentType: false,
                        // processData: false,
                        success: function(response) {
                            if(response.status == 'validations') {
                                $.each(response.errors, function (key, error) {
                                    $("#step2").attr('disabled',false);
                                    $("#step2").html("Save & Continue" + " " + "<i class='fal fa-arrow-right rightArrow' class='align-middle ms-sm-25 ms-0'></i>");
                                    $("#prev1").attr('disabled',false);
                                    $('form#add_company_industry_form').find("#" + key).removeClass('error').addClass('error');
                                    $('form#add_company_industry_form').find("#" + key + "-error-back").css('display','block');
                                    $('form#add_company_industry_form').find("#" + key + "-error-back").text(error[0]);
                                });
                            }

                            if(response.status == 'success') {

                                stepper.to(3);
                                $("#step2").attr('disabled',false);
                                $("#step2").html("Save & Continue" + " " + "<i class='fal fa-arrow-right rightArrow' class='align-middle ms-sm-25 ms-0'></i>");
                                $("#prev1").attr('disabled',false);

                                // JsUtility.showToastr('success', 'Company', response.message);

                            }

                            if(response.status == 'error') {
                                $("#step2").attr('disabled',false);
                                $("#step2").html("Save & Continue" + " " + "<i class='fal fa-arrow-right rightArrow' class='align-middle ms-sm-25 ms-0'></i>");
                                $("#prev1").attr('disabled',false);
                                JsUtility.showToastr('error', 'Company', response.message);
                            }
                            if(response.status =='errorlogout')
                            {
                                setTimeout(function(){
                                    window.location.href = "{{route('company.login')}}";
                                    }, 3000);
                                JsUtility.showToastr('error', 'Company', response.message);
                            }
                        },
                        error: function() {
                                $("#step2").attr('disabled',false);
                                $("#step2").html("Save & Continue" + " " + "<i class='fal fa-arrow-right rightArrow' class='align-middle ms-sm-25 ms-0'></i>");
                                $("#prev1").attr('disabled',false);
                            JsUtility.showToastr('error', 'Company', response.message);
                        }
                    });

                }

            });

            //Infrastructure Details
            $('#add_company_infrastructure_form').validate({
                rules: {
                    'productsorservicesname': {
                        required: true,
                        doubleSpace: true,
                        noSpace: true,
                        minlength:3
                    },
                    'latestturnover': {
                        required: true,
                        decimalTwo: true,
                    },
                    'totalpermanentemployees': {
                        required: true,
                    },
                     'totalcontractemployees': {
                        required: true,
                    },
                    'electricalenergyconsumption': {
                        required: true,
                        decimalTwo: true,
                    },
                    'thermalenergyconsumption': {
                        required: true,
                        decimalTwo: true,
                    },
                     'waterconsumption': {
                        required: true,
                        decimalTwo: true,
                    },
                    'totalarea': {
                        required: true,
                        decimalTwo: true,
                    },
                    'budget_allocation':{
                        required: true,
                        decimalTwo: true,
                    },
                },

                messages: {
                    productsorservicesname: {
                        required: "The Name of the Products or Services field is required",
                        noSpace: "Please Enter a Valid Name of the Products or Services",
                        doubleSpace: "Please Enter a Valid Name of the Products or Services",
                    },
                    latestturnover: {
                        required: "The Latest Turnover field is required",
                    },
                    totalpermanentemployees: {
                        required: "The Total no of permanent employees field is required",
                    },
                    totalcontractemployees: {
                        required: "The Total no of contract employees field is required",
                    },
                    electricalenergyconsumption: {
                        required: "The Yearly Electrical Energy Consumption field is required",
                    },
                    thermalenergyconsumption: {
                        required: "The Yearly Thermal Energy Consumption field is required",
                    },
                    waterconsumption: {
                        required: "The Yearly Water Consumption field is required",
                    },
                    totalarea: {
                        required: "The Total Area field is required",
                    },
                    budget_allocation: {
                        required: "The Total Budget Allocation field is required",
                    },
                },

                submitHandler : function(form){

                    $("#step3").attr('disabled',true);
                    $(".back").css('pointer','none');
                    $("#step3").html("Loading....");
                    $("#prev2").attr('disabled',true);
                    $.ajax({
                        type: "POST",
                        url: $('form#add_company_infrastructure_form').attr('action'),
                        data: $('form#add_company_infrastructure_form').serialize(),
                         headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        // contentType: false,
                        // processData: false,
                        success: function(response) {
                            if(response.status == 'validations') {
                                $.each(response.errors, function (key, error) {
                                    $("#step3").attr('disabled',false);
                                    $("#step3").html("Save & Continue" + " " + "<i class='fal fa-arrow-right rightArrow' class='align-middle ms-sm-25 ms-0'></i>");
                                    $("#prev2").attr('disabled',false);
                                    $('form#add_company_infrastructure_form').find("#" + key).removeClass('error').addClass('error');
                                    $('form#add_company_infrastructure_form').find("#" + key + "-error-back").css('display','block');
                                    $('form#add_company_infrastructure_form').find("#" + key + "-error-back").text(error[0]);
                                });
                            }

                            if(response.status == 'success') {

                                stepper.to(4);
                                $("#step3").attr('disabled',false);
                                $("#step3").html("Save & Continue" + " " + "<i class='fal fa-arrow-right rightArrow' class='align-middle ms-sm-25 ms-0'></i>");
                                $("#prev2").attr('disabled',false);

                                // JsUtility.showToastr('success', 'Company', response.message);

                            }

                            if(response.status == 'error') {
                                $("#step3").attr('disabled',false);
                                $("#step3").html("Save & Continue" + " " + "<i class='fal fa-arrow-right rightArrow' class='align-middle ms-sm-25 ms-0'></i>");
                                $("#prev2").attr('disabled',false);
                                JsUtility.showToastr('error', 'Company', response.message);
                            }
                            if(response.status =='errorlogout')
                            {
                                setTimeout(function(){
                                    window.location.href = "{{route('company.login')}}";
                                    }, 3000);
                                JsUtility.showToastr('error', 'Company', response.message);
                            }
                        },
                        error: function() {
                                $("#step3").attr('disabled',false);
                                $("#step3").html("Save & Continue" + " " + "<i class='fal fa-arrow-right rightArrow' class='align-middle ms-sm-25 ms-0'></i>");
                                $("#prev2").attr('disabled',false);
                            JsUtility.showToastr('error', 'Company', response.message);
                        }
                    });

                }

            });

            //Income Details
             $('#add_company_income_form').validate({
                rules: {
                    'tanno': {
                        required: true,
                        noSpace: true,
                        minlength:10,
                    },
                    'panno': {
                        required: true,
                        noSpace: true,
                        minlength:10
                    },
                    'gstinno':{
                        required: true,
                        noSpace: true,
                        minlength:15
                    }
                },
                messages:{
                    tanno: {
                        required:"The Tax Deduction Account Number field is required",
                        noSpace:"Please enter a valid Tax Deduction Account Number"
                    },
                    panno: {
                        required:"The Permanent Account Number field is required",
                        noSpace:"Please enter a valid Permanent Account Number"
                    },
                    gstinno: {
                        required:"The Goods and Services Tax Identification Number field is required",
                        noSpace:"Please enter a valid Goods and Services Tax Identification Number"
                    }
                },

                submitHandler : function(form){

                    $("#step4").attr('disabled',true);
                    $("#finalsubmit").attr('disabled',true);
                    $(".back").css('pointer','none');
                    $("#step4").html("Loading....");
                    $("#prev3").attr('disabled',true);
                    $.ajax({
                        type: "POST",
                        url: $('form#add_company_income_form').attr('action'),
                        data: $('form#add_company_income_form').serialize(),
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        // contentType: false,
                        // processData: false,
                        success: function(response) {
                            if(response.status == 'validations') {
                                $.each(response.errors, function (key, error) {
                                    $("#step4").attr('disabled',false);
                                    $("#finalsubmit").attr('disabled',false);
                                    $("#step4").html("Save");
                                    $("#prev3").attr('disabled',false);
                                    $('form#add_company_income_form').find("#" + key).removeClass('error').addClass('error');
                                    $('form#add_company_income_form').find("#" + key + "-error-back").css('display','block');
                                    $('form#add_company_income_form').find("#" + key + "-error-back").text(error[0]);
                                });
                            }

                            if(response.status == 'success') {

                                JsUtility.showToastr('success', 'Company', response.message);
                                // setTimeout(function(){
                                //         location.reload();
                                //     }, 3000);
                                $("#step4").attr('disabled',false);
                                $("#finalsubmit").attr('disabled',false);
                                $("#step4").html("Save");
                                $("#prev3").attr('disabled',false);
                                stepper.to(4);

                            }

                            if(response.status == 'error') {
                                $("#step4").attr('disabled',false);
                                $("#finalsubmit").attr('disabled',false);
                                $("#step4").html("Save");
                                $("#prev3").attr('disabled',false);
                                JsUtility.showToastr('error', 'Company', response.message);
                            }
                            if(response.status =='errorlogout')
                            {
                                setTimeout(function(){
                                    window.location.href = "{{route('company.login')}}";
                                    }, 3000);
                                JsUtility.showToastr('error', 'Company', response.message);
                            }
                        },
                        error: function() {

                                $("#step4").attr('disabled',false);
                                $("#finalsubmit").attr('disabled',false);
                                $("#step4").html("Save");
                                $("#prev3").attr('disabled',false);

                                JsUtility.showToastr('error', 'Company', response.message);
                        }
                    });

                }

            });


            $("#finalsubmit").click(function(){
                Swal.fire({
                    title: 'Are you sure, Once Submitted Data Cannot be reverted again ?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Submit',
                    showClass: {
                        popup: 'animate__animated animate__fadeIn'
                    },
                    customClass: {
                        confirmButton: 'btn btn-primary',
                        cancelButton: 'btn btn-outline-danger ml-1'
                    },
                    buttonsStyling: false
                    }).then(function(result) {
                    if (result.value) {

                        $("#step4").attr('disabled',true);
                        $("#finalsubmit").attr('disabled',true);
                        $(".back").css('pointer','none');
                        $("#finalsubmit").html("Loading....");
                        $("#prev3").attr('disabled',true);
                        $.ajax({
                            type: "POST",
                            url: "{{ route('company.submitprofile') }}",
                            data: $('form#add_company_income_form').serialize(),
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            // contentType: false,
                            // processData: false,
                            success: function(response) {
                                if(response.status == 'validations') {
                                    $.each(response.errors, function (key, error) {
                                        $("#step4").attr('disabled',false);
                                        $("#finalsubmit").attr('disabled',false);
                                        $("#finalsubmit").html("Final Submit");
                                        $("#prev3").attr('disabled',false);
                                        $('form#add_company_income_form').find("#" + key).removeClass('error').addClass('error');
                                        $('form#add_company_income_form').find("#" + key + "-error-back").css('display','block');
                                        $('form#add_company_income_form').find("#" + key + "-error-back").text(error[0]);
                                    });
                                }

                                if(response.status == 'success') {

                                    JsUtility.showToastr('success', 'Company', response.message);
                                    setTimeout(function(){
                                            location.reload();
                                        }, 3000);

                                    // JsUtility.showToastr('success', 'Company', response.message);

                                }

                                if(response.status == 'error') {
                                    $("#step4").attr('disabled',false);
                                    $("#finalsubmit").attr('disabled',false);
                                    $("#finalsubmit").html("Final Submit");
                                    $("#prev3").attr('disabled',false);
                                    JsUtility.showToastr('error', 'Company', response.message);
                                }
                                if(response.status =='errorlogout')
                                {
                                    setTimeout(function(){
                                        window.location.href = "{{route('company.login')}}";
                                        }, 3000);
                                    JsUtility.showToastr('error', 'Company', response.message);
                                }
                            },
                            error: function() {
                                $("#step4").attr('disabled',false);
                                $("#finalsubmit").attr('disabled',false);
                                $("#finalsubmit").html("Final Submit");
                                $("#prev3").attr('disabled',false);
                                JsUtility.showToastr('error', 'Company', response.message);
                            }
                        });

                    }
                });
            });

            }
        }
    }();

    jQuery(document).ready(function() {
        Profile.init();

        $('.txtQty').on('blur',function(event) {
            var amt = parseFloat(this.value);
            $(this).val(amt.toFixed(2));
        });


    //Basic Info
    $('.basic-info-form-controls').blur(function(){
        $("#add_company_form").validate().element(this);
    });

    $('.basic-info-form-controls').on('keyup',function(event){
        $("#add_company_form").validate().element(this);
    });

    //Industry Details
    $('.industry-details-form-controls').blur(function(){
        $("#add_company_industry_form").validate().element(this);
    });

    $('.industry-details-form-controls').on('keyup',function(event){
        $("#add_company_industry_form").validate().element(this);
    });

    //Income Details
    $('.income-details-form-controls').blur(function(){
        $("#add_company_income_form").validate().element(this);
    });

    $('.income-details-form-controls').on('keyup',function(event){
        $("#add_company_income_form").validate().element(this);
    });

    //Infrastructure Details
    $('.infrastructure-details-form-controls').blur(function(){
        $("#add_company_infrastructure_form").validate().element(this);
    });

    $('.infrastructure-details-form-controls').on('keyup',function(event){
        $("#add_company_infrastructure_form").validate().element(this);
    });

    // capitalize name input text fields
    $('input[type=text], textarea').on('keyup',function(event){
        event.currentTarget.value = this.value.toUpperCase();
    });

        if($('#profile_updated_status').val() =='1')
        {

            $("#industryinfo *").prop('disabled',true);
            $("#infradata *").prop('disabled',true);
            $("#industryinfo :input").addClass('remarks_data');
            $("#infradata :input").addClass('remarks_data');

            $("#basicinfo *").prop('disabled',true);
            $("#incomeinfo *").prop('disabled',true);
            $("#basicinfo :input").addClass('remarks_data');
            $("#incomeinfo :input").addClass('remarks_data');

        }
    });
</script>
@endpush
