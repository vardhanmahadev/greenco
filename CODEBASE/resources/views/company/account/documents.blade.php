@extends(COMPANY_THEME_NAME.'.layouts.app')

@section('content')
<div class="app-content content content-ps-stl">
    <div class="">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <section class="profile-style2">
                   
                    <div class="row">
                        <div class="col-lg-12">
                            <form id="add_company_documents_form" class="auth-reset-password-form form-auth-div" action="{{route('company.uploaddocuments')}}" method="POST">
                                @csrf 
                                <div class="container">
                                    <div class="row pb-1">  
                                        <div class="col-lg-6">
                                            <div class="card ">
                                                <div class="d-flex v-row title-strip mb-2">
                                                    <div class="col-6 pl-0">Proposal Document</div>
                                                </div>
                                                <div class="card-body ">
                                                    <div class="row  pb-1 mt--1">
                                                        <div class="col-lg-12">
                                                            <div class="row v-row mb-2">
                                                                @if(isset($profile->proposal_document))
                                                                <div class="col-lg-2 col-md-2">
                                                                    <label>Uploaded File :</label>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4">
                                                                    <div class="v-row uploadedfile">
                                                                        <span class="">{{url($profile->proposal_document)}}</span> <button class="btn btn-outline-secondary waves-effect ml-1"> <i data-feather="download"></i></button>
                                                                    </div>
                                                                </div>
                                                                @endif
                                                                <div class="col-lg-2 col-md-2">
                                                                    <label>Upload File :</label>
                                                                </div>
                                                                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12  mt-50">
                                                                    <div class="custom-file">
                                                                        <input type="file" class="custom-file-input" name="proposal_doc" id="customFile">
                                                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                                                    </div>
                                                                    <span class="error" id="proposal_doc-error"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 justify-content-center  d-flex">
                                                            <button type="submit" class="btn btn-primary btn-next" id="submitButton" name="doSubmit" value="Submit" tabindex="4">Submit </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="card ">
                                                <div class="d-flex v-row title-strip mb-2">
                                                    <div class="col-6 pl-0">PO Document</div>
                                                </div>
                                                <div class="card-body ">
                                                    <div class="row  pb-1 mt--1">
                                                        <div class="col-lg-12">
                                                            <div class="row v-row mb-2">
                                                                @if(isset($profile->po_doc))
                                                                <div class="col-lg-2 col-md-2">
                                                                    <label>Uploaded File :</label>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4">
                                                                    <div class="v-row  uploadedfile">
                                                                        <span class=""></span> <button class="btn btn-outline-secondary waves-effect ml-1"> <i data-feather="download"></i></button>
                                                                    </div>
                                                                </div> 
                                                                @endif
                                                                <div class="col-lg-2 col-md-2">
                                                                    <label>Upload File :</label>
                                                                </div>
                                                                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12  mt-50">
                                                                    <div class="custom-file">
                                                                        <input type="file" class="custom-file-input" name="po_doc" id="customFile">
                                                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                                                    </div>
                                                                    <span class="error" id="po_doc-error"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 justify-content-center  d-flex">
                                                            <button type="submit" class="btn btn-primary btn-next" id="submitButton" name="doSubmit" value="Submit" tabindex="4">Submit </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>



            </div>
        </div>
    </div>
</div>
@endsection
@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Profile = function () {
    return { //main function to initiate the module
        init: function () {
            
            $('#add_company_documents_form').submit(function(e){
                e.preventDefault();
                var formData = new FormData($(this)[0]);

                $("#submitButton").attr('disabled',true);
                $(".back").css('pointer','none');
                $("#submitButton").html("Loading....");

                $.ajax({
                    type: "POST",
                    url: $('form#add_company_documents_form').attr('action'),
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        if(response.status == 'validations') {
                            $.each(response.errors, function (key, error) {
                                $("#submitButton").attr('disabled',false);
                                $("#submitButton").html("Submit");
                                $('form#add_company_documents_form').find("#" + key).removeClass('error').addClass('error');
                                $('form#add_company_documents_form').find("#" + key + "-error").text(error[0]);
                            });
                        }

                        if(response.status == 'success') {
                            JsUtility.showToastr('success', 'Company', response.message);
                            
                        }

                        if(response.status == 'error') {
                            $("#submitButton").attr('disabled',false);
                            $("#submitButton").html("Submit");
                            JsUtility.showToastr('error', 'Company', response.message);
                        }
                    },
                    error: function() {
                            $("#submitButton").attr('disabled',false);
                            $("#submitButton").html("Submit");
                        JsUtility.showToastr('error', 'Company', response.message);
                    }
                }); 
                
                    

                return false;
            });
        }
    }
}();

jQuery(document).ready(function() {
    Profile.init();
});
</script>
@endpush