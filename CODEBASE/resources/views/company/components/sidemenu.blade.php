<div class="deznav">
    <div class="deznav-scroll">
        <ul class="metismenu" id="menu">
            <li class="{{($pageTitle=='Dashboard')?'active':''}}"><a class="ai-icon"  href="{{route('company.dashboard')}}" aria-expanded="false">
                    <i data-feather="airplay"></i><span class="menu-title " data-i18n="Dashboards"> Dashboard </span></a>
            </li>

            <li class="{{($pageTitle=='Profile')?'active':''}}"><a class="ai-icon" href="{{route('company.profile')}}" aria-expanded="false">
                <i class="fal fa-id-card pr-0"></i><span class="menu-title " data-i18n="Invoice">  Profile </span></a>
            </li>
            @if($authdata->verified_status == 1)
            <li class="{{($pageTitle=='Documents Page')?'active':''}}" ><a class="ai-icon" href="{{route('company.documents')}}" aria-expanded="false">
                <i class="fal fa-file-upload pr-0"></i><span class="menu-title " data-i18n="Invoice">  Documents Upload </span></a>
            </li>
            @endif
        </ul>
    </div>
</div>
