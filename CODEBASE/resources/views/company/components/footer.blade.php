<!-- BEGIN: Footer-->
<button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>
<footer class="footer footer-static footer-light">
    <p class="clearfix mb-0 font-small-4"><span class="float-md-left d-block d-md-block mt-25">COPYRIGHT &copy; {{date('Y')}}<a class="ml-25" href="https://mirakitech.com" target="_blank">Miraki Technologies</a><span class="d-none d-sm-inline-block">, All rights Reserved</span></span></p>
</footer>
