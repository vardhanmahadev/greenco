<!DOCTYPE html>
<html class="loaded" lang="en" data-textdirection="ltr">
    <!-- BEGIN: Head-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
        <meta name="description" content="GreenCO Rating System">
        <meta name="keywords" content="GreenCO Rating System">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="author" content="{{APP_NAME}}">
        <title>{{APP_NAME}} | {{ $pageTitle ?? ''}}</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{ url('/app-assets/images/logo/greenco.png') }}">
        <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet">
        <!---Font Awesome --->
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/fonts/font-awesome/css/font-awesome.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/vendors/css/extensions/toastr.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/vendors/css/vendors.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/vendors/css/forms/select/select2.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/vendors/css/tables/datatable/rowGroup.bootstrap4.min.css') }}">
        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/bootstrap-extended.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/colors.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/components.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/themes/dark-layout.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/themes/bordered-layout.min.css') }}">

        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/vendors/css/forms/select/select2.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/vendors/css/jquery-ui.css') }}">

        <!-- END: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/owl.theme.default.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/owl.carousel.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/plugins/extensions/ext-component-toastr.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">

        @if(isset($datatable_listing) && $datatable_listing===true)
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/vendors/css/tables/datatable/rowGroup.bootstrap4.min.css') }}">
        @endif

        @stack('PAGE_ASSETS_CSS')

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/plugins/forms/form-validation.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/pages/page-auth.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/pages/page-profile.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('/app-assets/css/plugins/forms/form-validation.min.css') }}">
        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="{{ url('/assets/css/style.css') }}">
        <style>
            .checkPointer{
                cursor: pointer !important;
            }
            .select2-container {
                width: 100% !important;
            }
            .highlight {
                position: inherit !important;
                color:red !important;
            }
            .borderRightError{
                border-color: #EA5455!important;
            }
            table.dataTable thead .sorting_asc:before{
                display:none;
            }
            table.dataTable thead .sorting_asc:after{
                display:none;
            }
        </style>
    </head>
    <!-- END: Head-->
    <!-- BEGIN: Body-->
    <body data-theme-version="light" data-layout="vertical" data-nav-headerbg="color_1" data-headerbg="color_1" data-sidebar-style="mini" data-sibebarbg="color_1" data-sidebar-position="fixed" data-header-position="fixed" data-container="wide" direction="ltr" data-primary="color_1">
     <div id="main-wrapper">

            @include(COMPANY_THEME_NAME.'.components.header')

            @include(COMPANY_THEME_NAME.'.components.sidemenu')

            @yield('content')

            <div class="sidenav-overlay"></div>

            @include(COMPANY_THEME_NAME.'.components.footer')

        </div>


        <script src="{{ url('/app-assets/vendors/js/vendors.min.js') }}"></script>
        <script src="{{ url('/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
        <script src="{{ url('/app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
        <script src="{{ url('/app-assets/vendors/js/forms/validation/jquery.validate.min.js') }}"></script>
        <script src="{{ url('/app-assets/js/core/app-menu.min.js') }}"></script>
        <script src="{{ url('/app-assets/js/core/app.js') }}"></script>
        <script src="{{ url('/assets/js/jsutility.js') }}"></script>
        <script src="{{ url('/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
        <script src="{{ url('/assets/js/scripts.js') }}"></script>
        <script src="{{ url('/app-assets/js/scripts/owl.carousel.min.js') }}"></script>

        <script src="{{ url('app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
        <script src="{{ url('app-assets/js/scripts/forms/form-select2.min.js') }}"></script>
        <script src="{{ url('app-assets/vendors/js/jquery-ui.js') }}"></script>
        <script src="{{ url('/app-assets/js/scripts/blockui/jquery.blockUI.js') }}"></script>

        <!-- BEGIN:Vendor JS-->
        <script src="{{ url('/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
        @if(isset($datatable_listing) && $datatable_listing===true)
        <script src="{{ url('/app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
        <script src="{{ url('/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
        <script src="{{ url('/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
        <script src="{{ url('/app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js') }}"></script>
        <script src="{{ url('/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
        <script src="{{ url('/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
        <script src="{{ url('/app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
        <script src="{{ url('/app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
        <script src="{{ url('/app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>

        <script src="{{ url('/app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
        <script src="{{ url('/app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
        <script src="{{ url('/app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>
        <script src="{{ url('/app-assets/vendors/js/tables/datatable/dataTables.fixedHeader.min.js') }}"></script>
        @endif

        <script src="{{ url('app-assets/vendors/js/forms/repeater/jquery.repeater.min.js') }}"></script>
        <script src="{{ url('app-assets/js/scripts/forms/form-repeater.js') }}"></script>
        <script src="{{ url('app-assets/vendors/js/forms/wizard/bs-stepper.min.js') }}"></script>
        <!-- BEGIN: Page JS-->
        <script src="{{ url('app-assets/js/scripts/forms/form-wizard.min.js') }}"></script>
        <!-- BEGIN: Page Vendor JS-->
        <script src="{{ url('/app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
        <script src="{{ url('/app-assets/vendors/js/extensions/polyfill.min.js') }}"></script>
        <script src="{{ url('/app-assets/js/scripts/extensions/ext-component-sweet-alerts.js') }}"></script>
        <script src="{{ url('/app-assets/js/scripts/customizer.min.js')}}"></script>

        <!-- END: Page Vendor JS-->

        @stack('PAGE_ASSETS_JS')

        <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
        </script>
        <script>
            $(function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                @if(Session::has('toast') && Session::has('status'))
                    JsUtility.showToastr("{{session('status')}}", "{{session('title')}}", "{{session('message')}}");
                @endif
            });

        </script>

        @stack('PAGE_SCRIPTS')

    </body>
    <!-- END: Body-->

</html>
