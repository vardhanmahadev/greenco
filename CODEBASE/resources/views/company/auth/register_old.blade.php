@extends(ASSESSOR_THEME_NAME.'.layouts.auth')

@section('content')

<div class="app-content content m-a0 ml-0 p-a0">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <div class="auth-wrapper auth-v2">
                <div class="auth-inner row m-0">
                    <!-- Left Text-->
                    <div class="col-lg-4 p-a0 width-img w-100">
                        <div class="position-sticky">
                                    <img class="img-fluid" src="{{ url('app-assets/images/login.jpg') }}" sclass="" alt="Login V2" />
                        </div>
                    </div>
                    <!-- /Left Text-->
                    <!-- Register-->
                    <div class="d-flex col-lg-8 align-items-center auth-bg px-2 ">
                        <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                            <div class="row v-row justify-content-center offset-lg-2">
                                <div class="col-lg-2">
                                    <a class="brand-logo" href="javascript:void(0);">
                                        <h2 class="brand-text text-primary ">
                                            <img class="img-fluid" src="{{ url('app-assets/images/logo/greenco.png') }}"  width="150" alt="Login V2">
                                        </h2>
                                    </a>
                                </div>
                                <div class="col-lg-10">
                                    <h3 class="card-title font-weight-bold mb-0"> Company Registration </h3>
                                    <!-- <p class="card-text">It's great to see you back!</p> -->
                                </div>
                            </div>
                            <form id="add_company_form" class="auth-register-form auth-form-div mt-2" method="POST" action="{{route('company.store')}}">
                                    @csrf
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-label-group">
                                            <input class="form-control form-caontrol-merge border-right-line" id="companyname" autocomplete="companyname" type="text" name="companyname" placeholder="Name of the Company" autocomplete="false" value="{{old('companyname')}}" readonly onfocus="this.removeAttribute('readonly');" />
                                            <label class="form-label float-label">Name of the Company</label>
                                            <span class="error" id="companyname-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-label-group">
                                            <input class="form-control form-caontrol-merge border-right-line " id="email" autocomplete="email" type="email" name="email" placeholder="Email Id" autocomplete="false" value="{{old('email')}}" readonly onfocus="this.removeAttribute('readonly');" />
                                            <label class="form-label float-label ">Email</label>
                                            <span class="error" id="email-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-label-group">
                                            <input class="form-control form-caontrol-merge border-right-line" id="mobileno" maxlength="10" min="0" step="1"   onkeypress="return event.charCode >= 48 && event.charCode <= 57"   oninput="this.value=this.value.slice(0,this.maxLength)" 
                                            autocomplete="mobileno" type="number" name="mobileno" placeholder="Mobile No" autocomplete="false" value="{{old('mobileno')}}" readonly onfocus="this.removeAttribute('readonly');" />
                                            <label class="form-label float-label">Mobile No</label>
                                            <span class="error" id="mobileno-error"></span>         
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-label-group">
                                            <input class="form-control border-right-line" id="location" type="text" name="location" placeholder="Location" autocomplete="false" value="{{old('location')}}" readonly onfocus="this.removeAttribute('readonly');" />
                                            <label class="form-label float-label" for="location">Location</label>
                                            <span class="error" id="location-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-label-group">
                                            <textarea placeholder="Postal Address with Pincode" class="form-control border-right-line" height="135" rows="4" name='postaladdress' id='postaladdress' >{{old('postaladdress')}}</textarea>
                                            <label class="form-label float-label" for="email">Postal Address with Pincode</label>
                                            <span class="error" id="postaladdress-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-label-group">
                                            <textarea placeholder="Billing Address with Pincode " class="form-control border-right-line" height="135" rows="4" name='billingaddress' id='billingaddress' >{{old('billingaddress')}}</textarea>
                                            <label class="form-label float-label" for="email">Billing Address with Pincode</label>
                                            <span class="error" id="billingaddress-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group mt--1">
                                            <label for="login-password">Type of the Industry</label>
                                            <select class="select2 form-control" data-placeholder="select" name='industrytype' id='industrytype'>
                                                <option value="">Select</option>
                                                @foreach($industries as $industry)
                                                <option value="{{$industry->id}}">{{$industry->name}}</option>
                                                @endforeach
                                            </select>
                                            <span class="error" id="industrytype-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group mt--1">
                                            <label for="login-password">Which type of Entity you are ?</label>
                                            <select class="select2 form-control" data-placeholder="select" name='entitytype' id='entitytype'>
                                                <option value="">Select</option>
                                                @foreach($entities as $entity)
                                                    <option value="{{$entity->id}}">{{$entity->name}}</option>
                                                @endforeach  
                                            </select>
                                            <span class="error" id="entitytype-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group ">
                                            <label for="login-password">Type of Sector</label>
                                            <select class="select2 form-control" data-placeholder="select" name='sector' id='sector'>
                                                <option value="">Select</option>
                                                     @foreach($sectors as $sector)
                                                        <option value="{{$sector->id}}">{{$sector->name}}</option>
                                                    @endforeach
                                            </select>
                                            <span class="error" id="sector-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-label-group mt-2">
                                            <input class="form-control form-caontrol-merge border-right-line" type="text" id="productsorservicesname" name="productsorservicesname" value="{{old('productsorservicesname')}}" placeholder="Name of the Products manufactured / Services offered" readonly onfocus="this.removeAttribute('readonly');" />
                                            <label class="form-label float-label">Name of the Products manufactured / Services offered</label>
                                            <span class="error" id="productsorservicesname-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-label-group">
                                            <input class="form-control form-caontrol-merge border-right-line" type="number" min="0"  id="latestturnover" name="latestturnover" value="{{old('latestturnover')}}" placeholder="Enter latest turnover of the unit/ facility, in Rs Crores only" readonly onfocus="this.removeAttribute('readonly');" />
                                            <label class="form-label float-label">Enter latest turnover of the unit/ facility, in Rs Crores only</label>
                                            <span class="error" id="latestturnover-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-label-group">
                                            <input class="form-control form-caontrol-merge border-right-line" type="number" min="0"  id="totalpermanentemployees" name="totalpermanentemployees" value="{{old('totalpermanentemployees')}}" placeholder="Total no of employees permanent employees" readonly onfocus="this.removeAttribute('readonly');" />
                                            <label class="form-label float-label">Total no of employees permanent employees</label>
                                            <span class="error" id="totalpermanentemployees-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-label-group">
                                            <input class="form-control form-caontrol-merge border-right-line" min="0" type="number" id="totalcontractemployees" name="totalcontractemployees" value="{{old('totalcontractemployees')}}"  placeholder="Total no of contract employees" readonly onfocus="this.removeAttribute('readonly');" />
                                            <label class="form-label float-label">Total no of contract employees</label>
                                            <span class="error" id="totalcontractemployees-error"></span>
                                        </div>
                                    </div>
                                   
                                    <div class="col-lg-6">
                                        <div class="form-label-group">
                                            <input class="form-control form-caontrol-merge border-right-line" type="number" min ="0" id="electricalenergyconsumption" name="electricalenergyconsumption" value="{{old('electricalenergyconsumption')}}"  placeholder="Yearly Electrical Energy Consumption in KWH / year" readonly onfocus="this.removeAttribute('readonly');" />
                                            <label class="form-label float-label">Yearly Electrical Energy Consumption in KWH / year</label>
                                            <span class="error" id="electricalenergyconsumption-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-label-group">
                                            <input class="form-control form-caontrol-merge border-right-line" type="number" min="0" id="thermalenergyconsumption" name="thermalenergyconsumption" value="{{old('thermalenergyconsumption')}}"  placeholder="Yearly Thermal Energy Consumption in Kcal / year" readonly onfocus="this.removeAttribute('readonly');" />
                                            <label class="form-label float-label">Yearly Thermal Energy Consumption in Kcal / year</label>
                                            <span class="error" id="thermalenergyconsumption-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-label-group">
                                            <input class="form-control form-caontrol-merge border-right-line" type="number" min="0" id="waterconsumption" name="waterconsumption" value="{{old('waterconsumption')}}"  placeholder="Yearly water consumption in Kilo Litres/ Cubic meteres" readonly onfocus="this.removeAttribute('readonly');" />
                                            <label class="form-label float-label">Yearly water consumption in Kilo Litres/ Cubic meteres</label>
                                            <span class="error" id="waterconsumption-error"></span>
                                        </div>
                                    </div>
                                   
                                    <div class="col-lg-12">
                                        <div class="form-label-group">
                                            <input class="form-control form-caontrol-merge border-right-line" id="totalarea" name="totalarea" value="{{old('totalarea')}}" type="number" placeholder="Total area of the unit / facility ( including Built up space, open area, colony or township etc ) in Acres only" readonly onfocus="this.removeAttribute('readonly');" />
                                            <label class="form-label float-label">Total area of the unit / facility ( including Built up space, open area, colony or township etc ) in Acres only</label>
                                            <span class="error" id="totalarea-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-label-group">
                                            <input class="form-control form-caontrol-merge border-right-line" type="text" id="budget_allocation" name="budget_allocation" value="{{old('budget_allocation')}}" placeholder="Budget Allocation" readonly onfocus="this.removeAttribute('readonly');" />
                                            <label class="form-label float-label">Budget Allocation</label>
                                            <span class="error" id="budget_allocation-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-label-group">
                                            <input class="form-control form-caontrol-merge border-right-line" type="text" id="tanno" name="tanno" value="{{old('tanno')}}" placeholder="TAN NO (Tax dedcution Account Number)" readonly onfocus="this.removeAttribute('readonly');" />
                                            <label class="form-label float-label">TAN NO (Tax dedcution Account Number)</label>
                                            <span class="error" id="tanno-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-label-group">
                                            <input class="form-control form-caontrol-merge border-right-line" type="text" id="panno" name="panno" value="{{old('panno')}}" placeholder="PAN NO (Permanent Account Number)" readonly onfocus="this.removeAttribute('readonly');" />
                                            <label class="form-label float-label">PAN NO (Permanent Account Number)</label>
                                            <span class="error" id="panno-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-label-group">
                                            <input class="form-control form-caontrol-merge border-right-line" id="gstinno" name="gstinno" value="{{old('gstinno')}}" type="text" placeholder="GSTIN NO (Goods and Services Tax Identification Number )" readonly onfocus="this.removeAttribute('readonly');" />
                                            <label class="form-label float-label">GSTIN NO (Goods and Services Tax Identification Number )</label>
                                            <span class="error" id="gstinno-error"></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group mt-1">
                                            <div class="custom-control custom-checkbox">
                                                <input class="custom-control-input" id="privacy-policy" name="privacy-policy" type="checkbox" tabindex="4" />
                                                <label class="custom-control-label" for="privacy-policy">I agree to<a target="_blank" href="">&nbsp;Privacy Policy & Terms</a></label>
                                                <span class="error" id="privacy-policy-error"></span>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 justify-content-center  d-flex">
                                        <button class="btn btn-primary" tabindex="5" type="submit" id="register" name="register" value="register"> Submit  </button>
                                    </div>
                                </div>
                            </form>
                            <p class="text-center mt-h5"><span>Already having account?</span><a href="{{route('company.login')}}"><span>&nbsp;Sign In</span></a></p>
                        </div>
                    </div>
                    <!-- /Register-->
                </div>
            </div>
        </div>
    </div>
</div> 
@endsection

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var Login = function () {
    return { //main function to initiate the module
        init: function () {

            $('#add_company_form').submit(function(e){
                e.preventDefault();
                var formData = new FormData($(this)[0]);

            
                $("#register").attr('disabled',true);
                $(".back").css('pointer','none');
                $("#register").html("Loading....");

                $.ajax({
                    type: "POST",
                    url: $('form#add_company_form').attr('action'),
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        if(response.status == 'validations') {
                            $.each(response.errors, function (key, error) {
                                $("#register").attr('disabled',false);
                                $("#register").html("Submit");
                                $('form#add_company_form').find("#" + key).removeClass('error').addClass('error');
                                $('form#add_company_form').find("#" + key + "-error").text(error[0]);
                            });
                        }

                        if(response.status == 'success') {
                            JsUtility.showToastr('success', 'Company', response.message);
                            window.location = "{{route('company.login')}}";
                        }

                        if(response.status == 'error') {
                            $("#register").attr('disabled',false);
                            $("#register").html("Submit");
                            JsUtility.showToastr('error', 'Company', response.message);
                        }
                    },
                    error: function() {
                            $("#register").attr('disabled',false);
                            $("#register").html("Submit");
                        JsUtility.showToastr('error', 'Company', response.message);
                    }
                }); 
                
                    

                return false;
            });
            
        }
    }
}();

jQuery(document).ready(function() {
    Login.init();
});



</script>
@endpush
