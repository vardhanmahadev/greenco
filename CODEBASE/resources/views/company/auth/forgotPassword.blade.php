@extends(COMPANY_THEME_NAME.'.layouts.auth')

@section('content')
<div class="auth-wrapper auth-v2">
    <div class="auth-inner row m-0">
        <!-- Brand logo-->
        <!-- /Brand logo-->
        <!-- Left Text-->
        <div class="d-none d-lg-flex col-lg-8 ">
            <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
                <img class="img-fluid" src="{{ url('app-assets/images/pages/forgot.jpg') }}" alt="Forgot password V2" /></div>
        </div>
        <!-- /Left Text-->
        <!-- Forgot password-->
        <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
            <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                <h2 class="card-title font-weight-bold mb-1">Forgot Password? 🔒</h2>
                <p class="card-text mb-2 font-small-4">Enter your email address and we'll send you a system generated password.</p>
                <form class="auth-forgot-password-form auth-form-div mt-2" onSubmit="return false" id="forgotPassword" action="{{route('company.forgotPassword')}}" method="POST">
                    @csrf
                    <input type="hidden" name="submit1" value="submit1">
                    <div class="form-label-group">
                        <div class="input-group" id="email-icon-error">
                            <div class="input-group-append">
                                <span class="input-group-text border-left-line cursor-pointer" ><i data-feather="user"></i></span>
                            </div>
                            <input class="form-control form-caontrol-merge border-right-line border-left-none" id="email" onkeypress=" $('#email-error-add').text('');" type="text" name="email" placeholder="Email Address*" aria-describedby="email" autofocus="" tabindex="1" />
                            <label class="form-label float-label" for="email">Email Address<span class="text-danger">*</span></label>
                            <!-- <span class="highlight">{{$errors->first('email')}}</span> -->
                        </div>
                        <span id="email-error-add" class="error"></span>
                    </div>
                    <button class="btn btn-primary btn-block" tabindex="2" name="submit" type="submit" id="submit" value="submit">Send Password</button>
                </form>
                <p class="text-center mt-2 font-small-4"><a href="{{route('company.login')}}"><i data-feather="chevron-left"></i> Back to login</a></p>
            </div>
        </div>
        <!-- /Forgot password-->
    </div>
</div>
@endsection

@push('PAGE_SCRIPTS')
<script type="text/javascript">
var ForgotPassword = function () {
    return { //main function to initiate the module
        init: function () {
            //applicationform upload start
            jQuery.validator.addMethod("emailAddress", function(value, element) {
               return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
            }, "Please Enter a Valid Email Address");

               $('#forgotPassword').validate({
                rules: {
                    'email': {
                        required: true,
                        emailAddress: true,
                    },

                },
                messages: {
                    email: {
                        required: "The Email Address field is required",
                        emailAddress : "Please Enter a Valid Email Address"
                    },

                },
                // });
                submitHandler : function(form){
                    $('#submit').prop("disabled",true);
                    $('#submit').html("Loading...");
                    $.ajax({
                        type: "POST",
                        url: $(form).attr('action'),
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: $(form).serialize(),
                        success: function(response) {
                            if(response.status == 'errors') {
                                $('#submit').prop("disabled",false);
                                $('#submit').html("Send Password");
                                $.each(response.errors, function (key, error) {
                                    $('form#forgotPassword').find("#" + key + "-icon-error").removeClass('borderRightError').addClass('borderRightError');
                                    $('form#forgotPassword').find("#" + key + "-icon-error").addClass('is-invalid');
                                    $('form#forgotPassword').find("#" + key).removeClass('error').addClass('error');
                                    $('form#forgotPassword').find("#" + key + "-error-add").css('display','block');
                                    $('form#forgotPassword').find("#" + key + "-error-add").text(error[0]);

                                });

                            }

                            if(response.status == 'success') {
                                JsUtility.showToastr('success', 'Authentication', response.message);
                                setTimeout(function(){
                                    window.location = "{{route('company.login')}}";
                                     }, 3000);
                            }

                            if(response.status == 'error') {
                                $('#submit').prop("disabled",false);
                                $('#submit').html("Send Password");
                                JsUtility.showToastr('error', 'Authentication', response.message);
                            }
                        },
                        error: function() {
                            $('#submit').prop("disabled",false);
                            $('#submit').html("Send Password");
                            JsUtility.showToastr('error', 'Authentication', response.message);
                        }


                    });

                }
            });
        }
    }
}();

jQuery(document).ready(function() {
    ForgotPassword.init();
    $('input').blur(function(){
       $('#forgotPassword').validate().element(this);
    });
    $('input').on('keyup',function(event){
        $("#forgotPassword").validate().element(this);
    });
});
</script>
@endpush
