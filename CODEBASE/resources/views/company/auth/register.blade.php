@extends(COMPANY_THEME_NAME.'.layouts.auth')

@section('content')
<div class="auth-wrapper auth-v2">
    <div class="auth-inner row m-0">
        <div class="col-lg-8  width-img pl-0 pr-0">
            <div class="item">
                <div class="slider-block">
                <img class="img-fluid authPage_banner_image" src="{{ url('app-assets/images/assessor_login_banner.png') }}" alt="Login V2">
                <!-- <div class="carousel-caption"> </div> -->
            </div>
        </div>
     </div>
        <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-2">
            <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                <a class="brand-logo" href="javascript:void(0);" style="cursor:default!important;">
                    <h2 class="brand-text text-primary ">
                        <img class="img-fluid" src="{{ url('/app-assets/images/logo/greenco.png') }}" width="150" alt="{{APP_NAME}}">
                    </h2>
                </a>
                <h2 class="card-title font-weight-bold mb-0 text-center">Company Registration</h2>
                <p class="card-text mb-3">&nbsp;</p>
                <form class="auth-register-form auth-form-div"  onSubmit="return false" id="auth-register-form" action="{{route('company.register')}}" method="POST">
                    @csrf
                    <div class="form-label-group">
                        <div class="input-group">
                            <!-- <div class="input-group-append">
                                <span class="input-group-text border-left-line cursor-pointer"><i data-feather="user"></i></span>
                                onkeypress="return /[a-zA-Z\s]/i.test(event.key)"
                            </div> -->

                            <input class="ui-autocomplete-input form-control form-control-merge border-right-line username-class" id="company_name" onkeypress="$('#company_name-error-add').text('');"  maxlength="50" type="text" name="company_name" placeholder="Company Name*" aria-describedby="name" autocomplete="off" tabindex="1" value="{{old('company_name')}}" />
                            <label for="name" class="float-label">Company Name<span class="text-danger">*</span></label>
                        </div>
                        <span class="error" id="company_name-error-add"></span>
                    </div>

                    <div class="form-label-group">
                        <div class="input-group">
                            <!-- <div class="input-group-append">
                                <span class="input-group-text border-left-line cursor-pointer"><i data-feather="at-sign"></i></span>
                            </div> -->
                            <input class="form-control border-right-line border-left-none form-caontrol-merge" onkeypress=" $('#email-error-add').text('');" id="email" type="text" name="email" placeholder="Email Address*" aria-describedby="email" autocomplete="false" tabindex="1" value="{{old('email')}}" />
                            <label for="email" class="float-label">Email Address<span class="text-danger">*</span></label>
                        </div>
                        <span class="error" id="email-error-add"></span>
                    </div>
                    <div class="form-label-group">
                        <div class="input-group">
                            <!-- <div class="input-group-append">
                                <span class="input-group-text border-left-line cursor-pointer"><i data-feather="phone"></i></span>
                            </div> -->
                            <input class="form-control border-right-line border-left-none form-caontrol-merge"  onkeypress="$('#mobileno-error-add').text(''); return /[0-9]/i.test(event.key)" maxlength="10" oninput="this.value=this.value.slice(0,this.maxLength)" id="mobileno" type="text" name="mobileno" value="{{old('mobileno')}}" placeholder="Mobile Number*" aria-describedby="mobileno" autocomplete="false" tabindex="1" />
                            <label for="mobileno" class="float-label">Mobile No<span class="text-danger">*</span></label>
                        </div>
                        <span class="error" id="mobileno-error-add"></span>
                    </div>

                    <div class="mt-2">
                        <button class="btn btn-primary btn-block" tabindex="4" type="submit" id="register" name="register" value="register">Register</button>
                    </div>
                </form>

                <p class="text-center mt-2 font-small-4">
					<span>Do you have an Account?</span>
					<a href="{{ route('company.login') }}">
						<span>Login</span>
					</a>
				</p>

            </div>
        </div>
        <!-- /Login-->
    </div>
</div>
@endsection

@push('PAGE_SCRIPTS')
<script type="text/javascript">

  $("#company_name").autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ route('company.companieslist') }}",
                type: 'get',
                dataType: "json",
                data:{name:request.term},
                success: function( data ) {
                response( data );
                }
            });
        },

        response: function(event, ui) {
            if (ui.content.length === 0) {
                $('#manufacturer_id').val('-1');
            }
        },

        select: function(event, ui) {
            $('#manufacturer_id').val('-1');
            $(this).val(ui.item.value);
            $('#manufacturer_id').val(ui.item.id);
        },
        minLength: 2,
        autoFocus: true
    });



</script>

<script type="text/javascript">
var Login = function () {
    return { //main function to initiate the module
        init: function () {
            jQuery.validator.addMethod("mobileNo", function(value, element) {
               return this.optional(element) || /^[6-9][0-9]{9}$/.test(value);
            }, "Mobile Number is invalid");

            jQuery.validator.addMethod("noSpace", function(value, element) {
              return value == '' || value.trim().length != 0;
            }, "This field is required");

            jQuery.validator.addMethod("doubleSpace", function(value, element) {
               return this.optional(element) || /^\S((?!.*  ).*\S)?$/.test(value);
            }, "This field is required");

            jQuery.validator.addMethod("emailAddress", function(value, element) {
               return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
            }, "Please Enter a Valid Email Address");


            $('#auth-register-form').validate({
                rules: {
                    'email': {
                        required: true,
                        // email: true,
                        emailAddress:true

                    },
                    'company_name': {
                        required: true,
                        noSpace: true,
                        doubleSpace:true,
                        maxlength: 50,
                        minlength:2
                    },
                    'mobileno':{
                        required: true,
                        digits:true,
                        minlength:10,
                        mobileNo: true
                    }
                },
                messages: {
                    email: {
                        required: "The Email Address field is required",
                        emailAddress : "Please Enter a Valid Email Address",
                        noSpace: "Please enter a valid email address",
                        doubleSpace: "Please enter a valid email address"

                    },
                    company_name: {
                        required: "The Company Name field is required",
                        noSpace: "Please Enter a Valid Company Name",
                        doubleSpace: "Please Enter a Valid Company Name"
                    },
                    mobileno: {
                        required: "The Mobile Number field is required",
                    },
                },
                // });
                submitHandler : function(form){
                    $("#register").prop('disabled',true);
                    $("#register").html("Loading....");
                    $.ajax({
                        type: "POST",
                        url: $(form).attr('action'),
                        // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: $(form).serialize(),
                        success: function(response) {
                            if(response.status == 'errors') {
                                $("#register").prop('disabled',false);
                                $("#register").html("Register");
                                $.each(response.errors, function (key, error) {
                                    $('form#auth-register-form').find("#" + key).removeClass('error').addClass('error');
                                    $('form#auth-register-form').find("#" + key + "-error-add").css('display','block');
                                    $('form#auth-register-form').find("#" + key + "-error-add").text(error[0]);

                                });

                            }
                            if(response.status == 'success') {
                                setTimeout(function(){
                                    window.location.href = "{{route('company.registerSuccess')}}";
                                }, 5000);
                            }

                            if(response.status == 'error') {
                                $("#register").prop('disabled',false);
                                $("#register").html("Register");
                                JsUtility.showToastr('error', 'Company', response.message);
                            }
                        },
                        error: function(response) {
                            console.log(response)
                            $("#register").prop('disabled',false);
                            $("#register").html("Register");
                            JsUtility.showToastr('error', 'Company', response.message);
                        }
                    });

                }
            });


        }
    }
}();

jQuery(document).ready(function() {
    Login.init();

    $('input').blur(function(){
        $("#auth-register-form").validate().element(this);
    });

    $('.username-class').on('keyup',function(event){
        event.currentTarget.value = this.value.toUpperCase();
        $("#auth-register-form").validate().element(this);
    });

    $('input').on('keyup',function(event){
        $("#auth-register-form").validate().element(this);
    });
});
</script>

@endpush
