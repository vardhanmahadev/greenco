<?php

namespace Database\Seeders;
use DB;

use Illuminate\Database\Seeder;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->insert(array(
            array(
            'name' => 'Andaman and Nicobar Islands',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Andhra Pradesh',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Arunachal Pradesh',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Assam',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Bihar',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Chandigarh',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Chhattisgarh',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Dadra and Nagar Haveli',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Daman and Diu',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Delhi',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Goa',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Gujarat',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Haryana',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Himachal Pradesh',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Jammu and Kashmir',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Jharkhand',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Karnataka',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Kerala',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Lakshadweep',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Madhya Pradesh',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Maharashtra',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Manipur',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Meghalaya',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Mizoram',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Nagaland',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Odisha',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Pondicherry',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Punjab',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Rajasthan',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Sikkim',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Tamil Nadu',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Telangana',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Tripura',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Uttar Pradesh',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'Uttarakhand',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            ),
            array(
            'name' => 'West Bengal',
            'created_by' => 1,
            'created_at' => now(),
            'updated_by' => 1,
            'updated_at' => now()
            )
        ));
    }
}
