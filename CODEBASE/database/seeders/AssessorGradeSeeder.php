<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AssessorGradeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('assessor_grades')->insert(array(
            array(
                'name' => "A1",
                'created_at' => now(),
                'updated_at' => now()
            ),
            array(
                'name' => "A2",
                'created_at' => now(),
                'updated_at' => now()
            ),
            array(
                'name' => "B1",
                'created_at' => now(),
                'updated_at' => now()
            ),
            array(
                'name' => "B2",
                'created_at' => now(),
                'updated_at' => now()
            ),
            array(
                'name' => "C1",
                'created_at' => now(),
                'updated_at' => now()
            ),
            array(
                'name' => "C2",
                'created_at' => now(),
                'updated_at' => now()
            ),
        ));
    }
}
