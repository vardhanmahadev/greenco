<?php

namespace Database\Seeders;
use DB;

use Illuminate\Database\Seeder;

class MetaIdsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('meta_ids')->insert(array(
            array(
            'parameter' => 'companies',
            'value' => 0,
            'latest_value' => 0,
            'created_at' => now()
            )
        ));
    }
}
