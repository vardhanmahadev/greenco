<?php

namespace Database\Seeders;
use DB;

use Illuminate\Database\Seeder;

class MasterIndustriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('master_industries')->insert(array(
            array(
            'name' => "Manufacturing",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Service",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            )
        ));
    }
}
