<?php

namespace Database\Seeders;
use DB;

use Illuminate\Database\Seeder;

class MasterSectorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('master_sectors')->insert(array(
            array(
            'name' => "Airports",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Auto Component",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Automobile",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Aviation",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Biotechnology",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Cement",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Chemicals",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Construction",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Defence Manufacturing",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Electrical & Electronics",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Engineering",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Fetilizer",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "FMCG",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Food rpocessing",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Glass",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Hospitality & Wellness",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Iron & Steel",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "IT & ITES",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Leather",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Mining",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Non ferrous Metals",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Oil & Gas",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Other Manufacturing Sector",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Other Service Sector",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Petrochemicals",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Pharma",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Ports & Shipping",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Power Plant",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Pulp & Paper",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Railway Production unit",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Railway Workshop",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Renewable Energy",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "SME ( only  if turn over Less than 50 Crore)",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Textile & Garments",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Tyre",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            )
        ));
    }
}
