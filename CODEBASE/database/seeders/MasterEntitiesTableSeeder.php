<?php

namespace Database\Seeders;
use DB;

use Illuminate\Database\Seeder;

class MasterEntitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('master_entities')->insert(array(
            array(
            'name' => "Private sector",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Public sector",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Indian Railways",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Other Government Enterprise",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
            array(
            'name' => "Not for profit",
            'status' => 1,
            'created_by' => 1,
            'created_at' => now()
            ),
        ));
    }
}
