<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyFacilatorDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies_facilitator', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('company_id')->nullable();
            $table->string('company_reg_id')->nullable();
            $table->tinyInteger('facilitator_id')->nullable();
            $table->tinyInteger('facilitator_assigned_by')->nullable();
            $table->integer('contract_acceptance')->default(0);
            $table->string('contract_doc')->nullable();
            $table->tinyInteger('contract_doc_status')->nullable();
            $table->string('contract_doc_remark')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies_facilitator');
    }
}
