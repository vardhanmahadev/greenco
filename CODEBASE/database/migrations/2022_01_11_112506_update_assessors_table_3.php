<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAssessorsTable3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assessors', function (Blueprint $table) {
            $table->renameColumn('pan_card', 'health_doc');
            $table->renameColumn('pan_card_status', 'health_doc_status');
            $table->renameColumn('pan_card_remark', 'health_doc_remark');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assessors', function (Blueprint $table) {
            $table->renameColumn('health_doc', 'pan_card');
            $table->renameColumn('health_doc_status', 'pan_card_status');
            $table->renameColumn('health_doc_remark', 'pan_card_remark');
        });
    }
}
