<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyWoLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies_wo_logs', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('company_id')->nullable();
            $table->string('company_reg_id')->nullable();
            $table->string('wo_doc')->nullable();
            $table->string('wo_status')->nullable();
            $table->tinyInteger('wo_doc_status_updated_by')->nullable();
            $table->timestamp('wo_doc_status_updated_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies_wo_logs');
    }
}
