<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyPaymentsInvoicesInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies_payments_invoices_info', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('company_id')->nullable();
            $table->string('company_reg_id')->nullable();
            $table->string('payment_for')->comment('reg-Registration')->nullable();
            $table->enum('payment_type', ['o1', 'o2'])->comment('o1-Online,o2-Offline')->nullable();
            $table->double('amount',10,2)->nullable();
            $table->tinyInteger('payment_status')->comment('1-Paid,0-Unpaid')->default(0);
            $table->timestamp('payment_date')->nullable();
            $table->string('trans_id')->nullable();
            $table->string('offline_tran_doc')->nullable();
            $table->string('invoice')->nullable();
            $table->tinyInteger('approval_status')->comment('1-Approved,2-Disapproved')->nullable();
            $table->tinyInteger('approved_by')->nullable();
            $table->timestamp('approved_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies_payments_invoices_info');
    }
}
