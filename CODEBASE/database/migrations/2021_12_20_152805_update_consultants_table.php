<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateConsultantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consultants', function (Blueprint $table) {
            $table->integer('verification_done_by')->nullable()->after('remember_token');
            $table->timestamp('verification_done_at')->nullable()->after('verification_done_by');
            $table->string('remark')->nullable()->after('verification_done_at');
            $table->integer('status_updated_by')->nullable()->after('remark');
            $table->timestamp('status_updated_at')->nullable()->after('status_updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consultants', function (Blueprint $table) {
            $table->dropColumn('verification_done_by');
            $table->dropColumn('verification_done_at');
            $table->dropColumn('remark');
            $table->dropColumn('status_updated_by');
            $table->dropColumn('status_updated_at');
        });
    }
}
