<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssessorVerifyLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessor_verify_log', function (Blueprint $table) {
            $table->id();
            $table->integer('assessor_id')->nullable();
            $table->unsignedTinyInteger('verified_status')->nullable();
            $table->string('verified_remark', 255)->nullable();
            $table->timestamp('verified_at')->nullable();
            $table->integer('verified_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessor_verify_log');
    }
}
