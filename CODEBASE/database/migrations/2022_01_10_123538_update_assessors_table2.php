<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAssessorsTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assessors', function (Blueprint $table) {
            $table->string('cancelled_check')->nullable();
            $table->tinyInteger('cancelled_check_status')->nullable();
            $table->string('cancelled_check_remark')->nullable();
            $table->tinyInteger('pan_card_status')->nullable();
            $table->string('pan_card_remark')->nullable();
            $table->tinyInteger('gst_form_status')->nullable();
            $table->string('gst_form_remark')->nullable();
            $table->tinyInteger('vendor_stamp_status')->nullable();
            $table->string('vendor_stamp_remark')->nullable();
            $table->tinyInteger('ndc_form_status')->nullable();
            $table->string('ndc_form_remark')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('acc_no')->nullable();
            $table->string('ifsc_code')->nullable();
            $table->string('branch_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assessors', function (Blueprint $table) {
            $table->dropColumn('cancelled_check');
            $table->dropColumn('cancelled_check_status');
            $table->dropColumn('cancelled_check_remark');
            $table->dropColumn('pan_card_status');
            $table->dropColumn('pan_card_remark');
            $table->dropColumn('gst_form_status');
            $table->dropColumn('gst_form_remark');
            $table->dropColumn('vendor_stamp_status');
            $table->dropColumn('vendor_stamp_remark');
            $table->dropColumn('ndc_form_status');
            $table->dropColumn('ndc_form_remark');
        });
    }
}
