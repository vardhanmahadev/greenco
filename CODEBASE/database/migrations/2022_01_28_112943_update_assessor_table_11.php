<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAssessorTable11 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assessors', function (Blueprint $table) {
            $table->string('pan_number')->nullable();
            $table->string('gst_number')->nullable();
            $table->tinyInteger('assessor_grade')->nullable();
            $table->timestamp('enrollment_date')->nullable();
            $table->tinyInteger('lead_assessor')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assessors', function (Blueprint $table) {
            $table->dropColumn('pan_number');
            $table->dropColumn('gst_number');
            $table->dropColumn('assessor_grade');
            $table->dropColumn('enrollment_date');
            $table->dropColumn('lead_assessor');
        });
    }
}
