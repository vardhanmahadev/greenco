<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyOnlineTransactionsLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies_online_transactions_logs', function (Blueprint $table) {
            $table->id();
            $table->string('trans_id')->nullable();
            $table->double('amount',10,2)->nullable();
            $table->enum('payment_type', ['o1', 'o2'])->comment('o1-Online,o2-Offline')->nullable();
            $table->enum('response', ['s', 'f'])->comment('s-Success,f-Failed')->nullable();
            $table->timestamp('payment_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies_online_transactions_logs');
    }
}
