<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssessorNdasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessor_ndas', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('assessor_id')->nullable();
            $table->string('document')->nullable();
            $table->tinyInteger('year')->nullable();
            $table->string('uploader_type')->nullable();
            $table->tinyInteger('uploader_id')->nullable();
            $table->tinyInteger('verified_status')->nullable();
            $table->string('verify_remark')->nullable();
            $table->tinyInteger('verified_by')->nullable();
            $table->timestamp('verified_on')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessor_ndas');
    }
}
