<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAssessorsTable1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assessors', function (Blueprint $table) {
            $table->string('pan_card')->nullable();
            $table->string('gst_form')->nullable();
            $table->string('vendor_stamp')->nullable();
            $table->string('ndc_form')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assessors', function (Blueprint $table) {
            $table->dropColumn('pan_card');
            $table->dropColumn('gst_form');
            $table->dropColumn('vendor_stamp');
            $table->dropColumn('ndc_form');
        });
    }
}
