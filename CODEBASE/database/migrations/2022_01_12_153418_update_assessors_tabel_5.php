<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAssessorsTabel5 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assessors', function (Blueprint $table) {
            $table->tinyInteger('category')->nullable();
            $table->tinyInteger('state')->nullable();
            $table->integer('alt_mobile')->nullable();
            $table->string('em_contact_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assessors', function (Blueprint $table) {
            $table->dropColumn('category');
            $table->dropColumn('state');
            $table->dropColumn('alt_mobile');
            $table->dropColumn('em_contact_name');
        });
    }
}
