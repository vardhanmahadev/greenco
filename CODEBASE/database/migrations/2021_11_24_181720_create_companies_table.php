<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('reg_id');
            $table->string('name')->nullable();
            $table->string('email',255)->nullable();
            $table->bigInteger('mobile')->unsigned()->nullable();
            $table->string('location')->nullable();
            $table->string('postal_address')->nullable();
            $table->string('billing_address')->nullable();
            $table->bigInteger('mst_industry_id')->nullable();
            $table->bigInteger('mst_entity_id')->nullable();
            $table->bigInteger('mst_sector_id')->nullable();
            $table->string('product_name')->nullable();
            $table->string('turnover',50)->nullable();
            $table->string('budget_allocation',50)->nullable();
            $table->bigInteger('no_of_employees')->nullable();
            $table->bigInteger('contract_employees')->nullable();
            $table->string('area',50)->nullable();
            $table->string('electrical_consumption',50)->nullable();
            $table->string('thermal_consumption',50)->nullable();
            $table->string('water_consumption',50)->nullable();
            $table->string('tanno',25)->nullable();
            $table->string('panno',25)->nullable();
            $table->string('gstno',25)->nullable();
            $table->integer('account_status')->default(0);
            $table->integer('verified_status')->default(0);
            $table->string('verified_remark')->nullable();
            $table->bigInteger('verified_by')->nullable();
            $table->timestamp('verified_at')->nullable();
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
