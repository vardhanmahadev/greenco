<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAssessorsTable10 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assessors', function (Blueprint $table) {
            $table->string('biodata')->nullable();
            $table->string('biodata_remark')->nullable();
            $table->tinyInteger('biodata_status')->nullable();
            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('city')->nullable();
            $table->bigInteger('pincode')->nullable();

            $table->string('em_address_1')->nullable();
            $table->string('em_address_2')->nullable();
            $table->string('em_city')->nullable();
            $table->tinyInteger('em_state')->nullable();
            $table->bigInteger('em_pincode')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assessors', function (Blueprint $table) {
            $table->dropColumn('biodata');
            $table->dropColumn('biodata_remark');
            $table->dropColumn('biodata_status');
            $table->dropColumn('address_1');
            $table->dropColumn('address_2');
            $table->dropColumn('city');
            $table->dropColumn('pincode');
            $table->dropColumn('em_address_1');
            $table->dropColumn('em_address_2');
            $table->dropColumn('em_city');
            $table->dropColumn('em_state');
            $table->dropColumn('em_pincode');
        });
    }
}
