<?php

if (!defined('APP_NAME')) define('APP_NAME', 'Green-co');
if (!defined('FRONTEND_THEME_NAME')) define('FRONTEND_THEME_NAME', 'frontend');
if (!defined('CONSULTANT_THEME_NAME')) define('CONSULTANT_THEME_NAME', 'consultant');
if (!defined('COMPANY_THEME_NAME')) define('COMPANY_THEME_NAME', 'company');
if (!defined('ASSESSOR_THEME_NAME')) define('ASSESSOR_THEME_NAME', 'assessor');
if (!defined('ADMIN_THEME_NAME')) define('ADMIN_THEME_NAME', 'admin');
if (!defined('ADMIN_EMAIL')) define('ADMIN_EMAIL', ['vardhanm@mirakitech.com', 'anirudhs@mirakitech.com', 'naveenk@mirakitech.com']);
if(!defined('APPROVAL_STATUS')) define('APPROVAL_STATUS',['Under Review','Accepted','Not Accepted']);
if(!defined('APPROVAL_STATUS_COLORS')) define('APPROVAL_STATUS_COLORS',['warning','success','danger']);
if (!defined('ADMIN_EMAILID')) define('ADMIN_EMAILID', 'info.ciigreenco@yopmail.com');
