<?php

return [
    'file_upload'=>[
        'total'=>[
            'large'=>29,
            'medium'=>28,
            'small'=>18
        ],
        'required'=>[
            'large'=>27,
            'medium'=>21,
            'small'=>10
        ],
        'optional'=>[
            'large'=>2,
            'medium'=>7,
            'small'=>8
        ],
    ]
];
