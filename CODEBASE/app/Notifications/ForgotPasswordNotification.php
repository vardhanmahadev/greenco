<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ForgotPasswordNotification extends Notification
{
    use Queueable;

    private $password = null;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($password,$type)
    {
        //
        $this->password = $password;
        $this->type = $type;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if($this->type == 'assesor')
        return (new MailMessage)->view(
            'assessor.emails.forgotPasswordUserEmail',
            ['user' => $notifiable, 'password' => $this->password]
        );
        elseif($this->type == 'company')
        return (new MailMessage)->view(
            'company.emails.forgotPasswordUserEmail',
            ['user' => $notifiable, 'password' => $this->password]
        );
        else
        return (new MailMessage)->view(
            'admin.emails.forgotPasswordUserEmail',
            ['user' => $notifiable, 'password' => $this->password]
        );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
