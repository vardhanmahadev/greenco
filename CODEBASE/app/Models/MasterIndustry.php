<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterIndustry extends Model
{
    protected $table        = 'master_industries';
    protected $primaryKey   = 'id';
}
