<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Company extends Authenticatable
{
       use SoftDeletes, HasApiTokens, HasFactory, Notifiable;


	protected $table        = 'companies';
    protected $primaryKey   = 'id';

    protected $fillable = [
    'reg_id',
    'name',	
    'email',	
    'password',	
    'mobile',	
    'location',	
    'postal_address',	
    'billing_address',	
    'mst_industry_id',	
    'mst_entity_id',
    'mst_sector_id',	
    'product_name',	
    'turnover',	
    'budget_allocation',	
    'no_of_employees',	
    'contract_employees',	
    'area',	
    'electrical_consumption',	
    'thermal_consumption',	
    'water_consumption',	
    'tanno',	
    'panno',	
    'gstno',	
    'proposal_document',	
    'po_document',	
    'account_status',	
    'verified_status',	
    'verified_remark',	
    'verified_by',	
    'verified_at'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

}