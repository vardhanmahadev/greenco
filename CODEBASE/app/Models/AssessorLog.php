<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssessorLog extends Model
{
    use HasFactory;

    protected $table        = 'assessor_verify_log';
    protected $primaryKey   = 'id';
}
