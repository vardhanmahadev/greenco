<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MetaId extends Model
{
    use HasFactory;

    protected $table        = 'meta_ids';
    protected $primaryKey   = 'id';
}
