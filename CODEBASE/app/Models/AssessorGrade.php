<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssessorGrade extends Model
{
    protected $table        = 'assessor_grades';
    protected $primaryKey   = 'id';
}
