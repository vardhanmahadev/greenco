<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterEntity extends Model
{
    protected $table        = 'master_entities';
    protected $primaryKey   = 'id';
}
