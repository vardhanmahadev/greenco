<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssessorCategories extends Model
{
    protected $table        = 'assessor_categories';
    protected $primaryKey   = 'id';
}
