<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterSector extends Model
{
    protected $table        = 'master_sectors';
    protected $primaryKey   = 'id';
}
