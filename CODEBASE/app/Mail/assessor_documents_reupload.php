<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;

class assessor_documents_reupload extends Mailable implements ShouldQueue
{
    use Queueable;

    public $details;

    public function __construct($details)
    {
        $this->details = $details;
    }

    public function build()
    {
        return $this->subject('GreenCo - Assessor Profile Documents Reupload')
                            ->with(['details', $this->details]) 
                            ->markdown('mails.assessor_documents_reupload');
    }
    
}
?>

