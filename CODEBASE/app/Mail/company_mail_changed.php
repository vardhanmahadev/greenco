<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;

class company_mail_changed extends Mailable implements ShouldQueue
{
    use Queueable;

    public $details;

    public function __construct($details)
    {
        $this->details = $details;
    }

    public function build()
    {
        return $this->subject('GreenCo - Account Details Changed')
                            ->with(['details', $this->details]) 
                            ->markdown('admin.emails.company_mail_changed');
    }
    
}
?>