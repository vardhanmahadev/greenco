<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;

class admin_not_accepted_docs extends Mailable implements ShouldQueue
{
    use Queueable;

    public function __construct()
    {
    }

    public function build()
    {
        return $this->subject('GreenCo - Documents Not Accepted')
                            ->markdown('mails.admin_not_accepted_docs');
    }
    
}
?>