<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        $path = $request->path();
        if (! $request->expectsJson()) {
            if(strpos($path, 'admin') !== false)
                return route('admin.login');
            elseif(strpos($path, 'assessor') !== false)
                return route('assessor.login');
            elseif(strpos($path, 'company') !== false)
                return route('company.login');
        }
        // if (! $request->expectsJson()) {
        //     return route('admin.login');
        // }
    }
}
