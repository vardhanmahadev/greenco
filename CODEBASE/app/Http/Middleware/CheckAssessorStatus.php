<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckAssessorStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(auth('assessor')->check() && auth('assessor')->user()->status == 0)
        {
            auth('assessor')->logout();

            if($request->ajax()) {
                return response()->json(['status'=>'errorlogout', 'message' => 'Your Account Email has been changed! Please Contact Greenco Team.']);
            }

            return redirect()->route('assessor.login')->with(['toast' => '1', 'status' => 'error', 'title' => 'Authentication', 
                                                        'message' => 'Account In-Active! Please Contact Greenco Team.']);
        
        }
        if(auth('assessor')->check() && auth('assessor')->user()->verification_status == 2 )
        {
            auth('assessor')->logout();

            if($request->ajax()) {
                return response()->json(['status'=>'errorlogout', 'message' => 'Your Account Email has been changed! Please Contact Greenco Team.']);
            }

            return redirect()->route('assessor.login')
                            ->with(['toast' => '1', 'status' => 'error', 'title' => 'Authentication','message' => 'Account Dis-Approved! Please Contact Greenco Team.']);
        
        }

        return $next($request);
        
    }
}
