<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckStatusCompany
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if(auth('company')->check() && auth('company')->user()->account_status == 0)
        {
            auth('company')->logout();

            if($request->ajax()) {
                return response()->json(['status'=>'errorlogout', 'message' => 'Account In-Active! Please Contact Greenco Team.']);
            }

            return redirect()->route('company.login')->with(['toast' => '1', 'status' => 'error', 'title' => 'Authentication', 
                                                        'message' => 'Account In-Active! Please Contact Greenco Team.']);
        
        }
        if(auth('company')->check() && auth('company')->user()->verified_status == 2 )
        {
            auth('company')->logout();

            if($request->ajax()) {
                return response()->json(['status'=>'errorlogout', 'message' => 'Account Dis-Approved! Please Contact Greenco Team.']);
            }

            return redirect()->route('company.login')
                            ->with(['toast' => '1', 'status' => 'error', 'title' => 'Authentication','message' => 'Account Dis-Approved! Please Contact Greenco Team.']);
        
        }

        return $next($request);
    
        
    }
}
