<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// Models
use App\Models\Company;

// check if assessor's email changed, logout if changed
class CheckCompanyEmailChanged
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $assessor = Company::find(session()->get('userId'));
        if(auth('company')->check()) {
            if(session()->get('email') != $assessor->email) {
                auth('company')->logout();

                if($request->ajax()) {
                    return response()->json(['status'=>'errorlogout', 'message' => 'Your Account Email has been changed! Please Contact Greenco Team.']);
                }

                return redirect()->route('company.login')
                                ->with(['toast' => '1', 'status' => 'error', 'title' => 'Authentication', 'message' => 'Your Account Email has been changed! Please Contact Greenco Team.']);
            }
        }
        
        return $next($request);
    }
}
