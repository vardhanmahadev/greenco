<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// Models
use App\Models\Assessor;

// check if assessor's email changed, logout if changed
class CheckAssessorEmailChanged
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $assessor = Assessor::find(session()->get('userId'));
        if(auth('assessor')->check()) {
            if(session()->get('email') != $assessor->email) {
                auth('assessor')->logout();

                if($request->ajax()) {
                    return response()->json(['status'=>'errorlogout', 'message' => 'Your Account Email has been changed! Please Contact Greenco Team.']);
                }

                return redirect()->route('assessor.login')
                                ->with(['toast' => '1', 'status' => 'error', 'title' => 'Authentication', 'message' => 'Your Account Email has been changed! Please Contact Greenco Team.']);
            }
        }
        
        return $next($request);
    }
}
