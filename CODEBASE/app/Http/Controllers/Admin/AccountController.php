<?php

namespace App\Http\Controllers\Admin;

use DB;
use Redirect;
use Validator;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

//Models
use Illuminate\Support\Facades\Hash;
use Notification;
//Mail
use App\Http\Controllers\BaseController;
use App\Notifications\PasswordUpdateNotification;
use App\Notifications\AdminAccountDetailsChangedNotification;

class AccountController extends BaseController
{
    public $data = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['activeMenu'] = 'account';
        //  $user = Auth::user();
    }

    public function profile(Request $request)
    {
        if ($request->mobile || $request->name) {

            $user = Auth::user();

            // dd($request->all());
     

            $validator = Validator::make(
                $request->all(),
                [
                    'email' => "required|email:rfc,dns|unique:assessors,email,$user->id",
                    'name' => 'required|min:3|max:50|regex:/^[a-zA-Z\s]+$/u',
                    'mobile' => "required|integer|starts_with: 6,7,8,9|digits:10|unique:assessors,mobile,$user->id",
                    
                ],
                [
                    'mobile.digits' => 'Mobile number must be of 10 digits.',
                    'email.unique' => 'Email ID is already exists',
                    'mobile.unique' => 'Mobile Number is already exists',
                    'mobile.starts_with' => 'Mobile Number should be valid'
                ]
            );

            if(!empty($request->mobile)) {
                $isExist = User::where('mobile',$request->mobile)->where('id', '<>', $user->id)->exists();
                if($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add(
                            'mobile', 'Mobile already exists!'
                        );
                    });
                }
            }
            if(!empty($request->email)) {
                $isExist = User::where('email',$request->email)->where('id', '<>', $user->id)->exists();
                if($isExist) {
                    $validator->after(function ($validator) {
                        $validator->errors()->add(
                            'email', 'Email already exists!'
                        );
                    });
                }
            }

            if ($validator->fails()) {
                return response()->json(['status' => 'errors', 'errors' => $validator->errors()->toArray()]);
            }
            $details = [
                'name' => $request->name,
                'email' => $request->email,
                'mobile' => $request->mobile,
            ];
            $admin = Auth::user();
            $admin->name = $request->name;
            $admin->email = $request->email;
            $admin->mobile = $request->mobile;

            if($admin->isDirty() =='true')
                Notification::send($admin,(new AdminAccountDetailsChangedNotification($details)));

            if ($admin->save()) {
                return response()->json(['status' => 'success', 'message' => 'Success! Profile updated successfully.']);
            } else {
                return response()->json(['status' => 'error', 'message' => 'Some error occurred!']);
            }
        } else {
            $this->data['pageTitle'] = 'Profile';
            $this->data['user_profile'] = Auth::user();
            return parent::adminView('account.profile', $this->data);
        }
    }

    public function changePasswordPage(Request $request)
    {
        $this->data['pageTitle'] = 'Change Password';
        return parent::adminView('account.changePassword', $this->data);
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'current_password' => 'required',
                'new_password' => ['required', 'string', 'min:6', 'regex:/[a-z]/', 'regex:/[A-Z]/', 'regex:/[0-9]/', 'regex:/[@$!%*#?&]/'],
                'confirmed' => 'required|same:new_password',
            ],
            [
                'current_password.required' => 'Please enter your current password',
                'new_password.required' => 'Please enter your new password',
                'confirmed.required' => 'Please enter your confirm password',
                'confirmed.same' => 'New Password and Confirm Password does not match.',
            ]
        );
        
        if ($validator->fails()) {
            return response()->json(['status' => 'errors', 'errors' => $validator->errors()->toArray()]);
        }

        $user = Auth::user();

        if (!(Hash::check($request->current_password, $user->password))) 
        {
            return response()->json(['status' => 'error', 'message' => 'Your current password does not matches with the password you provided. Please try again.']);
        }

        // dd("Hello");

        if (strcmp($request->current_password, $request->new_password) == 0) 
        {
            return response()->json(['status' => 'error', 'message' => 'New Password cannot be same as your current password. Please choose a different password.']);
        }

        DB::beginTransaction();
        try {

            $user->password = Hash::make($request->new_password);
            DB::commit();
            $user->save();
            $user->notify(new PasswordUpdateNotification);
            return response()->json(['status' => 'success', 'message' => 'Success! Your new Password has been updated successfully.']);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['status' => 'error', 'message' => 'Some Error occured! Please try again after sometime.']);
        }
    }


    public function logout()
    {
        Auth::logout();
        return redirect()
            ->route('admin.login')
            ->with(['toast' => '1', 'status' => 'success', 'title' => 'Thank You', 'message' => 'Thank you. You have been succesfully logged out']);
    }
}
