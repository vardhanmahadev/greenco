<?php

namespace App\Http\Controllers\Admin;

use App\Models\Consultant;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Exports\ConsultantExport;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;
use App\Notifications\SaveConsultantNotification;
use App\Notifications\ConsultantAccountDetailsChanged;
use App\Notifications\ConsultantDisapprovedNotification;
use App\Notifications\ConsultantRegistrationNotification;
use App\Notifications\AccountActivatedConsultantNotification;
use App\Notifications\AccountDeactivatedConsultantNotification;


class ConsultantController extends BaseController
{
    public $data = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['activeMenu'] = 'consultant';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->data['datatable_listing'] = true;
        $this->data['pageTitle'] = 'Consultant';
        return parent::adminView('consultant.index', $this->data);
    }

    public function data(Request $request)
    {

        $consultant = Consultant::orderBy('id', 'DESC');

        return  Datatables::of($consultant)
            ->addIndexColumn()

            ->filter(function ($query) use ($request) {
                if ($request->has('name') && $request->name != "") {
                    $query->where('name', 'like', "%{$request->get('name')}%");
                }
                if ($request->has('mobile') && $request->mobile != "") {
                    $query->where('mobile', 'like', "%{$request->get('mobile')}%");
                }
                if ($request->has('email') && $request->email != "") {
                    $query->where('email', 'like', "%{$request->get('email')}%");
                }

                if ($request->has('status') && $request->status != "") {
                    $query->where('status', $request->status);
                }

                if ($request->has('verification_status') && $request->verification_status != "") {
                    $query->where('verification_status', $request->verification_status);
                }

                if ($request->has('search') && @$request->search['value'] != "") {
                    $query->where('mobile', 'like', "%{$request->search['value']}%");
                    $query->orWhere('email', 'like', "%{$request->search['value']}%");
                    $query->orWhere('name', 'like', "%{$request->search['value']}%");
                }
            })
            ->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:50',
            'email' => 'required|email:rfc,dns|unique:consultants,email,',
            'mobile' => 'required|digits:10|starts_with: 6,7,8,9|unique:consultants,mobile',
            'status' => 'required|integer'
        ], [
            'mobile.digits' => 'The Mobile Number Must be 10 digits'
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->errors()->toArray()]);
        }
        //
        try {
            $randomPassword = passwordGeneration(8);
            $consultant = new Consultant;
            $consultant->name = $request->name;
            $consultant->email = $request->email;
            $consultant->mobile = $request->mobile;
            $consultant->status = $request->status;
            if($request->approveCheck)
            {
                $consultant->verification_status = 1;
            }
            else{
                $consultant->verification_status = 0;
            }

            $consultant->verification_done_by = Auth::user()->id;
            $consultant->verification_done_at = date('Y-m-d h:i:s');
            $consultant->status_updated_by = Auth::user()->id;
            $consultant->status_updated_at = date('Y-m-d h:i:s');

            $consultant->password = Hash::make($randomPassword);
            if ($consultant->save()) {
                if($request->approveCheck){
                    // echo ('hi');
                    $consultant->notify(new ConsultantRegistrationNotification($randomPassword));
                }
                else{
                    $consultant->notify(new SaveConsultantNotification());
                }
                return response()->json(['success' => true, 'message' => 'Consultant added successfully!']);
            }
        } catch (\Throwable $th) {
            return response()->json(['success' => false, 'message' => 'Consultant Add failed!']);
        }

        return response()->json(['success' => false, 'message' => 'Some error occurred. Try after sometime!']);
    }

    public function show(Request $request, Consultant $consultant)
    {
        return $consultant;
    }

    public function update(Request $request, Consultant $consultant)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:50',
            'email' => 'required|email:rfc,dns',
            'mobile' => 'required|digits:10|starts_with: 6,7,8,9',
            'status' => 'required'
        ]);

        if(!empty($request->email)) {
            $isExist = Consultant::where('email',$request->email)->where('id', '<>', $consultant->id)->exists();
            if($isExist) {
                $validator->after(function ($validator) {
                    $validator->errors()->add(
                        'email', 'Email already exists!'
                    );
                });
            }
        }

        if(!empty($request->mobile)) {
            $isExist = Consultant::where('mobile',$request->mobile)->where('id', '<>', $consultant->id)->exists();
            if($isExist) {
                $validator->after(function ($validator) {
                    $validator->errors()->add(
                        'mobile', 'Mobile already exists!'
                    );
                });
            }
        }

        // $state = State::find($request->state);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->errors()->toArray()]);
        }
        try {
            $consultant->name = $request->name;
            $consultant->email = $request->email;
            $consultant->mobile = $request->mobile;


            $consultant->status_updated_by = Auth::user()->id;
            $consultant->status_updated_at = date('Y-m-d h:i:s');

            if(($consultant->status) != ($request->status)){
                if($request->status == 0){
                    Notification::send($consultant, (new AccountDeactivatedConsultantNotification()));
                }
                else{
                    Notification::send($consultant, (new AccountActivatedConsultantNotification()));
                }
                $consultant->status = $request->status;
            }
            else{
                Notification::send($consultant,(new ConsultantAccountDetailsChanged()));
            }

            if ($consultant->save()) {
                return response()->json(['success' => true, 'message' => 'Consultant Update Successfully!']);
            }
        } catch (\Throwable $th) {
            DB::rollBack();
            return response()->json(['success' => false, 'message' => 'Some error occurred!']);
        }

        return response()->json(['success' => false, 'message' => 'Some error occurred!']);
    }

    public function statusUpdateBlock(Request $request)
    {        
        $consultants = Consultant::whereIn('id', $request->consultant_id)->get();

        $validator = Validator::make($request->all(), [
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->errors()->toArray()]);
        }

        try{
            DB::beginTransaction();
            foreach($consultants as $consultant)
            {
                if(($consultant->status) != ($request->status)){
                    if($request->status == 0){
                        Notification::send($consultant, (new AccountDeactivatedConsultantNotification()));
                    }
                    else{
                        Notification::send($consultant, (new AccountActivatedConsultantNotification()));
                    }
                    Consultant::where('id',$consultant->id)->update(['status' => $request->status, 'status_updated_by' => Auth::user()->id, 'status_updated_at' => date('Y-m-d h:i:s')]);
                }
            }
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Status update Successfull!']);
        }
        catch(\Throwable $th){
            DB::rollback();
            return response()->json(['success' => false, 'message' => 'Some Error Occurred. Please try again after sometime!']);
        }
    }

    public function approve(Consultant $consultant, Request $request)
    {
        // dd($consultant->verification_status);
        $response = [
            'success' => false,
            'message' => 'Some error occurred. Please try again after sometime!'
        ];
        $message = 'Consultant has been Approved.';
        try {
            DB::beginTransaction();
            if ($consultant->verification_status == 0) {

                $consultant->verification_status = $request->status;
                if ($request->status == 2) {
                    $message = "Consultant disapproved successfully";
                }
                $randomPassword = passwordGeneration(8);
                $consultant->password = Hash::make($randomPassword);
                $consultant->remark = $request->remark;
                $consultant->verification_done_by = Auth::user()->id;
                $consultant->verification_done_at = date('Y-m-d h:i:s');
                $remark = $request->remark;
                if ($consultant->save()) {
                    if($request->status == 2){
                        $consultant->notify(new ConsultantDisapprovedNotification($remark));
                        $response = [
                            'success' => true,
                            'message' => $message,
                        ];
                    }
                    else{
                        $consultant->notify(new ConsultantRegistrationNotification($randomPassword));
                        $response = [
                            'success' => true,
                            'message' => $message,
                        ];
                    }
                }
            } else {
                if ($request->status != $consultant->verification_status) {
                    $consultant->verification_status = $request->status;
                    $consultant->remark = $request->remark;
                    $consultant->verification_done_by = Auth::user()->id;
                    $consultant->verification_done_at = date('Y-m-d h:i:s');
                    $remark = $request->remark;
                    if ($request->status == 2) {
                        $message = "Consultant disapproved successfully";
                    }
                    if ($consultant->save()) {
                        $consultant->notify(new ConsultantDisapprovedNotification($remark));
                        $response = [
                            'success' => true,
                            'message' => $message,
                        ];
                    }
                } else {
                    $response['message'] = 'Consultant is already approved';
                    if ($request->status == 2) {
                        $response['message'] = 'Consultant is already disapproved';
                    }
                }
            }
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
        }
        return response()->json($response, 200);
    }

    public function exportBlock(Request $request)
    {
        $consultant = Consultant::orderBy('id','desc');
        if(!is_null($request->name)) $consultant = $consultant->where('name', 'LIKE', "%{$request->name}%");
        if(!is_null($request->email)) $consultant = $consultant->where('email', 'LIKE', "%{$request->email}%");
        if(!is_null($request->mobile)) $consultant = $consultant->where('mobile',$request->mobile);
        if(!is_null($request->status)) $consultant = $consultant->where('status', $request->status);
        if(!is_null($request->verificationStatus)) $consultant = $consultant->where('verification_status',$request->verificationStatus);

        $consultant = $consultant->get();
        return Excel::download(new ConsultantExport($consultant), 'consultant.xlsx');
    }
}
