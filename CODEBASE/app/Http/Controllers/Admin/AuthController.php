<?php

namespace App\Http\Controllers\Admin;

use Mail;
use Redirect;
use Validator;
use App\Models\User;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

//Models
use Illuminate\Support\Facades\Auth;

//Mail
use Illuminate\Support\Facades\Hash;
use App\Mail\ForgotPasswordUserEmail;
use App\Http\Controllers\BaseController;
use App\Notifications\ForgotPasswordNotification;

class AuthController extends BaseController
{
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest:admin')->except('logout');
    }

    protected function credentials(Request $request)
    {
        if (filter_var($request->email, FILTER_VALIDATE_EMAIL))
            return ['email' => $request->email, 'password' => $request->password];
    }

    public function logout()
    {
        Auth::logout();
        return redirect()
            ->route('admin.login')
            ->with(['toast' => '1', 'status' => 'success', 'title' => 'Thank You', 'message' => 'Thank you. You have been succesfully logged out']);
    }

    public function login(Request $request)
    {
        // dd(Hash::make('admin@1234'));
        if ($request->isMethod('POST')) {

            $validator = Validator::make(
                $request->all(),
                [
                    'email' => 'required|email:rfc',
                    'password'  => 'required',
                ],
                [
                    'email.email' => 'Please enter a valid email',
                    'email.required' => 'Please enter your email',
                    'password.required' => 'Please enter your password'
                ]
            );

            if ($validator->fails()) {
                return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
            }

            if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {
                return redirect()->route('admin.dashboard')->with(['toast' => '1', 'status' => 'success', 'title' => 'Login', 'message' => 'Welcome Back!']);
            } else {
                return redirect()->route('admin.login')->with(['toast' => '1', 'status' => 'error', 'title' => 'Login', 'message' => 'Your credentials are not valid! Please enter a valid Email and Password.']);
            }
        }

        $data = [
            'pageTitle' => 'Login'
        ];

        return parent::adminView('auth.login', $data);
    }

    public function forgotPasswordPage()
    {
        $data = [
            'pageTitle' => 'Forgot Password'
        ];

        return parent::adminView('auth.forgotPassword', $data);
    }

    public function forgotPassword(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'email' => 'required|email:rfc,dns'
            ],
            [
                'email.required' => 'Please enter your email',
                'email.email' => 'Please enter a proper email'
            ]
        );

        // $state = State::find($request->state);
        if ($validator->fails()) {
            return response()->json(['status' => 'errors', 'errors' => $validator->errors()->toArray()]);
        }
         else {

            $user = User::where('email', $request->email)->first();

            if ($user) {

                $password = passwordGeneration(6);

                User::where('email', $request->email)
                    ->update(['password' => Hash::make($password)]);
                
                    $type = 'user';
                    $user->notify(new ForgotPasswordNotification($password,$type));



                DB::commit();

                return response()->json(['status' => 'success', 'message' => 'Password sent to your email!']);
            } else {
                DB::rollback();
                return response()->json(['status' => 'error', 'message' => 'User doesnt exist!']);
            }
        }
    }
}
