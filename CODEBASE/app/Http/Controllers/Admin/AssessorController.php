<?php

namespace App\Http\Controllers\Admin;

use Auth;
use File;
use Mail;
use App\Models\User;
use App\Models\States;
use App\Models\Assessor;
use App\Mail\AssessorAdded;
use App\Models\AssessorLog;
use Illuminate\Http\Request;
use App\Models\AssessorGrade;
use App\Exports\AccessorExport;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use App\Mail\assessor_mail_changed;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use App\Mail\admin_not_accepted_docs;
use App\Mail\assessor_details_changed;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;
use App\Notifications\SaveAssessorNotification;
use App\Notifications\AssessorDocDetailsChanged;
use Facades\App\Repository\AssessorCategoryRepo;
use App\Notifications\AssessorApprovedNotification;
use App\Notifications\AssessorAccountDetailsChanged;
use App\Notifications\AssessorDisapprovedNotification;
use App\Notifications\AssessorRegistrationNotification;
use App\Notifications\AccountStatusAssessorNotification;
use App\Notifications\AccountActivatedAssessorNotification;
use App\Notifications\AccountDeactivatedAssessorNotification;

class AssessorController extends BaseController
{

    public $data = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['activeMenu'] = 'assessor';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->data['datatable_listing'] = true;
        $this->data['pageTitle'] = 'Assessor';
        $this->data['states'] = States::orderBy('name','asc')->get();
        $this->data['categories'] = AssessorCategoryRepo::allActive($orderColumn="created_at", $orderDir='desc');
        return parent::adminView('assessor.index', $this->data);
    }

    public function data(Request $request)
    {
        $assessor = Assessor::leftjoin('states','states.id','=','assessors.state')
                            ->select('assessors.*','states.name as state_name')
                            ->orderBy('assessors.id', 'DESC');

        return  Datatables::of($assessor)
            ->addIndexColumn()

            ->filter(function ($query) use ($request) {
                if ($request->has('name') && $request->name != "") {
                    $query->where('name', 'like', "%{$request->get('name')}%");
                }
                if ($request->has('mobile') && $request->mobile != "") {
                    $query->where('mobile', 'like', "%{$request->get('mobile')}%");
                }
                if ($request->has('email') && $request->email != "") {
                    $query->where('email', 'like', "%{$request->get('email')}%");
                }

                if ($request->has('status') && $request->status != "") {
                    $query->where('status', $request->status);
                }

                if ($request->has('state') && $request->state != "") {
                    $query->where('state', $request->state);
                }

                if ($request->has('category') && $request->category != "") {
                    $query->where('category','like',"%{$request->category}%");
                }

                if ($request->has('verification_status') && $request->verification_status != "") {
                    $query->where('verification_status', $request->verification_status);
                }

                if ($request->has('profile_status') && $request->profile_status != "") {
                    $query->where('profile_updated', $request->profile_status);
                }

                if ($request->has('search') && @$request->search['value'] != "") {
                    $query->where('mobile', 'like', "%{$request->search['value']}%");
                    $query->orWhere('email', 'like', "%{$request->search['value']}%");
                    $query->orWhere('name', 'like', "%{$request->search['value']}%");
                }
            })

            ->addColumn('state', function ($assessor) {
                return ($assessor->state_name == null) ? 'N/A' : $assessor->state_name;
            })

            ->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:3|max:50',
            'email' => 'required|email:rfc,dns|unique:assessors,email,',
            'mobile' => 'required|digits:10|unique:assessors,mobile',
            'status' => 'required|integer'
        ], [
            'mobile.digits' => 'The Mobile Number Must be 10 digits'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'validations', 'errors' => $validator->errors()->toArray()]);
        }

        try 
        {
            DB::beginTransaction();
            // dd($request->status);
            $randomPassword = passwordGeneration(6);
            $password = $randomPassword;
            $assessor = new Assessor;
            $assessor->name = $request->name;
            $assessor->email = $request->email;
            $assessor->mobile = $request->mobile;
            $assessor->status = $request->status;
            $assessor->verification_done_by = Auth::user()->id;
            $assessor->verification_done_at = date('Y-m-d h:i:s');
            $assessor->status_updated_by = Auth::user()->id;
            $assessor->status_updated_at = date('Y-m-d h:i:s');

            $assessor->password = Hash::make($randomPassword);

            if ($assessor->save()) {

                if($request->status==0){
                    __account_activation($request->email,'assessor');
                }
                else{
                    $assessor->notify(new AssessorRegistrationNotification($randomPassword));
                }

                // if($request->approveCheck){
                //     // echo ('hi');
                //     $assessor_log = new AssessorLog();
                //     $assessor_log->assessor_id = $assessor->id;
                //     $assessor_log->verified_status = $assessor->verification_status;
                //     $assessor_log->verified_at = date('Y-m-d h:i:s');
                //     $assessor_log->verified_by = Auth::user()->id;
                //     $assessor_log->save();
                //     $assessor->notify(new AssessorRegistrationNotification($randomPassword));
                // }
                // else{
                //     $assessor->notify(new SaveAssessorNotification($password));
                // }
                DB::commit();
                return response()->json(['status' => 'success', 'message' => 'Assessor added successfully!'],200);
            }
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(['status' => 'error', 'message' => 'Assessor add failed!'],200);
        }

        return response()->json(['status' => 'error', 'message' => 'some error occurred try after sometime!'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Assessor  $assessor
     * @return \Illuminate\Http\Response
     */
    public function show(Assessor $assessor)
    {
        //
        return $assessor;
    }

    public function remarkLogs(Request $request, $id)
    {
        $logs = AssessorLog::join('users','assessor_verify_log.verified_by','users.id')->where('assessor_id',$id)->orderBy('assessor_verify_log.created_at','desc')->first();
        // dd($logs);
        return $logs;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Assessor  $assessor
     * @return \Illuminate\Http\Response
     */
    public function edit(Assessor $assessor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Assessor  $assessor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Assessor $assessor)
    {
        // dd($request->all());
        if($request->final_submit_value==1 || $request->final_submit_value==2){
            $validator = Validator::make($request->all(), [
                'name' => 'required|regex:/^[a-zA-Z\s]+$/u|min:3|max:50',
                'email' => 'required|email:rfc,dns',
                'mobile' => 'required|digits:10',
                'ind_category' => 'required',
                'address_1' => 'required',
                'city' => 'required',
                'state' => 'required',   
                'pincode' => 'required',     
                'pan_number' => 'required',
                'gst_number' => 'required',
                'em_contact_name' => 'required|regex:/^[a-zA-Z\s]+$/u|min:3|max:50',
                'em_contact_no' => 'required',
                'em_address_1' => 'required',
                'status' => 'required',
                'em_city' => 'required',
                'em_state' => 'required',
                'em_pincode' => 'required',
                'bank_name' => 'required|regex:/^[a-zA-Z\s]+$/u|min:3|max:50',
                'acc_no' => 'required',
                'branch_name' => 'required|regex:/^[a-zA-Z\s]+$/u|min:3|max:50',
                'ifsc_code' => 'required',
            ],
            [
                'name.required' =>'This field is required.',
                'email.required' =>'This field is required.',
                'mobile.required' =>'This field is required.',
                'ind_category.required' =>'This field is required.',
                'state.required' =>'This field is required.',
                'address_1.required' => 'This field is required',
                'city.required' => 'This field is required',
                'pincode.required' => 'This field is required',
                'pan_number.required' => 'This field is required',
                'gst_number.required' => 'This field is required',
                'em_contact_name.required' => 'This field is required',
                'em_contact_no.required' => 'This field is required',
                'em_address_1.required' => 'This field is required',
                'status.required' => 'This field is required',
                'em_city.required' => 'This field is required',
                'em_state.required' => 'This field is required',
                'em_pincode.required' => 'This field is required',
                'bank_name.required' => 'This field is required',
                'acc_no.required' => 'This field is required',
                'branch_name.required' => 'This field is required',
                'ifsc_code.required' => 'This field is required',
                'em_contact_name.regex' => 'Format is Invalid',
                'em_contact_name.min' => 'Please enter minimum of 3 characters',
                'em_contact_name.max' => 'Please enter maxmimum of 50 characters',
                'bank_name.regex' => 'Format is Invalid',
                'bank_name.min' => 'Please enter minimum of 3 characters',
                'bank_name.max' => 'Please enter maxmimum of 50 characters',
                'branch_name.regex' => 'Format is Invalid',
                'branch_name.min' => 'Please enter minimum of 3 characters',
                'branch_name.max' => 'Please enter maxmimum of 50 characters',
            ]);
        }
        else{
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|min:3|max:50',
                'email' => 'required|email:rfc,dns',
                'mobile' => 'required|digits:10'    ,
                'ind_category' => 'required',
                'address_1' => 'required',
                'city' => 'required',
                'state' => 'required',   
                'pincode' => 'required',        
                // 'em_contact_name' => 'regex:/^[a-zA-Z\s]+$/u|min:3|max:50',  
                // 'bank_name' => 'regex:/^[a-zA-Z\s]+$/u|min:3|max:50',
                // 'branch_name' => 'regex:/^[a-zA-Z\s]+$/u|min:3|max:50'
            ],
            [
                'name.required' =>'This field is required.',
                'email.required' =>'This field is required.',
                'mobile.required' =>'This field is required.',
                'ind_category.required' =>'This field is required.',
                'state.required' =>'This field is required.',
                'address_1.required' => 'This field is required',
                'city.required' => 'This field is required',
                'pincode.requried' => 'This field is required',
                // 'em_contact_name.regex' => 'Format is Invalid',
                // 'em_contact_name.min' => 'Please enter minimum of 3 characters',
                // 'em_contact_name.max' => 'Please enter maxmimum of 50 characters',
                // 'bank_name.regex' => 'Format is Invalid',
                // 'bank_name.min' => 'Please enter minimum of 3 characters',
                // 'bank_name.max' => 'Please enter maxmimum of 50 characters',
                // 'branch_name.regex' => 'Format is Invalid',
                // 'branch_name.min' => 'Please enter minimum of 3 characters',
                // 'branch_name.max' => 'Please enter maxmimum of 50 characters',
            ]);
        }

        if(!empty($request->email)) {
            $isExist = Assessor::where('email',$request->email)->where('id', '<>', $assessor->id)->exists();
            if($isExist) {
                $validator->after(function ($validator) {
                    $validator->errors()->add(
                        'email', 'Email already exists!'
                    );
                });
            }
        }
        if(!empty($request->mobile)) {
            $isExist = Assessor::where('mobile',$request->mobile)->where('id', '<>', $assessor->id)->exists();
            if($isExist) {
                $validator->after(function ($validator) {
                    $validator->errors()->add(
                        'mobile', 'Mobile already exists!'
                    );
                });
            }
        }
        if ($validator->fails()) {
            return response()->json(['status' => 'validations', 'errors' => $validator->errors()->toArray()]);
        }
        
        try {
            // {
                DB::beginTransaction();
                //
                $email = $assessor->email;
                $hc_status = $assessor->health_doc_status;
                $gf_status = $assessor->gst_form_status;
                $vs_status = $assessor->vendor_stamp_status;
                $nf_status = $assessor->ndc_form_status;
                $p_status = $assessor->pan_status;
                $cc_status = $assessor->cancelled_check_status;
                $b_status = $assessor->biodata_status;
                $hc_status_1 = $assessor->health_doc_status;
                $gf_status_1 = $assessor->gst_form_status;
                $vs_status_1 = $assessor->vendor_stamp_status;
                $nf_status_1 = $assessor->ndc_form_status;
                $p_status_1 = $assessor->pan_status;
                $cc_status_1 = $assessor->cancelled_check_status;
                $b_status_1 = $assessor->biodata_status;
                //
                $password = passwordGeneration(8);
                $details = [
                    'name' => $request->name,
                    'email' => $request->email,
                    'mobile' => $request->mobile,
                    'password' => $password
                ]; 
                if(isset($request->health_card_status)){
                    $hc_status_1 = $request->health_card_status;
                    if($request->health_card_status==2){
                        if(isset($request->health_card_remark)){}
                        else{
                            return response()->json(['status' => 'error', 'message' => 'Remark field is mandatory!']);
                        }
                    }
                    $assessor->health_doc_status = $request->health_card_status;
                }
                if(isset($request->gst_form_status)){
                    $gf_status_1 = $request->gst_form_status;
                    if($request->gst_form_status==2){
                        if(isset($request->gst_form_remark)){}
                        else{
                            return response()->json(['status' => 'error', 'message' => 'Remark field is mandatory!']);
                        }
                    }
                    $assessor->gst_form_status = $request->gst_form_status;
                }
                if(isset($request->vendor_stamp_status)){
                    $vs_status_1 = $request->vendor_stamp_status;
                    if($request->vendor_stamp_status==2){
                        if(isset($request->vendor_stamp_remark)){}
                        else{
                            return response()->json(['status' => 'error', 'message' => 'Remark field is mandatory!']);
                        }
                    }
                    $assessor->vendor_stamp_status = $request->vendor_stamp_status;
                }
                if(isset($request->ndc_form_status)){
                    $nf_status_1 = $request->ndc_form_status;
                    if($request->ndc_form_status==2){
                        if(isset($request->ndc_form_remark)){}
                        else{
                            return response()->json(['status' => 'error', 'message' => 'Remark field is mandatory!']);
                        }
                    }
                    $assessor->ndc_form_status = $request->ndc_form_status;
                }
                if(isset($request->pan_status)){
                    $p_status_1 = $request->pan_status;
                    if($request->pan_status==2){
                        if(isset($request->pan_remark)){}
                        else{
                            return response()->json(['status' => 'error', 'message' => 'Remark field is mandatory!']);
                        }
                    }
                    $assessor->pan_status = $request->pan_status;
                }
                if(isset($request->cancelled_check_status)){
                    $cc_status_1 = $request->cancelled_check_status;
                    if($request->cancelled_check_status==2){
                        if(isset($request->cancelled_check_remark)){}
                        else{
                            return response()->json(['status' => 'error', 'message' => 'Remark field is mandatory!']);
                        }
                    }
                    $assessor->cancelled_check_status = $request->cancelled_check_status;
                }
                if(isset($request->biodata_status)){
                    $b_status_1 = $request->biodata_status;
                    if($request->biodata_status==2){
                        if(isset($request->biodata_remark)){}
                        else{
                            return response()->json(['status' => 'error', 'message' => 'Remark field is mandatory!']);
                        }
                    }
                    $assessor->biodata_status = $request->biodata_status;
                }
                // dd($assessor->isDirty() == 'true');
                
                $category_name = json_decode($assessor->category);

               if((($assessor->name!=$request->name || $assessor->mobile!=$request->mobile || $category_name!=$request->ind_category || $assessor->alt_mobile!=$request->alt_mobile || $assessor->address_1!=$request->address_1 || $assessor->address_2!=$request->address_2 || $assessor->city!=$request->city || $assessor->state!=$request->state || $assessor->pincode!=$request->pincode || $assessor->pan_number!=$request->pan_number || $assessor->gst_number!=$request->gst_number || $assessor->assessor_grade!=$request->assessor_grade || (date('d/m/Y',strtotime($assessor->enrollment_date)))!=(date('d/m/Y',strtotime($request->enrollment_date))) || $assessor->lead_assessor!=$request->lead_assessor || $assessor->em_contact_name!=$request->em_contact_name || $assessor->em_contact_no!=$request->em_contact_no || $assessor->em_address_1!=$request->em_address_1 || $assessor->em_address_2!=$request->em_address_2 || $assessor->em_city!=$request->em_city || $assessor->em_state!=$request->em_state || $assessor->em_pincode!=$request->em_pincode || $assessor->bank_name!=$request->bank_name || $assessor->acc_no!=$request->acc_no || $assessor->branch_name!=$request->branch_name || $assessor->ifsc_code!=$request->ifsc_code) && (($email)==($request->email))))
               {
                   if($cc_status!=$cc_status_1 || $hc_status!=$hc_status_1 || $gf_status!=$gf_status_1 || $vs_status!=$vs_status_1 || $nf_status!=$nf_status_1 || $p_status!=$p_status_1 || $b_status!=$b_status_1)
                   {
                    Notification::send($assessor,(new AssessorDocDetailsChanged($details)));
                   }
                   else{
                    Notification::send($assessor,(new AssessorAccountDetailsChanged($details)));
                   }
               }
               else{
                if($request->biodata_status==2 || $request->cancelled_check_status==2 || $request->pan_status==2 || $request->ndc_form_status==2 || $request->vendor_stamp_status==2 || $request->gst_form_status==2 || $request->health_card_status==2){
                    Mail::to($request->email)->send(new admin_not_accepted_docs());
                }
               }
               $assessor->name = $request->name;
               if(isset($request->email)){
                   if(($assessor->email) != ($request->email)){
                       Mail::to($request->email)->send(new assessor_mail_changed($details));
                       Mail::to($email)->send(new assessor_details_changed($details));
                       // Notification::send($assessor,(new AssessorAccountDetailsChanged($details)));
                   $assessor->password = Hash::make($password);
                   }
                   $assessor->email = $request->email;
               }
               $assessor->mobile = $request->mobile;
               if(isset($request->status)){
                   if(($assessor->status) != ($request->status)){
                       if($request->status == 0){
                           Notification::send($assessor, (new AccountDeactivatedAssessorNotification($details)));
                       }
                       else{
                           Notification::send($assessor, (new AccountActivatedAssessorNotification($details)));
                       }
                       $assessor->status = $request->status;
                   }
               }
                $category = json_encode($request->ind_category);
                $assessor->category = $category;
                //
                $assessor->pan_number = $request->pan_number;
                $assessor->gst_number = $request->gst_number;
                $assessor->assessor_grade = $request->assessor_grade;
                $assessor->lead_assessor = $request->lead_assessor;
                if(isset($request->enrollment_date)){
                    $assessor->enrollment_date = date('Y-m-d', strtotime($request->enrollment_date));;
                }
                $assessor->address_1 = $request->address_1;
                $assessor->address_2 = $request->address_2;
                $assessor->city = $request->city;
                $assessor->pincode = $request->pincode;
                $assessor->em_address_1 = $request->em_address_1;
                $assessor->em_address_2 = $request->em_address_2;
                $assessor->em_city = $request->em_city;
                $assessor->em_state = $request->em_state;
                $assessor->em_pincode = $request->em_pincode;
                //
                $assessor->state = $request->state;
                $assessor->alt_mobile = $request->alt_mobile;
                $assessor->em_contact_name = $request->em_contact_name;
                $assessor->em_contact_no = $request->em_contact_no;
                $assessor->em_contact_address = $request->em_contact_address;
                $assessor->bank_name = $request->bank_name;
                $assessor->acc_no = $request->acc_no;
                $assessor->branch_name = $request->branch_name;
                $assessor->ifsc_code = $request->ifsc_code;
                
                if(isset($request->health_card_remark)){
                    $assessor->health_doc_remark = $request->health_card_remark;
                }
                if(isset($request->gst_form_remark)){
                    $assessor->gst_form_remark = $request->gst_form_remark;
                }
                if(isset($request->vendor_stamp_remark)){
                    $assessor->vendor_stamp_remark = $request->vendor_stamp_remark;
                }
                if(isset($request->ndc_form_remark)){
                    $assessor->ndc_form_remark = $request->ndc_form_remark;
                }
                if(isset($request->pan_remark)){
                    $assessor->pan_remark = $request->pan_remark;
                }
                if(isset($request->cancelled_check_remark)){
                    $assessor->cancelled_check_remark = $request->cancelled_check_remark;
                }
                if(isset($request->biodata_remark)){
                    $assessor->biodata_remark = $request->biodata_remark;
                }

                //documents upload
                $folder = ('uploads/assessors/'.$assessor->id);
                ## CREATE FOLDER
                if (!File::exists($folder)) {
                    mkdir($folder, 0777, true);
                    chmod($folder, 0777);
                }
                $pan_card_path = '';
                $gst_form_path = '';
                $vendor_stamp_path = '';
                $ndc_form_path ='';
                $health_doc_path ='';
                $cancelled_check_path ='';
                $biodata_path ='';

                if($file5 = $request->hasFile('cancelled_check')){
                    $file5 = $request->file('cancelled_check') ;
                    $fileName5 = date('YmdHis')."_".$file5->getClientOriginalName();
                    $destinationPath5 = $folder;
                    $file5->move($destinationPath5,$fileName5);
                    $cancelled_check_path = $destinationPath5.'/'.$fileName5;
                    $assessor->cancelled_check = $cancelled_check_path;
                    $assessor->cancelled_check_status =1;
                }
                if($file = $request->hasFile('health_doc')){
                    $file = $request->file('health_doc') ;
                    $fileName = date('YmdHis')."_".$file->getClientOriginalName();
                    $destinationPath = $folder;
                    $file->move($destinationPath,$fileName);
                    $health_doc_path = $destinationPath.'/'.$fileName;
                    $assessor->health_doc = $health_doc_path;
                    $assessor->health_doc_status =1;
                }
                if($file1 = $request->hasFile('gst_form')){
                    $file1 = $request->file('gst_form') ;
                    $fileName1 = date('YmdHis')."_".$file1->getClientOriginalName();
                    $destinationPath1 = $folder;
                    $file1->move($destinationPath1,$fileName1);
                    $gst_form_path = $destinationPath1.'/'.$fileName1;
                    $assessor->gst_form = $gst_form_path;
                    $assessor->gst_form_status =1;
                }
                if($file2 = $request->hasFile('vendor_stamp')){
                    $file2 = $request->file('vendor_stamp') ;
                    $fileName2 = date('YmdHis')."_".$file2->getClientOriginalName();
                    $destinationPath2 = $folder;
                    $file2->move($destinationPath2,$fileName2);
                    $vendor_stamp_path = $destinationPath2.'/'.$fileName2;
                    $assessor->vendor_stamp = $vendor_stamp_path;
                    $assessor->vendor_stamp_status =1;
                }
                if($file3 = $request->hasFile('ndc_form')){
                    $file3 = $request->file('ndc_form') ;
                    $fileName3 = date('YmdHis')."_".$file3->getClientOriginalName();
                    $destinationPath3 = $folder;
                    $file3->move($destinationPath3,$fileName3);
                    $ndc_form_path = $destinationPath3.'/'.$fileName3;
                    $assessor->ndc_form = $ndc_form_path;
                    $assessor->ndc_form_status =1;
                }
                if($file4 = $request->hasFile('pan')){
                    $file4 = $request->file('pan') ;
                    $fileName4 = date('YmdHis')."_".$file4->getClientOriginalName();
                    $destinationPath4 = $folder;
                    $file4->move($destinationPath4,$fileName4);
                    $pan_card_path = $destinationPath4.'/'.$fileName4;
                    $assessor->pan = $pan_card_path;
                    $assessor->pan_status =1;
                }
                if($file6 = $request->hasFile('biodata')){
                    $file6 = $request->file('biodata') ;
                    $fileName6 = date('YmdHis')."_".$file6->getClientOriginalName();
                    $destinationPath6 = $folder;
                    $file6->move($destinationPath6,$fileName6);
                    $biodata_path = $destinationPath6.'/'.$fileName6;
                    $assessor->biodata = $biodata_path;
                    $assessor->biodata_status =1;
                }
                // dd($request->email);
                //documents uploaded
                if($assessor->save()) {
                    DB::commit();
                    if($request->final_submit_value==1){
                        if(isset($assessor->name) && isset($assessor->email) && isset($assessor->mobile) && isset($assessor->status) && isset($assessor->health_doc) && isset($assessor->gst_form) && isset($assessor->vendor_stamp) && isset($assessor->ndc_form) && isset($assessor->cancelled_check) && isset($assessor->bank_name) && isset($assessor->acc_no) && isset($assessor->ifsc_code) && isset($assessor->branch_name) && isset($assessor->category) && isset($assessor->state) && isset($assessor->em_contact_name) && isset($assessor->pan)){
                            $assessor->profile_updated = 1;
                            $assessor->save();
                            DB::commit();
                            return response()->json(['status' => 'success','final_submit' => true, 'message' => 'Assessor Updated Successfully!']);
                        }
                        else{
                            DB::commit();
                            return response()->json(['status' => 'error', 'message' => 'Please fill all the fields!']);
                        }
                    }
                    return response()->json(['status' => 'success', 'message' => 'Data updated Successfully!']);
                }
            // }
        } catch (\Throwable $th) {
            DB::rollBack();
            return($th);
            return response()->json(['status' => 'error', 'message' => 'Some error occurred! Please try again after sometime.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Assessor  $assessor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Assessor $assessor)
    {
        /*  if ($assessor->delete()) {
            return response()->json(['success' => true, 'message' => 'Delete Successful!']);
        }
        return response()->json(['success' => true, 'message' => 'Some error occurred. Please try again after sometime!']); */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Assessor  $assessor
     * @return \Illuminate\Http\Response
     */
    public function statusUpdateBlock(Request $request)
    {
        $assessors = Assessor::whereIn('id', $request->assessor_id)->get();

        $validator = Validator::make($request->all(), [
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->errors()->toArray()]);
        }

        try{
            DB::beginTransaction();
            foreach($assessors as $assessor)
            {
                $details = [
                    'name' => $assessor->name,
                    'email' => $assessor->email,
                    'mobile' => $assessor->mobile
                ]; 

                if(($assessor->status) != ($request->status)){
                    if($request->status == 0){
                        Notification::send($assessor, (new AccountDeactivatedAssessorNotification($details)));
                    }
                    else{
                        Notification::send($assessor, (new AccountActivatedAssessorNotification($details)));
                    }
                    Assessor::where('id',$assessor->id)->update(['status' => $request->status, 'status_updated_by' => Auth::user()->id, 'status_updated_at' => date('Y-m-d h:i:s')]);
                }
            }
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Status update Successfull!']);
        }
        catch(\Throwable $th){
            DB::rollback();
            return response()->json(['success' => false, 'message' => 'Some Error Occurred. Please try again after sometime!']);
        }
    }

    public function approve(Assessor $assessor, Request $request)
    {
        $response = [
            'success' => false,
            'message' => 'Some error occurred. Please try again after sometime!'
        ];
        $message = 'Assessor has been Approved.';
        try {
            DB::beginTransaction();
            if ($assessor->verification_status == 0) {
                $assessor->verification_status = $request->status;
                if ($request->status == 2) {
                    $message = "Assessor Disapproved successfully";
                }
                $randomPassword = passwordGeneration(8);
                $assessor->remark = $request->remark;
                $assessor->verification_done_by = Auth::user()->id;
                $assessor->verification_done_at = date('Y-m-d h:i:s');
                $remark = $request->remark;
                if ($assessor->save()) {
                    $assessor_log = new AssessorLog();
                    $assessor_log->assessor_id = $assessor->id;
                    $assessor_log->verified_status = $assessor->verification_status;
                    $assessor_log->verified_remark = $assessor->remark;
                    $assessor_log->verified_at = date('Y-m-d h:i:s');
                    $assessor_log->verified_by = Auth::user()->id;
                    $assessor_log->save();
                    if($request->status == 2){
                        $assessor->notify(new AssessorDisapprovedNotification($remark));
                        $response = [
                            'success' => true,
                            'message' => $message,
                        ];
                    }
                    else{
                        $assessor->notify(new AssessorApprovedNotification());
                        $response = [
                            'success' => true,
                            'message' => $message,
                        ];
                    }
                }
            } else {
                if ($request->status != $assessor->verification_status) {
                    $assessor->verification_status = $request->status;
                    $assessor->remark = $request->remark;
                    $assessor->verification_done_by = Auth::user()->id;
                    $assessor->verification_done_at = date('Y-m-d h:i:s');
                    $remark = $request->remark;
                    if ($request->status == 2) {
                        $message = "Assessor disapproved successfully";
                    }
                    if ($assessor->save()) {
                        $assessor_log = new AssessorLog();
                        $assessor_log->assessor_id = $assessor->id;
                        $assessor_log->verified_status = $assessor->verification_status;
                        $assessor_log->verified_remark = $assessor->remark;
                        $assessor_log->verified_at = date('Y-m-d h:i:s');
                        $assessor_log->verified_by = Auth::user()->id;
                        $assessor_log->save();
                        if($request->status == 2){
                            $assessor->notify(new AssessorDisapprovedNotification($remark));
                            $response = [
                                'success' => true,
                                'message' => $message,
                            ];
                        }
                        else{
                            $assessor->notify(new AssessorApprovedNotification());
                            $response = [
                                'success' => true,
                                'message' => $message,
                            ];
                        }
                        $response = [
                            'success' => true,
                            'message' => $message,
                        ];
                    }
                } else {
                    $response['message'] = 'Assessor is already approved';
                    if ($request->status == 2) {
                        $response['message'] = 'Assessor is already disapproved';
                    }
                }
            }
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
        }
        return response()->json($response, 200);
    }

    public function addEditAssessor(Request $request, $id)
    {
        $this->data['assessor'] = Assessor::where('id',$id)->first();
        $this->data['states'] = States::orderBy('name','asc')->get();
        $this->data['grades'] = AssessorGrade::orderBy('name','asc')->get();
        $this->data['categories'] = AssessorCategoryRepo::allActive($orderColumn="created_at", $orderDir='desc');
        $this->data['title'] = 'Assessor Management';
        $this->data['pageTitle'] = 'Assessor';

        return parent::adminView('assessor.addEditAssessor', $this->data);
    }

    public function nda(Request $request, $id)
    {
        $this->data['assessor'] = Assessor::where('id',$id)->first();
        $this->data['title'] = 'Assessor Management';
        $this->data['pageTitle'] = 'Assessor';

        return parent::adminView('assessor.nda', $this->data);
    }

    public function exportBlock(Request $request)
    {
        $assessor = Assessor::leftjoin('states','states.id','=','assessors.state')
                                ->select('assessors.*','states.name as state_name')
                                ->orderBy('assessors.id', 'DESC');
        if(!is_null($request->name)) $assessor = $assessor->where('name', 'LIKE', "%{$request->name}%");
        if(!is_null($request->email)) $assessor = $assessor->where('email', 'LIKE', "%{$request->email}%");
        if(!is_null($request->mobile)) $assessor = $assessor->where('mobile',$request->mobile);
        if(!is_null($request->status)) $assessor = $assessor->where('status', $request->status);
        if(!is_null($request->verificationStatus)) $assessor = $assessor->where('verification_status',$request->verificationStatus);

        $assessor = $assessor->get();

        return Excel::download(new AccessorExport($assessor), 'assessors.xlsx');
    }
}
