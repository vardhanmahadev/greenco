<?php

namespace App\Http\Controllers\Admin;

use DB;
use Auth;
use File;
use Hash;
use Session;
use Validator;
use App\Models\MetaId;
use App\Models\Company;
use Illuminate\Http\Request;

//Models
use Yajra\Datatables\Datatables;


//Repositories
use Facades\App\Repository\EntityRepo;
use Facades\App\Repository\SectorRepo;
//Models
use App\Models\States;
use App\Http\Controllers\BaseController;

//Notifications
use Facades\App\Repository\IndustryRepo;
use Illuminate\Support\Facades\Notification;
use App\Notifications\SaveCompanyNotification;
use App\Notifications\CompanyDisapprovedNotification;
use App\Notifications\CompanyRegistrationNotification;
use App\Notifications\AccountActivatedCompanyNotification;
use App\Notifications\AccountDeactivatedCompanyNotification;

class CompanyController extends BaseController
{
    public $data = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['activeMenu'] = 'company';
    }

    public function index(Request $request)
    {
        $this->data['datatable_listing'] = true;
        $this->data['title'] = 'Company Management';
        $this->data['pageTitle'] = 'Company';

        return parent::adminView('company.index', $this->data);
    }

    public function data(Request $request)
    {
        $company = Company::orderBy('id', 'DESC');

        return  Datatables::of($company)
            ->addIndexColumn()
            ->filter(function ($query) use ($request) {
                if ($request->has('reg_id') && $request->reg_id != "") {
                    $query->where('reg_id', 'like', "%{$request->get('reg_id')}%");
                }
                if ($request->has('name') && $request->name != "") {
                    $query->where('name', 'like', "%{$request->get('name')}%");
                }
                if ($request->has('mobile') && $request->mobile != "") {
                    $query->where('mobile', 'like', "%{$request->get('mobile')}%");
                }
                if ($request->has('email') && $request->email != "") {
                    $query->where('email', 'like', "%{$request->get('email')}%");
                }

                if ($request->has('status') && $request->status != "") {
                    $query->where('account_status', $request->status);
                }

                if ($request->has('verification_status') && $request->verification_status != "") {
                    $query->where('verified_status', $request->verification_status);
                }

                if ($request->has('search') && @$request->search['value'] != "") {
                    $query->where('mobile', 'like', "%{$request->search['value']}%");
                    $query->orWhere('email', 'like', "%{$request->search['value']}%");
                    $query->orWhere('name', 'like', "%{$request->search['value']}%");
                }
            })
            ->make(true);
    }

    public function show(Company $company)
    {
        $this->data['title'] = 'Company Management';
        $this->data['pageTitle'] = 'Company';
        $this->data['company'] = $company;

        $this->data['sectors'] = SectorRepo::allActive($orderColumn="created_at", $orderDir='desc');
        $this->data['industries'] = IndustryRepo::allActive($orderColumn="created_at", $orderDir='desc');
        $this->data['entities'] = EntityRepo::allActive($orderColumn="created_at", $orderDir='desc');
        $this->data['states'] = States::orderBy('name','asc')->get();

        return parent::adminView('company.addEditCompany', $this->data);
    }

    public function addEditCompany()
    {
        $this->data['title'] = 'Company Management';
        $this->data['pageTitle'] = 'Company';

        $this->data['sectors'] = SectorRepo::allActive($orderColumn="created_at", $orderDir='desc');
        $this->data['industries'] = IndustryRepo::allActive($orderColumn="created_at", $orderDir='desc');
        $this->data['entities'] = EntityRepo::allActive($orderColumn="created_at", $orderDir='desc');
        $this->data['states'] = States::orderBy('name','asc')->get();

        return parent::adminView('company.addEditCompany', $this->data);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        try {
            if($request->tab==1){
                $validator = Validator::make($request->all(), [
                    "company_name" => 'required|min:3|max:50',
                    "company_email" => 'required|email:rfc,dns|unique:companies,email,',
                    "company_mobile" => 'required|digits:10|starts_with: 6,7,8,9|unique:companies,mobile',
                    "company_location" => 'required|regex:/^[a-zA-Z\s]*$/',
                    "state" => "required",
                    "postal_address" => 'required|string',
                    "postal_address_pincode" => 'required',
                    "billing_address" => 'required|string',
                    "billing_address_pincode" => 'required',
                ], [
                    "company_name.required" => 'This field is required.',
                    "company_name.max" => 'Maximum of 50 characters allowed.',
                    "company_email.required" => 'This field is required.',
                    "company_mobile.required" => 'This field is required.',
                    "state.required" => 'This field is required',
                    "company_location.required" => 'This field is required.',
                    "postal_address.required" => 'This field is required.',
                    "billing_address.required" => 'This field is required.',
                    "postal_address_pincode.required" => 'This field is required.',
                    "billing_address_pincode.required" => 'This field is required.',
                ]);

                if ($validator->fails()) {
                    return response()->json(['status' => 'validations', 'errors' => $validator->errors()->toArray()]);
                }

                    DB::beginTransaction();

                    $company = new Company;
                    $company->name = $request->company_name;
                    $company->email = $request->company_email;
                    $company->mobile  = $request->company_mobile;
                    $company->location  = $request->company_location;
                    $company->postal_address  = $request->postal_address;
                    $company->state  = $request->state;
                    $company->billing_address  = $request->billing_address;
                    $company->postal_address_pincode  = $request->postal_address_pincode;
                    $company->billing_address_pincode  = $request->billing_address_pincode;

                    $company->verified_by = Auth::user()->id;
                    $company->verified_at = date('Y-m-d H:i:s');
                    $company->created_by = Auth::user()->id;
                    $password = passwordGeneration(8);
                    if(isset($request->account_approve)){
                        $company->reg_id = __generateNewCompanyId();
                        $company->account_status = 1;
                        $company->verified_status = 1;
                        $company->password = Hash::make($password);
                    }
                    if ($company->save()) {
                        if(isset($request->account_approve))
                        {
                            $val = MetaId::where('id',1)->select('value')->first();
                            MetaId::where('id',1)->update(['value' => $val->value+1, 'latest_value' => $company->reg_id]);
                        }

                    //     $folder = ('uploads/companies/'.$company->id);
                    //     ## CREATE FOLDER
                    //     if (!File::exists($folder)) {
                    //         mkdir($folder, 0777, true);
                    //         chmod($folder, 0777);
                    //     }
                    //     $proposal_doc_path = '';
                    //     $po_doc_path = '';

                    //     if ($file = $request->hasFile('proposal_doc')) {
                    //         $file = $request->file('proposal_doc') ;
                    //         $fileName = date('YmdHis')."_".$file->getClientOriginalName();
                    //         $destinationPath = $folder;
                    //         $file->move($destinationPath,$fileName);
                    //         $proposal_doc_path = $destinationPath.'/'.$fileName;
                    //     }
                    //     if ($file1 = $request->hasFile('po_doc')) {
                    //         $file1 = $request->file('po_doc') ;
                    //         $fileName1 = date('YmdHis')."_".$file1->getClientOriginalName();
                    //         $destinationPath1 = $folder;
                    //         $file1->move($destinationPath1,$fileName1);
                    //         $po_doc_path = $destinationPath1.'/'.$fileName1;
                    //     }

                    //     $company->update([
                    //         'proposal_document' => $proposal_doc_path,
                    //         'po_document' => $po_doc_path
                    //     ]);

                        DB::commit();
                        if($request->account_status==1){
                            $company->notify(new CompanyRegistrationNotification($password));
                        }
                        else{
                            $company->notify(new SaveCompanyNotification($password));
                        }
                        return response()->json(['status' => 'success', 'id'=> $company->id, 'message' => 'Company added successfully!']);
                    }
            }
            if($request->tab==4){
                DB::beginTransaction();

                if(isset($request->tanno)){
                    Company::where('id',$request->company_id)->update(['tanno' => $request->tanno]);
                }
                if(isset($request->panno)){
                    Company::where('id',$request->company_id)->update(['panno' => $request->panno]);
                }
                if(isset($request->gstinno)){
                    Company::where('id',$request->company_id)->update(['gstno' => $request->gstinno]);
                }

                DB::commit();
                return response()->json(['status' => 'success','message' => 'Company added successfully!']);
            }
            else{
                    DB::beginTransaction();
                    // dd($request->all());
                    if(isset($request->industry)){
                        Company::where('id',$request->company_id)->update(['mst_industry_id' => $request->industry]);
                    }
                    if(isset($request->entity)){
                        Company::where('id',$request->company_id)->update(['mst_entity_id' => $request->entity]);
                    }
                    if(isset($request->sector)){
                        Company::where('id',$request->company_id)->update(['mst_sector_id' => $request->sector]);
                    }
                    if(isset($request->name_of_product)){
                        Company::where('id',$request->company_id)->update(['product_name' => $request->name_of_product]);
                    }
                    if(isset($request->turnover)){
                        Company::where('id',$request->company_id)->update(['turnover' => $request->turnover]);
                    }
                    if(isset($request->budget_allocation)){
                        Company::where('id',$request->company_id)->update(['budget_allocation' => $request->budget_allocation]);
                    }
                    if(isset($request->no_of_permanent_employees)){
                        Company::where('id',$request->company_id)->update(['no_of_employees' => $request->no_of_permanent_employees]);
                    }
                    if(isset($request->no_of_contract_employees)){
                        Company::where('id',$request->company_id)->update(['contract_employees' => $request->no_of_contract_employees]);
                    }
                    if(isset($request->electrical_energy)){
                        Company::where('id',$request->company_id)->update(['electrical_consumption' => $request->electrical_energy]);
                    }
                    if(isset($request->thermal_energy)){
                        Company::where('id',$request->company_id)->update(['thermal_consumption' => $request->thermal_energy]);
                    }
                    if(isset($request->water_consumption)){
                        Company::where('id',$request->company_id)->update(['water_consumption' => $request->water_consumption]);
                    }
                    if(isset($request->total_area)){
                        Company::where('id',$request->company_id)->update(['area' => $request->total_area]);
                    }
                    // dd($request->all());
                    DB::commit();
                    return response()->json(['status' => 'success', 'id'=> $request->company_id , 'message' => 'Company added successfully!']);
            }
        } catch (\Throwable $th) {
            DB::rollback();
            return $th;
            return response()->json(['status' => 'error', 'message' => 'Company add failed!']);
        }
    }

    public function update(Request $request, Company $company)
    {
        try{
            if($request->tab==1){
                $validator = Validator::make($request->all(), [
                    "company_name" => 'required|min:3|max:50',
                    "company_email" => 'required|email:rfc,dns',
                    "company_mobile" => 'required|digits:10|starts_with: 6,7,8,9',
                    "company_location" => 'required|regex:/^[a-zA-Z\s]*$/',
                    "state" => "required",
                    "account_status" => 'required',
                    "postal_address" => 'required|string',
                    "postal_address_pincode" => 'required',
                    "billing_address" => 'required|string',
                    "billing_address_pincode" => 'required',
                ], [
                    "company_name.required" => 'This field is required.',
                    "company_name.max" => 'Maximum of 50 characters allowed.',
                    "company_email.required" => 'This field is required.',
                    "company_mobile.required" => 'This field is required.',
                    "state.required" => 'This field is required',
                    "company_location.required" => 'This field is required.',
                    "postal_address.required" => 'This field is required.',
                    "billing_address.required" => 'This field is required.',
                    "postal_address_pincode.required" => 'This field is required.',
                    "billing_address_pincode.required" => 'This field is required.',
                ]);

                if(!empty($request->company_email)) {
                    $isExist = Company::where('email',$request->company_email)->where('id', '<>', $company->id)->exists();
                    if($isExist) {
                        $validator->after(function ($validator) {
                            $validator->errors()->add(
                                'email', 'Email already exists!'
                            );
                        });
                    }
                }

                if(!empty($request->company_mobile)) {
                    $isExist = Company::where('mobile',$request->company_mobile)->where('id', '<>', $company->id)->exists();
                    if($isExist) {
                        $validator->after(function ($validator) {
                            $validator->errors()->add(
                                'mobile', 'Mobile already exists!'
                            );
                        });
                    }
                }

                if ($validator->fails()) {
                    return response()->json(['status' => 'validations', 'errors' => $validator->errors()->toArray()]);
                }

                DB::beginTransaction();
                if(isset($request->company_name)){
                    Company::where('id',$company->id)->update(['name' => $request->company_name]);
                }
                if(isset($request->company_email)){
                    Company::where('id',$company->id)->update(['email' => $request->company_email]);
                }
                if(isset($request->company_mobile)){
                    Company::where('id',$company->id)->update(['mobile' => $request->company_mobile]);
                }
                if(isset($request->account_status)){
                    Company::where('id',$company->id)->update(['account_status' => $request->account_status]);
                }
                if(isset($request->company_location)){
                    Company::where('id',$company->id)->update(['location' => $request->company_location]);
                }
                if(isset($request->state)){
                    Company::where('id',$company->id)->update(['state' => $request->state]);
                }
                if(isset($request->postal_address)){
                    Company::where('id',$company->id)->update(['postal_address' => $request->postal_address]);
                }
                if(isset($request->postal_address_pincode)){
                    Company::where('id',$company->id)->update(['postal_address_pincode' => $request->postal_address_pincode]);
                }
                if(isset($request->billing_address)){
                    Company::where('id',$company->id)->update(['billing_address' => $request->billing_address]);
                }
                if(isset($request->billing_address_pincode)){
                    Company::where('id',$company->id)->update(['billing_address_pincode' => $request->billing_address_pincode]);
                }
                DB::commit();
            }
            // dd($request->tab);
            if($request->tab==4){
                DB::beginTransaction();

                if(isset($request->tanno)){
                    Company::where('id',$company->id)->update(['tanno' => $request->tanno]);
                }
                if(isset($request->panno)){
                    Company::where('id',$company->id)->update(['panno' => $request->panno]);
                }
                if(isset($request->gstinno)){
                    Company::where('id',$company->id)->update(['gstno' => $request->gstinno]);
                }

                DB::commit();
                return response()->json(['status' => 'success','message' => 'Company added successfully!']);
            }
            else{
                DB::beginTransaction();

                if(isset($request->industry)){
                    Company::where('id',$company->id)->update(['mst_industry_id' => $request->industry]);
                }
                if(isset($request->entity)){
                    Company::where('id',$company->id)->update(['mst_entity_id' => $request->entity]);
                }
                if(isset($request->sector)){
                    Company::where('id',$company->id)->update(['mst_sector_id' => $request->sector]);
                }
                if(isset($request->name_of_product)){
                    Company::where('id',$company->id)->update(['product_name' => $request->name_of_product]);
                }
                if(isset($request->turnover)){
                    Company::where('id',$company->id)->update(['turnover' => $request->turnover]);
                }
                if(isset($request->budget_allocation)){
                    Company::where('id',$company->id)->update(['budget_allocation' => $request->budget_allocation]);
                }
                if(isset($request->no_of_permanent_employees)){
                    Company::where('id',$company->id)->update(['no_of_employees' => $request->no_of_permanent_employees]);
                }
                if(isset($request->no_of_contract_employees)){
                    Company::where('id',$company->id)->update(['contract_employees' => $request->no_of_contract_employees]);
                }
                if(isset($request->electrical_energy)){
                    Company::where('id',$company->id)->update(['electrical_consumption' => $request->electrical_energy]);
                }
                if(isset($request->thermal_energy)){
                    Company::where('id',$company->id)->update(['thermal_consumption' => $request->thermal_energy]);
                }
                if(isset($request->water_consumption)){
                    Company::where('id',$company->id)->update(['water_consumption' => $request->water_consumption]);
                }
                if(isset($request->total_area)){
                    Company::where('id',$company->id)->update(['area' => $request->total_area]);
                }
                DB::commit();
                return response()->json(['status' => 'success','message' => 'Company updated successfully!']);
            }
        }catch (\Throwable $th) {
            DB::rollback();
            return response()->json(['status' => 'error', 'message' => 'Company add failed!']);
        }

    }

    public function deleteCompany(Request $request)
    {
        dd($request->all());
    }

    public function status_change(Request $request)
    {
        $companies = Company::whereIn('id', $request->company_id)->get();

        $validator = Validator::make($request->all(), [
            'status' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->errors()->toArray()]);
        }

        try{
            DB::beginTransaction();
            foreach($companies as $company)
            {
                if(($company->account_status) != ($request->status)){
                    if($request->status == 0){
                        Notification::send($company, (new AccountDeactivatedCompanyNotification()));
                    }
                    else{
                        Notification::send($company, (new AccountActivatedCompanyNotification()));
                    }
                    Company::where('id',$company->id)->update(['account_status' => $request->status,'updated_by' => Auth::user()->id, 'status_updated_by' => Auth::user()->id, 'status_updated_at' => date('Y-m-d h:i:s')]);
                }
            }
            DB::commit();
            return response()->json(['success' => true, 'message' => 'Status update Successfull!']);
        }
        catch(\Throwable $th){
            DB::rollback();
            return response()->json(['success' => false, 'message' => 'Some Error Occurred. Please try again after sometime!']);
        }
    }

    public function approve(Company $company, Request $request)
    {
        $response = [
            'success' => false,
            'message' => 'Some error occurred. Please try again after sometime!'
        ];
        $message = 'Company has been Approved.';
        try {
            DB::beginTransaction();
            if ($company->verified_status == 0) {

                $company->verified_status = $request->status;
                if ($request->status == 2) {
                    $message = "Company Disapproved successfully";
                }
                $randomPassword = passwordGeneration(8);
                // $company->password = Hash::make($randomPassword);
                $remark = $request->remark;
                if ($company->save()) {
                    if($request->status == 2){
                        $company->notify(new CompanyDisapprovedNotification($remark));

                        $response = [
                            'success' => true,
                            'message' => $message,
                        ];
                    }
                    else{
                        $val = MetaId::where('id',1)->select('value')->first();
                        $company->notify(new CompanyRegistrationNotification($randomPassword));
                        Company::where('id',$company->id)->update(['reg_id' => __generateNewCompanyId()]);
                        $reg_id = Company::where('id',$company->id)->select('reg_id')->first();
                        MetaId::where('id',1)->update(['value' => $val->value+1, 'latest_value' => $reg_id->reg_id]);
                        $response = [
                            'success' => true,
                            'message' => $message,
                        ];
                    }
                }
            } else {
                $randomPassword = passwordGeneration(8);
                if ($request->status != $company->verified_status) {
                    $company->verified_status = $request->status;
                    $remark = $request->remark;
                    if ($request->status == 2) {
                        $message = "company disapproved successfully";
                    }
                    if ($company->save()) {
                        if($request->status == 2){
                            $company->notify(new CompanyDisapprovedNotification($remark));
                            $response = [
                                'success' => true,
                                'message' => $message,
                            ];
                        }
                        else{
                            $company->notify(new CompanyRegistrationNotification($randomPassword));
                            $val = MetaId::where('id',1)->select('value')->first();
                            Company::where('id',$company->id)->update(['reg_id' => __generateNewCompanyId()]);
                            $reg_id = Company::where('id',$company->id)->select('reg_id')->first();
                            MetaId::where('id',1)->update(['value' => $val->value+1, 'latest_value' => $reg_id->reg_id]);
                            $response = [
                                'success' => true,
                                'message' => $message,
                            ];
                        }
                        $response = [
                            'success' => true,
                            'message' => $message,
                        ];
                    }
                } else {
                    $response['message'] = 'Company is already approved';
                    if ($request->status == 2) {
                        $response['message'] = 'Company is already disapproved';
                    }
                }
            }
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
        }
        return response()->json($response, 200);
    }
}
