<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

class DashboardController extends BaseController
{
    public $data = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['activeMenu'] = 'dashboard';
    }

    public function index(Request $request)
    {
        $this->data['pageTitle'] = 'Dashboard';
        return parent::adminView('dashboard', $this->data);
    }
}