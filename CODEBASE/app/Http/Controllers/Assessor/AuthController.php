<?php

namespace App\Http\Controllers\Assessor;
use Str;

use Mail;
use Redirect;
use Validator;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Admin;
use App\Models\Assessor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

//Models
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Mail\ForgotPasswordUserEmail;

//Mail
use App\Http\Controllers\BaseController;
use App\Notifications\SaveAssessorNotification;
use App\Notifications\ForgotPasswordNotification;
use App\Notifications\AssessorActivatedTokenPasswordNotification;

//Repos


class AuthController extends BaseController
{
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/assessor/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:assessor')->except('logout');
    }

    protected function credentials(Request $request)
    {
        if (filter_var($request->email, FILTER_VALIDATE_EMAIL))
            return ['email' => $request->email, 'password' => $request->password];
    }

    public function logout()
    {
        Auth::guard('assessor')->logout();

        return redirect()
            ->route('assessor.login')
            ->with(['toast' => '1', 'status' => 'success', 'title' => 'Thank You', 'message' => 'Thank you. You have been succesfully logged out']);
    }

    public function register(Request $request)
    {
        if($request->register)
        {
                // @dd($request->all());
                $validator = Validator::make($request->all(), [
                    'email' => "required|email:rfc,dns|unique:assessors,email",
                    'name' => 'required|regex:/^[a-zA-Z\s]+$/u|min:3|max:50',
                    'mobileno' => "required|numeric|regex:/^[6-9]\d{9}$/|digits:10|unique:assessors,mobile",
                ], [
                    'name.required' => 'Please enter your name',
                    'name.regex' => 'Please enter a valid name',
                    'email.email' => 'Please enter a valid email',
                    'email.required' => 'Please enter your email',
                    'mobileno.required' => 'Please enter your mobile',
                    'mobileno.starts_with' => 'Please enter valid mobile',
                    'email.unique' => 'The account already exists with this email address',
                    'mobileno.unique' => 'The account already exists with this mobile number'
                ]);

                
                if ($validator->fails()) {
                    
                    return response()->json(['status' => 'errors', 'errors' => $validator->errors()->toArray()])->withCallback($request->input('callback'));
                }
                
                DB::beginTransaction();
                try
                {
    
                    $assesorval = new Assessor();
                    $assesorval->name = ucwords($request->name);
                    $assesorval->mobile = $request->mobileno;
                    $assesorval->email = $request->email;
                    $assesorval->status = 0;
                    $assesorval->save();
    
                    __account_activation($request->email,'assessor');
    
                    DB::commit();
                    return response()->json(['status' => 'success', 'message' => 'Assessor Registered Successfull.']);
    
               }
                catch(\Exception $e) {
                    DB::rollback();
                   return $e;
                   return response()->json(['status' => 'error', 'message' => 'Something went wrong. Please try again.']);
       
                }
    
            
        }

        $this->data ['pageTitle'] = 'Register' ;

        return parent::assessorView('auth.register', $this->data);
    }

    public function registerSuccess(Request $request)
    {
        $this->data ['pageTitle'] = 'Register Success' ;
        
        return parent::assessorView('auth.registerSuccess', $this->data);
    }

    public function accountActivated(Request $request)
    {
        $this->data ['pageTitle'] = 'Account Activation' ;
        
        return parent::assessorView('auth.accountActivated', $this->data);
    }

    
    public function verify($token = null)
    {
        if(!$token)
            return redirect()->route('assessor.login')->with(['toast' => '1', 'status' => 'error', 'title' => 'Authentication', 'message' => 'Invalid token!']);

        $assessor = Assessor::where('email_verification_token',$token)->first();
        if(!$assessor)
            return redirect()->route('assessor.login')->with(['toast' => '1', 'status' => 'error', 'title' => 'Authentication', 'message' => 'Account Already Activated']);

        // update account status
        $randomPassword = passwordGeneration(6);
        $assessor->password = Hash::make($randomPassword);
        $assessor->email_verification_token = null;
        $assessor->email_verified_at = Carbon::now();
        $assessor->status_updated_at = Carbon::now();
        $assessor->status = 1;

        if($assessor->save()) {
            $details = [
                'email' => $assessor->email,
                'password' => $randomPassword
            ];
            $assessor->notify(new AssessorActivatedTokenPasswordNotification($details));

            return redirect()->route('assessor.accountActivated');
            // ->with(['toast' => '1', 'status' => 'success', 'title' => 'Authentication', 'message' => 'Account Activated successfully!']);
        }
    }

    public function login(Request $request)
    {
        // dd(Hash::make('Cii@1234'));
        if ($request->isMethod('POST')) {

            $validator = Validator::make(
                $request->all(),
                [
                    'email' => 'required|email:rfc,dns',
                    'password'  => 'required',
                ],
                [
                    'email.email' => 'Please enter a valid email address',
                    'email.required' => 'Please enter your email address',
                    'password.required' => 'Please enter your password'
                ]
            );

            if ($validator->fails()) {
                return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
            }
            $check = Assessor::where(['email' => $request->email])->first();

            if($check) {
                if( $check->status =='1' )
                {
                    if (Auth::guard('assessor')->attempt(['email' => $request->email, 'password' => $request->password])) 
                    {
                        session(['userId' =>$check->id, 'email' => $check->email]);
                        return redirect()->route('assessor.dashboard')->with(['toast' => '1', 'status' => 'success', 'title' => 'Authentication', 'message' => 'Welcome Back!']);
                  
                    }
                    else
                        return redirect()->route('assessor.login')->with(['toast' => '1', 'status' => 'error', 'title' => 'Authentication', 
                        'message' => 'Your credentials are not valid! Please enter a valid Email and Password.'])->withInput();
                }
                elseif($check->status =='0' )
                {
                    Auth::guard('assessor')->logout();
                    return redirect()->route('assessor.login')->with(['toast' => '1', 'status' => 'error', 'title' => 'Authentication', 'message' => 'Account In-Active! Please Contact Greenco Team.'])->withInput();
                }
               
                elseif( $check->verification_status =='2')
                {
                    Auth::guard('assessor')->logout();
                    return redirect()->route('assessor.login')->with(['toast' => '1', 'status' => 'error', 'title' => 'Authentication', 'message' => 'Account is Disapproved! Please Contact Greenco Team.'])->withInput();
                }
            } else {
                return redirect()->route('assessor.login')->with(['toast' => '1', 'status' => 'error', 'title' => 'Authentication', 'message' => 'No Account Found! Please enter a valid Email Address.'])->withInput();
            }
        }

        $data = [
            'pageTitle' => 'Login'
        ];

        return parent::assessorView('auth.login', $data);
    }

    public function forgotPasswordPage()
    {
        $data = [
            'pageTitle' => 'Forgot Password'
        ];

        return parent::assessorView('auth.forgotPassword', $data);
    }
    public function forgotPassword(Request $request)
    {

        // if ($request->submit) {
            $validator = Validator::make(
                $request->all(),
                [
                    'email' => 'required|email:rfc,dns'
                ],
                [
                    'email.required' => 'Please Enter Email Address',
                    'email.email' => 'Please Enter a Valid Email Address'
                ]
            );

            $user = Assessor::where('email', $request->email)->first();
            if ($user) {
                if($user->status =='0' )
                {
                    // return response()->json(['status' => 'error', 'message' => "Account In-Active! Please Contact Greenco Team."]);
                    
                            $validator->after(function ($validator) {
                                $validator->errors()->add(
                                    'email', 'Account In-Active! Please Contact Greenco Team.'
                                );
                            });
                    
                }
                
                elseif( $user->verification_status =='2')
                {
                    Auth::guard('assessor')->logout();
                    $validator->after(function ($validator) {
                        $validator->errors()->add(
                            'email', 'Account is Disapproved! Please Contact Greenco Team.'
                        );
                    });
                    // return response()->json(['status' => 'error', 'message' => "Account is Disapproved! Please Contact Greenco Team."]);
                }
            }
            else
            {   
                $validator->after(function ($validator) {
                    $validator->errors()->add(
                        'email', "Account doesn't exist. Please Signup to register."
                    );
                });
                // return response()->json(['status' => 'error', 'message' => "User doesn't exist. Please Signup to register"]);
            }


            if ($validator->fails()) {
                return response()->json(['status' => 'errors', 'errors' => $validator->errors()->toArray()]); // return redirect()->back()->withErrors($validator);
            } else {

                // $user = Assessor::where('email', $request->email)->first();

                if ($user) {

                    

                    $password = passwordGeneration(6);
                    $type = 'assesor';
                    Assessor::where('email', $request->email)
                        ->update(['password' => Hash::make($password)]);
                    $user->notify(new ForgotPasswordNotification($password,$type));

                    DB::commit();
                    return response()->json(['status' => 'success', 'message' => 'Password sent to your email!']);
                } else {
                    DB::rollback();
                    return response()->json(['status' => 'error', 'message' => "User doesn't exist. Please Signup to register"]);
                }
            }



    }
}
