<?php

namespace App\Http\Controllers\Assessor;

use DB;
use File;
use Redirect;
use Validator;
use App\Models\User;
use App\Models\Assessor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mail;
use App\Mail\assessor_documents_reupload;

//Models
use App\Models\States;

//Repos
use Facades\App\Repository\AssessorCategoryRepo;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;

//Mail
use App\Http\Controllers\BaseController;
use App\Notifications\PasswordUpdateNotification;
use App\Notifications\AssessorAccountDetailsChanged;
use App\Notifications\AssessorProfileSubmittedNotification;
// use App\Notifications\AssessorProfileDocumentReuploadNotification;

class AccountController extends BaseController
{
    public $data = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['activeMenu'] = 'account';
        // $this->middleware('guest:assessor')->except('logout')->except('profile');
    }


    public function profile(Request $request)
    {
        $this->data['pageTitle'] = 'Profile';
        $this->data['profile'] = Auth::user();
        $this->data['states'] = States::orderBy('name','asc')->get();
        $this->data['categories'] = AssessorCategoryRepo::allActive($orderColumn="created_at", $orderDir='desc');

        return parent::assessorView('account.profile', $this->data);
    }

   
    public function profiledocs(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cancelled_cheque' => 'required|mimes:png,jpeg,jpg,pdf',
            'gst_doc' => 'required|mimes:png,jpeg,jpg,pdf',
            'vendorstamp_doc' => 'required|mimes:png,jpeg,jpg,pdf',
            'ndc_form' => 'required|mimes:png,jpeg,jpg,pdf'
        ]);



        if ($validator->fails()) {
            return response()->json(['status' => 'errors', 'errors' => $validator->errors()->toArray()]);
        }

        try 
        {
            DB::beginTransaction();
            $assessor = Auth::user();

            // if ($assessor->save()) {
                $folder = ('uploads/assessors/'.$assessor->id);
                ## CREATE FOLDER
                if (!File::exists($folder)) {
                    mkdir($folder, 0777, true);
                    chmod($folder, 0777);
                }
                $pan_card_path = '';
                $gst_form_path = '';
                $vendor_stamp_path = '';
                $ndc_form_path ='';

                if ($file = $request->hasFile('pan_doc')) {
                    $file = $request->file('pan_doc') ;
                    $fileName = date('YmdHis')."_".$file->getClientOriginalName();
                    $destinationPath = $folder;
                    $file->move($destinationPath,$fileName);
                    $pan_card_path = $destinationPath.'/'.$fileName;
                }
                if ($file1 = $request->hasFile('gst_doc')) {
                    $file1 = $request->file('gst_doc') ;
                    $fileName1 = date('YmdHis')."_".$file1->getClientOriginalName();
                    $destinationPath1 = $folder;
                    $file1->move($destinationPath1,$fileName1);
                    $gst_form_path = $destinationPath1.'/'.$fileName1;
                }
                if ($file2 = $request->hasFile('vendorstamp_doc')) {
                    $file2 = $request->file('vendorstamp_doc') ;
                    $fileName2 = date('YmdHis')."_".$file2->getClientOriginalName();
                    $destinationPath2 = $folder;
                    $file2->move($destinationPath2,$fileName2);
                    $vendor_stamp_path = $destinationPath2.'/'.$fileName2;
                }
                if ($file3 = $request->hasFile('ndc_form')) {
                    $file3 = $request->file('ndc_form') ;
                    $fileName3 = date('YmdHis')."_".$file3->getClientOriginalName();
                    $destinationPath3 = $folder;
                    $file3->move($destinationPath3,$fileName3);
                    $ndc_form_path = $destinationPath3.'/'.$fileName3;
                }

                    $assessor->pan_card = $pan_card_path;
                    $assessor->gst_form = $gst_form_path;
                    $assessor->vendor_stamp = $vendor_stamp_path;
                    $assessor->ndc_form = $ndc_form_path;  
                    $assessor->save();
                                  

                // $assessor->notify(new SaveAssessorNotification());
                // }
                DB::commit();
                return response()->json(['status' => 'success', 'message' => 'Assessor added successfully!']);
            
        } catch (\Throwable $th) {
            DB::rollback();
            // return $th;
            return response()->json(['status' => 'error', 'message' => 'Assessor add failed!']);
        }

        // return response()->json(['status' => 'error', 'message' => 'some error occurred try after sometime!']);
    }

    public function profileupdate(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'cancelled_cheque' => 'mimes:png,jpeg,jpg,pdf',
            'gst_doc' => 'mimes:png,jpeg,jpg,pdf',
            'vendorstamp_doc' => 'mimes:png,jpeg,jpg,pdf',
            'ndc_form' => 'mimes:png,jpeg,jpg,pdf',
            'vendorstamp_doc' => 'mimes:png,jpeg,jpg,pdf',
            'pancard_doc' => 'mimes:png,jpeg,jpg,pdf',
            // 'bank_name' =>'regex:/^[a-zA-Z\s]+$/u',
            // 'account_number' =>'regex:/^[a-zA-Z0-9]+$/u',
            // 'ifsc_code' =>'regex:/^[a-zA-Z0-9]+$/u',
            // 'branch_name' =>'regex:/^[a-zA-Z0-9\s]+$/u',
            // 'alt_mobile' =>'regex:/^[0-9\s]+$/u',
            // 'city' =>'regex:/^[a-zA-Z\s]+$/u',
            // 'pincode' =>'regex:/^[0-9\s]+$/u',
            // 'pancardno' =>'regex:/^[a-zA-Z0-9\s]+$/u',
            // 'gstnumber' =>'regex:/^[a-zA-Z0-9\s]+$/u',
            // 'em_contact_name' =>'regex:/^[a-zA-Z\s]+$/u',
            // 'em_contact_no' =>'regex:/^[0-9\s]+$/u',
            // 'em_city' =>'regex:/^[a-zA-Z\s]+$/u',
            // 'em_pincode' =>'regex:/^[0-9\s]+$/u',
        ],
        [
            // 'bank_name.regex' =>'Please Enter a Valid Bank Name',
            // 'account_number.regex' =>'Please Enter a Valid Account Number',
            // 'ifsc_code.regex' =>'Please Enter a Valid IFSC Code',
            // 'branch_name.regex' =>'Please Enter a Valid Branch Name',
            // 'alt_mobile.regex' =>'Please Enter a Valid Alternate Mobile Number',
            // 'city.regex' =>'Please Enter a Valid City',
            // 'pincode.regex' =>'Please Enter a Valid Pincode',
            // 'pancardno.regex' =>'Please Enter a Valid Pancard Number',
            // 'gstnumber.regex' =>'Please Enter a Valid GST Number',
            // 'em_contact_name.regex' =>'Please Enter a Valid Contact Name',
            // 'em_contact_no.regex' =>'Please Enter a Valid Contact Number',
            // 'em_city.regex' =>'Please Enter a Valid City',
            // 'em_pincode.regex' =>'Please Enter a Valid Pincode',
        ]

        );

        if ($validator->fails()) {
            return response()->json(['status' => 'errors', 'errors' => $validator->errors()->toArray()]);
        }

        try 
        {
            DB::beginTransaction();
            $assessor = Auth::user();
            // $assessor = Assessor::find($request->id);
            // dd($request->all());

            // if ($assessor->save()) {
                $folder = ('uploads/assessors/'.$request->id);
                ## CREATE FOLDER
                if (!File::exists($folder)) {
                    mkdir($folder, 0777, true);
                    chmod($folder, 0777);
                }
                $health_form_path ='';
                $pan_card_path = '';
                $gst_form_path = '';
                $vendor_stamp_path = '';
                $ndc_form_path ='';
                $cancelled_cheque_path = '';
                $bio_data_path = '';

              
                if ($file = $request->hasFile('pancard_doc')) {
                    $file = $request->file('pancard_doc') ;
                    $fileName = date('YmdHis')."_".$file->getClientOriginalName();
                    $destinationPath = $folder;
                    $file->move($destinationPath,$fileName);
                    $pan_card_path = $destinationPath.'/'.$fileName;
                    $assessor->pan = $pan_card_path;
                }
                                     
                if ($file1 = $request->hasFile('cancelled_cheque')) {
                    $file1 = $request->file('cancelled_cheque') ;
                    $fileName1 = date('YmdHis')."_".$file1->getClientOriginalName();
                    $destinationPath1 = $folder;
                    $file1->move($destinationPath1,$fileName1);
                    $cancelled_cheque_path = $destinationPath1.'/'.$fileName1;
                    $assessor->cancelled_check = $cancelled_cheque_path;
                }

                if ($file2 = $request->hasFile('health_doc')) {
                    $file2 = $request->file('health_doc') ;
                    $fileName2 = date('YmdHis')."_".$file2->getClientOriginalName();
                    $destinationPath2 = $folder;
                    $file2->move($destinationPath2,$fileName2);
                    $health_form_path = $destinationPath2.'/'.$fileName2;
                     $assessor->health_doc = $health_form_path;
                }
                if ($file3 = $request->hasFile('gst_doc')) {
                    $file3 = $request->file('gst_doc') ;
                    $fileName3 = date('YmdHis')."_".$file3->getClientOriginalName();
                    $destinationPath3 = $folder;
                    $file3->move($destinationPath3,$fileName3);
                    $gst_form_path = $destinationPath3.'/'.$fileName3;
                    $assessor->gst_form = $gst_form_path;
                }
                if ($file4 = $request->hasFile('vendorstamp_doc')) {
                    $file4 = $request->file('vendorstamp_doc') ;
                    $fileName4 = date('YmdHis')."_".$file4->getClientOriginalName();
                    $destinationPath4 = $folder;
                    $file4->move($destinationPath4,$fileName4);
                    $vendor_stamp_path = $destinationPath4.'/'.$fileName4;
                    $assessor->vendor_stamp = $vendor_stamp_path;
                }
                if ($file5 = $request->hasFile('ndc_form')) {
                    $file5 = $request->file('ndc_form') ;
                    $fileName5 = date('YmdHis')."_".$file5->getClientOriginalName();
                    $destinationPath5 = $folder;
                    $file5->move($destinationPath5,$fileName5);
                    $ndc_form_path = $destinationPath5.'/'.$fileName5;
                    $assessor->ndc_form = $ndc_form_path;
                    
                }

                if ($file6 = $request->hasFile('biodata')) {
                    $file6 = $request->file('biodata') ;
                    $fileName6 = date('YmdHis')."_".$file6->getClientOriginalName();
                    $destinationPath6 = $folder;
                    $file6->move($destinationPath6,$fileName6);
                    $bio_data_path = $destinationPath6.'/'.$fileName6;
                    $assessor->biodata = $bio_data_path;
                    
                }

                    $assessor->bank_name = $request->bank_name;
                    $assessor->acc_no = $request->account_number;
                    $assessor->ifsc_code = $request->ifsc_code;
                    $assessor->branch_name = $request->branch_name; 
                    $assessor->category = $request->ind_category;
                    $assessor->alt_mobile = $request->alt_mobile;
                    
                    $assessor->address_1 = $request->address_1;
                    $assessor->address_2 = $request->address_2;
                    $assessor->city = $request->city;
                    $assessor->state = $request->state;
                    $assessor->pincode = $request->pincode;

                    $assessor->pan_number = $request->pancardno ;
                    $assessor->gst_number = $request->gstnumber ;

                    $assessor->em_contact_name = $request->em_contact_name;
                    $assessor->em_contact_no = $request->em_contact_no;
                    $assessor->em_address_1 = $request->em_address_1;
                    $assessor->em_address_2 = $request->em_address_2;
                    $assessor->em_city = $request->em_city;
                    $assessor->em_state = $request->em_state;
                    $assessor->em_pincode = $request->em_pincode;
                    $assessor->save();
 
                // $assessor->notify(new SaveAssessorNotification());
                // }
                    
                DB::commit();
                return response()->json(['status' => 'success', 'message' =>  'Success! Profile updated successfully.']);
            
        } catch (\Throwable $th) {
            DB::rollback();
            return $th;
            return response()->json(['status' => 'error', 'message' => 'Some error occurred!']);
        }
    }

    public function submitprofile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cancelled_cheque' => 'mimes:png,jpeg,jpg,pdf',
            'gst_doc' => 'mimes:png,jpeg,jpg,pdf',
            'vendorstamp_doc' => 'mimes:png,jpeg,jpg,pdf',
            'ndc_form' => 'mimes:png,jpeg,jpg,pdf',
            'vendorstamp_doc' => 'mimes:png,jpeg,jpg,pdf',
            'pancard_doc' => 'mimes:png,jpeg,jpg,pdf',
            'bank_name' =>'regex:/^[a-zA-Z\s]+$/u',
            'account_number' =>'regex:/^[a-zA-Z0-9]+$/u',
            'ifsc_code' =>'regex:/^[a-zA-Z0-9]+$/u',
            'branch_name' =>'regex:/^[a-zA-Z0-9\s]+$/u',
            // 'alt_mobile' =>'regex:/^[0-9\s]+$/u',
            'city' =>'regex:/^[a-zA-Z\s]+$/u',
            'pincode' =>'regex:/^[0-9\s]+$/u',
            'pancardno' =>'regex:/^[a-zA-Z0-9\s]+$/u',
            'gstnumber' =>'regex:/^[a-zA-Z0-9\s]+$/u',
            'em_contact_name' =>'regex:/^[a-zA-Z\s]+$/u',
            'em_contact_no' =>'regex:/^[0-9\s]+$/u',
            'em_city' =>'regex:/^[a-zA-Z\s]+$/u',
            'em_pincode' =>'regex:/^[0-9\s]+$/u',
        ],
        [
            'bank_name.regex' =>'Please Enter a Valid Bank Name',
            'account_number.regex' =>'Please Enter a Valid Account Number',
            'ifsc_code.regex' =>'Please Enter a Valid IFSC Code',
            'branch_name.regex' =>'Please Enter a Valid Branch Name',
            // 'alt_mobile.regex' =>'Please Enter a Valid Alternate Mobile Number',
            'city.regex' =>'Please Enter a Valid City',
            'pincode.regex' =>'Please Enter a Valid Pincode',
            'pancardno.regex' =>'Please Enter a Valid Pancard Number',
            'gstnumber.regex' =>'Please Enter a Valid GST Number',
            'em_contact_name.regex' =>'Please Enter a Valid Contact Name',
            'em_contact_no.regex' =>'Please Enter a Valid Contact Number',
            'em_city.regex' =>'Please Enter a Valid City',
            'em_pincode.regex' =>'Please Enter a Valid Pincode',
        
        ]);

        // dd($request->all());

        if ($validator->fails()) {
            return response()->json(['status' => 'errors', 'errors' => $validator->errors()->toArray()]);
        }

        try 
        {
            DB::beginTransaction();
            $assessor = Auth::user();
            

            // if ($assessor->save()) {
                $folder = ('uploads/assessors/'.$assessor->id);
                ## CREATE FOLDER
                if (!File::exists($folder)) {
                    mkdir($folder, 0777, true);
                    chmod($folder, 0777);
                }
                $health_form_path ='';
                $pan_card_path = '';
                $gst_form_path = '';
                $vendor_stamp_path = '';
                $ndc_form_path ='';
                $cancelled_cheque_path = '';
                $bio_data_path = '';
                    
                $doc_count = 0;

                
                if ($file6 = $request->hasFile('biodata')) {
                    $file6 = $request->file('biodata') ;
                    $fileName6 = date('YmdHis')."_".$file6->getClientOriginalName();
                    $destinationPath6 = $folder;
                    $file6->move($destinationPath6,$fileName6);
                    $bio_data_path = $destinationPath6.'/'.$fileName6;
                    $assessor->biodata = $bio_data_path;
                    if($assessor->biodata_status == '2') $doc_count += 1;
                    $assessor->biodata_status = 0 ;
                }
                else{
                    if(isset( $assessor->biodata ) && $assessor->biodata_status !='1'){
                        $assessor->biodata_status = 0 ;
                    }
                }

                if ($file = $request->hasFile('pancard_doc')) {
                    $file = $request->file('pancard_doc') ;
                    $fileName = date('YmdHis')."_".$file->getClientOriginalName();
                    $destinationPath = $folder;
                    $file->move($destinationPath,$fileName);
                    $pan_card_path = $destinationPath.'/'.$fileName;
                    $assessor->pan = $pan_card_path;
                    if($assessor->pan_status == '2') $doc_count += 1;
                    $assessor->pan_status = 0 ;
                }
                else{
                    if(isset( $assessor->pan ) && $assessor->pan_status !='1'){$assessor->pan_status = 0 ;}
                    
                    // if(isset( $assessor->pan )){
                    //     if($assessor->pan_status === NULL || $assessor->pan_status == '2')
                    //         $assessor->pan_status = 0 ;
                    // }

                }
                                     
                if ($file1 = $request->hasFile('cancelled_cheque')) {
                    $file1 = $request->file('cancelled_cheque') ;
                    $fileName1 = date('YmdHis')."_".$file1->getClientOriginalName();
                    $destinationPath1 = $folder;
                    $file1->move($destinationPath1,$fileName1);
                    $cancelled_cheque_path = $destinationPath1.'/'.$fileName1;
                    $assessor->cancelled_check = $cancelled_cheque_path;
                    if($assessor->cancelled_check_status == '2') $doc_count += 1;
                    $assessor->cancelled_check_status = 0 ;
                }
                else{
                    if(isset( $assessor->cancelled_check ) && $assessor->cancelled_check_status !='1'){$assessor->cancelled_check_status = 0 ;}
                    
                    // if(isset( $assessor->cancelled_check )){
                    //     if($assessor->cancelled_check_status === NULL || $assessor->cancelled_check_status == '2')
                    //         $assessor->cancelled_check_status = 0 ;
                    // }
                }

                if ($file2 = $request->hasFile('health_doc')) {
                    $file2 = $request->file('health_doc') ;
                    $fileName2 = date('YmdHis')."_".$file2->getClientOriginalName();
                    $destinationPath2 = $folder;
                    $file2->move($destinationPath2,$fileName2);
                    $health_form_path = $destinationPath2.'/'.$fileName2;
                     $assessor->health_doc = $health_form_path;
                     if($assessor->health_doc_status == '2') $doc_count += 1;
                    $assessor->health_doc_status = 0 ;
                }
                else{
                    if(isset( $assessor->health_doc ) && $assessor->health_doc_status !='1'){$assessor->health_doc_status = 0 ;}
                    
                    // if(isset( $assessor->health_doc )){
                    //     if($assessor->health_doc_status === NULL || $assessor->health_doc_status == '2')
                    //         $assessor->health_doc_status = 0 ;
                    // }
                }

                if ($file3 = $request->hasFile('gst_doc')) {
                    $file3 = $request->file('gst_doc') ;
                    $fileName3 = date('YmdHis')."_".$file3->getClientOriginalName();
                    $destinationPath3 = $folder;
                    $file3->move($destinationPath3,$fileName3);
                    $gst_form_path = $destinationPath3.'/'.$fileName3;
                    $assessor->gst_form = $gst_form_path;
                    if($assessor->gst_form_status == '2') $doc_count += 1;
                    $assessor->gst_form_status = 0 ;
                }
                else{
                    if(isset( $assessor->gst_form ) && $assessor->gst_form_status !='1'){$assessor->gst_form_status = 0 ;}
                    
                    // if(isset( $assessor->gst_form )){
                    //     if($assessor->gst_form_status === NULL || $assessor->gst_form_status == '2')
                    //         $assessor->gst_form_status = 0 ;
                    // }
                }

                if ($file4 = $request->hasFile('vendorstamp_doc')) {
                    $file4 = $request->file('vendorstamp_doc') ;
                    $fileName4 = date('YmdHis')."_".$file4->getClientOriginalName();
                    $destinationPath4 = $folder;
                    $file4->move($destinationPath4,$fileName4);
                    $vendor_stamp_path = $destinationPath4.'/'.$fileName4;
                    $assessor->vendor_stamp = $vendor_stamp_path;
                    if($assessor->vendor_stamp_status == '2') $doc_count += 1;
                    $assessor->vendor_stamp_status = 0 ;
                }
                else{
                    if(isset( $assessor->vendor_stamp ) && $assessor->vendor_stamp_status !='1'){$assessor->vendor_stamp_status = 0 ;}
                    
                    // if(isset( $assessor->vendor_stamp )){
                    //     if($assessor->vendor_stamp_status === NULL || $assessor->vendor_stamp_status == '2')
                    //         $assessor->vendor_stamp_status = 0 ;
                    // }
                }

                if ($file5 = $request->hasFile('ndc_form')) {
                    $file5 = $request->file('ndc_form') ;
                    $fileName5 = date('YmdHis')."_".$file5->getClientOriginalName();
                    $destinationPath5 = $folder;
                    $file5->move($destinationPath5,$fileName5);
                    $ndc_form_path = $destinationPath5.'/'.$fileName5;
                    $assessor->ndc_form = $ndc_form_path;
                    if($assessor->ndc_form_status == '2') $doc_count += 1;
                    $assessor->ndc_form_status = 0 ;
                    
                }
                else{
                    if(isset( $assessor->ndc_form ) && $assessor->ndc_form_status !='1'){$assessor->ndc_form_status = 0 ;}
                    
                    // if(isset( $assessor->ndc_form )){
                    //     if($assessor->ndc_form_status === NULL || $assessor->ndc_form_status == '2')
                    //         $assessor->ndc_form_status = 0 ;
                    // }
                }
                    $category = json_encode($request->ind_category);
                    if($request->profile_updated_status ==0)
                    {
                        $assessor->bank_name = $request->bank_name;
                        $assessor->acc_no = $request->account_number;
                        $assessor->ifsc_code = $request->ifsc_code;
                        $assessor->branch_name = $request->branch_name; 
                        $assessor->category = $category;
                        $assessor->alt_mobile = $request->alt_mobile;
                        $assessor->address_1 = $request->address_1;
                        $assessor->address_2 = $request->address_2;
                        $assessor->city = $request->city;
                        $assessor->state = $request->state;
                        $assessor->pincode = $request->pincode;
                        $assessor->pan_number = $request->pancardno;
                        $assessor->gst_number = $request->gstnumber;
    
                    }    
                    $assessor->em_contact_name = $request->em_contact_name;
                    $assessor->em_contact_no = $request->em_contact_no;
                    $assessor->em_address_1 = $request->em_address_1;
                    $assessor->em_address_2 = $request->em_address_2;
                    $assessor->em_city = $request->em_city;
                    $assessor->em_state = $request->em_state;
                    $assessor->em_pincode = $request->em_pincode;
            
                    $assessor->profile_updated = 1 ;
                    $assessor->save();
                    $details = [
                        'name' => $assessor->name,
                        'email' => $assessor->email,
                    ]; 

                    if($doc_count > 0)
                    {
                        // $assessor->notify(new AssessorProfileDocumentReuploadNotification());
                        Mail::to(ADMIN_EMAILID)->send(new assessor_documents_reupload($details));
                    }

                    DB::commit();

                    if($request->profile_updated_status ==0)
                    {
                        $assessor->notify(new AssessorProfileSubmittedNotification());
                        return response()->json(['status' => 'success', 'message' =>  'Success! Profile Submitted successfully.']);
                    }
                    else
                        return response()->json(['status' => 'success', 'message' =>  'Success! Profile Saved successfully.']);
            
        } catch (\Throwable $th) {
            DB::rollback();
            return $th;
            return response()->json(['status' => 'error', 'message' => 'Some error occurred!']);
        }
    }


    public function ndadocumentslist(Request $request)
    {
        $this->data['pageTitle'] = 'NDA Documents List';
        $this->data['profile'] = Auth::user();
        $this->data['states'] = States::orderBy('name','asc')->get();
        $this->data['categories'] = AssessorCategoryRepo::allActive($orderColumn="created_at", $orderDir='desc');

        return parent::assessorView('account.ndadocumentslist', $this->data);
    }

    public function changePassword(Request $request)
    {
        $this->data['pageTitle'] = 'Change Password';
        
        return parent::assessorView('account.changePassword', $this->data);
    }

    public function changePasswordSave(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'current_password' => 'required',
                'new_password' => ['required', 'string', 'min:6', 'regex:/[a-z]/', 'regex:/[A-Z]/', 'regex:/[1-9]/', 'regex:/[@]/'],
                'confirmed' => 'required|same:new_password',
            ],
            [
                'current_password.required' => 'Please enter your old password',
                'new_password.required' => 'Please enter your new password',
                'confirmed.required' => 'Please enter your confirm password',
                'confirmed.same' => 'New Password and Confirm Password does not match.',
            ]
        );
        // dd($validator->fails());

        if ($validator->fails()) {
            return response()->json(['status' => 'errors', 'errors' => $validator->errors()->toArray()]);
        }

        $user = Auth::user();

        if (!(Hash::check($request->current_password, $user->password))) {
            // The passwords matches
            return response()->json(['status' => 'error', 'message' => 'Your current password does not matches with the password you provided. Please try again.']);
        }


        if (strcmp($request->current_password, $request->new_password) == 0) {
            // If Current password and new password are same
            return response()->json(['status' => 'error', 'message' => 'New Password cannot be same as your current password. Please choose a different password.']);
        }

        DB::beginTransaction();
        try {

            $user->password = Hash::make($request->new_password);
            DB::commit();
            $user->save();
            $user->notify(new PasswordUpdateNotification);
            return response()->json(['status' => 'success', 'message' => 'Success! Your new Password has been updated successfully.']);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['status' => 'error', 'message' => 'Error! Some error occured, please try again.']);
        }
    }
    public function logout()
    {
        Auth::logout();
        return redirect()
            ->route('assessor.login')
            ->with(['toast' => '1', 'status' => 'success', 'title' => 'Thank You', 'message' => 'Thank you. You have been succesfully logged out']);
    }
}
