<?php

namespace App\Http\Controllers\Assessor;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Auth;

class DashboardController extends BaseController
{
    public $data = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['activeMenu'] = 'dashboard';
    }

    public function index(Request $request)
    {
        $this->data['pageTitle'] = 'Dashboard';
        $this->data['profile'] = Auth::user();
        return parent::assessorView('dashboard', $this->data);
    }
}