<?php

namespace App\Http\Controllers\Company;

use DB;
use Redirect;
use Validator;
use Mail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

//Models
use App\Models\Company;
use App\Models\States;
use Illuminate\Support\Facades\Hash;
use App\Mail\company_mail_changed;


//Mail
use App\Http\Controllers\BaseController;
use App\Notifications\PasswordUpdateNotification;
use App\Notifications\CompanyProfileDetailsNotification;
use App\Notifications\CompanyProfileSubmittedNotification;
//Repo
use Facades\App\Repository\EntityRepo;
use Facades\App\Repository\IndustryRepo;
use Facades\App\Repository\SectorRepo;

class AccountController extends BaseController
{
    public $data = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['activeMenu'] = 'account';
        parent::__construct();

        $this->middleware(function ($request, $next) {
            $this->loggedInUser = auth()->user();
            $this->data['authdata'] = auth()->user();
            return $next($request);
        });
        // $this->middleware('guest:assessor')->except('logout')->except('profile');
    }

    public function profile(Request $request)
    {
        $this->data['sectors'] = SectorRepo::allActive($orderColumn="created_at", $orderDir='desc');
        $this->data['industries'] = IndustryRepo::allActive($orderColumn="created_at", $orderDir='desc');
        $this->data['entities'] = EntityRepo::allActive($orderColumn="created_at", $orderDir='desc');
        $this->data['states'] = States::orderBy('name','asc')->get();

        $this->data['pageTitle'] = 'Profile';
        $this->data['profile'] = auth()->user();
        return parent::companyView('account.profile', $this->data);
        
    }

    public function updateprofile(Request $request)
    {
        try
        {
           
            if($request->tab == 1)
            {
                $validator = Validator::make($request->all(), [
                    "location" => 'required|regex:/^[a-zA-Z\s]+$/u',
                    "postaladdress" => 'required|string',
                    "billingaddress" => 'required|string',
                    "postal_address_pincode" => 'required',
                    "billing_address_pincode" => 'required',
                    "state" =>'required',
                    
                ], [
                    "location.required" => 'This field is required',
                    "company_location.required" => 'This field is required.',
                    "postaladdress.required" => 'This field is required.',
                    "billingaddress.required" => 'This field is required.',
                    "postal_address_pincode.required" => 'This field is required.',
                    "billing_address_pincode.required" => 'This field is required.',
                ]);
        
                if ($validator->fails()) {
                    return response()->json(['status' => "validations", 'errors' => $validator->errors()->toArray()]);
                }
                    DB::beginTransaction();
                
                    $companyval = Company::find($request->userid);

                    if( $companyval->profile_update == 1) //$companyval->verified_status == 1) 
                        return response()->json(['status' => 'error', 'message' => "Account Already Submitted, Profile Can't Update."]);

                    $companyval->location = $request->location;
                    $companyval->postal_address = $request->postaladdress;
                    $companyval->billing_address = $request->billingaddress;
                    $companyval->postal_address_pincode = $request->postal_address_pincode;
                    $companyval->billing_address_pincode = $request->billing_address_pincode;
                    $companyval->state = $request->state;


                    if ($companyval->save()) {
                        DB::commit();
                        return response()->json(['status' => 'success', 'message' => 'Company Basic Info Saved Successfull.']);
                    } 

                    else {
                        return response()->json(['status' => 'error', 'message' => 'Some error occurred!']);
                    }
                    
        
            }

            if($request->tab == 2)
            {
                $validator = Validator::make($request->all(), [
                    "industry" => 'required',
                    "entity" => 'required',
                    "sector" => 'required',

                    
                ], [
                    "industry.required" => 'This field is required',
                    "entity.required" => 'This field is required',
                    "sector.required" => 'This field is required',
                ]);
        
                if ($validator->fails()) {
                    return response()->json(['status' => "validations", 'errors' => $validator->errors()->toArray()]);
                }
                DB::beginTransaction();
        
                    $companyval = Company::find($request->userid);
                    $companyval->mst_industry_id = $request->industry;
                    $companyval->mst_entity_id = $request->entity;
                    $companyval->mst_sector_id = $request->sector;
                    $companyval->save();
                
        
                    DB::commit();
                    return response()->json(['status' => 'success', 'message' => 'Company Industrial Info Saved Successfull.']);
        
                
            }

            if($request->tab == 3)
            {
                $validator = Validator::make($request->all(), [
                    "productsorservicesname" => 'required|regex:/^[a-zA-Z\s]+$/u',
                    "latestturnover" => 'required',
                    "totalpermanentemployees" => 'required|integer',
                    "totalcontractemployees" => 'required|integer',
                    "electricalenergyconsumption" => 'required',
                    "thermalenergyconsumption" => 'required',
                    "waterconsumption" => 'required',
                    "totalarea" => 'required',
                    "budget_allocation" => 'required',
                    
                ], [
                    "productsorservicesname.required" => 'This field is required',
                    "latestturnover.required" => 'This field is required',
                    "totalpermanentemployees.required" => 'This field is required',
                    "totalcontractemployees.required" => 'This field is required',
                    "electricalenergyconsumption.required" => 'This field is required',
                    "thermalenergyconsumption.required" => 'This field is required',
                    "waterconsumption.required" => 'This field is required',
                    "totalarea.required" => 'This field is required',
                    "budget_allocation.required" => 'This field is required',

                ]);
        
                if ($validator->fails()) {
                    return response()->json(['status' => "validations", 'errors' => $validator->errors()->toArray()]);

                }
                DB::beginTransaction();
        
                    $companyval = Company::find($request->userid);
                    $companyval->product_name = $request->productsorservicesname;
                    $companyval->turnover = $request->latestturnover;
                    $companyval->no_of_employees = $request->totalpermanentemployees;
                    $companyval->contract_employees = $request->totalcontractemployees;
                    $companyval->electrical_consumption = $request->electricalenergyconsumption;
                    $companyval->thermal_consumption = $request->thermalenergyconsumption;
                    $companyval->water_consumption = $request->waterconsumption;
                    $companyval->area = $request->totalarea;
                    $companyval->budget_allocation = $request->budget_allocation;
                
                    $companyval->save();
                
        
                    DB::commit();
                    return response()->json(['status' => 'success', 'message' => 'Company Infrastructure Info Saved Successfull.']);
        
                
            }

            if($request->tab == 4)
            {
                $validator = Validator::make($request->all(), [
                    "tanno" => 'required',
                    "panno" => 'required',
                    "gstinno" => 'required',
                    
                ], [
                    "tanno.required" => 'This field is required',
                    "panno.required" => 'This field is required',
                    "gstinno.required" => 'This field is required',
                ]);
        
                if ($validator->fails()) {
                    return response()->json(['status' => "validations", 'errors' => $validator->errors()->toArray()]);
                }
                DB::beginTransaction();
        
                    $companyval = Company::find($request->userid);
                    $companyval->tanno = $request->tanno;
                    $companyval->panno = $request->panno;
                    $companyval->gstno = $request->gstinno;
                    // $companyval->profile_update ='1';
                    // dd($request->all());
                    $companyval->save();
                
                    // $companyval->notify(new CompanyProfileDetailsNotification());

                    DB::commit();
                    return response()->json(['status' => 'success', 'message' => 'Company Profile Saved Successfull.']);
        
                
            }

        }
        catch(\Exception $e) {
            DB::rollback();
            // return $e;
            return response()->json(['status' => 'error', 'message' => 'Something went wrong. Please try again.']);

        }

        // dd($user);
    }

    public function submitprofile(Request $request)
    {
        try{

                $validator = Validator::make($request->all(), [
                    "tanno" => 'required',
                    "panno" => 'required',
                    "gstinno" => 'required',
                    
                ], [
                    "tanno.required" => 'This field is required',
                    "panno.required" => 'This field is required',
                    "gstinno.required" => 'This field is required',
                ]);
        
                if ($validator->fails()) {
                    return response()->json(['status' => "validations", 'errors' => $validator->errors()->toArray()]);
                }
                DB::beginTransaction();

                $companyval = Company::find($request->userid);
                $companyval->tanno = $request->tanno;
                $companyval->panno = $request->panno;
                $companyval->gstno = $request->gstinno;
                $companyval->profile_update ='1';
                // dd($request->all());
                $companyval->save();

                $companyval->notify(new CompanyProfileSubmittedNotification());
            
                // $companyval->notify(new CompanyProfileDetailsNotification());

                DB::commit();
                return response()->json(['status' => 'success', 'message' => 'Company Profile Submitted Successfull.']);
                
        }


        catch(\Exception $e) {
            DB::rollback();
            // return $e;
            return response()->json(['status' => 'error', 'message' => 'Something went wrong. Please try again.']);

        }
    }

    public function changePasswordPage(Request $request)
    {
        $this->data['pageTitle'] = 'Change Password';
        return parent::companyView('account.changePassword', $this->data);
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'current_password' => 'required',
                'new_password' => ['required', 'string', 'min:6', 'regex:/[a-z]/', 'regex:/[A-Z]/', 'regex:/[0-9]/', 'regex:/[@$!%*#?&]/'],
                'confirmed' => 'required|same:new_password',
            ],
            [
                'current_password.required' => 'Please enter your old password',
                'new_password.required' => 'Please enter your new password',
                'confirmed.required' => 'Please enter your confirm password',
                'confirmed.same' => 'New Password and Confirm Password does not match.',
            ]
        );
        
        if ($validator->fails()) {
            return response()->json(['status' => 'errors', 'errors' => $validator->errors()->toArray()]);
        }

        $user = auth()->user();

        if (!(Hash::check($request->current_password, $user->password))) 
        {
            return response()->json(['status' => 'error', 'message' => 'Your current password does not matches with the password you provided. Please try again.']);
        }

        // dd("Hello");

        if (strcmp($request->current_password, $request->new_password) == 0) 
        {
            return response()->json(['status' => 'error', 'message' => 'New Password cannot be same as your current password. Please choose a different password.']);
        }

        DB::beginTransaction();
        try {

            $user->password = Hash::make($request->new_password);
            DB::commit();
            $user->save();
            $user->notify(new PasswordUpdateNotification);
            return response()->json(['status' => 'success', 'message' => 'Success! Your new Password has been updated successfully.']);
        } catch (Exception $e) {
            DB::rollback();
            return response()->json(['status' => 'error', 'message' => 'Some Error occured! Please try again after sometime.']);
        }
    }
    

    public function documentsPage(Request $request)
    {
        // dd($this->data['profile']);
        // if($this->loggedInUser->verified_status == 0)
        //     return redirect()->back()->with(['toast' => '1', 'status' => 'error', 'title' => 'Authentication', 'message' => "Account Not Approved, Can't Access the Page!"]);

        $this->data['pageTitle'] = 'Documents Page';
        return parent::companyView('account.documents', $this->data);
    }

    public function uploaddocuments(Request $request)
    {
        //dd($request->all());
        $validator = Validator::make($request->all(), [
            "proposal_doc" => 'required',
            "po_doc" => 'required',
            
        ], [
            
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => "validations", 'errors' => $validator->errors()->toArray()]);
        }
        try
        {
            DB::beginTransaction();

                $companyval = Company::find($request->userid);
                $companyval->tanno = $request->tanno;
                $companyval->panno = $request->panno;
                $companyval->gstno = $request->gstinno;
                $companyval->save();
            

                DB::commit();
                return response()->json(['status' => 'success', 'message' => 'Company Profile Updated Successfull.']);

        }
        catch(\Exception $e) {
            DB::rollback();
            // return $e;
            return response()->json(['status' => 'error', 'message' => 'Something went wrong. Please try again.']);

        }

    }

    public function logout()
    {
        auth('company')->logout();
        return redirect()
            ->route('company.login')
            ->with(['toast' => '1', 'status' => 'success', 'title' => 'Thank You', 'message' => 'Thank you. You have been succesfully logged out']);
    }
}
