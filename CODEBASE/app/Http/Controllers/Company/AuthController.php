<?php

namespace App\Http\Controllers\Company;

use Mail;
use Redirect;
use Validator;
// use Redis;
use Illuminate\Support\Facades\Redis;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

//Models
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Company;
use Carbon\Carbon;

//Mail
use Illuminate\Support\Facades\Hash;
use App\Mail\ForgotPasswordUserEmail;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Notification;
use App\Notifications\ForgotPasswordNotification;
use App\Notifications\SaveCompanyNotification;

//Repo
use Facades\App\Repository\EntityRepo;
use Facades\App\Repository\IndustryRepo;
use Facades\App\Repository\SectorRepo;


class AuthController extends BaseController
{
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/company/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest:company')->except('logout');
    }

    protected function credentials(Request $request)
    {
        if (filter_var($request->email, FILTER_VALIDATE_EMAIL))
            return ['email' => $request->email, 'password' => $request->password];
    }



    public function registerSuccess(Request $request)
    {
        $this->data ['pageTitle'] = 'Register Success' ;

        return parent::companyView('auth.registerSuccess', $this->data);
    }

    public function accountActivated(Request $request)
    {
        $this->data ['pageTitle'] = 'Account Activation' ;
        return parent::companyView('auth.accountActivated', $this->data);
    }



    public function companieslist(Request $request)
    {
        $val = trim($request->name);
        $companieslist = Company::select('name')->where('name','like', '%'.$val.'%')->distinct()
                        ->orderBy('name','asc')->get() ;
        $result = [];
        if($val){
            if(!empty($companieslist)){
                foreach ($companieslist as $company) {
                        $result[] = [ 'value'=>$company->name];
                }
            }
        }

        return response()->json($result);
    }

    public function register(Request $request)
    {
            if($request->register)
            {
                // @dd($request->all());
                $validator = Validator::make($request->all(), [
                    'email' => "required|email:rfc,dns|unique:companies,email",
                    'company_name' => 'required|min:2|max:50|',
                    'mobileno' => "required|numeric|regex:/^[6-9]\d{9}$/|digits:10|unique:companies,mobile",

                ], [
                    'mobileno.digits' => 'The Mobile Number Must be 10 digits',
                    'company_name.required' => 'Please Enter Your Company Name',
                    'email.email' => 'Please Enter a Valid Email Address',
                    'email.required' => 'Please Enter Your Email Address',
                    'mobileno.required' => 'Please Enter Your Mobile Number',
                    'mobileno.regex' => 'Please Enter a Valid Mobile Number',

                    'email.unique' => 'The Account already exists with this Email Address',
                    'mobileno.unique' => 'The Account already exists with this Mobile Number'
                ]);

                if ($validator->fails()) {
                    return response()->json(['status' => "errors", 'errors' => $validator->errors()->toArray()]);
                }

                DB::beginTransaction();
                try
                {
                    $companyval = new Company();
                    $companyval->name = $request->company_name;
                    $companyval->email = $request->email;
                    $companyval->mobile = $request->mobileno;
                    $companyval->account_status = '0';
                    //remove this lines

                    $companyval->save();

                    __account_activation($request->email,'company');
                    // Notification::send($companyval, (new SaveCompanyNotification()));

                    DB::commit();
                    return response()->json(['status' => 'success', 'message' => 'Company Registered Succesfull.']);

                }
                catch(\Exception $e) {
                    DB::rollback();
                // return $e;
                return response()->json(['status' => 'error', 'message' => 'Something went wrong. Please try again.']);

                }
            }

            $this->data ['pageTitle'] = 'Register' ;

            return parent::companyView('auth.register', $this->data);


    }


    public function verify($token = null)
    {
        if(!$token)
            return redirect()->route('company.login')->with(['toast' => '1', 'status' => 'error', 'title' => 'Authentication', 'message' => 'Invalid token!']);

        $company = Company::where('email_verification_token',$token)->first();
        if(!$company)
            return redirect()->route('company.login')->with(['toast' => '1', 'status' => 'error', 'title' => 'Authentication', 'message' => 'Account Already Activated']);

        // update account status
        $password = passwordGeneration(6);

        $company->email_verification_token = null;
        $company->password = Hash::make($password);
        $company->email_verified_at = Carbon::now();
        $company->status_updated_at = Carbon::now();
        $company->account_status = 1;
        if($company->save()) {
            $company->notify(new SaveCompanyNotification($password));

            return redirect()->route('company.accountActivated');

            // return redirect()->route('company.login')->with(['toast' => '1', 'status' => 'success', 'title' => 'Authentication', 'message' => 'Account Activated successfully!']);
        }
    }

    public function login(Request $request)
    {
            if($request->login) {

            $validator = Validator::make(
                $request->all(),
                [
                    'email' => 'required|email:rfc',
                    'password'  => 'required',
                ],
                [
                    'email.email' => 'Please enter a valid email',
                    'email.required' => 'Please enter your email',
                    'password.required' => 'Please enter your password'
                ]
            );

            if ($validator->fails()) {
                return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
            }
            $check = Company::where(['email' => $request->email])->first();

            if($check) {
                if($check->account_status =='1' )
                {
                    if (Auth::guard('company')->attempt(['email' => $request->email, 'password' => $request->password]))
                    {
                        session(['userId' =>$check->id, 'email' => $check->email]);
                        return redirect()->route('company.dashboard')->with(['toast' => '1', 'status' => 'success', 'title' => 'Authentication', 'message' => 'Welcome Back!']);
                    }
                    else
                        return redirect()->route('company.login')->with(['toast' => '1', 'status' => 'error', 'title' => 'Authentication', 'message' => 'Your credentials are not valid! Please enter a valid Email and Password.'])->withInput();
                }
                elseif($check->account_status =='0' )
                {
                    auth('company')->logout();
                    return redirect()->route('company.login')->with(['toast' => '1', 'status' => 'error', 'title' => 'Authentication', 'message' => 'Account In-Active! Please Contact Greenco Team.'])->withInput();
                }
                elseif( $check->verified_status =='2')
                {
                    auth('company')->logout();
                    return redirect()->route('company.login')->with(['toast' => '1', 'status' => 'error', 'title' => 'Authentication', 'message' => 'Account is Disapproved! Please Contact Greenco Team.'])->withInput();
                }
            } else {
                return redirect()->route('company.login')->with(['toast' => '1', 'status' => 'error', 'title' => 'Authentication', 'message' => 'No Account Found! Please enter a valid Email.'])->withInput();
            }
        }

        $data = [
            'pageTitle' => 'Login'
        ];

        return parent::companyView('auth.login', $data);
    }


    public function forgotPassword(Request $request)
    {

        // if ($request->submit) {
            $validator = Validator::make(
                $request->all(),
                [
                    'email' => 'required|email:rfc,dns'
                ],
                [
                    'email.required' => 'Please Enter Email Address',
                    'email.email' => 'Please Enter a Valid Email Address'
                ]
            );

            $user = Company::where('email', $request->email)->first();
            if ($user) {
                if($user->account_status =='0' )
                {
                            $validator->after(function ($validator) {
                                $validator->errors()->add(
                                    'email', 'Account In-Active! Please Contact Greenco Team.'
                                );
                            });

                }

                elseif( $user->verified_status =='2')
                {
                    Auth::guard('assessor')->logout();
                    $validator->after(function ($validator) {
                        $validator->errors()->add(
                            'email', 'Account is Disapproved! Please Contact Greenco Team.'
                        );
                    });

                }
            }
            else
            {
                $validator->after(function ($validator) {
                    $validator->errors()->add(
                        'email', "Account doesn't exist. Please Signup to register."
                    );
                });
                // return response()->json(['status' => 'error', 'message' => "User doesn't exist. Please Signup to register"]);
            }


            if ($validator->fails()) {
                return response()->json(['status' => 'errors', 'errors' => $validator->errors()->toArray()]); // return redirect()->back()->withErrors($validator);
            } else {

                // $user = Assessor::where('email', $request->email)->first();

                if ($user) {



                    $password = passwordGeneration(6);
                    $type = 'company';
                    Company::where('email', $request->email)
                        ->update(['password' => Hash::make($password)]);
                    $user->notify(new ForgotPasswordNotification($password,$type));

                    DB::commit();
                    return response()->json(['status' => 'success', 'message' => 'Password sent to your email!']);
                } else {
                    DB::rollback();
                    return response()->json(['status' => 'error', 'message' => "User doesn't exist. Please Signup to register"]);
                }
            }



    }

    public function forgotPasswordPage()
    {
        $data = [
            'pageTitle' => 'Forgot Password'
        ];

        return parent::companyView('auth.forgotPassword', $data);
    }


}
