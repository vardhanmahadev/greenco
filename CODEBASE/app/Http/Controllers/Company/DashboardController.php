<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Auth;

class DashboardController extends BaseController
{
    public $data = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['activeMenu'] = 'dashboard';
        parent::__construct();

        $this->middleware(function ($request, $next) {
            $this->loggedInUser = auth()->user();
            $this->data['authdata'] = auth()->user();
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        $this->data['pageTitle'] = 'Dashboard';
        $this->data['profile'] = auth()->user();
        return parent::companyView('dashboard', $this->data);
    }
}