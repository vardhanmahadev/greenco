<?php

namespace App\Http\Controllers;

use App\Mail\TestingMaile;
use App\Models\User;
use Illuminate\Http\Request;

class TestingContraoller extends Controller
{
    //
    public function maileView()
    {
        $user=User::first();
        return (new TestingMaile($user))->render();
    }
}
