<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BaseController extends Controller
{
	public $loggedInUser;

	public function __construct()
    {

    }

	public static function frontView($path=null, $data=[])
	{
		$viewPath = FRONTEND_THEME_NAME.".{$path}";
		return view($viewPath, $data);
	}

	public static function consultantView($path=null, $data=[])
	{
		$viewPath = CONSULTANT_THEME_NAME.".{$path}";
		return view($viewPath, $data);
	}

	public static function companyView($path=null, $data=[])
	{
		$viewPath = COMPANY_THEME_NAME.".{$path}";
		return view($viewPath, $data);
	}

    public static function adminView($path=null, $data=[])
	{
		$viewPath = ADMIN_THEME_NAME.".{$path}";
		return view($viewPath, $data);
	}

	public static function assessorView($path=null, $data=[])
	{
		$viewPath = ASSESSOR_THEME_NAME.".{$path}";
		return view($viewPath, $data);
	}
}