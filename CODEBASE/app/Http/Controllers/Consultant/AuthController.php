<?php

namespace App\Http\Controllers\Consultant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Mail;
use Redirect;
use Validator;
use Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Models\Consultant;

class AuthController extends Controller
{
/**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/consultant/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:consultant')->except('logout');
    }
}
