<?php

namespace App\Http\Controllers\Consultant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public $data = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->data['activeMenu'] = 'dashboard';
    }

    public function index(Request $request)
    {
        $this->data['pageTitle'] = 'Dashboard';
        return parent::consultantView('dashboard', $this->data);
    }
}
