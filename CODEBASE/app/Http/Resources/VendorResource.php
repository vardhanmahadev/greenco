<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VendorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $log=$this->logs()->latest()
        ->first();

        $result= [
            'id'=>$this->id,
            'name'=>$this->name,
            'email'=>$this->email,
            'mobile'=>$this->mobile,
            'designation'=>$this->designation,
            'status'=>$this->status,
            'company_contact_number'=>$this->number,
            'company_name'=>$this->company_name,
            'company_address'=>$this->company_address,
            'registered_date' => $this->created_at->format('d-m-Y ( h:i a )'),
        ];

        if($this->status==1) {
            $result['disapprove_date']=null;
            $result['approve_date'] = ($log != null) ? $log->created_at->format('d-m-Y ( h:i a )') : null;
        }elseif ($this->status==2) {
            $result['disapprove_date'] = ($log != null) ? @$log->created_at->format('d-m-Y ( h:i a )') : null;
            $result['approve_date']=null;
        }else{
            $result['disapprove_date']=null;
            $result['approve_date']=null;
        }

        return $result;
    }
}
