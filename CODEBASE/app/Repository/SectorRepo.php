<?php

namespace App\Repository;
use Illuminate\Support\Facades\Cache;
use App\Repository\BaseRepo;

// Models
use App\Models\MasterSector;

class SectorRepo extends BaseRepo
{
	const CACHE_KEY = 'SECTORS';
	const CACHE_TAG = 'SECTORS';

    function __construct()
    {
        parent::__construct(self::CACHE_TAG, self::CACHE_KEY);
    }

	public function all($orderColumn="created_at", $orderDir='desc')
	{
		$key = parent::getCacheKey("all.{$orderColumn}.{$orderDir}");

		return Cache::tags(self::CACHE_TAG)->rememberForever($key, function() use($orderColumn, $orderDir) {
			return MasterSector::orderBy($orderColumn, $orderDir)
								->get();
		});
	}

	public function allActive($orderColumn="created_at", $orderDir='desc')
	{
		$key = parent::getCacheKey("allActive.{$orderColumn}.{$orderDir}");

		return Cache::tags(self::CACHE_TAG)->rememberForever($key, function() use($orderColumn, $orderDir) {
			return MasterSector::where('status', 1)
								->orderBy($orderColumn, $orderDir)
								->get();
		});
	}

	public function getPaginateData($page=1, $orderColumn="created_at", $orderDir='desc')
	{
		$key = parent::getCacheKey("paginate.{$page}.{$orderColumn}.{$orderDir}");

		return Cache::tags(self::CACHE_TAG)->rememberForever($key, function() use($page, $orderColumn, $orderDir) {
			return MasterSector::orderBy($orderColumn, $orderDir)
							->paginate(5, '*');
		});
	}

	public function get($id)
	{
		$key = parent::getCacheKey("get.{$id}");

		return Cache::tags(self::CACHE_TAG)->rememberForever($key, function() use($id) {
			return MasterSector::where('id', $id)->first();
		});
	}
}