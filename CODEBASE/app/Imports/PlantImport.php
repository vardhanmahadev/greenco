<?php

namespace App\Imports;

use App\Models\Plant;
use App\Models\State;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class PlantImport implements ToCollection
{
    private $application_id=null;
    private $categori_id=null;

    public function __construct($application_id,$categori_id)
    {
        $this->application_id=$application_id;
        $this->categori_id=$categori_id;
    }
    /**
     * @param array $row
     *
     * @return User|null
     */
    public function collection(Collection $rows)
    {
        unset($rows[0]);

        foreach ($rows as $index => $row) {
            $status=(@strtoupper($row[7])=="YES")?1:0;
            $state=State::where('name',$row[4])->first();
             Plant::create([
                'application_id' => $this->application_id,
                'category_id'    => $this->categori_id,
                'plant_sl'=>(int)$row[0],
                'plant_size' => $row[1],
                'city' => $row[2],
                'district' => $row[3],
                'state_id' =>@$state->id,
                'date_of_commissioning' =>  Date::excelToDateTimeObject($row[5])->format('Y-m-d'),
                'sector' => $row[6],
                'is_msme' => $status,
             ]);
        }

    }
}
