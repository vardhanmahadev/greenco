<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class AccessorExport implements FromView
{
    public $assessor = [];

    public function __construct($assessor)
    {
        $this->assessor = $assessor;
    }

    public function view(): View
    {
        return view('admin.assessor.export', [
            'assessors' => $this->assessor
        ]);
    }
}
