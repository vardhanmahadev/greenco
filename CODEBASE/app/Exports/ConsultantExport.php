<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ConsultantExport implements FromView
{
    public $consultant = [];

    public function __construct($consultant)
    {
        $this->consultant = $consultant;
    }

    public function view(): View
    {
        return view('admin.consultant.export', [
            'consultants' => $this->consultant
        ]);
    }
}
