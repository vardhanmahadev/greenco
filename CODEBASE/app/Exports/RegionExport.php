<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class RegionExport implements FromView
{
    public $regions = [];

	public function __construct($regions)
    {
        $this->regions = $regions;
    }

    public function view(): View
    {
        return view('admin.region.export', [
            'regions' => $this->regions
        ]);
    }
}
