<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ApplicationExport implements FromView
{
    public $application = [];

	public function __construct($application)
    {
        $this->application = $application;
    }

    public function view(): View
    {
        return view('admin.applications.export', [
            'applications' => $this->application
        ]);
    }
}
