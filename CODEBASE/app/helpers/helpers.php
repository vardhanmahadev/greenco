<?php

use Illuminate\Support\Str;

// Models
use App\Models\Assessor;
use App\Models\Company;

// Mails/Notifications
use App\Notifications\AccountActivationNotification;

if (! function_exists('passwordGeneration')) {
    function passwordGeneration($n)
    {
        $generator = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789@";
        $result = "";
        for ($i = 1; $i <= $n; $i++) {
            $result .= substr($generator, (rand()%(strlen($generator))), 1);
        }
        return $result;
    }
}

if (!function_exists('__generateNewCompanyId')) {
    function __generateNewCompanyId()
    {
        $company = DB::table('meta_ids')->where('parameter','companies')->select('value')->first();
        $companyValue = $company->value;
        $companyId = $companyValue+1;
        $serial = sprintf('C%05d',$companyId);
        return $serial;
    }
}

if (!function_exists('__setDatatableCurrPage')) {
    function __setDatatableCurrPage($for, $length=10, $page=1){

        Session::put('company_perpage', Session::get('company_perpage', 10));
        Session::put('company_page', Session::get('company_page', 1));

        Session::put('vendor_perpage', Session::get('vendor_perpage', 10));
        Session::put('vendor_page', Session::get('vendor_page', 1));

        switch ($for) {
            case 'vendor':
                Session::put('vendors_perpage', $length);
                Session::put('vendor_page', $page);
                break;

            case 'company':
                Session::put('company_perpage', $length);
                Session::put('company_page', $page);
                break;

            default:
                # code...
                break;
        }
    }
}

if (! function_exists('__generateOtp')) {
    function __generateOtp($n)
    {
        $random = str_shuffle('0123456789');
        return substr($random, 0, $n);
    }
}

if (! function_exists('__base64_to_jpeg')) {
    function __base64_to_jpeg( $base64_string, $output_file ) {
        \File::put(storage_path(). '/' . $output_file, base64_decode($base64_string));

        $disk = Storage::disk('local')->put($output_file, base64_decode($image));

        // $ifp = fopen( 'uploads/'.$output_file, "wb" );
        // fwrite( $ifp, base64_decode( $base64_string) );
        // fclose( $ifp );
        // return( $output_file );
    }
}


if (! function_exists('__percentage_calculet')) {
    function __percentage_calculet( $totale,$persentage) {
        return ($persentage*$totale)/100;
    }
}


if (! function_exists('__tax_calculet')) {
    function __tax_calculet( $amount,$tax) {
        return (($amount * $tax) / 100);
    }
}

if (!function_exists('__maximum_score')) {
    function __maximum_score($json_data, $input_value, $filed_value = null)
    {
        $array_value = json_decode($json_data);
        foreach ($array_value as $value) {
            if ($value->max >= $input_value && $value->min <= $input_value) {
                return  $value->value;
            }
        }
    }
}


if (!function_exists('__find_radio_score')) {
    function __find_radio_score($json_data, $input_value)
    {
        // dd($json_data);
        $array_value = json_decode($json_data);
        $filed = $array_value[$input_value];
        return $filed->value;
    }
}


if (!function_exists('__find_checkbox_score')) {
    function __find_checkbox_score($json_data, $input_value)
    {
        $sum = 0;
        $array_value = json_decode($json_data);
        // dd($array_value);
        foreach ($input_value as $key => $value) {
            $filed = $array_value[$value];
            // dd($filed);
            $sum += $filed->value;
        }
        return $sum;
    }
}

if (!function_exists('__account_activation')) {
    function __account_activation($email=null, $panel=null)
    {
        if(!$email) return false;

        if($panel=='assessor') {    // For Assessor
            $user = Assessor::where('email', $email)->first();
        }

        if($panel=='company') {    // For Company
            $user = Company::where('email', $email)->first();
        }

        // Generate Account activation token
        $user->email_verification_token = Str::random(32);
        $user->save();

        $user->notify(new AccountActivationNotification($panel));

        return true;
    }
}


// $image = __base64_to_jpeg( $my_base64_string, 'tmp.jpg' );
