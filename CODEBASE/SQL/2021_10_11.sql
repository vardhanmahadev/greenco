CREATE TABLE `verify_otp`(
    `id` INT(11) NOT NULL,
    `email` VARCHAR(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `otp` VARCHAR(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `otp_expiry` TIMESTAMP NULL DEFAULT NULL,
    `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP(),
    `updated_at` TIMESTAMP NULL DEFAULT NULL
) ENGINE = INNODB DEFAULT CHARSET = utf8mb4;

ALTER TABLE `assessors` ADD `designation` VARCHAR(255) NULL DEFAULT NULL AFTER `mobile`;