<?php

use Illuminate\Support\Facades\Route;

// Controllers
use App\Http\Middleware\CheckAssessorStatus;
use App\Http\Middleware\CheckAssessorEmailChanged;
use App\Http\Controllers\Assessor\AuthController;
use App\Http\Controllers\Assessor\AccountController;
use App\Http\Controllers\Assessor\DashboardController;
use App\Http\Controllers\Assessor\ApplicationController;

/*
|--------------------------------------------------------------------------
| Assessor Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/', function () {
    echo 'Assessor';
});

Route::any('login', [AuthController::class, 'login'])->name('login');
Route::any('register', [AuthController::class, 'register'])->name('register');
Route::post('addassessor', [AuthController::class, 'addassessor'])->name('addassessor');
Route::any('registerSuccess', [AuthController::class, 'registerSuccess'])->name('registerSuccess');
Route::any('accountActivated', [AuthController::class, 'accountActivated'])->name('accountActivated');
Route::get('verify/{token}', [AuthController::class, 'verify'])->name('verify');
Route::any('forgot-password', [AuthController::class, 'forgotPassword'])->name('forgotPassword');
Route::any('forgotPassword', [AuthController::class, 'forgotPasswordPage'])->name('forgotPasswordPage');


Route::post('/send_otp', [AuthController::class, 'send_otp'])->name('send_otp');

Route::middleware(['auth:assessor','CheckAssessorStatus','CheckAssessorEmailChanged'])->group(function () {

    Route::any('profile', [AccountController::class, 'profile'])->name('profile');
    // Route::any('profiledocs', [AccountController::class, 'profiledocs'])->name('profiledocs');
    Route::any('profileupdate', [AccountController::class, 'profileupdate'])->name('profileupdate');
    Route::any('submitprofile',[AccountController::class,'submitprofile'])->name('submitprofile');
    Route::any('ndadocumentslist',[AccountController::class,'ndadocumentslist'])->name('ndadocumentslist');

    

    Route::any('dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('change-password', [AccountController::class, 'changePassword'])->name('changePassword');
    Route::post('changepassword', [AccountController::class, 'changePasswordSave'])->name('changePassword.save');
    Route::any('logout', [AccountController::class, 'logout'])->name('logout');
});
