<?php

use Illuminate\Support\Facades\Route;

// Controllers
use App\Http\Middleware\checkStatusCompany;
use App\Http\Middleware\CheckCompanyEmailChanged;
use App\Http\Controllers\Company\AuthController;
use App\Http\Controllers\Company\AccountController;
use App\Http\Controllers\Company\DashboardController;
/*
|--------------------------------------------------------------------------
| Company Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/', function () {
    echo 'Company';
});

Route::any('login', [AuthController::class, 'login'])->name('login');
Route::any('forgot-password', [AuthController::class, 'forgotPassword'])->name('forgotPassword');
Route::any('forgotPassword', [AuthController::class, 'forgotPasswordPage'])->name('forgotPasswordPage');
Route::any('register', [AuthController::class, 'register'])->name('register');
Route::post('addcompany', [AuthController::class, 'addcompany'])->name('addcompany');
Route::any('registerSuccess', [AuthController::class, 'registerSuccess'])->name('registerSuccess');
Route::any('accountActivated', [AuthController::class, 'accountActivated'])->name('accountActivated');
Route::get('verify/{token}', [AuthController::class, 'verify'])->name('verify');
Route::get('companieslist',[AuthController::class, 'companieslist'])->name('companieslist');



Route::middleware(['auth:company','checkStatusCompany','CheckCompanyEmailChanged'])->group(function () {

    Route::any('profile', [AccountController::class, 'profile'])->name('profile');
    Route::post('updateprofile', [AccountController::class, 'updateprofile'])->name('updateprofile');
    Route::post('submitprofile', [AccountController::class, 'submitprofile'])->name('submitprofile');
    Route::any('documents', [AccountController::class, 'documentsPage'])->name('documents');
    Route::post('uploaddocuments', [AccountController::class, 'uploaddocuments'])->name('uploaddocuments');
    Route::any('dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::any('change-password', [AccountController::class, 'changePassword'])->name('changePassword');
    Route::any('changePassword', [AccountController::class, 'changePasswordPage'])->name('changePasswordPage');
    Route::any('logout', [AccountController::class, 'logout'])->name('logout');
});
