<?php

use Illuminate\Support\Facades\Route;

// Controllers
use App\Http\Controllers\Consultant\AuthController;
use App\Http\Controllers\Consultant\AccountController;
use App\Http\Controllers\Consultant\DashboardController;

/*
|--------------------------------------------------------------------------
| Consultant Routes
|--------------------------------------------------------------------------
|
*/
