<?php

use Illuminate\Support\Facades\Route;

// Controllers
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\RegionController;
use App\Http\Controllers\Admin\VendorController;
use App\Http\Controllers\Admin\AccountController;
use App\Http\Controllers\Admin\AssessorController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\ConsultantController;
use App\Http\Controllers\Admin\ApplicationController;
use App\Http\Controllers\Admin\CompanyController;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
*/

Route::get('/', function () {
    echo 'Admin';
});

Route::any('login', [AuthController::class, 'login'])->name('login');
Route::any('forgot-password', [AuthController::class, 'forgotPassword'])->name('forgotPassword');
Route::any('forgotPassword', [AuthController::class, 'forgotPasswordPage'])->name('forgotPasswordPage');

Route::middleware(['auth:admin'])->group(function () {

    Route::any('dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::any('profile', [AccountController::class, 'profile'])->name('profile');
    Route::any('change-password', [AccountController::class, 'changePassword'])->name('changePassword');
    Route::any('changePassword', [AccountController::class, 'changePasswordPage'])->name('changePasswordPage');
    Route::any('logout', [AuthController::class, 'logout'])->name('logout');

    Route::get('assessors_data', [AssessorController::class, 'data'])->name('assessors.data');
    Route::put('assessors_bulk_update', [AssessorController::class, 'statusUpdateBlock'])->name('assessors.bulk_update');
    Route::get('assessors_bulk_export', [AssessorController::class, 'exportBlock'])->name('assessors.bulk_export');
    Route::resource('assessors', AssessorController::class);
    Route::any('nda/{id}',[AssessorController::class, 'nda'])->name('assessors.nda');
    Route::get('assessors_approve/{assessor}', [AssessorController::class, 'approve'])->name('assessors.approve');
    Route::get('remark_logs/{id}', [AssessorController::class, 'remarkLogs'])->name('assessors.logs');
    Route::any('edit/{id}', [AssessorController::class, 'addEditAssessor'])->name('assessors.addEditAssessor');


    Route::get('consultant_data', [ConsultantController::class, 'data'])->name('consultant.data');
    Route::put('consultant_bulk_update', [ConsultantController::class, 'statusUpdateBlock'])->name('consultant.bulk_update');
    Route::get('consultant_bulk_export', [ConsultantController::class, 'exportBlock'])->name('consultant.bulk_export');
    Route::resource('consultant', ConsultantController::class);
    Route::get('consultant_approve/{consultant}', [ConsultantController::class, 'approve'])->name('consultant.approve');

    Route::any('', [CompanyController::class, 'index'])->name('company.index');
    Route::resource('company', CompanyController::class);
    Route::put('status_change', [CompanyController::class, 'status_change'])->name('company.status_change');
    Route::get('company_approve/{company}', [CompanyController::class, 'approve'])->name('company.approve');
    Route::any('addCompany', [CompanyController::class, 'addCompany'])->name('company.addCompany');
    Route::any('addEditCompany', [CompanyController::class, 'addEditCompany'])->name('company.addEditCompany');
    Route::get('company_data', [CompanyController::class, 'data'])->name('company.data');
    Route::any('editCompany', [CompanyController::class, 'editCompany'])->name('company.editCompany');
    Route::any('deleteCompany/{id}', [CompanyController::class, 'deleteCompany'])->name('company.deleteCompany');
});
