/**
Core script to handle the entire theme and core functions
**/
var JsUtility = function() {
    return {
        showToastr: function(type='success', title='', message='') {
            toastr[type](message, title, {
                closeButton: true,
                tapToDismiss: false,
                progressBar: true,
                timeOut: 1000
            });
        },

        reload: function(time=0) {
            setTimeout(function () {
                location.reload(true);
            }, time);
        }
    }
}();