-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 11, 2022 at 06:51 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `greenco`
--

-- --------------------------------------------------------

--
-- Table structure for table `assessors`
--

CREATE TABLE `assessors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verification_token` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) DEFAULT 0,
  `verification_status` tinyint(1) NOT NULL DEFAULT 0,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verification_done_by` int(11) DEFAULT NULL,
  `verification_done_at` timestamp NULL DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_updated_by` int(11) DEFAULT NULL,
  `status_updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `health_doc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst_form` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_stamp` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ndc_form` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cancelled_check` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cancelled_check_status` tinyint(4) DEFAULT 0,
  `cancelled_check_remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `health_doc_status` tinyint(1) DEFAULT 0,
  `health_doc_remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst_form_status` tinyint(4) DEFAULT 0,
  `gst_form_remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_stamp_status` tinyint(4) DEFAULT 0,
  `vendor_stamp_remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ndc_form_status` tinyint(4) DEFAULT 0,
  `ndc_form_remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acc_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ifsc_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_updated` tinyint(4) DEFAULT 0,
  `category` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` tinyint(4) DEFAULT NULL,
  `alt_mobile` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `em_contact_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pan_status` tinyint(4) DEFAULT 0,
  `pan_remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `em_contact_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `em_contact_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biodata` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biodata_remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biodata_status` tinyint(4) DEFAULT NULL,
  `address_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pincode` bigint(20) DEFAULT NULL,
  `em_address_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `em_address_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `em_city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `em_state` tinyint(4) DEFAULT NULL,
  `em_pincode` bigint(20) DEFAULT NULL,
  `pan_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gst_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assessor_grade` tinyint(4) DEFAULT NULL,
  `enrollment_date` timestamp NULL DEFAULT NULL,
  `lead_assessor` tinyint(4) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assessors`
--

INSERT INTO `assessors` (`id`, `name`, `email`, `mobile`, `designation`, `email_verification_token`, `email_verified_at`, `status`, `verification_status`, `password`, `remember_token`, `verification_done_by`, `verification_done_at`, `remark`, `status_updated_by`, `status_updated_at`, `created_at`, `updated_at`, `health_doc`, `gst_form`, `vendor_stamp`, `ndc_form`, `cancelled_check`, `cancelled_check_status`, `cancelled_check_remark`, `health_doc_status`, `health_doc_remark`, `gst_form_status`, `gst_form_remark`, `vendor_stamp_status`, `vendor_stamp_remark`, `ndc_form_status`, `ndc_form_remark`, `bank_name`, `acc_no`, `ifsc_code`, `branch_name`, `profile_updated`, `category`, `state`, `alt_mobile`, `em_contact_name`, `pan`, `pan_status`, `pan_remark`, `em_contact_no`, `em_contact_address`, `biodata`, `biodata_remark`, `biodata_status`, `address_1`, `address_2`, `city`, `pincode`, `em_address_1`, `em_address_2`, `em_city`, `em_state`, `em_pincode`, `pan_number`, `gst_number`, `assessor_grade`, `enrollment_date`, `lead_assessor`) VALUES
(1, 'mahadev vardhan', 'vardhanm@mirakitech.com', '7036221419', NULL, NULL, '2022-01-12 06:59:02', 1, 1, '$2y$10$286Wqx6YfQQo3f2v1rF8/uAvYFTvcQ/8VYh9uwV/wP/UI8hJLmfzy', NULL, 1, '2022-01-12 06:58:14', NULL, 1, '2022-01-12 06:59:02', '2022-01-12 06:58:15', '2022-01-18 10:25:51', 'a', 'a', 'a', 'a', 'a', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 'asd', NULL, NULL, NULL, NULL, 0, '[\"1\"]', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'mahadev vardhan', 'vardhan.mahadev@gmail.com', '8234234234', NULL, NULL, NULL, 1, 1, '$2y$10$ydMCogmQfhktBmyHdzSkAeu1CwgZ4QyFA.w42gwSDUIOt00ebaGru', NULL, 1, '2022-01-12 07:05:28', NULL, 1, '2022-01-12 07:05:28', '2022-01-12 07:05:28', '2022-01-12 07:05:28', 'a', 'a', 'a', 'a', 'a', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '[\"1\"]', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'mahadev vardhan', 'vardhanm@yopmail.com', '7034533454', NULL, NULL, NULL, 0, 1, '$2y$10$29ciXoS7Xmf5dpaqIiMhB.ZARPmPYtnL5f8TaUKeMzZGveNJGrrKu', NULL, 1, '2022-01-13 05:31:10', NULL, 1, '2022-01-13 05:31:10', '2022-01-13 05:31:10', '2022-01-18 10:30:31', 'uploads/assessors/3/20220113114454_GreenCo.xml', 'uploads/assessors/3/20220113114454_assessors (6).xlsx', 'uploads/assessors/3/20220113114454_GreenCo.xml', 'uploads/assessors/3/20220113114454_states (4).sql', 'uploads/assessors/3/20220113124446_GreenCo.xml', NULL, NULL, 2, 'asdads', NULL, NULL, NULL, NULL, NULL, NULL, 'fgghf', '6666666666', '5555555', 'kjhkjkhj', 0, '[\"1\"]', 4, '2147483647', 'hhh', 'uploads/assessors/3/20220113114454_assessors (6).xlsx', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'anirudh', 'anirudhs@yopmail.com', '8232342342', NULL, NULL, NULL, 1, 1, '$2y$10$oHSnwMb/hrzTau6XCxPrKeV9JEqw9CuIGbAi7MAXPPWO.dodQ6nse', NULL, 1, '2022-01-13 07:19:19', NULL, 1, '2022-01-13 07:19:19', '2022-01-13 07:19:19', '2022-01-18 06:40:09', 'uploads/assessors/4/20220113185810_1641774328590.jfif', 'uploads/assessors/4/20220113185810_greenco (2).sql', 'uploads/assessors/4/20220113185810_1641774328590.jfif', 'uploads/assessors/4/20220113185810_1641774328590.jfif', 'uploads/assessors/4/20220113185810_1641774328590.jfif', 1, NULL, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 'adsasd', 'asddadasdasdasdasdasdsadsadsadsadsasdadsa', 'sadsadsdads', 'asd1', 1, '[\"1\"]', 4, '2147483647', 'asdasd', 'uploads/assessors/4/20220113185810_1641774328590.jfif', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'naveen', 'naveen@yopmail.com', '9888834234', NULL, 'SCA3Hi8ihQ027aaPhvQjmhJhZJLQ9hdp', NULL, 0, 1, '$2y$10$HW1pgM4dodDSVlKvV2XcXuGf.Q4Py9c1TuTl2Qq4tGoJg3XKWOqF6', NULL, 1, '2022-01-19 04:18:00', NULL, 1, '2022-01-12 19:30:53', '2022-01-13 07:30:54', '2022-01-19 16:18:00', NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, '[\"1\"]', 10, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'ranjan', 'ranjan@yopmail.com', '8234234243', NULL, 'PWw9HwMv5dcsMqKAFkqOQN7LMsKpADVr', NULL, 0, 0, '$2y$10$gLmiiLQWBupfbYjp5JfSzurDmWbiO4KPlxhUzRLkQSZtoh3RKUEny', NULL, 1, '2022-01-18 06:19:58', NULL, 1, '2022-01-18 06:19:58', '2022-01-18 06:19:58', '2022-01-19 03:52:27', 'uploads/assessors/8/20220118224805_New Guide to Eastern College for International Agents.pdf', 'uploads/assessors/8/20220118224805_New Guide to Eastern College for International Agents.pdf', 'uploads/assessors/8/20220118224806_New Guide to Eastern College for International Agents.pdf', 'uploads/assessors/8/20220118224806_New Guide to Eastern College for International Agents.pdf', 'uploads/assessors/8/20220118224805_New Guide to Eastern College for International Agents.pdf', 2, 'asdasdasd', 1, 'asdasd', 1, NULL, 0, NULL, 1, 'asdasd', NULL, NULL, NULL, NULL, 2, '2', 10, '1222222222', NULL, 'uploads/assessors/8/20220118224806_New Guide to Eastern College for International Agents.pdf', 1, 'asdasdasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'asasd', 'abc@yopmail.com', '9242342342', NULL, NULL, NULL, 1, 1, '$2y$10$8qJV4qKFUg/71Jl7xXMgoetHOMJDTJIeq4pfBKMvdevaZMX7yFxmm', NULL, 1, '2022-01-18 19:53:21', NULL, 1, '2022-01-18 06:18:28', '2022-01-18 18:18:28', '2022-01-21 10:31:21', 'uploads/assessors/12/20220121160121_20220119114926_Holiday Calendar_Miraki Technologies - 2022.pdf', 'uploads/assessors/12/20220121160121_20220119114926_Holiday Calendar_Miraki Technologies - 2022.pdf', 'uploads/assessors/12/20220120182316_20220119114926_Holiday Calendar_Miraki Technologies - 2022.pdf', 'uploads/assessors/12/20220121160121_20220119114926_Holiday Calendar_Miraki Technologies - 2022.pdf', 'uploads/assessors/12/20220121160121_20220119114926_Holiday Calendar_Miraki Technologies - 2022.pdf', 1, NULL, 1, NULL, 1, NULL, 1, NULL, 1, NULL, 'fdgdfgdfgd', 'dfgdfgdfgdf', 'dfgdfgdfgdf', 'gdfgdfgdfg', 1, '[\"1\"]', 2, '2147483647', 'dfgdfgdfg', 'uploads/assessors/12/20220121160121_20220119114926_Holiday Calendar_Miraki Technologies - 2022.pdf', 1, NULL, '2222222222', 'dfgdfgdfg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'abcd', 'abcd@yopmail.com', '8234843534', NULL, 'odT3aNIw7aaNV123djlsuEb8ubQguTWj', NULL, 0, 0, '$2y$10$HBxoKlM8DxmHddHTe6c8pu8mYKmaqgGNE4Xuz7kfiKGG6GpxYvg0K', NULL, 1, '2022-01-18 22:55:32', NULL, 1, '2022-01-18 22:55:32', '2022-01-19 10:55:32', '2022-02-02 07:22:59', 'uploads/assessors/13/20220119162605_20220119114926_Holiday Calendar_Miraki Technologies - 2022.pdf', 'uploads/assessors/13/20220119162605_20220119114926_Holiday Calendar_Miraki Technologies - 2022.pdf', 'uploads/assessors/13/20220119162605_20220119114926_Holiday Calendar_Miraki Technologies - 2022.pdf', 'uploads/assessors/13/20220119162605_20220119114926_Holiday Calendar_Miraki Technologies - 2022.pdf', 'uploads/assessors/13/20220119162641_20220119114926_Holiday Calendar_Miraki Technologies - 2022.pdf', 1, 'asd', 1, NULL, 1, NULL, 1, NULL, 1, NULL, 'SDFSDFSDFSDFS', 'SDFSDFSDFSDFSDFSS', 'FFSDFFSDFFS', 'SDFSDFSDFSDSS', 1, '[\"1\"]', 12, '7147483647', 'asdasd', 'uploads/assessors/13/20220119162641_20220119114926_Holiday Calendar_Miraki Technologies - 2022.pdf', 1, NULL, '8778784444', NULL, NULL, NULL, NULL, 'ASD', NULL, 'ADS', 234234, 'ASD', NULL, 'ADS', 12, 234234, '1111111111', '111111111111111', NULL, NULL, 0),
(14, 'asdasd', 'asdas@asdsa.com', '7231231231', NULL, 'kk3Wlejfn5Zv34l3WDVRzuNTojl7y3VG', NULL, 0, 0, '$2y$10$nHF6e640Ewl/mH4u8PYpU.4Mvk38PjeAWTSeXKGaU28kXbahsfOCO', NULL, 1, '2022-01-19 20:57:55', NULL, 1, '2022-01-19 20:57:55', '2022-01-20 08:57:55', '2022-01-24 11:46:23', NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '[\"1\"]', 12, NULL, NULL, NULL, 0, NULL, NULL, NULL, 'uploads/assessors/14/20220124171417_20220120114544_20220119114926_Holiday Calendar_Miraki Technologies - 2022.pdf', NULL, 1, 'asd', NULL, 'hyderabda', 234234, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'mahadev vardha', 'vardhanmasd@yopmail.com', '7036221167', NULL, '41G2Jr4uWjJWx7An0rkkfQDAUsiOzF9Z', NULL, 0, 0, '$2y$10$eGNNaSiQ9gAiN2lj.ER.L.Rq1XoZSS8B71/4oi7cSySywMa8t9NA6', NULL, 1, '2022-01-19 21:10:00', NULL, 1, '2022-01-19 21:10:00', '2022-01-20 09:10:00', '2022-02-02 07:56:31', 'uploads/assessors/15/20220120150403_20220120114544_20220119114926_Holiday Calendar_Miraki Technologies - 2022.pdf', 'uploads/assessors/15/20220120150403_20220120114544_20220119114926_Holiday Calendar_Miraki Technologies - 2022.pdf', 'uploads/assessors/15/20220120150403_20220120114544_20220119114926_Holiday Calendar_Miraki Technologies - 2022.pdf', 'uploads/assessors/15/20220120150403_20220120114544_20220119114926_Holiday Calendar_Miraki Technologies - 2022.pdf', 'uploads/assessors/15/20220120150403_20220120114544_20220119114926_Holiday Calendar_Miraki Technologies - 2022.pdf', 2, 'sdsd', 2, 'asd', 2, 'asd', 2, 'asd', 2, 'asd', 'dsfsdfsdfsd', 'ASDASDASDASDASDASDAS', 'sdfsdfsdfsd', 'sdfsdfsdfsdfsdfsdfsdfsdf', 1, '[\"3\"]', 12, '9147488888', 'MAHADEV VARDHANNNNNWWW', 'uploads/assessors/15/20220120150403_20220120114544_20220119114926_Holiday Calendar_Miraki Technologies - 2022.pdf', 2, 'asdasd', '7036221419', NULL, NULL, NULL, NULL, 'ASD', 'ASDASD', 'ADS', 234234, '71 ST. NICHOLAS DRIVE', 'ABC', 'NORTH POLE', 15, 997052, '1111111111', '888888888888888', NULL, '2022-11-29 18:30:00', 0),
(16, 'MAHADEVVARDHANNNNNNNNNNNNNNNNNNNNNNNNNNNN', 'vardhanmasdasd@mirakitech.com', '7037897897', NULL, 'miBcuj1CE2VZIa7MrPI7Mxt6e2i8TU9k', NULL, 0, 0, '$2y$10$h62OwawjBUTplHYsnvnRMO4MxVarRlbMojdUijA9FUH8fLkS.NZGW', NULL, 1, '2022-01-24 21:54:49', NULL, 1, '2022-01-24 21:54:49', '2022-01-25 09:54:49', '2022-02-01 13:44:58', NULL, NULL, 'uploads/assessors/16/20220125163658_20220120114544_20220119114926_Holiday Calendar_Miraki Technologies - 2022.pdf', NULL, NULL, 0, NULL, 0, NULL, 0, NULL, 1, NULL, 0, NULL, 'ASD', NULL, NULL, 'ASD', 0, '[\"1\",\"2\"]', 12, NULL, 'ASD', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'asd', NULL, 'ads', 234234, NULL, NULL, NULL, NULL, NULL, '12121AASDA', '12312SADASDASDA', 4, '2022-01-19 18:30:00', 1),
(17, 'ARAV', 'aravii@yopmail.com', '6888234234', NULL, NULL, NULL, 1, 0, '$2y$10$8lczznApAnMba2aNAV3tGe13gy8.S.oHatwYYM8W5.Rf6.1YajGqm', NULL, 1, '2022-01-29 19:39:29', NULL, 1, '2022-01-29 19:39:29', '2022-01-30 07:39:29', '2022-02-09 12:10:31', NULL, NULL, NULL, NULL, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, '[\"1\"]', 12, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'ASD', NULL, 'ADS', 234234, NULL, NULL, NULL, NULL, NULL, '1111111111', '111111111111111', NULL, '2022-11-29 18:30:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `assessor_categories`
--

CREATE TABLE `assessor_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` tinyint(4) DEFAULT NULL,
  `updated_by` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assessor_categories`
--

INSERT INTO `assessor_categories` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Automobile & Auto Components', 1, 1, '2022-01-24 05:29:45', '2022-01-24 05:29:45'),
(2, 'Chemicals, Pharma & Fertilizers', 1, 1, '2022-01-24 05:29:45', '2022-01-24 05:29:45'),
(3, 'Cement', 1, 1, '2022-01-24 05:29:45', '2022-01-24 05:29:45'),
(4, 'Consumer Goods / FMCG', 1, 1, '2022-01-24 05:29:45', '2022-01-24 05:29:45'),
(5, 'Engineering', 1, 1, '2022-01-24 05:29:45', '2022-01-24 05:29:45'),
(6, 'Foundry / Metallurgical', 1, 1, '2022-01-24 05:29:45', '2022-01-24 05:29:45'),
(7, 'Glass', 1, 1, '2022-01-24 05:29:45', '2022-01-24 05:29:45'),
(8, 'Iron, Steel & Non Ferrous Metals', 1, 1, '2022-01-24 05:29:45', '2022-01-24 05:29:45'),
(9, 'Pulp & Paper', 1, 1, '2022-01-24 05:29:45', '2022-01-24 05:29:45'),
(10, 'Petroleum & Petrochemicals', 1, 1, '2022-01-24 05:29:45', '2022-01-24 05:29:45'),
(11, 'Railways', 1, 1, '2022-01-24 05:29:45', '2022-01-24 05:29:45'),
(12, 'Renewable Energy', 1, 1, '2022-01-24 05:29:45', '2022-01-24 05:29:45'),
(13, 'Textile & Garment', 1, 1, '2022-01-24 05:29:45', '2022-01-24 05:29:45'),
(14, 'Tyre', 1, 1, '2022-01-24 05:29:45', '2022-01-24 05:29:45'),
(15, 'Mines', 1, 1, '2022-01-24 05:29:45', '2022-01-24 05:29:45'),
(16, 'Airports', 1, 1, '2022-01-24 05:29:45', '2022-01-24 05:29:45'),
(17, 'IT & ITES companies', 1, 1, '2022-01-24 05:29:45', '2022-01-24 05:29:45'),
(18, 'Ports & Shipping', 1, 1, '2022-01-24 05:29:45', '2022-01-24 05:29:45'),
(19, 'Others', 1, 1, '2022-01-24 05:29:45', '2022-01-24 05:29:45');

-- --------------------------------------------------------

--
-- Table structure for table `assessor_grades`
--

CREATE TABLE `assessor_grades` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assessor_grades`
--

INSERT INTO `assessor_grades` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'A1', '2022-01-28 06:09:44', '2022-01-28 06:09:44'),
(2, 'A2', '2022-01-28 06:09:44', '2022-01-28 06:09:44'),
(3, 'B1', '2022-01-28 06:09:44', '2022-01-28 06:09:44'),
(4, 'B2', '2022-01-28 06:09:44', '2022-01-28 06:09:44'),
(5, 'C1', '2022-01-28 06:09:44', '2022-01-28 06:09:44'),
(6, 'C2', '2022-01-28 06:09:44', '2022-01-28 06:09:44');

-- --------------------------------------------------------

--
-- Table structure for table `assessor_ndas`
--

CREATE TABLE `assessor_ndas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `assessor_id` tinyint(4) DEFAULT NULL,
  `document` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` tinyint(4) DEFAULT NULL,
  `uploader_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `uploader_id` tinyint(4) DEFAULT NULL,
  `verified_status` tinyint(4) DEFAULT NULL,
  `verify_remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verified_by` tinyint(4) DEFAULT NULL,
  `verified_on` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `assessor_verify_log`
--

CREATE TABLE `assessor_verify_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `assessor_id` int(11) DEFAULT NULL,
  `verified_status` tinyint(3) UNSIGNED DEFAULT NULL,
  `verified_remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verified_at` timestamp NULL DEFAULT NULL,
  `verified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assessor_verify_log`
--

INSERT INTO `assessor_verify_log` (`id`, `assessor_id`, `verified_status`, `verified_remark`, `verified_at`, `verified_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, '2022-01-12 06:58:18', 1, '2022-01-12 06:58:18', '2022-01-12 06:58:18'),
(2, 2, 1, NULL, '2022-01-12 07:05:28', 1, '2022-01-12 07:05:28', '2022-01-12 07:05:28'),
(3, 3, 1, NULL, '2022-01-13 05:31:10', 1, '2022-01-13 05:31:10', '2022-01-13 05:31:10'),
(4, 4, 1, NULL, '2022-01-13 07:19:19', 1, '2022-01-13 07:19:19', '2022-01-13 07:19:19'),
(7, 12, 1, NULL, '2022-01-18 19:53:21', 1, '2022-01-19 07:53:21', '2022-01-19 07:53:21'),
(8, 7, 2, 'remark', '2022-01-19 04:17:48', 1, '2022-01-19 16:17:48', '2022-01-19 16:17:48'),
(9, 7, 1, NULL, '2022-01-19 04:18:00', 1, '2022-01-19 16:18:00', '2022-01-19 16:18:00');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reg_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` bigint(20) UNSIGNED DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `postal_address_pincode` int(11) DEFAULT NULL,
  `billing_address_pincode` int(11) DEFAULT NULL,
  `mst_industry_id` bigint(20) DEFAULT NULL,
  `mst_entity_id` bigint(20) DEFAULT NULL,
  `mst_sector_id` bigint(20) DEFAULT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `turnover` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `budget_allocation` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_employees` bigint(20) DEFAULT NULL,
  `contract_employees` bigint(20) DEFAULT NULL,
  `area` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `electrical_consumption` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thermal_consumption` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `water_consumption` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanno` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `panno` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gstno` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `proposal_document` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `po_document` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_status` int(11) NOT NULL DEFAULT 0,
  `verified_status` int(11) NOT NULL DEFAULT 0,
  `verified_remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verified_by` bigint(20) DEFAULT NULL,
  `verified_at` timestamp NULL DEFAULT NULL,
  `status_updated_by` int(11) DEFAULT NULL,
  `status_updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `email_verification_token` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `profile_update` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `reg_id`, `name`, `email`, `password`, `mobile`, `location`, `postal_address`, `billing_address`, `state`, `postal_address_pincode`, `billing_address_pincode`, `mst_industry_id`, `mst_entity_id`, `mst_sector_id`, `product_name`, `turnover`, `budget_allocation`, `no_of_employees`, `contract_employees`, `area`, `electrical_consumption`, `thermal_consumption`, `water_consumption`, `tanno`, `panno`, `gstno`, `proposal_document`, `po_document`, `account_status`, `verified_status`, `verified_remark`, `verified_by`, `verified_at`, `status_updated_by`, `status_updated_at`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`, `email_verification_token`, `email_verified_at`, `profile_update`) VALUES
(26, '', 'Ravi Company', 'ravikcompany@yopmail.com', '$2y$10$Nw53gd2QKgiEaMw2vREEteneWwna8jkE5i1gKAglPjsztPiKMWGgS', 8765679999, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, NULL, NULL, '2022-02-02 06:03:30', NULL, NULL, '2022-02-02 06:02:03', '2022-02-02 06:03:30', NULL, NULL, '2022-02-02 06:03:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `companies_coordinator`
--

CREATE TABLE `companies_coordinator` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` tinyint(4) DEFAULT NULL,
  `company_reg_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coordinator_id` tinyint(4) DEFAULT NULL,
  `coordinator_assigned_by` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `companies_facilitator`
--

CREATE TABLE `companies_facilitator` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` tinyint(4) DEFAULT NULL,
  `company_reg_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facilitator_id` tinyint(4) DEFAULT NULL,
  `facilitator_assigned_by` tinyint(4) DEFAULT NULL,
  `contract_acceptance` int(11) NOT NULL DEFAULT 0,
  `contract_doc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contract_doc_status` tinyint(4) DEFAULT NULL,
  `contract_doc_remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `companies_online_transactions_logs`
--

CREATE TABLE `companies_online_transactions_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `trans_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(10,2) DEFAULT NULL,
  `payment_type` enum('o1','o2') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'o1-Online,o2-Offline',
  `response` enum('s','f') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 's-Success,f-Failed',
  `payment_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `companies_payments_invoices_info`
--

CREATE TABLE `companies_payments_invoices_info` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` tinyint(4) DEFAULT NULL,
  `company_reg_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_for` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'reg-Registration',
  `payment_type` enum('o1','o2') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'o1-Online,o2-Offline',
  `amount` double(10,2) DEFAULT NULL,
  `payment_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1-Paid,0-Unpaid',
  `payment_date` timestamp NULL DEFAULT NULL,
  `trans_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `offline_tran_doc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoice` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approval_status` tinyint(4) DEFAULT NULL COMMENT '1-Approved,2-Disapproved',
  `approved_by` tinyint(4) DEFAULT NULL,
  `approved_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `companies_wo_logs`
--

CREATE TABLE `companies_wo_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_id` tinyint(4) DEFAULT NULL,
  `company_reg_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wo_doc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wo_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wo_doc_status_updated_by` tinyint(4) DEFAULT NULL,
  `wo_doc_status_updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `consultants`
--

CREATE TABLE `consultants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT 0,
  `verification_status` int(11) NOT NULL DEFAULT 0,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verification_done_by` int(11) DEFAULT NULL,
  `verification_done_at` timestamp NULL DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_updated_by` int(11) DEFAULT NULL,
  `status_updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `consultants`
--

INSERT INTO `consultants` (`id`, `name`, `email`, `mobile`, `email_verified_at`, `status`, `verification_status`, `password`, `remember_token`, `verification_done_by`, `verification_done_at`, `remark`, `status_updated_by`, `status_updated_at`, `created_at`, `updated_at`) VALUES
(1, 'mahadev vardhan', 'vardhanm@mirakitech.com', '7036221419', NULL, 0, 2, '$2y$10$AmYNRKFg0S3qej6LasD/JuXpIn.zUZ5pXujbzfP1kE.G/7QUPr2Ly', NULL, 1, '2021-12-20 21:51:55', 'Rejected', NULL, NULL, NULL, '2021-12-21 09:51:55'),
(2, 'mahadev vardhan', 'vardhanm123@mirakitech.com', '7036221420', NULL, 0, 2, '$2y$10$G4uNxTqNrXub8tEn7IQ5HOa/vC7xhTVH3Z1TCX.BVXSNIbC9e7qvm', NULL, 1, '2021-12-20 21:51:11', 'Rejected', 1, '2021-12-21 05:09:33', '2021-12-20 09:55:28', '2021-12-21 09:51:11'),
(3, 'Nanda Gopal', 'basarlapudi@yopmail.com', '9799089809', NULL, 0, 1, '$2y$10$qIU2L1nWiBLoabIcVU45cehEcbRG/blwaoqtdw0dgEQyq1rmAdcae', NULL, 1, '2021-12-20 21:50:35', NULL, 1, '2021-12-21 05:09:37', '2021-12-20 12:30:07', '2021-12-21 09:50:35'),
(4, 'puri', 'puri@yopmail.com', '7867867867', NULL, 0, 1, '$2y$10$MOXV/IbkU.eBZNE4AZToU.PHMruPyMjviOBKNeiaW.goWicyOFgsG', NULL, 1, '2021-12-20 21:49:43', NULL, 1, '2021-12-20 00:38:58', '2021-12-20 12:35:35', '2021-12-21 09:49:43'),
(9, 'mahadev vardhan', 'vardhanmm@mirakitech.com', '7036221555', NULL, 0, 0, '$2y$10$1Ti4nEPuHbAxOaiVEZnyaubT912y2u9XMsqXGbm1m3H5NRDbeYFe6', NULL, 1, '2022-01-04 07:05:36', NULL, 1, '2022-01-04 07:05:36', '2022-01-04 07:05:36', '2022-01-04 07:05:36'),
(10, 'mahadev', 'vardhanma@mirakitech.com', '7036234234', NULL, 0, 1, '$2y$10$M/O3/FNzxsToIZ8/o3usIOGaxtC8qP12O82toP.ZDxg78WClWk2qW', NULL, 1, '2022-01-05 06:05:25', NULL, 1, '2022-01-06 07:21:32', '2022-01-05 06:05:25', '2022-01-06 07:21:36'),
(11, 'ravi', 'ravi@yopmail.com', '9812312312', NULL, 0, 0, '$2y$10$.9q25QiZETJ5j8iePR4Sf.Me9eniEgApm.zei5RumBvSZFXmnXRE2', NULL, 1, '2022-01-06 07:08:38', NULL, 1, '2022-01-06 07:21:19', '2022-01-06 07:08:38', '2022-01-06 07:21:23');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `master_entities`
--

CREATE TABLE `master_entities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `master_entities`
--

INSERT INTO `master_entities` (`id`, `name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Private sector', 1, 1, NULL, NULL, NULL, NULL),
(2, 'Public sector', 1, 1, NULL, NULL, NULL, NULL),
(3, 'Indian Railways', 1, 1, NULL, NULL, NULL, NULL),
(4, 'Other Government Enterprise', 1, 1, NULL, NULL, NULL, NULL),
(5, 'Not for profit', 1, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_industries`
--

CREATE TABLE `master_industries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `master_industries`
--

INSERT INTO `master_industries` (`id`, `name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Manufacturing', 1, 1, NULL, NULL, NULL, NULL),
(2, 'Service', 1, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_sectors`
--

CREATE TABLE `master_sectors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `master_sectors`
--

INSERT INTO `master_sectors` (`id`, `name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Airports', 1, 1, NULL, NULL, NULL, NULL),
(2, 'Auto Component', 1, 1, NULL, NULL, NULL, NULL),
(3, 'Automobile', 1, 1, NULL, NULL, NULL, NULL),
(4, 'Aviation', 1, 1, NULL, NULL, NULL, NULL),
(5, 'Biotechnology', 1, 1, NULL, NULL, NULL, NULL),
(6, 'Cement', 1, 1, NULL, NULL, NULL, NULL),
(7, 'Chemicals', 1, 1, NULL, NULL, NULL, NULL),
(8, 'Construction', 1, 1, NULL, NULL, NULL, NULL),
(9, 'Defence Manufacturing', 1, 1, NULL, NULL, NULL, NULL),
(10, 'Electrical & Electronics', 1, 1, NULL, NULL, NULL, NULL),
(11, 'Engineering', 1, 1, NULL, NULL, NULL, NULL),
(12, 'Fetilizer', 1, 1, NULL, NULL, NULL, NULL),
(13, 'FMCG', 1, 1, NULL, NULL, NULL, NULL),
(14, 'Food rpocessing', 1, 1, NULL, NULL, NULL, NULL),
(15, 'Glass', 1, 1, NULL, NULL, NULL, NULL),
(16, 'Hospitality & Wellness', 1, 1, NULL, NULL, NULL, NULL),
(17, 'Iron & Steel', 1, 1, NULL, NULL, NULL, NULL),
(18, 'IT & ITES', 1, 1, NULL, NULL, NULL, NULL),
(19, 'Leather', 1, 1, NULL, NULL, NULL, NULL),
(20, 'Mining', 1, 1, NULL, NULL, NULL, NULL),
(21, 'Non ferrous Metals', 1, 1, NULL, NULL, NULL, NULL),
(22, 'Oil & Gas', 1, 1, NULL, NULL, NULL, NULL),
(23, 'Other Manufacturing Sector', 1, 1, NULL, NULL, NULL, NULL),
(24, 'Other Service Sector', 1, 1, NULL, NULL, NULL, NULL),
(25, 'Petrochemicals', 1, 1, NULL, NULL, NULL, NULL),
(26, 'Pharma', 1, 1, NULL, NULL, NULL, NULL),
(27, 'Ports & Shipping', 1, 1, NULL, NULL, NULL, NULL),
(28, 'Power Plant', 1, 1, NULL, NULL, NULL, NULL),
(29, 'Pulp & Paper', 1, 1, NULL, NULL, NULL, NULL),
(30, 'Railway Production unit', 1, 1, NULL, NULL, NULL, NULL),
(31, 'Railway Workshop', 1, 1, NULL, NULL, NULL, NULL),
(32, 'Renewable Energy', 1, 1, NULL, NULL, NULL, NULL),
(33, 'SME ( only  if turn over Less than 50 Crore)', 1, 1, NULL, NULL, NULL, NULL),
(34, 'Textile & Garments', 1, 1, NULL, NULL, NULL, NULL),
(35, 'Tyre', 1, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `meta_ids`
--

CREATE TABLE `meta_ids` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parameter` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` int(11) NOT NULL DEFAULT 0,
  `latest_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `meta_ids`
--

INSERT INTO `meta_ids` (`id`, `parameter`, `value`, `latest_value`, `created_at`, `updated_at`) VALUES
(1, 'companies', 11, 'C00011', '2021-12-14 09:18:14', '2022-01-05 10:54:12');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(4, '2021_10_18_163911_create_jobs_table', 1),
(5, '2021_11_18_133855_create_users_table', 1),
(6, '2021_11_18_185817_create_assessors_table', 1),
(7, '2021_11_19_165859_create_consultants_table', 1),
(11, '2021_11_24_181720_create_companies_table', 2),
(12, '2021_11_25_111718_create_master_entities_table', 3),
(13, '2021_11_25_111935_create_master_industries_table', 4),
(14, '2021_11_25_112218_create_master_sectors_table', 4),
(15, '2021_12_10_163158_update_companies_table', 5),
(16, '2021_12_13_173033_update_companies1_table', 6),
(18, '2021_12_14_143722_create_meta_ids_table', 7),
(23, '2021_12_16_175742_update_assessor_table', 8),
(24, '2021_12_17_110501_update_companies2_table', 9),
(25, '2021_12_20_152805_update_consultants_table', 10),
(26, '2021_12_31_122557_create_assessor_verify_log_table', 11),
(27, '2022_01_06_104355_create_states_table', 12),
(28, '2022_01_07_120654_update_companies_table1', 13),
(29, '2022_01_10_103132_update_assessors_table_1', 14),
(31, '2022_01_10_123538_update_assessors_table2', 15),
(32, '2022_01_11_112506_update_assessors_table_3', 16),
(34, '2022_01_11_183708_update_assessors_table_4', 17),
(35, '2022_01_12_153418_update_assessors_tabel_5', 18),
(36, '2022_01_12_163754_update_assessors_table_6', 19),
(37, '2022_01_18_125110_update_assessors_table_7', 20),
(38, '2022_01_18_182545_create_assessor_categories_table', 21),
(40, '2022_01_24_122736_update_assessors_table_10', 22),
(41, '2022_01_28_112943_update_assessor_table_11', 23),
(42, '2022_01_28_113428_create_assessor_grades_table', 23),
(43, '2022_02_03_162524_create_assessor_ndas_table', 24),
(44, '2022_02_10_120404_create_comapny_coordinator_detail_table', 25),
(45, '2022_02_10_120650_create_company_facilator_detail_table', 25),
(46, '2022_02_10_120750_create_company_wo_logs_table', 25),
(47, '2022_02_10_120839_create_company_payments_invoices_info_table', 25),
(48, '2022_02_10_120903_create_company_online_transactions_logs_table', 25);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE `states` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` tinyint(4) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`, `country_id`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`) VALUES
(1, 'Andaman and Nicobar Islands', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(2, 'Andhra Pradesh', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(3, 'Arunachal Pradesh', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(4, 'Assam', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(5, 'Bihar', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(6, 'Chandigarh', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(7, 'Chhattisgarh', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(8, 'Dadra and Nagar Haveli', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(9, 'Daman and Diu', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(10, 'Delhi', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(11, 'Goa', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(12, 'Gujarat', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(13, 'Haryana', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(14, 'Himachal Pradesh', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(15, 'Jammu and Kashmir', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(16, 'Jharkhand', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(17, 'Karnataka', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(18, 'Kerala', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(19, 'Lakshadweep', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(20, 'Madhya Pradesh', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(21, 'Maharashtra', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(22, 'Manipur', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(23, 'Meghalaya', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(24, 'Mizoram', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(25, 'Nagaland', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(26, 'Odisha', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(27, 'Pondicherry', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(28, 'Punjab', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(29, 'Rajasthan', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(30, 'Sikkim', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(31, 'Tamil Nadu', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(32, 'Telangana', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(33, 'Tripura', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(34, 'Uttar Pradesh', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(35, 'Uttarakhand', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43'),
(36, 'West Bengal', 1, 1, 1, NULL, '2022-01-06 05:57:43', '2022-01-06 05:57:43');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `roll` enum('A','SA','RT') COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'SA-Supra Admin,A-Admin, RT-Regular Team Member',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `mobile`, `email_verified_at`, `roll`, `status`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'adminasdasd', 'admin@admin.com', '7036221419', '2021-11-24 11:13:02', 'SA', 1, '$2y$10$aXV1P41R/v5oc59znRbJlemnV0zyh/Wwjms3Z40XQtRervzsgEBIC', NULL, NULL, '2022-01-07 07:39:08'),
(3, 'Vardhan Mahadev', 'vardhanm@mirakitech.com', '7036221420', NULL, 'SA', 1, '$2y$10$KGviy0sebyYHycPfUXN0R.9hkUiy80Bac.6G17aQh9gLyxM4Q2JGu', NULL, NULL, '2021-12-07 10:32:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assessors`
--
ALTER TABLE `assessors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `assessors_email_unique` (`email`),
  ADD UNIQUE KEY `assessors_mobile_unique` (`mobile`);

--
-- Indexes for table `assessor_categories`
--
ALTER TABLE `assessor_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assessor_grades`
--
ALTER TABLE `assessor_grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assessor_ndas`
--
ALTER TABLE `assessor_ndas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assessor_verify_log`
--
ALTER TABLE `assessor_verify_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies_coordinator`
--
ALTER TABLE `companies_coordinator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies_facilitator`
--
ALTER TABLE `companies_facilitator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies_online_transactions_logs`
--
ALTER TABLE `companies_online_transactions_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies_payments_invoices_info`
--
ALTER TABLE `companies_payments_invoices_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies_wo_logs`
--
ALTER TABLE `companies_wo_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `consultants`
--
ALTER TABLE `consultants`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `consultants_email_unique` (`email`),
  ADD UNIQUE KEY `consultants_mobile_unique` (`mobile`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `master_entities`
--
ALTER TABLE `master_entities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_industries`
--
ALTER TABLE `master_industries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_sectors`
--
ALTER TABLE `master_sectors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meta_ids`
--
ALTER TABLE `meta_ids`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_mobile_unique` (`mobile`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assessors`
--
ALTER TABLE `assessors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `assessor_categories`
--
ALTER TABLE `assessor_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `assessor_grades`
--
ALTER TABLE `assessor_grades`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `assessor_ndas`
--
ALTER TABLE `assessor_ndas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `assessor_verify_log`
--
ALTER TABLE `assessor_verify_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `companies_coordinator`
--
ALTER TABLE `companies_coordinator`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `companies_facilitator`
--
ALTER TABLE `companies_facilitator`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `companies_online_transactions_logs`
--
ALTER TABLE `companies_online_transactions_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `companies_payments_invoices_info`
--
ALTER TABLE `companies_payments_invoices_info`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `companies_wo_logs`
--
ALTER TABLE `companies_wo_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `consultants`
--
ALTER TABLE `consultants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `master_entities`
--
ALTER TABLE `master_entities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `master_industries`
--
ALTER TABLE `master_industries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `master_sectors`
--
ALTER TABLE `master_sectors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `meta_ids`
--
ALTER TABLE `meta_ids`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
