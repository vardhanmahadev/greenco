/*=========================================================================================
    File Name: form-select2.js
    Description: Select2 is a jQuery-based replacement for select boxes.
    It supports searching, remote data sets, and pagination of results.
    ----------------------------------------------------------------------------------------
  
    Author URL: hhttp://www.themeforest.net/user/solar roof top
==========================================================================================*/
(function (window, document, $) {
  'use strict';
  var select = $('.select2'),
    tagSelect = $('.tagSelect'),
   roleSelect = $('.js-select2'),
    selectIcons = $('.select2-icons'),
    checkboxSelect = $('.checkboxSelect'),
    hideSearch = $('.hide-search'),
    selectArray = $('.select2-data-array'),
    selectAjax = $('.select2-data-ajax'),
    selectLg = $('.select2-size-lg'),
    selectSm = $('.select2-size-sm'),
    selectInModal = $('.select2InModal');
  
  
  tagSelect.each(function () {
    var $this = $(this);
    $this.wrap('<div class="position-relative"></div>');
     tagSelect.select2({
      // placeholder: "Select value",
      maximumSelectionLength: 3,
      dropdownAutoWidth: true,
      closeOnSelect : false,
       width: '100%',
      placeholder: 'Select maximum 3 Tags',
      dropdownParent: tagSelect.parent()
  });
  });
  select.each(function () {
    var $this = $(this);
    $('.others-div').hide();
    $this.wrap('<div class="position-relative"></div>');
    $this.select2({
      dropdownAutoWidth: true,
      allowClear: false,
      width: '100%',
     
      dropdownParent: $this.parent(),
  
    })
  
  });
 
  checkboxSelect.each(function () {
    var $this = $(this);
    $('.others-div').hide();
    $this.wrap('<div class="position-relative"></div>');
    $this.select2({
      dropdownAutoWidth: true,
      allowClear: false,
      width: '100%',
      closeOnSelect : false,
      dropdownParent: $this.parent(),
  
    })
  
  });

  function formatRepo(repo) {
    if (repo.loading) return repo.text;

    var markup =
      "<div class='select2-result-repository clearfix'>" +
      "<div class='select2-result-repository__avatar'><img src='" +
      repo.owner.avatar_url +
      "' /></div>" +
      "<div class='select2-result-repository__meta'>" +
      "<div class='select2-result-repository__title'>" +
      repo.full_name +
      '</div>';

    if (repo.description) {
      markup += "<div class='select2-result-repository__description'>" + repo.description + '</div>';
    }

    markup +=
      "<div class='select2-result-repository__statistics'>" +
      "<div class='select2-result-repository__forks'>" +
      feather.icons['share-2'].toSvg({ class: 'mr-50' }) +
      repo.forks_count +
      ' Forks</div>' +
      "<div class='select2-result-repository__stargazers'>" +
      feather.icons['star'].toSvg({ class: 'mr-50' }) +
      repo.stargazers_count +
      ' Stars</div>' +
      "<div class='select2-result-repository__watchers'>" +
      feather.icons['eye'].toSvg({ class: 'mr-50' }) +
      repo.watchers_count +
      ' Watchers</div>' +
      '</div>' +
      '</div></div>';

    return markup;
  }

  function formatRepoSelection(repo) {
    return repo.full_name || repo.text;
  }

  // Sizing options

  // Large
  selectLg.each(function () {
    var $this = $(this);
    $this.wrap('<div class="position-relative"></div>');
    $this.select2({
      dropdownAutoWidth: true,
      dropdownParent: $this.parent(),
      width: '100%',
      containerCssClass: 'select-lg'
    });
  });

  // Small
  selectSm.each(function () {
    var $this = $(this);
    $this.wrap('<div class="position-relative"></div>');
    $this.select2({
      dropdownAutoWidth: true,
      dropdownParent: $this.parent(),
      width: '100%',
      containerCssClass: 'select-sm'
    });
  });

  $('#select2InModal').on('shown.bs.modal', function () {
    selectInModal.select2({
      placeholder: 'Select a state'
    });
  });
})(window, document, jQuery);
