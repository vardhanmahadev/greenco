// /*=========================================================================================
//     File Name: form-repeater.js
//     Description: form repeater page specific js
//     ----------------------------------------------------------------------------------------
//  
//  
//    
//   
// ==========================================================================================*/

// $(function () {
//   'use strict';

//   // form repeater jquery
//   $('.invoice-repeater, .repeater-default').repeater({
//     show: function () {
//       $(this).slideDown();
//       // Feather Icons
//       if (feather) {
//         feather.replace({ width: 14, height: 14 });
//       }
//     },
//     hide: function (deleteElement) {
//       if (confirm('Are you sure you want to delete this element?')) {
//         $(this).slideUp(deleteElement);
//       }
//     },
//     ready: function (setIndexes) {
//       $dragAndDrop.on('drop', setIndexes);
//   },
//   // (Optional)
//   // Removes the delete button from the first list item,
//   // defaults to false.
//   isFirstItemUndeletable: true
//   });
// });
