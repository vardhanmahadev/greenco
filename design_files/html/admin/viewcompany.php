<?php include('includes/header.php'); ?>
<link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/forms/wizard/bs-stepper.min.css">
<link rel="stylesheet" type="text/css" href="../../app-assets/css/plugins/forms/form-validation.css">
<link rel="stylesheet" type="text/css" href="../../app-assets/css/plugins/forms/form-wizard.min.css">
<div class="app-content content content-ps-stl pt-4">
    <div class="">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
            <section class="p-relative">
                    <ul class="nav nav-pills tabs-pages tab-leftside">
                        <li class="nav-item">
                            <a class="nav-link active" href="companymanagement.php">
                                <span class="bs-stepper-box">
                                    <i class="fal fa-long-arrow-left"></i>
                                </span>Company Management </a>
                        </li>
                    </ul>
                    <p class="edit-tab">View Company? <a href="#" onclick="goBack()"><i class="fal fa-times"></i></a></p>
                </section>
                <section class="col-lg-12 horizontal-wizard">
                    <div class="bs-stepper horizontal-wizard-example">
                        <div class="bs-stepper-header" role="tablist">
                            <div class="step" data-target="#contact-details" role="tab" id="contact-details-trigger">
                                <button type="button" class="step-trigger">
                                    <span class="bs-stepper-box">1</span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">Basic Info</span>
                                    </span>
                                </button>
                            </div>
                            <div class="line">
                                <i data-feather="chevron-right" class="font-medium-2"></i>
                            </div>
                            <div class="step" data-target="#industry-details" role="tab" id="industry-details-trigger">
                                <button type="button" class="step-trigger">
                                    <span class="bs-stepper-box">2</span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">Industry Details</span>
                                    </span>
                                </button>
                            </div>
                            <div class="line">
                                <i data-feather="chevron-right" class="font-medium-2"></i>
                            </div>
                            <div class="step" data-target="#infrastructure-step" role="tab" id="infrastructure-step-trigger">
                                <button type="button" class="step-trigger">
                                    <span class="bs-stepper-box">3</span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">Infrastructure Details</span>
                                    </span>
                                </button>
                            </div>
                            <div class="line">
                                <i data-feather="chevron-right" class="font-medium-2"></i>
                            </div>
                            <div class="step" data-target="#income-details" role="tab" id="income-details-trigger">
                                <button type="button" class="step-trigger">
                                    <span class="bs-stepper-box">4</span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">Income Details</span>
                                    </span>
                                </button>
                            </div>
                            <!-- <div class="line">
                                <i data-feather="chevron-right" class="font-medium-2"></i>
                            </div>
                            <div class="step" data-target="#document-upload" role="tab" id="document-upload-trigger">
                                <button type="button" class="step-trigger">
                                    <span class="bs-stepper-box">5</span>
                                    <span class="bs-stepper-label">
                                        <span class="bs-stepper-title">Document Upload</span>
                                    </span>
                                </button>
                            </div> -->
                        </div>
                        <div class="bs-stepper-content">
                            <div id="contact-details" class="content mt-2" role="tabpanel" aria-labelledby="contact-details-trigger">
                                <form>
                                <div class="row">
                                        <div class="col-lg-3">
                                            <div class="form-label-group">
                                                <input type="text" placeholder="Company Name" name="manufacturer_name" value="Asahi India Glass Ltd" id="manufacturer_name" class="ui-autocomplete-input form-control form-caontrol-merge border-right-line " autocomplete="off">
                                                <!-- <input class="form-control form-caontrol-merge border-right-line " type="text" placeholder="Name of the Company*" value="CompanyName" autocomplete="false" /> -->
                                                <label class="form-label float-label">Name of the Company<span class="text-danger">*</span></label>
                                            </div>
                                            <div class="form-label-group">
                                                <input class="form-control form-caontrol-merge border-right-line " type="text" placeholder="Phone Number*" value="9923232323" autocomplete="false" />
                                                <label class="form-label float-label">Phone Number<span class="text-danger">*</span></label>
                                            </div>
                                            <div class="form-label-group">
                                                <input class="form-control border-right-line " type="text" placeholder="City*" value="cityName" autocomplete="false" />
                                                <label class="form-label float-label">City<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-label-group mb-50">
                                                <input class="form-control form-caontrol-merge border-right-line " type="text" placeholder="Email Address*" value="company@gmail.com" autocomplete="false" />
                                                <label class="form-label float-label">Email Address<span class="text-danger">*</span></label>
                                            </div>
                                            <div class="form-group mt-25">
                                                <label class="form-label float-label">State<span class="text-danger">*</span></label>
                                                <select class="form-control select2" data-placeholder="Select State">
                                                    <option value="Telangana">Telangana</option>
                                                </select>
                                            </div>

                                            <div class="form-group mt-50">
                                                <label class="form-label mb-0">Account Status<span class="text-danger">*</span></label>
                                                <select class="form-control" >
                                                    <option value="">Select Status*</option>
                                                    <option value="" selected>Active</option>
                                                    <option value="">In-Active</option>
                                                </select>
                                             
                                            </div>
                                        </div>
                                        <!-- <div class="col-lg-3">
                                            <div class="form-label-group">
                                                <input class="form-control border-right-line error" id="register-email error" type="text"  placeholder="City" autocomplete="false" />
                                                <label class="form-label float-label">City</label>
                                                <span class="error">This Field is Required</span>
                                            </div>
                                        </div> -->
                                        <div class="col-lg-3">
                                            <div class="form-label-group">
                                                <textarea placeholder="Postal Address*" class="form-control border-right-line " height="125" value="" rows="3"> 2nd Floor, VSSR Square, Vittal Rao Nagar, HITEC City, Hyderabad, Telangana </textarea>
                                                <label class="form-label float-label">Postal Address<span class="text-danger">*</span></label>
                                            </div>
                                            <div class="form-label-group">
                                                <input class="form-control border-right-line" type="text" placeholder="Postal Address Pincode*" value="500081" autocomplete="false" />
                                                <label class="form-label float-label">Postal Address Pincode<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-label-group">
                                                <textarea placeholder="Billing Address*" class="form-control border-right-line " value="" height="125" rows="3"> 2nd Floor, VSSR Square, Vittal Rao Nagar, HITEC City, Hyderabad, Telangana </textarea>
                                                <label class="form-label float-label">Billing Address<span class="text-danger">*</span> </label>
                                            </div>
                                            <div class="form-label-group">
                                                <input class="form-control border-right-line " type="text" value="500081" placeholder="Billing Address Pincode*" autocomplete="false" />
                                                <label class="form-label float-label">Billing Address Pincode<span class="text-danger">*</span></label>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="d-flex justify-content-end">
                                    <button class="btn btn-primary btn-next">
                                        <span class="align-middle d-sm-inline-block d-none">Save & Continue</span>
                                        <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                    </button>
                                </div>
                            </div>
                            <div id="industry-details" class="content" role="tabpanel" aria-labelledby="industry-details-trigger">
                                <form>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group ">
                                                <label for="login-password">Type of the Industry</label>
                                                <select class="select2 form-control" data-placeholder="select">
                                                    <option value="">-Select Sector-</option>
                                                    <option value="manufacturing">Manufacturing </option>
                                                    <option value="service">service</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="login-password">Which type of Entity you are ?</label>
                                                <select class="select2 form-control" data-placeholder="select">
                                                    <option value="">-Select Sector-</option>
                                                    <option value="privatesector">Private sector</option>
                                                    <option value="publicesectoresector">Public sector</option>
                                                    <option value="indianrailway">Indian Railways</option>
                                                    <option value="othergovernmententerprise">Other Government Enterprise</option>
                                                    <option value="notgorprofit">Not for profit</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group ">
                                                <label for="login-password">Type of Sector</label>
                                                <select class="select2 form-control" data-placeholder="select">
                                                    <option value="">-Select Sector-</option>
                                                    <option value="Airports"> Airports </option>
                                                    <option value="Auto component"> Auto component </option>
                                                    <option value="Automobile"> Automobile </option>
                                                    <option value="Aviatiion"> Aviatiion </option>
                                                    <option value="Biotechnology"> Biotechnology </option>
                                                    <option value="Cement"> Cement </option>
                                                    <option value="Chemicals"> Chemicals </option>
                                                    <option value="Construction"> Construction </option>
                                                    <option value="Defence Manufacturing"> Defence Manufacturing </option>
                                                    <option value="Electrical & Electronics"> Electrical & Electronics </option>
                                                    <option value="Engineering"> Engineering </option>
                                                    <option value="Fetilizer"> Fetilizer </option>
                                                    <option value="FMCG"> FMCG </option>
                                                    <option value="Food rpocessing"> Food rpocessing </option>
                                                    <option value="Glass"> Glass </option>
                                                    <option value="Hospitality & Wellness"> Hospitality & Wellness </option>
                                                    <option value="Iron & Steel"> Iron & Steel </option>
                                                    <option value="IT & ITES"> IT & ITES </option>
                                                    <option value="leather"> leather </option>
                                                    <option value="Mining"> Mining </option>
                                                    <option value="Non ferrous Metals"> Non ferrous Metals </option>
                                                    <option value="Oil & Gas"> Oil & Gas </option>
                                                    <option value="Other Manufacturing sector"> Other Manufacturing sector </option>
                                                    <option value="Other Service sector"> Other Service sector </option>
                                                    <option value="Petrochemicals"> Petrochemicals </option>
                                                    <option value="Pharma"> Pharma </option>
                                                    <option value="Ports & Shipping"> Ports & Shipping </option>
                                                    <option value="Power Plant"> Power Plant </option>
                                                    <option value="Pulp & Paper"> Pulp & Paper </option>
                                                    <option value="Railway Production unit"> Railway Production unit </option>
                                                    <option value="Railway Workshop"> Railway Workshop </option>
                                                    <option value="Renewable Energy"> Renewable Energy </option>
                                                    <option value="SME ( only  if turn over Less than 50 Crore)"> SME ( only if turn over Less than 50 Crore) </option>
                                                    <option value="Textile & Garments"> Textile & Garments </option>
                                                    <option value="Tyre"> Tyre </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <button class="btn btn-outline-secondary btn-prev">
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                </button>
                                <span class="float-right">
                                    <button class="btn btn-primary btn-next">
                                        <span class="align-middle d-sm-inline-block d-none">Save & Continue</span>
                                        <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                    </button>
                                </span>
                            </div>
                            <div id="infrastructure-step" class="content" role="tabpanel" aria-labelledby="infrastructure-step-trigger">
                                <form>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-label-group">
                                                <input class="form-control form-caontrol-merge border-right-line" type="text" placeholder="Name of the Products manufactured / Services offered" />
                                                <label class="form-label float-label">Name of the Products manufactured / Services offered</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-label-group">
                                                <input class="form-control form-caontrol-merge border-right-line" type="text" placeholder="Enter latest turnover of the unit/ facility, in Rs Crores only" />
                                                <label class="form-label float-label">Enter latest turnover of the unit/ facility, in Rs Crores only</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-label-group">
                                                <input class="form-control form-caontrol-merge border-right-line" type="text" placeholder="Total no of employees permanent employees" />
                                                <label class="form-label float-label">Total no of employees permanent employees</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-label-group">
                                                <input class="form-control form-caontrol-merge border-right-line" type="text" placeholder="Total no of contract employees" />
                                                <label class="form-label float-label">Total no of contract employees</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-label-group">
                                                <input class="form-control form-caontrol-merge border-right-line" type="text" placeholder="Yearly Electrical Energy Consumption in KWH / year" />
                                                <label class="form-label float-label">Yearly Electrical Energy Consumption in KWH / year</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-label-group">
                                                <input class="form-control form-caontrol-merge border-right-line" type="text" placeholder="Yearly Thermal Energy Consumption in Kcal / year" />
                                                <label class="form-label float-label">Yearly Thermal Energy Consumption in Kcal / year</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-label-group">
                                                <input class="form-control form-caontrol-merge border-right-line" type="text" placeholder="Yearly water consumption in Kilo Litres/ Cubic meteres" />
                                                <label class="form-label float-label">Yearly water consumption in Kilo Litres/ Cubic meteres</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="form-label-group">
                                                <input class="form-control form-caontrol-merge border-right-line" type="text" placeholder="Total area of the unit / facility ( including Built up space, open area, colony or township etc ) in Acres only" />
                                                <label class="form-label float-label">Total area of the unit / facility ( including Built up space, open area, colony or township etc ) in Acres only</label>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <button class="btn btn-outline-secondary btn-prev">
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                </button>
                                <span class="float-right">
                                    <button class="btn btn-primary btn-next">
                                        <span class="align-middle d-sm-inline-block d-none">Save & Continue</span>
                                        <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                                    </button>
                                </span>
                            </div>
                            <div id="income-details" class="content" role="tabpanel" aria-labelledby="income-details-trigger">
                                <form>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-label-group">
                                                <input class="form-control form-caontrol-merge border-right-line" type="text" placeholder="TAN NO (Tax dedcution Account Number)" />
                                                <label class="form-label float-label">TAN NO (Tax dedcution Account Number)</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-label-group">
                                                <input class="form-control form-caontrol-merge border-right-line" type="text" placeholder="PAN NO (Permanent Account Number)" />
                                                <label class="form-label float-label">PAN NO (Permanent Account Number)</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-label-group">
                                                <input class="form-control form-caontrol-merge border-right-line" type="text" placeholder="GSTIN NO (Goods and Services Tax Identification Number )" />
                                                <label class="form-label float-label">GSTIN NO (Goods and Services Tax Identification Number )</label>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <button class="btn btn-outline-secondary btn-prev">
                                    <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                    <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                </button>
                                <span class="float-right">
                                    <button class="btn btn-success btn-submit">
                                    <span class="align-middle d-sm-inline-block d-none">Save & Submit</span>
                                      
                                    </button>
                                </span>
                            </div>
                            <!-- <div id="document-upload" class="content" role="tabpanel" aria-labelledby="document-upload-trigger">
                                <form>
                                    <div class="row  pb-1 mt--1">
                                        <div class="col-lg-6">
                                            <div class="card ">
                                                <div class="d-flex v-row title-strip mb-2">
                                                    <div class="col-6 pl-0">Proposal Document</div>
                                                </div>
                                                <div class="card-body ">
                                                    <div class="row  pb-1 mt--1">
                                                        <div class="col-lg-12">
                                                            <div class="row v-row mb-2">
                                                                <div class="col-lg-2 col-md-2">
                                                                    <label>Uploaded File :</label>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4">
                                                                    <div class="v-row  uploadedfile">
                                                                        <span class="">DC Bill of material.pdf</span> <button class="btn btn-outline-secondary waves-effect ml-1"> <i data-feather="download"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12  mt-50">
                                                                    <div class="custom-file">
                                                                        <input type="file" class="custom-file-input" id="customFile">
                                                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 offset-lg-2">
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="card ">
                                                <div class="d-flex v-row title-strip mb-2">
                                                    <div class="col-6 pl-0">PO Document</div>
                                                </div>
                                                <div class="card-body ">
                                                    <div class="row  pb-1 mt--1">
                                                        <div class="col-lg-12">
                                                            <div class="row v-row mb-2">
                                                                <div class="col-lg-2 col-md-2">
                                                                    <label>Uploaded File :</label>
                                                                </div>
                                                                <div class="col-lg-4 col-md-4">
                                                                    <div class="v-row  uploadedfile">
                                                                        <span class="">DC Bill of material.pdf</span> <button class="btn btn-outline-secondary waves-effect ml-1"> <i data-feather="download"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12  mt-50">
                                                                    <div class="custom-file">
                                                                        <input type="file" class="custom-file-input" id="customFile">
                                                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 offset-lg-2">
                                                                    <!-- <div class="form-group">
                                                      <label>Remarks</label>
                                                      <textarea class="form-control" rows="2"></textarea>
                                                  </div> -->
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="d-flex justify-content-between">
                                    <button class="btn btn-outline-secondary btn-prev">
                                        <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                        <span class="align-middle d-sm-inline-block d-none">Previous</span>
                                    </button>
                                    <button class="btn btn-success btn-submit mr-1">
                                        <i data-feather="check" class="align-middle ms-sm-25 ms-0"></i>Save & Submit</button>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </section>
                <!-- /Horizontal Wizard -->
            </div>
        </div>
    </div>
</div> <?php include('includes/footer.php'); ?> <script>
    var manufacturer_list = [{
        "id": "19",
        "value": "ABC Ceramic Services Limited"
    }, {
        "id": "40",
        "value": "ACC Limited"
    }, {
        "id": "96",
        "value": "ACC Limited"
    }, {
        "id": "20",
        "value": "Aeropure UV Systems Pvt. Ltd"
    }, {
        "id": "83",
        "value": "AET Building Products (WI) Pvt Ltd"
    }, {
        "id": "305",
        "value": "AICA Laminates India Pvt Ltd"
    }, {
        "id": "41",
        "value": "AkzoNobel India Ltd"
    }, {
        "id": "252",
        "value": "Aliferous Technologies Private Limited (Clairco)"
    }, {
        "id": "89",
        "value": "Anutone Acoustics Limited"
    }, {
        "id": "42",
        "value": "Aquatron International AB"
    }, {
        "id": "281",
        "value": "ArcelorMittal Nippon Steel India Ltd"
    }, {
        "id": "297",
        "value": "Archidply Industries Ltd"
    }, {
        "id": "7",
        "value": "Armstrong World Industries (India) Pvt. Ltd"
    }, {
        "id": "311",
        "value": "Arvind Envisol Limited"
    }, {
        "id": "21",
        "value": "Asahi India Glass Ltd"
    }, {
        "id": "208",
        "value": "Asawa Insulation Pvt Ltd"
    }, {
        "id": "90",
        "value": "Ashish Bags & Fashions Ltd"
    }, {
        "id": "84",
        "value": "Ashok Chemicals"
    }, {
        "id": "189",
        "value": "Astral Poly Technik Limited"
    }, {
        "id": "207",
        "value": "Bayer CropScience Ltd"
    }, {
        "id": "22",
        "value": "Berger Becker Coatings Pvt Ltd"
    }, {
        "id": "8",
        "value": "Berger Paints India Limited"
    }, {
        "id": "98",
        "value": "Berger Paints India Limited-ETICS Division"
    }, {
        "id": "92",
        "value": "BioMicrobics Inc"
    }, {
        "id": "68",
        "value": "Bonphul Air Products"
    }, {
        "id": "213",
        "value": "Bonton Technomake Pvt Ltd"
    }, {
        "id": "9",
        "value": "British Paints"
    }, {
        "id": "217",
        "value": "Camfil India Private Limited"
    }, {
        "id": "116",
        "value": "Cera Sanitaryware Limited"
    }, {
        "id": "209",
        "value": "Cipy Polyurethanes Private Limited"
    }, {
        "id": "183",
        "value": "Daiki Axis India Pvt. Ltd"
    }, {
        "id": "10",
        "value": "Dalmia Cement"
    }, {
        "id": "11",
        "value": "Deeya Panel Products Pvt. Ltd"
    }, {
        "id": "103",
        "value": "Diamond Cements (Prop: HeidelbergCement India Ltd)"
    }, {
        "id": "299",
        "value": "Dolphy India Private Limited"
    }, {
        "id": "214",
        "value": "Durlum India Pvt Ltd"
    }, {
        "id": "184",
        "value": "E3 Extrusion Inc"
    }, {
        "id": "317",
        "value": "Ecowood Exports Pvt Ltd"
    }, {
        "id": "45",
        "value": "Eden Innovations India Pvt. Ltd"
    }, {
        "id": "46",
        "value": "Ekam Eco Solutions Pvt. Ltd"
    }, {
        "id": "47",
        "value": "Emerson Commercial & Residential Solutions"
    }, {
        "id": "133",
        "value": "Emmvee Photovoltaic Power Private Limited"
    }, {
        "id": "273",
        "value": "EUKPRO Biotech Pvt Ltd"
    }, {
        "id": "102",
        "value": "Euronics Industries Private Limited"
    }, {
        "id": "155",
        "value": "Everest Industries Ltd"
    }, {
        "id": "260",
        "value": "Featherlite"
    }, {
        "id": "48",
        "value": "Furaat Earth Pvt  Ltd"
    }, {
        "id": "1",
        "value": "Godrej & Boyce Mfg. Co.Ltd - Construction Division"
    }, {
        "id": "50",
        "value": "Godrej and Boyce Mfg. Co.Ltd \u2013 Electrical Division"
    }, {
        "id": "24",
        "value": "Godrej Interio"
    }, {
        "id": "51",
        "value": "Green Build Products (I) Pvt. Ltd"
    }, {
        "id": "117",
        "value": "Greenrich Grow India Pvt Ltd"
    }, {
        "id": "206",
        "value": "Greentek Plast"
    }, {
        "id": "85",
        "value": "Grenove Services Pvt. Ltd"
    }];
    $("#manufacturer_name").autocomplete({
        source: manufacturer_list,
        function(request, response) {
            response($.map(manufacturer_list, function(item) {
                return {
                    id: item.id,
                    value: item.value,
                }
            }))
        },
        response: function(event, ui) {
            if (ui.content.length === 0) {
                $('#manufacturer_id').val('-1');
            }
        },
        select: function(event, ui) {
            $('#manufacturer_id').val('-1');
            $(this).val(ui.item.value);
            $('#manufacturer_id').val(ui.item.id);
        },
        minLength: 2,
        autoFocus: true
    });
</script>
<script src="../../app-assets/vendors/js/forms/repeater/jquery.repeater.min.js"></script>
<script src="../../app-assets/js/scripts/forms/form-repeater.js"></script>
<script src="../../app-assets/vendors/js/forms/wizard/bs-stepper.min.js"></script>
<!-- BEGIN: Page JS-->
<script src="../../app-assets/js/scripts/forms/form-wizard.min.js"></script>
<!-- END: Page JS-->