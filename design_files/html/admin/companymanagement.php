<?php include('includes/header.php'); ?>
 <div class="app-content content content-ps-stl">
    <div class="">
        <div class="content-overlay"></div>
     
        <div class="content-wrapper">
            <div class="content-body">
                <section class="">
                    <div class="col-12">
                        <section id="advanced-search-datatable ">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card table-filters-design">
                                        <div class="d-flex v-row title-strip">
                                            <div class="col-6 pl-0">Company Management</div>
                                            <div class="col-6 pr-0">
                                                <div class="filter-tab-btn card-option ">
                                                    <span class="minimize-card btn btn-primary "><i data-feather="filter"></i> Filters</span>
                                                    <span style="display:none"><i data-feather="filter"></i> Filters</span></li>
                                                </div>
                                            </div>
                                        </div>
                                        <!--Search Form -->
                                        <div class="card-body mt-2" style="display:none;">
                                            <form method="POST">
                                                <div class="row">
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label>REG ID</label>
                                                            <input type="text" class="form-control dt-input dt-full-name" data-column="2" placeholder="Reg ID" />
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label>Company Name</label>
                                                            <input type="text" class="form-control dt-input dt-full-name" data-column="2" placeholder="Company Name" />
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label>Email Address</label>
                                                            <input type="text" class="form-control dt-input dt-full-name" data-column="2" placeholder="Email Address " />
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label>Phone Number</label>
                                                            <input type="text" class="form-control dt-input dt-full-name" data-column="2" placeholder="Phone Number " />
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label>Approval Status</label>
                                                            <select class="form-control">
                                                                <option>Select</option>
                                                                <option>Pending</option>
                                                                <option>Approved </option>
                                                                <option>DisApproved</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label>Status</label>
                                                            <select class="form-control">
                                                                <option>Select</option>
                                                                <option>Active </option>
                                                                <option>InActive</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-3 mt-2 float-right">
                                                        <div class="form-group">
                                                        <button type="submit" class="btn btn-primary m0-auto  ">Submit</button>
                                                        <button type="reset" class="btn btn-secondary m0-auto  ">Reset</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <hr>
                                        </div>
                                        <div class="basic-datatable datatable-list">
                                            <table class="dt-vendorlist table table-bordered table-responsive">
                                                <thead>
                                                    <th></th>
                                                    <th>S.no</th>
                                                    <th>Reg ID</th>
                                                    <th>Company Name</th>
                                                    <th>Email Id</th>
                                                    <th>Phone Number</th>
                                                    <th>Status</th>
                                                    <th>Approval Status</th>
                                                    <th>Action</th>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

<div class="modal float-labeldispaly fade" id="view-vendor" tabindex="-1" role="dialog" aria-labelledby="editWarehouseTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <form class="add-new-user modal-content pt-0">
            <div class="modal-header mb-1">
                <h5 class="modal-title">Vendor Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
            </div>
            <div class="modal-body flex-grow-1">
            <div class="col-lg-12 order-infodetails order-detailspage userquick-section ">
													<ul>
														<li> <span> Name Of the Person </span>Rahul </li>
														<li> <span> Company Name</span>Solar  Energy</li>
                                                        <li> <span> Email Address</span>Solar@gmail.com</li>
                                                        <li> <span> Mobile Number</span>10000478</li>
														<li> <span> Status </span><label class="badge badge-light-success">Approved</label></li>
                                                        <li> <span>Registered Date</span>13-12-2021, 1:27 P.M</li>
                                                        <li> <span>Approved Date</span>13-12-2021, 1:27 P.M</li>
														<!-- <li> <span>Pincode </span>507003</li> -->
													</ul>
												</div>
 
            </div>
        </form>
    </div>
</div>
<!---Start Modal Popup-->
<div class="modal float-labeldispaly fade" id="changestatus" tabindex="-1" role="dialog" aria-labelledby="editWarehouseTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form class="add-new-user modal-content pt-0">
            <div class="modal-header mb-1">
                <h5 class="modal-title">Change Account Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
            </div>
            <div class="modal-body flex-grow-1">
                <div class="form-label mb-2">
                    <label class="">Status</label>
                    <div class="mb-1 d-flex select-tag w-100">
                        <select class="form-control ">
                            <option value="Select">Select Status</option>
                            <option value="Active">Active</option>
                            <option value="InActive"> InActive</option>
                        </select>
                    </div>
                </div>
                <div class="form-label-group m0-auto text-center">
                    <button type="submit" class="btn btn-primary m0-auto  ">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!---end Modal Popup-->
 <?php include('includes/footer.php'); ?> 
 <script>
    function filterColumn(a, e) {
        if (5 == a) {
            var t = $(".start_date").val(),
                l = $(".end_date").val();
            filterByDate(a, t, l), $(".dt-vendorlist").dataTable().fnDraw()
        } else $(".dt-vendorlist").DataTable().column(a).search(e, !1, !0).draw()
    }
    $((function() {
        $("html").attr("data-textdirection");
        AddProduct = 'edit-product.html'
        var productlist = $(".dt-vendorlist")
        if ($("input.dt-input").on("keyup", (function() {
                filterColumn($(this).attr("data-column"), $(this).val())
            })), productlist.length) productlist.DataTable({
            ajax: "../../app-assets/data/vendorlist.json",
            columns: [{
                data: 'id'
            }, {
                data: 'id'
            },
            {
                data: 'id'
            },
            {
                data: 'full_name'
            },
            {
                data: 'email'
            }, {
                data: 'phonenumber'
            }, {
                data: 'status'
            }, {
                data: 'status'
            }],
           
            displayLength: 100,
            scrollY: "350px",
            scrollX: true,
            scrollCollapse: true,
            fixedColumns: {
                leftColumns: 0,
                rightColumns: 0
            },
            columnDefs: [{
                orderable: false,
                targets: [0]
            }, {
                    targets: 0,
                    orderable: false,
                    render: function(e, t, a, s) {
                        return "display" === t && (e = '<div class="custom-control custom-checkbox"> <input class="custom-control-input dt-checkboxes" type="checkbox" value=""  id="checkbox2"><label class="custom-control-label"  for="checkbox2" ></label></div>'), e
                    },
                    checkboxes: {
                        selectRow: !0,
                        selectAllRender: '<div class="custom-control custom-checkbox"> <input class="custom-control-input" type="checkbox" value="" id="checkboxSelectAll" /> <label class="custom-control-label" for="checkboxSelectAll"></label></div>'
                    }
                },  {
                // Label
                targets: 7,
                render: function(data, type, full, meta) {
                    var $status_number = full['status'];
                    var $status = {
                        2: {
                            title: 'Approved',
                            class: 'badge-light-success'
                        },
                        1: {
                            title: 'DisApproved',
                            class: ' badge-light-danger'
                        },
                        3: {
                            title: 'Pending',
                            class: ' badge-light-warning'
                        },
                    };
                    if (typeof $status[$status_number] === 'undefined') {
                        return data;
                    }
                    return ('<span class="badge badge-pill ' + $status[$status_number].class + '" >' + $status[$status_number].title + '</span>');
                }
            },
            {
                // Label
                targets: 6,
                render: function(data, type, full, meta) {
                    var $status_number = full['status'];
                    var $status = {
                        1: {
                            title: 'Active',
                            class: 'badge-light-success'
                        },
                        2: {
                            title: 'InActive',
                            class: ' badge-light-danger'
                        },
                    };
                    if (typeof $status[$status_number] === 'undefined') {
                        return data;
                    }
                    return ('<span class="badge badge-pill ' + $status[$status_number].class + '">' + $status[$status_number].title + '</span>');
                }
            },
            {
                // Avatar image/badge, Name and post
                targets: 2,
                orderable: false,
                render: function(data, type, full, meta) {
                    var $id = full['id']
                    return (  '<a class="id-anchor align-items-center" href="viewcompany.php">REG1000'  + $id + '</a>');
                }
            },
            // {
            //         Invoice status
            //         targets:3,
            //         className: 'price-variant text-left list-data',
            //         render: function(data, type, full, meta) {
            //             var $id = full['id'],
            //                 $email = full['email'],
            //                 $phonenumber=full['phonenumber']
            //             return (' <span class="">Company Name  :</span> CompanyName '+ $id + '<br> <span class="">Phone Number   :</span> ' + $phonenumber + '<br> <span class="">Email Address   :</span> ' + $email );
            //         }
            //     },
            {
                // Actions
                targets: 8,
                title: 'Actions',
                orderable: false,
                render: function(data, type, full, meta) {
                    return ('<div class="d-inline-flex">' + '<a href="javascript:;" class="approve action-icons">' + '<span data-toggle="tooltip" title data-original-title="Approve / DisApprove">' + feather.icons['user-check'].toSvg({
                        class: 'font-small-4 mr-50'
                    }) + '</span>' + '</a>' + '<a href="#"  class="action-icons"    data-toggle= "modal" data-target= "#changestatus">' + '<span data-toggle="tooltip" title data-original-title="Active / InActive">' + feather.icons['check-square'].toSvg({
                        class: 'font-small-4 mr-50'
                    }) + '</span>' + '</a>' + '<a href="viewcompany.php" class="action-icons">' + '<span data-toggle="tooltip" title data-original-title="View / Edit">' + feather.icons['edit'].toSvg({
                        class: 'font-small-4 mr-50'
                    }) + '</span>' + '</a>' + '</div>');
                }
            }
        
        ],
            dom: '<"row d-flex justify-content-between align-items-center m-1"' + '<"col-lg-1 d-flex align-items-center"l>' + '<"col-lg-5 "f>' + '<"col-lg-5 d-flex align-items-center justify-content-lg-end flex-wrap  p-0"<"dt-action-buttons text-right"B>>' + '>t' + '<"d-flex justify-content-between  mx-2"' + '<"col-sm-12 col-md-6"i>' + '<"col-sm-12 col-md-6"p>' + '>',
            // orderCellsTop: !0,
            select: {
                style: "multi",
                selector: "td:first-child",
                items: "row"
            },
            language: {
                sLengthMenu: 'Show _MENU_',
                search: '',
                searchPlaceholder: 'Search',
                paginate: {
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            },
            buttons: [{
                extend: 'collection',
                className: 'btn btn-outline-secondary dropdown-toggle pr-1 ml-1',
                text: '<i class="fas fa-tasks "></i>' + 'Bulk Actions',
                buttons: [{
                    text: feather.icons['check-square'].toSvg({
                        class: 'font-small-4 mr-50'
                    }) + 'Change Account Status',
                    className: 'dropdown-item approve',
                    attr: {
                        "data-toggle": "modal",
                        "data-target": "#changestatus"
                    }
                }, 
                {
                    text: feather.icons['file-text'].toSvg({
                        class: 'font-small-4 mr-50'
                    }) + 'Export',
                    className: 'dropdown-item',
                    exportOptions: {
                        columns: [1, 2, 3, 4, 5, 6]
                    }
                }],
                init: function(api, node, config) {
                    $(node).removeClass('btn-secondary');
                    $(node).parent().removeClass('btn-group');
                    setTimeout(function() {
                        $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                    }, 50);
                }
            }, {
                text: feather.icons['plus'].toSvg({
                    class: 'mr-50 font-small-4'
                }) + 'Add Company',
                className: 'btn btn-primary btn-add-record ml-2',
                action: function(e, dt, button, config) {
                        window.location = "addcompany.php";
                    },
                init: function(api, node, config) {
                    $(node).removeClass('btn-secondary');
                }
            }],
            drawCallback: function() {
                $(document).find('[data-toggle="tooltip"]').tooltip();
            }
        });
        $('.dt-vendorlist').on('click', '.approve', function() {
            Swal.fire({
                title: 'Do You Want to Approve?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Approve it!',
                showDenyButton: true,
                denyButtonText: 'No, Reject it',
                showClass: {
                    popup: 'animate__animated animate__fadeIn'
                },
                customClass: {
                    confirmButton: 'btn btn-primary',
                    cancelButton: 'btn btn-outline-danger ml-1',
                    denyButton: 'btn btn-secondary ml-1',
                },
                buttonsStyling: false
            }).then(function(result) {
                if (result.value) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Approved!',
                        confirmButtonText: 'Okay',
                        text: 'Your Company has been Approved.',
                        showClass: {
                            popup: 'animate__animated animate__fadeIn'
                        },
                        customClass: {
                            confirmButton: 'btn btn-primary'
                        },
                        buttonsStyling: false
                    });
                } else if (result.isDenied) {
                    Swal.fire({
                        title: 'Reason for Rejection!',
                        input: 'textarea',
                        showCancelButton: true,
                        confirmButtonText: 'Submit',
                        showClass: {
                            popup: 'animate__animated animate__fadeIn'
                        },
                        customClass: {
                            confirmButton: 'btn btn-primary',
                            cancelButton: 'btn btn-outline-danger ml-1',
                            denyButton: 'btn btn-secondary ml-1',
                        },
                        buttonsStyling: false
                    })
                  
                }
            });
        })
        $('.dt-vendorlist').on('click', '.active-record', function() {
         
            Swal.fire({
                title: 'Are you Sure?',
             
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Activate it!',
                showClass: {
                    popup: 'animate__animated animate__fadeIn'
                },
                customClass: {
                    confirmButton: 'btn btn-primary',
                    cancelButton: 'btn btn-outline-danger ml-1'
                },
                buttonsStyling: false
            }).then(function(result) {
                if (result.value) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Activated!',
                        text: 'Company has been activated.',
                        showClass: {
                            popup: 'animate__animated animate__fadeIn'
                        },
                        customClass: {
                            confirmButton: 'btn btn-success'
                        }
                    });
                }
           
        })
        })
    }));
</script>