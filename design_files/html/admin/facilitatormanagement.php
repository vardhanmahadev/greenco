<?php include('includes/header.php'); ?> <div class="app-content content content-ps-stl">
    <div class="">
        <div class="content-overlay"></div>
     
        <div class="content-wrapper">
            <div class="content-body">
                <section class="">
                    <div class="col-12">
                        <section id="advanced-search-datatable ">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card table-filters-design">
                                        <div class="d-flex v-row title-strip">
                                            <div class="col-6 pl-0">Facilitator/Consultant Management </div>
                                            <div class="col-6 pr-0">
                                                <div class="filter-tab-btn card-option ">
                                                    <span class="minimize-card btn btn-primary "><i data-feather="filter"></i> Filters</span>
                                                    <span style="display:none"><i data-feather="filter"></i> Filters</span></li>
                                                </div>
                                            </div>
                                        </div>
                                        <!--Search Form -->
                                        <div class="card-body mt-2 " style="display:none;">
                                            <form method="POST">
                                                <div class="row">
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label>Name</label>
                                                            <input type="text" class="form-control dt-input dt-full-name" data-column="2" placeholder="Name" />
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label>Email Address</label>
                                                            <input type="text" class="form-control dt-input dt-full-name" data-column="2" placeholder="Email Address" />
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label>Phone Number</label>
                                                            <input type="text" class="form-control dt-input dt-full-name" data-column="2" placeholder="Phone Number " />
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label>Account Status</label>
                                                            <select class="form-control">
                                                                <option>Select</option>
                                                                <option>Active</option>
                                                                <option>InActive</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="form-group">
                                                            <label>Approval Status</label>
                                                            <select class="form-control">
                                                                <option>Select</option>
                                                                <option>Pending</option>
                                                                <option>Approved </option>
                                                                <option>DisApproved</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                            <div class="form-group"style="margin-top: 7.7%;">
                                                                <button type="submit" class="btn btn-primary m0-auto"
                                                                    id="filterButton">Submit</button>
                                                                <button type="reset" class="btn btn-secondary m0-auto"
                                                                    id="filterResetButton">Reset</button>
                                                            </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <hr>
                                        </div>
                                        <div class="basic-datatable datatable-list">
                                            <table class="dt-accessorlist table table-bordered table-responsive">
                                                <thead>
                                                    <th></th>
                                                    <th>S.no</th>
                                                    <th>Name</th>
                                                    <th>Email ID</th>
                                                    <th>Phone Number</th>
                                                    <th>Approval Status</th>
                                                    <th>Account Status</th>
                                                    <th>Action</th>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-slide-in float-labeldispaly fade" id="accessor-modal" tabindex="-1" role="dialog" aria-labelledby="editWarehouseTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form class="add-new-user modal-content pt-0">
            <div class="modal-header mb-1">
                <h5 class="modal-title">Consultant Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
            </div>
            <div class="modal-body flex-grow-1 pt-3">
                <div class="form-label-group ">
                    <input type="text" class="form-control error" placeholder="Name*">
                    <label>Name </label>
                    <span class="error">This Field is Required</span>
                </div>
                <div class="form-label-group">
                    <input type="text" class="form-control" placeholder="Email Address*">
                    <label>Email Address </label>
                </div>
                <div class="form-label-group  mb-1">
                    <input type="text" class="form-control" placeholder="Mobile Number*">
                    <label>Mobile Number </label>
                </div>
                
                <div class="form-label mb-2">
                    <label class="">Status</label>
                    <div class="mb-1 d-flex select-tag w-100">
                        <select class="form-control ">
                            <option value="Select">Select Status*</option>
                            <option value="Active">Active</option>
                            <option value="InActive"> InActive</option>
                        </select>
                    </div>
                </div>
                <div class="mb-1">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" id="remember-me" type="checkbox" tabindex="3">
                                        <label class="custom-control-label" for="remember-me"> Account Approve</label>
                                    </div>
                                </div>
                <div class="form-label-group m0-auto text-center">
                    <button type="submit" class="btn btn-primary m0-auto  ">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Edit Facilitator Details -->
<div class="modal modal-slide-in float-labeldispaly fade" id="edit-accessor-modal" tabindex="-1" role="dialog" aria-labelledby="editWarehouseTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form class="add-new-user modal-content pt-0">
            <div class="modal-header mb-1">
                <h5 class="modal-title">Consultant Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
            </div>
            <div class="modal-body flex-grow-1 pt-3">
                <div class="form-label-group ">
                    <input type="text" class="form-control error" placeholder="Name*">
                    <label>Name </label>
                    <span class="error">This Field is Required</span>
                </div>
                <div class="form-label-group">
                    <input type="text" class="form-control" placeholder="Email Address*">
                    <label>Email Address </label>
                </div>
                <div class="form-label-group  mb-1">
                    <input type="text" class="form-control" placeholder="Mobile Number*">
                    <label>Mobile Number </label>
                </div>
                
                <div class="form-label mb-2">
                    <label class="">Status</label>
                    <div class="mb-1 d-flex select-tag w-100">
                        <select class="form-control ">
                            <option value="Select">Select Status*</option>
                            <option value="Active">Active</option>
                            <option value="InActive"> InActive</option>
                        </select>
                    </div>
                </div>
                <div class="form-label-group m0-auto text-center">
                    <button type="submit" class="btn btn-primary m0-auto  ">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Edit Facilitator Details END -->
<!---Start Modal Popup-->
<div class="modal float-labeldispaly fade" id="changestatus" tabindex="-1" role="dialog" aria-labelledby="editWarehouseTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form class="add-new-user modal-content pt-0">
            <div class="modal-header mb-1">
                <h5 class="modal-title">Change Account Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
            </div>
            <div class="modal-body flex-grow-1">
                <div class="form-label mb-2">
                    <label class="">Status</label>
                    <div class="mb-1 d-flex select-tag w-100">
                        <select class="form-control ">
                            <option value="Select">Select Status</option>
                            <option value="Active">Active</option>
                            <option value="InActive"> InActive</option>
                        </select>
                    </div>
                </div>
                <div class="form-label-group m0-auto text-center">
                    <button type="submit" class="btn btn-primary m0-auto  ">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!---end Modal Popup-->
 <?php include('includes/footer.php'); ?> 
 <script>
    function filterColumn(a, e) {
        if (5 == a) {
            var t = $(".start_date").val(),
                l = $(".end_date").val();
            filterByDate(a, t, l), $(".dt-accessorlist").dataTable().fnDraw()
        } else $(".dt-accessorlist").DataTable().column(a).search(e, !1, !0).draw()
    }
    $((function() {
        $("html").attr("data-textdirection");
       
        var productlist = $(".dt-accessorlist")
        if ($("input.dt-input").on("keyup", (function() {
                filterColumn($(this).attr("data-column"), $(this).val())
            })), productlist.length) productlist.DataTable({
            ajax: "../../app-assets/data/vendorlist.json",
            columns: [{
                data: 'id'
            },{
                data: 'id'
            },  {
                data: 'full_name'
            }, {
                data: 'email'
            },
            {
                data: 'phonenumber'
            },
             {
                data: 'status'
            }],
            displayLength: 10,
            scrollY: "350px",
            scrollX: true,
            scrollCollapse: true,
            columnDefs: [{
                targets: 0,
                orderable: false,
                render: function(e, t, a, s) {
                    return "display" === t && (e = '<div class="custom-control custom-checkbox"> <input class="custom-control-input dt-checkboxes" type="checkbox" value=""  id="checkbox2"><label class="custom-control-label"  for="checkbox2" ></label></div>'), e
                },
                checkboxes: {
                    selectRow: !0,
                    selectAllRender: '<div class="custom-control custom-checkbox"> <input class="custom-control-input" type="checkbox" value="" id="checkboxSelectAll" /> <label class="custom-control-label" for="checkboxSelectAll"></label></div>'
                }
            },
            {  
                // Label
                targets:6,
                render: function(data, type, full, meta) {
                    var $status_number = full['status'];
                    var $status = {
                        1: {
                            title: 'Active',
                            class: 'badge-light-success'
                        },
                        2: {
                            title: 'InActive',
                            class: ' badge-light-danger'
                        },
                    };
                    if (typeof $status[$status_number] === 'undefined') {
                        return data;
                    }
                    return ('<span class="badge badge-pill ' + $status[$status_number].class + '">' + $status[$status_number].title + '</span>');
                }
            }, 
            {
                // Label
                targets:5,
                render: function(data, type, full, meta) {
                    var $status_number = full['status'];
                    var $status = {
                        1: {
                            title: 'Approved',
                            class: 'badge-light-success'
                        },
                        2: {
                            title: 'DisApproved',
                            class: ' badge-light-danger'
                        },
                        3: {
                            title: 'Pending',
                            class: ' badge-light-warning'
                        },
                    };
                    if (typeof $status[$status_number] === 'undefined') {
                        return data;
                    }
                    return ('<span class="badge badge-pill ' + $status[$status_number].class + '">' + $status[$status_number].title + '</span>');
                }
            },
            {
                // Actions
                targets: 7,
                title: 'Actions',
                orderable: false,
                render: function(data, type, full, meta) {
                    return ('<div class="d-inline-flex">' + '<a href="javascript:;" class="approve action-icons">' + '<span data-toggle="tooltip" title data-original-title="Approve / DisApprove">' + feather.icons['user-check'].toSvg({
                        class: 'font-small-4 mr-50'
                    }) + '</span>' + '</a>' + '<a href="#"  class="active-record action-icons">' + '<span data-toggle="tooltip" title data-original-title="Active / InActive">' + feather.icons['check-square'].toSvg({
                        class: 'font-small-4 mr-50'
                    }) + '</span>' + '</a>' + '<a href="viewfacilitator.php"  data-toggle="modal" data-target="#edit-accessor-modal" class="action-icons">' + '<span data-toggle="tooltip" title data-original-title="View / Edit">' + feather.icons['edit'].toSvg({
                        class: 'font-small-4 mr-50'
                    }) + '</span>' + '</a>' + '</div>');
                }
            }
        
        ],
            dom: '<"row d-flex justify-content-between align-items-center m-1"' + '<"col-lg-1 d-flex align-items-center"l>' + '<"col-lg-5 "f>' + '<"col-lg-5 d-flex align-items-center justify-content-lg-end flex-wrap  p-0"<"dt-action-buttons text-right"B>>' + '>t' + '<"d-flex justify-content-between  mx-2"' + '<"col-sm-12 col-md-6"i>' + '<"col-sm-12 col-md-6"p>' + '>',
            // orderCellsTop: !0,
            select: {
                style: "multi",
                selector: "td:first-child",
                items: "row"
            },
            language: {
                sLengthMenu: 'Show _MENU_',
                search: '',
                searchPlaceholder: 'Search',
                paginate: {
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            },
            buttons: [{
                extend: 'collection',
                className: 'btn btn-outline-secondary dropdown-toggle ml-1 pr-1',
                text: '<i class="fas fa-tasks "></i>' + 'Bulk Actions',
                buttons: [{
                    text: feather.icons['check-square'].toSvg({
                        class: 'font-small-4 mr-50'
                    }) + 'Update Account Status',
                    className: 'dropdown-item',
                    attr: {
                    "data-toggle": "modal",
                    "data-target": "#changestatus"
                }
                },
               {
                    text: feather.icons['file-text'].toSvg({
                        class: 'font-small-4 mr-50'
                    }) + 'Export',
                    className: 'dropdown-item',
                    exportOptions: {
                        columns: [1, 2, 3, 4, 5, 6]
                    }
                }],
                init: function(api, node, config) {
                    $(node).removeClass('btn-secondary');
                    $(node).parent().removeClass('btn-group');
                    setTimeout(function() {
                        $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                    }, 50);
                }
            }, {
                text: feather.icons['plus'].toSvg({
                    class: 'mr-50 font-small-4'
                }) + 'Add Consultant',
                className: 'btn btn-primary btn-add-record ml-2',
                attr: {
                    "data-toggle": "modal",
                    "data-target": "#accessor-modal"
                },
                init: function(api, node, config) {
                    $(node).removeClass('btn-secondary');
                }
            }],
            drawCallback: function() {
                $(document).find('[data-toggle="tooltip"]').tooltip();
            }
        });
        $('.dt-accessorlist').on('click', '.approve', function() {
            Swal.fire({
                title: 'Do You Want to Approve?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Approve it!',
                showDenyButton: true,
                denyButtonText: 'No, Reject it',
                showClass: {
                    popup: 'animate__animated animate__fadeIn'
                },
                customClass: {
                    confirmButton: 'btn btn-primary',
                    cancelButton: 'btn btn-outline-danger ml-1',
                    denyButton: 'btn btn-secondary ml-1',
                },
                buttonsStyling: false
            }).then(function(result) {
                if (result.value) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Approved!',
                        confirmButtonText: 'Okay',
                        text: 'Your Facilitator has been Approved.',
                        showClass: {
                            popup: 'animate__animated animate__fadeIn'
                        },
                        customClass: {
                            confirmButton: 'btn btn-primary'
                        },
                        buttonsStyling: false
                    });
                } else if (result.isDenied) {
                    Swal.fire({
                        title: 'Reason for Rejection!',
                        input: 'textarea',
                        showCancelButton: true,
                        confirmButtonText: 'Submit',
                        showClass: {
                            popup: 'animate__animated animate__fadeIn'
                        },
                        customClass: {
                            confirmButton: 'btn btn-primary',
                            cancelButton: 'btn btn-outline-danger ml-1',
                            denyButton: 'btn btn-secondary ml-1',
                        },
                        buttonsStyling: false
                    })
                  
                }
            });
        })
        $('.dt-accessorlist').on('click', '.active-record', function() {
            Swal.fire({
                title: 'Are you Sure?',
                text: "",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Activate it!',
                showClass: {
                    popup: 'animate__animated animate__fadeIn'
                },
                customClass: {
                    confirmButton: 'btn btn-primary',
                    cancelButton: 'btn btn-outline-danger ml-1'
                },
                buttonsStyling: false
            }).then(function(result) {
                if (result.value) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Activated!',
                        text: 'Your facilitator has been Activated.',
                        showClass: {
                            popup: 'animate__animated animate__fadeIn'
                        },
                        buttonsStyling: false,
                        customClass: {
                            confirmButton: 'btn btn-success'
                        }
                    });
                }
            });
        })
    }));
</script>