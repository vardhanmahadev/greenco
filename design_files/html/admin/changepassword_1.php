<?php include('includes/header.php'); ?> 
 <div class="app-content content content-ps-stl pt-4">
    <div class="">
        <div class="content-overlay"></div>

        <div class="content-wrapper">
            <div class="content-body">
            <section class="p-relative">
                    <ul class="nav nav-pills tabs-pages tab-leftside">
                        <li class="nav-item">
                            <a class="nav-link " href="profile1.php">
                                <span class="bs-stepper-box">
                                    <i class="fal fa-long-arrow-left"></i>
                                </span>Profile </a>
                        </li>
                    </ul>
                    <p class="edit-tab">Change Password? <a href="#" onclick="goBack()"><i class="fal fa-times"></i></a></p>
                </section>

                <div class="col-lg-6 pb-3 offset-lg-3 pt-4">
                <div class="tab-content">
                        <div class="tab-pane active" id="changepasswordIcon" aria-labelledby="changepassword-tab" role="tabpanel">
                            <section>
                                <div class="container">
                                    <div class="row  change-password v-row">
                                        <div class="col-lg-10 offset-lg-1  mt-2 pb-3 ">
                                            <div id="changepassword" class="">
                                                <form class="auth-reset-password-form form-auth-div mt-2" action="login.html" method="POST">
                                                    <div class="form-group">
                                                        <div class="d-flex justify-content-between">
                                                            <label for="reset-password-new">Old Password</label>
                                                        </div>
                                                        <div class="input-group input-group-merge form-password-toggle">
                                                            <input class="form-control form-control-merge" id="reset-password-old" type="password" name="reset-password-old" placeholder="" aria-describedby="reset-password-old" autofocus="" tabindex="1" />
                                                            <div class="input-group-append"><span class="input-group-text border-right-line cursor-pointer"><i data-feather="eye"></i></span></div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="d-flex justify-content-between">
                                                            <label for="reset-password-new">New Password</label>
                                                        </div>
                                                        <div class="input-group input-group-merge form-password-toggle">
                                                            <input class="form-control form-control-merge" id="reset-password-new" type="password" name="reset-password-new" placeholder="" aria-describedby="reset-password-new" autofocus="" tabindex="2" />
                                                            <div class="input-group-append"><span class="input-group-text  border-right-line cursor-pointer"><i data-feather="eye"></i></span></div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="d-flex justify-content-between">
                                                            <label for="reset-password-confirm">Confirm Password</label>
                                                        </div>
                                                        <div class="input-group input-group-merge form-password-toggle">
                                                            <input class="form-control form-control-merge" id="reset-password-confirm" type="password" name="reset-password-confirm" placeholder="" aria-describedby="reset-password-confirm" tabindex="3" />
                                                            <div class="input-group-append"><span class="input-group-text border-right-line cursor-pointer"><i data-feather="eye"></i></span></div>
                                                        </div>
                                                    </div>
                                                    <button class="btn btn-primary btn-block" tabindex="3">Set New Password</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>               
                <!-- Modern Horizontal Wizard -->
            </div>
        </div>
    </div>
</div>
<!-- END: Content--> <?php include('includes/footer.php'); ?>