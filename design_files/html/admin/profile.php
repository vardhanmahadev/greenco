<?php include('includes/header.php'); ?> 
 <div class="app-content content content-ps-stl pt-4">
    <div class="">
        <div class="content-overlay"></div>

        <div class="content-wrapper">
            <div class="content-body">
                <ul class="nav nav-tabs tabs-pages tab-leftside" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link tab-links active mini-border" href="profile.php"  aria-selected="true">
                            <i data-feather="user"></i>Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link tab-links mini-border" href="changePassword.php" aria-controls="changepasswordIcon" aria-selected="false">
                        <i data-feather="lock"></i>Change Password</a>
                    </li>
                </ul>

                <div class="col-lg-6 pb-3 ">
                    <div class="tab-content">
                        <div class="tab-pane active" id="profileIcon" aria-labelledby="profile-tab" role="tabpanel">
                                <div class="container">
                                    <div class="row profile v-row">
                                        <div class="col-lg-10 offset-lg-1  mt-2 pb-3 ">
                                            <div id="profile" class="">
                                                <form class="auth-reset-password-form form-auth-div mt-2" action="#" method="POST">
                                                    <div class="form-label-group">
                                                        <label class="form-label float-label" for="register-username">Username</label>
                                                        <div class="input-group">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text border-left-line cursor-pointer"><i data-feather="user"></i></span>
                                                            </div>
                                                            <input class="form-control form-caontrol-merge border-right-line" id="register-username" autocomplete="register-username" type="text" name="register-username" placeholder="Username*" value=" Rajesh" autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" />
                                                        </div>
                                                    </div>
                                                    <div class="form-label-group">
                                                        <label class="form-label float-label" for="register-email">Email Address</label>
                                                        <div class="input-group">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text border-left-line cursor-pointer"><i data-feather="at-sign"></i></span>
                                                            </div>
                                                            <input class="form-control border-right-line" id="register-email" type="text" name="register-email" placeholder="Email Address*" value="rajesh@gmail.com" autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" />
                                                        </div>
                                                    </div>
                                                    <div class="form-label-group">
                                                        <label class="form-label float-label" for="register-mobile">Mobile Number</label>
                                                        <div class="input-group">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text border-left-line cursor-pointer"><i data-feather="phone"></i></span>
                                                            </div>
                                                            <input class="form-control border-right-line" id="register-mobile" type="text" name="register-mobile" placeholder="Mobile Number" value="9999999999" autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" />
                                                        </div>
                                                    </div>
                                                    <button class="btn btn-primary btn-block" tabindex="3">Submit</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>               
                <!-- Modern Horizontal Wizard -->
            </div>
        </div>
    </div>
</div>
<!-- END: Content--> <?php include('includes/footer.php'); ?>