<?php include('includes/header.php'); ?> 
 <div class="app-content content content-ps-stl pt-4">
    <div class="">
        <div class="content-overlay"></div>

        <div class="content-wrapper">
            <div class="content-body">
          

                <div class="col-lg-6 pb-3 offset-lg-3 pt-5 mt-3">
                <div class="row">
                            <a class="btn btn-primary mb-2 text-right ml-auto" href="changepassword_1.php" tabindex="3"><i class="lock"></i>Change Password</a>
                        </div>
                    <div class="tab-content">
                  
                        <div class="tab-pane active" id="profileIcon" aria-labelledby="profile-tab" role="tabpanel">
                                <div class="container">  
                                    <div class="row profile v-row">
                                        <div class="col-lg-10 offset-lg-1  mt-2 pb-3 ">
                                     
                                            <div id="profile" class="">
                                                <form class="auth-reset-password-form form-auth-div mt-2" action="#" method="POST">
                                                    <div class="form-label-group">
                                                        <label class="form-label float-label" for="register-username">Username</label>
                                                        <div class="input-group">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text border-left-line cursor-pointer"><i data-feather="user"></i></span>
                                                            </div>
                                                            <input class="form-control form-caontrol-merge border-right-line" id="register-username" autocomplete="register-username" type="text" name="register-username" placeholder="Username*" value=" Rajesh" autocomplete="false"  onfocus="this.removeAttribute('readonly');" />
                                                        </div>
                                                    </div>
                                                    <div class="form-label-group">
                                                        <label class="form-label float-label" for="register-email">Email Address</label>
                                                        <div class="input-group">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text border-left-line cursor-pointer"><i data-feather="at-sign"></i></span>
                                                            </div>
                                                            <input class="form-control border-right-line" id="register-email" type="text" name="register-email" placeholder="Email Address*" value="rajesh@gmail.com" autocomplete="false"  onfocus="this.removeAttribute('readonly');" />
                                                        </div>
                                                    </div>
                                                    <div class="form-label-group">
                                                        <label class="form-label float-label" for="register-mobile">Mobile Number</label>
                                                        <div class="input-group">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text border-left-line cursor-pointer"><i data-feather="phone"></i></span>
                                                            </div>
                                                            <input class="form-control border-right-line" id="register-mobile" type="text" name="register-mobile" placeholder="Mobile Number" value="9999999999" autocomplete="false"  onfocus="this.removeAttribute('readonly');" />
                                                        </div>
                                                    </div>
                                                    <button class="btn btn-primary btn-block" tabindex="3">Submit</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>               
                <!-- Modern Horizontal Wizard -->
            </div>
        </div>
    </div>
</div>
<!-- END: Content--> <?php include('includes/footer.php'); ?>