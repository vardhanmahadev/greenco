<?php include('includes/header.php'); ?> 
 <div class="app-content content content-ps-stl pt-4">
    <div class="">
        <div class="content-overlay"></div>

        <div class="content-wrapper">
            <div class="content-body">
                <ul class="nav nav-tabs tabs-pages tab-leftside" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link tab-links mini-border" href="profile.php" aria-selected="true">
                            <i data-feather="user"></i>Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link tab-links active mini-border" href="changePassword.php" aria-selected="false">
                        <i data-feather="lock"></i>Change Password</a>
                    </li>
                </ul>

                <div class="col-lg-6 pb-3">
                    <div class="tab-content changepassword">
                        <div class="tab-pane active" id="changepasswordIcon" aria-labelledby="changepassword-tab" role="tabpanel">
                            <section>
                                <div class="container">
                                    <div class="row  change-password v-row">
                                        <div class="col-lg-10 offset-lg-1  mt-2 pb-3 ">
                                            <div id="changepassword" class="">
                                                <form class="auth-reset-password-form form-auth-div mt-2" action="login.html" method="POST">
                                                    <div class="form-group">
                                                        <div class="d-flex justify-content-between">
                                                            <label for="reset-password-new">Old Password</label>
                                                        </div>
                                                        <div class="input-group input-group-merge form-password-toggle">
                                                            <input class="form-control form-control-merge" id="reset-password-old" type="password" name="reset-password-old" placeholder="" aria-describedby="reset-password-old" autofocus="" tabindex="1" />
                                                            <div class="input-group-append"><span class="input-group-text border-right-line cursor-pointer"><i data-feather="eye"></i></span></div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="d-flex justify-content-between">
                                                            <label for="reset-password-new">New Password</label>
                                                        </div>
                                                        <div class="input-group input-group-merge form-password-toggle">
                                                            <input class="form-control form-control-merge" id="reset-password-new" type="password" name="reset-password-new" placeholder="" aria-describedby="reset-password-new" autofocus="" tabindex="2" />
                                                            <div class="input-group-append"><span class="input-group-text  border-right-line cursor-pointer"><i data-feather="eye"></i></span></div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="d-flex justify-content-between">
                                                            <label for="reset-password-confirm">Confirm Password</label>
                                                        </div>
                                                        <div class="input-group input-group-merge form-password-toggle">
                                                            <input class="form-control form-control-merge" id="reset-password-confirm" type="password" name="reset-password-confirm" placeholder="" aria-describedby="reset-password-confirm" tabindex="3" />
                                                            <div class="input-group-append"><span class="input-group-text border-right-line cursor-pointer"><i data-feather="eye"></i></span></div>
                                                        </div>
                                                    </div>
                                                    <button class="btn btn-primary btn-block" tabindex="3">Set New Password</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>               
                <!-- Modern Horizontal Wizard -->
            </div>
        </div>
    </div>
</div>
<!-- END: Content--> <?php include('includes/footer.php'); ?>