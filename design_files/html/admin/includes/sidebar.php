

    <div class="deznav">
    <div class="deznav-scroll">
        <ul class="metismenu" id="menu">
            <li class="<?php echo basename($_SERVER['PHP_SELF']) == 'index.php' ? 'active' : '';?>"><a class="ai-icon"  href="index.php" aria-expanded="false">
                    <i data-feather="airplay"></i><span class="menu-title " data-i18n="Dashboards"> Dashboard </span></a>
            </li>
            <li class="<?php echo basename($_SERVER['PHP_SELF']) == 'companymanagement.php' ? 'active' : '';?>"><a class="ai-icon" href="companymanagement.php" aria-expanded="false">
            <i class="fal fa-building"></i><span class="menu-title " data-i18n="Invoice">  Company Management </span></a>
            </li>
            <li class="<?php echo basename($_SERVER['PHP_SELF']) == 'assessormanagement.php' ? 'active' : '';?>"><a class="ai-icon" href="assessormanagement.php" aria-expanded="false">
            <i class="fal fa-users"></i><span class="menu-title " data-i18n="Invoice">  Assessor Management </span></a>
            </li>
            <li class="<?php echo basename($_SERVER['PHP_SELF']) == 'facilitatormanagement.php' ? 'active' : '';?>"><a class="ai-icon" href="facilitatormanagement.php" aria-expanded="false">
            <i class="fal fa-comments-alt"></i><span class="menu-title " data-i18n="Invoice">  Facilitator Management </span></a>
            </li>
        </ul>
    </div></div>
       