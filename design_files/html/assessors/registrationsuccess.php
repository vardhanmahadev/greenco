<?php include('includes/auth-header.php'); ?>
    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
     
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="auth-wrapper auth-v2">
                    <div class="auth-inner row m-0">
                        <!-- Left Text-->
                        <div class="col-lg-8  width-img">
                    <div class="item pt-5 mt-5  col-lg-10 offset-lg-1 text-center">
                            <div class="slider-block">
                                <img class="img-fluid" src="http://13.126.160.234/greenco/app-assets/images/login_slider.png" alt="Login V2">
                                <div class="carousel-caption">
                                    <!-- <h3> The Confederation of Indian Industry (CII) works to create and sustain an environment conducive to the development of India, partnering industry, Government, and civil society, through advisory and consultative processes.</h3> -->
                                </div>
                            </div>
                            <!-- </div> -->
                        </div>
                </div> 
                        <!-- /Left Text-->
                        <!-- Register-->
                        <div class="d-flex col-lg-4 align-items-center auth-bg px-2 reg-successtext">
                            <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                            <span class="success-tick">✔</span>
                                <h3 class="card-title font-weight-bold text-center mb-0">Registration Successfull</h3>
                              <p class="">Thanks for registering, Your Account will approved by our team. you will be able to access the assessor Portal. usually, it's take 24 to 48 hours to validate any user</p>
                            
                                   
                                    <div class="form-group d-flex justify-content-center mb-0">
                                         <a class="btn btn-primary " href="login" tabindex="5">Click here to login <i data-feather="arrow-right"></i> </a>
                                
                                    </div>
                               
                                </form>
                              </div>
                        </div>
                        <!-- /Register-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('includes/auth-footer.php'); ?>