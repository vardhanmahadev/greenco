<?php include('includes/auth-header.php'); ?> <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
     
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="auth-wrapper auth-v2">
                    <div class="auth-inner row m-0">
                        <!-- Brand logo-->
                        <!-- /Brand logo-->
                        <!-- Left Text-->
                        <div class="d-none d-lg-flex col-lg-8 ">
                            <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
                                <img class="img-fluid" src="../../app-assets/images/pages/forgot.jpg" alt="Forgot password V2" /></div>
                        </div>
                        <!-- /Left Text-->
                        <!-- Forgot password-->
                        <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
                            <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                                <h2 class="card-title font-weight-bold mb-3">Forgot Password?</h2>
                                <form class="auth-forgot-password-form auth-form-div mt-2" action="page-auth-reset-password-v2.html" method="POST">
                                    <div class="form-label-group">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text border-left-line cursor-pointer"><i data-feather="user"></i></span>
                                            </div>
                                            <input class="form-control form-caontrol-merge border-right-line" id="forgot-password-email" type="text" name="forgot-password-email" placeholder="Enter your Email Address" aria-describedby="forgot-password-email" autofocus="" tabindex="1" />
                                            <label class="form-label float-label" for="forgot-password-email">Email Address</label>
                                        </div>
                                    </div>
                                    <button class="btn btn-primary btn-block" tabindex="2">Submit</button>
                                </form>
                                <p class="text-center mt-2"><a href="login.php"><i data-feather="chevron-left"></i> Back to login</a></p>
                            </div>
                        </div>
                        <!-- /Forgot password-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('includes/auth-footer.php'); ?>