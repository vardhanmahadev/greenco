<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<!-- BEGIN: Footer-->
<button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>
<footer class="footer footer-static footer-light">
    <p class="clearfix mb-0"><span class="float-md-left d-block d-md-block mt-25">COPYRIGHT &copy; 2021<a class="ml-25" href="https://mirakitech.com" target="_blank">Miraki Technologies</a><span class="d-none d-sm-inline-block">, All rights Reserved</span></span></p>
</footer>
</div>



    <script src="../../app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN:Vendor JS-->
    <script src="../../app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js"></script>
    <script src="../../app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>
    <script src="../../app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
    <script src="../../app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js"></script>
    <script src="../../app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
    <script src="../../app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js"></script>
    <script src="../../app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="../../app-assets/vendors/js/tables/datatable/jszip.min.js"></script>
    <script src="../../app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
    <script src="../../app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
    <script src="../../app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
    <script src="../../app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
    <script src="../../app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js"></script>
    <script src="../../app-assets/vendors/js/tables/datatable/dataTables.fixedHeader.min.js"></script>
    <script src="../../app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js"></script>
    <!-- BEGIN: Page Vendor JS-->
    <script src="../../app-assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
    <script src="../../app-assets/vendors/js/extensions/polyfill.min.js"></script>
    <script src="../../app-assets/js/scripts/extensions/ext-component-sweet-alerts.js"></script>
    <!-- BEGIN: Page JS-->
    <script src="../../app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <script src="../../app-assets/js/core/app-menu.min.js"></script>
    <script src="../../app-assets/js/core/app.js"></script>
    <script src="../../assets/js/scripts.js"></script>
    <script src="../../app-assets/js/scripts/owl.carousel.min.js"></script>
    <script src="../../app-assets/js/scripts/pages/authentication_validation.js"></script>
    <script src="../../app-assets/js/scripts/forms/form-select2.min.js"></script>
    <script src="../../app-assets/js/scripts/components/components-navs.min.js"></script>
    
    <script src="../../app-assets/js/scripts/customizer.min.js"></script>
    <!-- END: Page JS-->
    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>
  
    
    <!-- END: Page Vendor JS-->

</body>
<!-- END: Body-->

</html>