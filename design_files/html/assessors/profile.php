<?php include('includes/header.php'); ?> <div class="app-content content content-ps-stl pt-4">
    <div class="">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <ul class="nav nav-tabs tabs-pages tab-leftside" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link tab-links active mini-border" href="profile.php" aria-selected="true">
                            <i data-feather="user"></i>Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link tab-links mini-border" href="changePassword.php" aria-controls="changepasswordIcon" aria-selected="false">
                            <i data-feather="lock"></i>Change Password</a>
                    </li>
                </ul>
                <div class="col-lg-6 pb-3 ">
                    <div class="tab-content">
                        <div class="tab-pane active" id="profileIcon" aria-labelledby="profile-tab" role="tabpanel">
                            <div class="container">
                                <div class="row profile v-row">
                                    <div class="col-lg-12 mt-2 pl-3 pr-3 pb-3 ">
                                        <div id="profile" class="">
                                            <form class="auth-reset-password-form form-auth-div mt-2" action="#" method="POST">
                                                <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-label-group ">
                                                    <input type="text" placeholder="Username" class="form-control form-caontrol-merge border-right-line error" autocomplete="off">
                                                    <label class="form-label float-label" for="register-username">Username<span class="text-danger">*</span></label>
                                                    <span class="error">This Field is Required</span>
                                                </div> </div>
                                                <div class="col-lg-12">
                                                    <div class="form-label-group ">
                                                    <input class="form-control border-right-line error" type="text" placeholder="Email Address*" autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" />
                                                    <label class="form-label float-label" for="register-email">Email Address<span class="text-danger">*</span></label>
                                                    <span class="error">This Field is Required</span>
                                                </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-label-group ">
                                                    <input class="form-control border-right-line error" type="text" placeholder="Mobile Number*" />
                                                    <label class="form-label float-label" for="register-email">Mobile Number<span class="text-danger">*</span></label>
                                                    <span class="error">This Field is Required</span>
                                                </div> </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="v-row">
                                                        <div class="col-lg-5 col-md-5  pl-0 pr-0">
                                                            <label class="mb-0">Approval Status :</label>
                                                        </div>
                                                        <div class="col-lg-7 col-md-7">
                                                            <p class="card-text font-small-2s mt-0 verified-text badge badge-light-success badge-pill ">Approved</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="v-row ">
                                                        <div class="col-lg-5 col-md-5  pl-0 pr-0">
                                                            <label class="mb-0">Account Status :</label>
                                                        </div>
                                                        <div class="col-lg-7 col-md-7">
                                                            <p class="card-text font-small-2s mt-0 verified-text badge badge-light-success badge-pill ">Active</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class=" v-row ">
                                                        <div class="col-lg-5 col-md-5  pl-0 pr-0">
                                                            <label class="mb-0">Approval Date : </label>
                                                        </div>
                                                        <div class="col-lg-7 col-md-7">
                                                            <p class="card-text font-small-2s mt-0 verified-text pl-0">23-12-2021 13.24 </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="v-row ">
                                                        <div class="col-lg-5 col-md-5  pl-0 pr-0">
                                                            <label class="mb-0">Account Activated Date : </label>
                                                        </div>
                                                        <div class="col-lg-7 col-md-7">
                                                            <p class="card-text font-small-2s mt-0 verified-text pl-0">23-12-2021 13.24 </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row d-flex justify-content-center col-lg-12 mt-2">  <button class="btn btn-primary " tabindex="3">Submit</button></div>
                                              
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modern Horizontal Wizard -->
            </div>
        </div>
    </div>
</div>
<!-- END: Content--> <?php include('includes/footer.php'); ?>