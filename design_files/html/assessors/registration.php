<?php include('includes/auth-header.php'); ?>
<link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/forms/wizard/bs-stepper.min.css">
<link rel="stylesheet" type="text/css" href="../../app-assets/css/plugins/forms/form-validation.min.css">
<link rel="stylesheet" type="text/css" href="../../app-assets/css/plugins/forms/form-wizard.min.css">
    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
     
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="auth-wrapper auth-v2">
                    <div class="auth-inner row m-0">
                        <!-- Left Text-->
                        <div class="col-lg-8  width-img">
                    <div class="item pt-5 mt-5  col-lg-10 offset-lg-1 text-center">
                            <div class="slider-block">
                                <img class="img-fluid" src="http://13.126.160.234/greenco/app-assets/images/login_slider.png" alt="Login V2">
                                <div class="carousel-caption">
                                    <!-- <h3> The Confederation of Indian Industry (CII) works to create and sustain an environment conducive to the development of India, partnering industry, Government, and civil society, through advisory and consultative processes.</h3> -->
                                </div>
                            </div>
                            <!-- </div> -->
                        </div>
                </div> 
                        <!-- /Left Text-->
                        <!-- Register-->
                        <div class="d-flex col-lg-4 align-items-center auth-bg px-2 ">
                            <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                                <a class="brand-logo" href="javascript:void(0);" style="cursor:default!important;">
                                    <h2 class="brand-text text-primary ">
                                        <img class="img-fluid" src="../../app-assets/images/logo/greenco.png" width="150" alt="Login V2">
                                    </h2>
                                </a>
                                <h3 class="card-title font-weight-bold text-center mb-0">Assessor Registration</h3>
                                <form class="auth-register-form auth-form-div mt-2"  method="POST" autocomplete="false">
                                    <div class="form-label-group">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text border-left-line cursor-pointer"><i data-feather="user"></i></span>
                                            </div>
                                            <input class="form-control form-caontrol-merge border-right-line" id="register-username"  autocomplete="register-username" type="text" name="register-username" placeholder="Username*"  autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" />
                                            <label class="form-label float-label" for="register-username">Username<span class="text-danger">*</span></label>
                                        </div>
                                    </div>
                                    <div class="form-label-group">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text border-left-line cursor-pointer"><i data-feather="at-sign"></i></span>
                                            </div>
                                            <input class="form-control border-right-line" id="register-email" type="text" name="register-email" placeholder="Email Address*" autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" />
                                            <label class="form-label float-label" for="register-email">Email Address<span class="text-danger">*</span></label>
                                        </div>
                                    </div>
                                  
                                    <div class="form-label-group">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text border-left-line cursor-pointer"><i data-feather="phone"></i></span>
                                            </div>
                                            <input class="form-control border-right-line" id="register-mobile" type="text" name="register-mobile" placeholder="Mobile Number*" />
                                            <label class="form-label float-label" for="register-email">Mobile Number<span class="text-danger">*</span></label>
                                        </div>
                                    </div>
                                    
                                  
                                   
                                    <button class="btn btn-primary btn-block SendOtp-registration" tabindex="5">Register <i class="fal fa-long-arrow-right"></i> </button>
                                    <!-- <div class="form-group d-flex justify-content-center mb-0">
                                        <button type="button" class="btn btn-primary btn-block "> Continue  </button>
                                    </div> -->
                               
                                </form>
                                <p class="text-center mt-h5"><span>Do you have an Account?</span><a href="login.php"><span>&nbsp;Sign In</span></a></p>
                            </div>
                        </div>
                        <!-- /Register-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('includes/auth-footer.php'); ?>