<?php include('includes/auth-header.php'); ?>
<link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/forms/wizard/bs-stepper.min.css">
<link rel="stylesheet" type="text/css" href="../../app-assets/css/plugins/forms/form-validation.min.css">
<link rel="stylesheet" type="text/css" href="../../app-assets/css/plugins/forms/form-wizard.min.css">
<!-- BEGIN: Content-->
<div class="app-content content ">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <div class="auth-wrapper auth-v2">
                <div class="auth-inner row m-0">
                    <!-- Left Text-->
                    <div class="col-lg-8  width-img">
                        <div class="item  col-lg-10 offset-lg-1 text-center position-sticky div-image-left">
                            <div class="slider-block">
                                <img class="img-fluid" src="../../app-assets/images/login_slider.png" alt="Login V2">
                                <div class="carousel-caption">
                                    <!-- <h3> The Confederation of Indian Industry (CII) works to create and sustain an environment conducive to the development of India, partnering industry, Government, and civil society, through advisory and consultative processes.</h3> -->
                                </div>
                            </div>
                            <!-- </div> -->
                        </div>
                    </div>
                    <!-- /Left Text-->
                    <!-- Register-->
                    <div class="d-flex col-lg-4 align-items-center auth-bg px-2 ">
                    <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                                <a class="brand-logo" href="javascript:void(0);" style="cursor:default!important;">
                                    <h2 class="brand-text text-primary ">
                                        <img class="img-fluid" src="../../app-assets/images/logo/greenco.png" width="150" alt="Login V2">
                                    </h2>
                                </a>
                                <h3 class="card-title font-weight-bold text-center mb-0">Company Registration</h3>
                                <form class="auth-register-form auth-form-div mt-2"  method="POST" autocomplete="false">
                                    <div class="form-label-group">
                                        <div class="input-group is-invalid">
                                            <div class="input-group-append">
                                                <span class="input-group-text border-left-line cursor-pointer"><i data-feather="user"></i></span>
                                            </div>
                                            <input type="text" placeholder="Company Name*" name="manufacturer_name"  id="manufacturer_name" class="error border-left-none ui-autocomplete-input form-control form-caontrol-merge border-right-line " autocomplete="off">
                               
                                            <label class="form-label float-label" for="register-username">Company Name <span class="text-danger">*</span></label>
                                            
                                        </div>
                                        <span id="login-email-error" class="error">This field is required.</span>
                                    </div>
                                    <div class="form-label-group">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text border-left-line cursor-pointer"><i data-feather="at-sign"></i></span>
                                            </div>
                                            <input class="form-control border-right-line" id="register-email" type="text" name="register-email" placeholder="Email Address*" autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" />
                                            <label class="form-label float-label" for="register-email">Email Address<span class="text-danger">*</span></label>
                                        </div>
                                    </div>
                                  
                                    <div class="form-label-group">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text border-left-line cursor-pointer"><i data-feather="phone"></i></span>
                                            </div>
                                            <input class="form-control border-right-line" id="register-mobile" type="text" name="register-mobile" placeholder="Mobile Number*" />
                                            <label class="form-label float-label" for="register-email">Mobile Number<span class="text-danger">*</span></label>
                                        </div>
                                    </div>
                                    
                                  
                                   
                                    <button class="btn btn-primary btn-block SendOtp-registration" tabindex="5">Register <i class="fal fa-long-arrow-right"></i> </button>
                                    <!-- <div class="form-group d-flex justify-content-center mb-0">
                                        <button type="button" class="btn btn-primary btn-block "> Continue  </button>
                                    </div> -->
                               
                                </form>
                                <p class="text-center mt-h5"><span>Do you have an Account?</span><a href="login.php"><span>&nbsp;Sign In</span></a></p>
                            </div>
                      
                    </div>
                    <!-- /Register-->
                </div>
            </div>
        </div>
    </div>
</div> <?php include('includes/auth-footer.php'); ?>

<script>
    var manufacturer_list = [{
        "id": "19",
        "value": "ABC Ceramic Services Limited"
    }, {
        "id": "40",
        "value": "ACC Limited"
    }, {
        "id": "96",
        "value": "ACC Limited"
    }, {
        "id": "20",
        "value": "Aeropure UV Systems Pvt. Ltd"
    }, {
        "id": "83",
        "value": "AET Building Products (WI) Pvt Ltd"
    }, {
        "id": "305",
        "value": "AICA Laminates India Pvt Ltd"
    }, {
        "id": "41",
        "value": "AkzoNobel India Ltd"
    }, {
        "id": "252",
        "value": "Aliferous Technologies Private Limited (Clairco)"
    }, {
        "id": "89",
        "value": "Anutone Acoustics Limited"
    }, {
        "id": "42",
        "value": "Aquatron International AB"
    }, {
        "id": "281",
        "value": "ArcelorMittal Nippon Steel India Ltd"
    }, {
        "id": "297",
        "value": "Archidply Industries Ltd"
    }, {
        "id": "7",
        "value": "Armstrong World Industries (India) Pvt. Ltd"
    }, {
        "id": "311",
        "value": "Arvind Envisol Limited"
    }, {
        "id": "21",
        "value": "Asahi India Glass Ltd"
    }, {
        "id": "208",
        "value": "Asawa Insulation Pvt Ltd"
    }, {
        "id": "90",
        "value": "Ashish Bags & Fashions Ltd"
    }, {
        "id": "84",
        "value": "Ashok Chemicals"
    }, {
        "id": "189",
        "value": "Astral Poly Technik Limited"
    }, {
        "id": "207",
        "value": "Bayer CropScience Ltd"
    }, {
        "id": "22",
        "value": "Berger Becker Coatings Pvt Ltd"
    }, {
        "id": "8",
        "value": "Berger Paints India Limited"
    }, {
        "id": "98",
        "value": "Berger Paints India Limited-ETICS Division"
    }, {
        "id": "92",
        "value": "BioMicrobics Inc"
    }, {
        "id": "68",
        "value": "Bonphul Air Products"
    }, {
        "id": "213",
        "value": "Bonton Technomake Pvt Ltd"
    }, {
        "id": "9",
        "value": "British Paints"
    }, {
        "id": "217",
        "value": "Camfil India Private Limited"
    }, {
        "id": "116",
        "value": "Cera Sanitaryware Limited"
    }, {
        "id": "209",
        "value": "Cipy Polyurethanes Private Limited"
    }, {
        "id": "183",
        "value": "Daiki Axis India Pvt. Ltd"
    }, {
        "id": "10",
        "value": "Dalmia Cement"
    }, {
        "id": "11",
        "value": "Deeya Panel Products Pvt. Ltd"
    }, {
        "id": "103",
        "value": "Diamond Cements (Prop: HeidelbergCement India Ltd)"
    }, {
        "id": "299",
        "value": "Dolphy India Private Limited"
    }, {
        "id": "214",
        "value": "Durlum India Pvt Ltd"
    }, {
        "id": "184",
        "value": "E3 Extrusion Inc"
    }, {
        "id": "317",
        "value": "Ecowood Exports Pvt Ltd"
    }, {
        "id": "45",
        "value": "Eden Innovations India Pvt. Ltd"
    }, {
        "id": "46",
        "value": "Ekam Eco Solutions Pvt. Ltd"
    }, {
        "id": "47",
        "value": "Emerson Commercial & Residential Solutions"
    }, {
        "id": "133",
        "value": "Emmvee Photovoltaic Power Private Limited"
    }, {
        "id": "273",
        "value": "EUKPRO Biotech Pvt Ltd"
    }, {
        "id": "102",
        "value": "Euronics Industries Private Limited"
    }, {
        "id": "155",
        "value": "Everest Industries Ltd"
    }, {
        "id": "260",
        "value": "Featherlite"
    }, {
        "id": "48",
        "value": "Furaat Earth Pvt  Ltd"
    }, {
        "id": "1",
        "value": "Godrej & Boyce Mfg. Co.Ltd - Construction Division"
    }, {
        "id": "50",
        "value": "Godrej and Boyce Mfg. Co.Ltd \u2013 Electrical Division"
    }, {
        "id": "24",
        "value": "Godrej Interio"
    }, {
        "id": "51",
        "value": "Green Build Products (I) Pvt. Ltd"
    }, {
        "id": "117",
        "value": "Greenrich Grow India Pvt Ltd"
    }, {
        "id": "206",
        "value": "Greentek Plast"
    }, {
        "id": "85",
        "value": "Grenove Services Pvt. Ltd"
    }];
    $("#manufacturer_name").autocomplete({
        source: manufacturer_list,
        function(request, response) {
            response($.map(manufacturer_list, function(item) {
                return {
                    id: item.id,
                    value: item.value,
                }
            }))
        },
        response: function(event, ui) {
            if (ui.content.length === 0) {
                $('#manufacturer_id').val('-1');
            }
        },
        select: function(event, ui) {
            $('#manufacturer_id').val('-1');
            $(this).val(ui.item.value);
            $('#manufacturer_id').val(ui.item.id);
        },
        minLength: 2,
        autoFocus: true
    });
</script>