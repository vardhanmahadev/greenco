<?php include('includes/auth-header.php'); ?> <div class="app-content content m-a0 ml-0 p-a0">
    <div class="content-overlay"></div>
 
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <div class="auth-wrapper auth-v2">
                <div class="auth-inner row m-0">
                    <div class="col-lg-8  width-img">
                        <div class="item  col-lg-10 offset-lg-1 text-center position-sticky div-image-left">
                            <div class="slider-block">
                                <img class="img-fluid" src="../../app-assets/images/login_slider.png" alt="Login V2">
                                <div class="carousel-caption">  </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-2">
                        <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                            <a class="brand-logo" href="javascript:void(0);">
                                <h2 class="brand-text text-primary ">
                                    <img class="img-fluid" src="../../app-assets/images/logo/greenco.png" width="150" alt="Login V2">
                                </h2>
                            </a>
                            <h2 class="card-title font-weight-bold mb-3 text-center">Company Login</h2>
                        
                            <form class="auth-login-form auth-form-div" action="index.php" method="POST">
                                <div class="form-label-group">
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text border-left-line cursor-pointer"><i data-feather="user"></i></span>
                                        </div>
                                        <input class="form-control border-right-line form-caontrol-merge" id="login-email" type="text" name="login-email" placeholder="Enter Your Username" aria-describedby="login-email" autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" tabindex="1" />
                                        <label for="login-email" class="float-label">Email</label>
                                    </div>
                                </div>
                                <div class="form-label-group mb-h5">
                                    <div class="d-flex justify-content-end label-link position-absolute-right">
                                        <a href="forgotpassword.php" class="id-anchor">Forgot Password?</a>
                                    </div>
                                    <div class="input-group input-group-merge form-password-toggle">
                                        <div class="input-group-append">
                                            <span class="input-group-text  border-left-line cursor-pointer"><i data-feather="lock"></i></span>
                                        </div>
                                        <input type="password" class="form-control form-control-merge" id="login-password" name="login-password" tabindex="2" placeholder="Enter Your Password" aria-describedby="login-password" />
                                        <label for="login-password" class="float-label">Password</label>
                                        <div class="input-group-append">
                                            <span class="input-group-text border-right-line  cursor-pointer"><i data-feather="eye"></i></span>
                                        </div>
                                    </div>
                                </div>
                              
                                <div class="mt-2">
                                    <button class="btn btn-primary btn-block" tabindex="4">Sign in</button>
                                </div>
                            </form>
                            <p class="text-center mt-h5"><span>If You Don't have an Account?</span><a href="registration.php"><span>&nbsp;Create an account</span></a></p>
                        </div>
                    </div>
                    <!-- /Login-->
                </div>
            </div>
        </div>
    </div>
</div> <?php include('includes/auth-footer.php'); ?>