<?php include('includes/header.php');
?><div class="app-content content content-ps-stl pt-4">
    <div class="mt-2">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                    <section>
                        <div class="container">
                            <div class="row  pb-1">
                                <div class="col-lg-6">
                                    <div class="card ">
                                        <div class="d-flex v-row title-strip mb-2">
                                            <div class="col-6 pl-0">Proposal Document</div>
                                        </div>
                                        <div class="card-body ">
                                            <div class="row  pb-1 mt--1">
                                                <form>
                                                    <div class="col-lg-12">
                                                        <div class="row v-row mb-2">
                                                            <div class="col-lg-2 col-md-2"><label>Uploaded File :</label></div>
                                                            <div class="col-lg-4 col-md-4">
                                                                <div class="v-row  uploadedfile"><span class="">DC Bill of material.pdf</span><button class="btn btn-outline-secondary waves-effect ml-1"><i data-feather="download"></i></button></div>
                                                            </div>
                                                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12  mt-50">
                                                                <div class="custom-file"><input type="file" class="custom-file-input" id="customFile"><label class="custom-file-label" for="customFile">Choose file</label></div>
                                                            </div>
                                                            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 offset-lg-2"></div>
                                                        </div>
                                                        <div class="row"></div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="card ">
                                        <div class="d-flex v-row title-strip mb-2">
                                            <div class="col-6 pl-0">PO Document</div>
                                        </div>
                                        <div class="card-body ">
                                            <div class="row  pb-1 mt--1">
                                                <div class="col-lg-12">
                                                    <div class="row v-row mb-2">
                                                        <div class="col-lg-2 col-md-2"><label>Uploaded File :</label></div>
                                                        <div class="col-lg-4 col-md-4">
                                                            <div class="v-row  uploadedfile"><span class="">DC Bill of material.pdf</span><button class="btn btn-outline-secondary waves-effect ml-1"><i data-feather="download"></i></button></div>
                                                        </div>
                                                        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12  mt-50">
                                                            <div class="custom-file"><input type="file" class="custom-file-input" id="customFile"><label class="custom-file-label" for="customFile">Choose file</label></div>
                                                        </div>
                                                        <div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 offset-lg-2">
                                                          <!-- <div class="form-group"><label>Remarks</label><textarea class="form-control" rows="2"></textarea>
                                                        </div> -->
                                                    </div>
                                                </div>
                                                <!-- <div class="row"></div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
            </section>
        </div>
    </div>
</div>
</div><?php include('includes/footer.php');?>
