<?php include('includes/header.php'); ?>
        <div class="app-content content content-ps-stl">
            <div class="">
                <div class="content-overlay"></div>
             
                <div class="content-wrapper">
                    <div class="content-body">
                        <!-- Dashboard Ecommerce Starts -->
                        <section>
                            <div class="container">
                                <div class="row  change-password v-row">
                                    <div class="col-lg-6 card offset-lg-3  mt-3 px-3 pt-3 pb-3 ">
                                        <div id="changepassword" class="">
                                            <div class="content-header">
                                                <h4 class="mb-0 text-center">Change Password </h4>
                                            </div>
                                            <form class="auth-reset-password-form form-auth-div mt-2" action="login.html" method="POST">
                                                <div class="form-group">
                                                    <div class="d-flex justify-content-between">
                                                        <label for="reset-password-new">Old Password</label>
                                                    </div>
                                                    <div class="input-group input-group-merge form-password-toggle">
                                                        <input class="form-control form-control-merge" id="reset-password-old" type="password" name="reset-password-old"  aria-describedby="reset-password-old" autofocus="" tabindex="1" />
                                                        <div class="input-group-append"><span class="input-group-text border-right-line cursor-pointer"><i data-feather="eye"></i></span></div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="d-flex justify-content-between">
                                                        <label for="reset-password-new">New Password</label>
                                                    </div>
                                                    <div class="input-group input-group-merge form-password-toggle">
                                                        <input class="form-control form-control-merge" id="reset-password-new" type="password" name="reset-password-new"  aria-describedby="reset-password-new" autofocus="" tabindex="2" />
                                                        <div class="input-group-append"><span class="input-group-text  border-right-line cursor-pointer"><i data-feather="eye"></i></span></div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="d-flex justify-content-between">
                                                        <label for="reset-password-confirm">Confirm Password</label>
                                                    </div>
                                                    <div class="input-group input-group-merge form-password-toggle">
                                                        <input class="form-control form-control-merge" id="reset-password-confirm" type="password" name="reset-password-confirm"  aria-describedby="reset-password-confirm" tabindex="3" />
                                                        <div class="input-group-append"><span class="input-group-text border-right-line cursor-pointer"><i data-feather="eye"></i></span></div>
                                                    </div>
                                                </div>
                                                <button class="btn btn-primary btn-block" tabindex="3">Set New Password</button>
                                            </form>
                                    
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Dashboard Ecommerce ends -->
                    </div>
                </div>
            </div>

            </div>
          
            <?php include('includes/footer.php'); ?>