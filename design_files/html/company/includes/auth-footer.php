  <!-- END: Content-->
    <!-- BEGIN: Vendor JS-->
    <script src="../../app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->
    <!-- BEGIN: Page Vendor JS-->
    <script src="../../app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <script src="../../app-assets/js/scripts/owl.carousel.min.js"></script>
    <script src="../../app-assets/js/core/app-menu.min.js"></script>
    <script src="../../app-assets/js/core/app.js"></script>
    <script src="../../assets/js/scripts.js"></script>
    <script src="../../app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="../../app-assets/js/scripts/forms/form-select2.min.js"></script>
    <script src="../../app-assets/vendors/js/jquery-ui.js"></script>

    <script src="../../app-assets/js/scripts/pages/authentication_validation.js"></script>
    <!-- END: Page JS-->
    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>
</body>
<!-- END: Body-->

</html>