<!DOCTYPE html>
<html class="loaded" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="description" content="GreenCO Rating System">
    <meta name="keywords" content="GreenCO Rating System">
    <meta name="author" content="GreenCO Rating System">
    <title>GreenCO Rating System</title>
    
    <link rel="shortcut icon" type="image/x-icon" href="../../app-assets/images/logo/greenco.png">
    <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&display=swap" rel="stylesheet">
    <!---Font Awesome --->
    <link rel="stylesheet" type="text/css" href="../../app-assets/fonts/font-awesome/css/font-awesome.css">
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/tables/datatable/responsive.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/forms/select/select2.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/vendors/css/jquery-ui.css">
     <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/themes/dark-layout.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/themes/bordered-layout.min.css">
    <!-- END: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/owl.carousel.min.css">
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/plugins/forms/form-validation.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/pages/page-auth.min.css">
    <link rel="stylesheet" type="text/css" href="../../app-assets/css/pages/page-profile.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
    
    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="../../assets/css/style.css">
</head>
<!-- END: Head-->
<!-- BEGIN: Body-->

<body data-theme-version="light" data-layout="vertical" data-nav-headerbg="color_1" data-headerbg="color_1" data-sidebar-style="mini" data-sibebarbg="color_1" data-sidebar-position="fixed" data-header-position="fixed" data-container="wide" direction="ltr" data-primary="color_1">
    
    <div id="main-wrapper">
        <nav class="header-navbar navbar navbar-expand-lg align-items-center navbar-light navbar-shadow fixed-top">
            <div class="navbar-container d-flex content">
            <ul class="nav navbar-nav align-items-center">
                  
                  
                  <li class="nav-item d-none d-lg-block">
                      <a class="nav-link nav-link-expand"><i class="ficon" data-feather="maximize"></i></a>
                  </li>
                  <li class="nav-item dropdown dropdown-notification mr-25"><a class="nav-link" href="javascript:void(0);" data-toggle="dropdown"><i class="ficon" data-feather="bell"></i><span class="badge badge-pill badge-danger badge-up">5</span></a>
                      <ul class="dropdown-menu dropdown-menu-media dropdown-menu-left">
                          <li class="dropdown-menu-header">
                              <div class="dropdown-header d-flex">
                                  <h4 class="notification-title mb-0 mr-auto">Notifications</h4>
                                  <div class="badge badge-pill badge-light-primary">6 New</div>
                              </div>
                          </li>
                          <li class="scrollable-container media-list"><a class="d-flex" href="javascript:void(0)">
                                  <div class="media d-flex align-items-start">
                                      <div class="media-left">
                                          <div class="avatar"><img src="../../app-assets/images/avatars/avatar-s-11.jpg" alt="avatar" width="32" height="32"></div>
                                      </div>
                                      <div class="media-body">
                                          <p class="media-heading"><span class="font-weight-bolder">Congratulation Sam 🎉</span>winner!</p><small class="notification-text"> Won the monthly best seller badge.</small>
                                      </div>
                                  </div>
                              </a><a class="d-flex" href="javascript:void(0)">
                                  <div class="media d-flex align-items-start">
                                      <div class="media-left">
                                          <div class="avatar"><img src="../../app-assets/images/avatars/avatar-s-11.jpg" alt="avatar" width="32" height="32"></div>
                                      </div>
                                      <div class="media-body">
                                          <p class="media-heading"><span class="font-weight-bolder">New message</span>&nbsp;received</p><small class="notification-text"> You have 10 unread messages</small>
                                      </div>
                                  </div>
                              </a><a class="d-flex" href="javascript:void(0)">
                                  <div class="media d-flex align-items-start">
                                      <div class="media-left">
                                          <div class="avatar bg-light-danger">
                                              <div class="avatar-content">MD</div>
                                          </div>
                                      </div>
                                      <div class="media-body">
                                          <p class="media-heading"><span class="font-weight-bolder">Revised Order 👋</span>&nbsp;checkout</p><small class="notification-text"> MD Inc. order updated</small>
                                      </div>
                                  </div>
                              </a>
                          </li>
                          <li class="dropdown-menu-footer"><a class="btn btn-primary btn-block" href="../support/notifications.html">Read all notifications</a></li>
                      </ul>
                  </li>
                
              </ul>
                <ul class="nav navbar-nav align-items-center ml-auto">
                  
                  
                  
                    <li class="nav-item dropdown dropdown-user"><a class="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div class="user-nav d-sm-flex d-none"><span class="user-name font-weight-bolder">John Doe</span><span class="user-status">Company</span></div><span class="avatar"><img class="round" src="../../app-assets/images/avatars/avatar-s-11.jpg" alt="avatar" height="40" width="40"><span class="avatar-status-online"></span></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-user"><a class="dropdown-item" href="profile.php"><i class="mr-50" data-feather="user"></i> Profile</a>
                            <a class="dropdown-item" href="changePassword.php"><i class="mr-50" data-feather="unlock"></i> Change Password</a>
                            <a class="dropdown-item" href="#"><i class="mr-50" data-feather="power"></i> Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="nav-header">
            <a href="index.php" class="brand-logo pb-1 pt-1 pl-50 pr-50 ">
                <img class="img-fluid" src="../../app-assets/images/logo/logo-white.png" alt="Login V2">
              
            </a>
            <div class="nav-control">
                <div class="hamburger">
                    <span class="line"></span><span class="line"></span><span class="line"></span>
                </div>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->
        <!--**********************************
            Sidebar start
        ***********************************-->
        <?php include('includes/sidebar.php'); ?>